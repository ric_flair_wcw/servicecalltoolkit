package org.sct.clientframework.call;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.sct.clientframework.config.IConfigurationReaderUtil;
import org.sct.clientframework.exception.ClientFrameworkException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:clientFramework-applicationContext-test.xml")
public class EndpointsQueueTest {

	
	@Autowired
	private ApplicationContext applicationContext;
	
	@Autowired
	private IConfigurationReaderUtil mockedConfigurationReaderUtil;
	
	private EndpointsQueue endpointsQueue = null;
	private String id = "asdf";
	
	
	@Test
	public void testNext() throws ClientFrameworkException {
		// defining mockito expectations
		List<String> endpoints = new ArrayList<String>();
		endpoints.add("http://1"); endpoints.add("http://2"); endpoints.add("http://3");
		when(mockedConfigurationReaderUtil.getEndpoints()).thenReturn(endpoints);
		
		// getting the bean
		endpointsQueue = (EndpointsQueue) applicationContext.getBean("endpointsQueue");
		
		// checking the result
		String endpoint = endpointsQueue.next(id);
		assertEquals("http://1", endpoint);
		verify(mockedConfigurationReaderUtil).getEndpoints();
		
		endpoint = endpointsQueue.next(id);
		assertEquals("http://2", endpoint);
		
		endpoint = endpointsQueue.next(id);
		assertEquals("http://3", endpoint);
		
		endpoint = endpointsQueue.next(id);
		assertEquals("http://1", endpoint);
	}
	
	
	@After
	public void clearMocks() {
		Mockito.reset(mockedConfigurationReaderUtil);
		endpointsQueue.remove(id);
	}
	
}