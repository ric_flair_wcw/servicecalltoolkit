package org.sct.clientframework.call;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.client.match.RequestMatchers.content;
import static org.springframework.test.web.client.match.RequestMatchers.method;
import static org.springframework.test.web.client.match.RequestMatchers.requestTo;
import static org.springframework.test.web.client.response.ResponseCreators.withSuccess;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.sct.clientframework.config.IConfigurationReaderUtil;
import org.sct.clientframework.exception.ClientFrameworkException;
import org.sct.clientframework.exception.ServerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestClientException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:clientFramework-applicationContext-test.xml")
public class RestClientTest {

	
	@Autowired
	private ApplicationContext applicationContext;

	@Autowired
	private IConfigurationReaderUtil mockedConfigurationReaderUtil;
	
	private MockRestServiceServer mockRestServiceServer;
	private RestClient restClient;


	@Before
	public void setUp() throws ClientFrameworkException {
		// defining mockito expectations
		List<String> endpoints = new ArrayList<String>();
		endpoints.add("http://1"); endpoints.add("http://2"); endpoints.add("http://3");
		when(mockedConfigurationReaderUtil.getEndpoints()).thenReturn(endpoints);

		restClient = (RestClient) applicationContext.getBean("restClient");
		
		Map<String,Object> credentials = new HashMap<String, Object>();
		credentials.put("username", "admin"); credentials.put("password", "admin");
		CredentialsStorage.get().set(credentials);
		
		// setting up the mocked REST server
		mockRestServiceServer = MockRestServiceServer.createServer(restClient.getRestTemplate());
	}
	
	
	@Test
	public void testGet_simple_poiAndId_noEncryption_successful() throws RestClientException, ClientFrameworkException, ServerException {
		String poiName = "SAP->HR->employeeNameById";
		String id = "007";
		
		// defining the endpoint and http method to be called and set the response -to be sent back
		String response = "simple_string_value";
		String body = "{\"body\":{\"params\":{\"id\":\"007\"},\"poiName\":\"SAP->HR->employeeNameById\"},\"header\":{\"encrypted\":\"false\"}}";
		mockRestServiceServer.expect(requestTo("http://1"))
                             .andExpect(method(HttpMethod.POST))
                             .andExpect(content().string(body))
                             .andRespond(withSuccess(response, MediaType.APPLICATION_JSON));

		// calling the method
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", id);
		Map<String, Object> bodyMap = new HashMap<String, Object>();
		bodyMap.put("poiName", poiName);
		bodyMap.put("params", params);
		
		
		ResponseEntity<String> responseEntity = restClient.forward(bodyMap, false);
		
		// checking the response
		assertEquals("simple_string_value", responseEntity.getBody());
		assertTrue(responseEntity.getStatusCode().is2xxSuccessful());
	}
	

	@Test
	public void testGet_simple_poiAndId_withEncryption_successful() throws RestClientException, ClientFrameworkException, ServerException {
		// defining the endpoint and http method to be called and set the response -to be sent back
		String response = "simple_string_value";
		String body = "{\"body\":\"7TbnxVELPO3XVkT4UjDlhwSRzHEbG4yiEv0ewbaxMPCPMBjvKp6Zdx//GweZRrEk\",\"header\":{\"encrypted\":\"true\"}}";
		mockRestServiceServer.expect(requestTo("http://1"))
                             .andExpect(method(HttpMethod.POST))
                             .andExpect(content().string(body))
                             .andRespond(withSuccess(response, MediaType.TEXT_PLAIN));

		// calling the method
		Object bodyMap = "7TbnxVELPO3XVkT4UjDlhwSRzHEbG4yiEv0ewbaxMPCPMBjvKp6Zdx//GweZRrEk";
		
		
		ResponseEntity<String> responseEntity = restClient.forward(bodyMap, true);
		
		// checking the response
		assertEquals("simple_string_value", responseEntity.getBody());
		assertTrue(responseEntity.getStatusCode().is2xxSuccessful());
	}

	
//	@Test
	// TODO I cannot test the load balancing as I cannot give a HTTP status code which would result to throw a ResourceAccessException...
	public void testGet_simple_poiAndId_successful_withLoadBalancing() throws RestClientException, ClientFrameworkException, ServerException {
//		String poiName = "SAP->HR->employeeNameById";
//		String id = "007";
//
//		// defining the endpoint and http method to be called and set the response -to be sent back
//		String response = "simple_string_value";
//		String body = "{\"params\":{\"id\":\""+id+"\"},\"poiName\":\""+poiName+"\"}";
//		mockRestServiceServer.expect(requestTo("http://1"))
////		                     .andRespond(withStatus(HttpStatus.INTERNAL_SERVER_ERROR));
//                             .andRespond(withServerError());
//		mockRestServiceServer.expect(requestTo("http://2"))
//                             .andExpect(method(HttpMethod.POST))
//                             .andExpect(content().string(body))
//                             .andRespond(withSuccess(response, MediaType.TEXT_PLAIN));
//
//		// calling the method
//		Map<String, Object> params = new HashMap<String, Object>();
//		params.put("id", id);
//		
//		ResponseEntity<String> responseEntity = restClient.forward(poiName, params);
//		
//		// checking the response
//		assertEquals("simple_string_value", responseEntity.getBody());
//		assertTrue(responseEntity.getStatusCode().is2xxSuccessful());
	}
	
	
	@After
	public void clearMocks() {
		Mockito.reset(mockedConfigurationReaderUtil);
	}
	
}
