package org.sct.clientframework.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Map;

import org.junit.Before;
import org.junit.Test;


public class ParameterBuilderTest {

	
	private ParameterBuilder builder = null;
	
	
	@Before
	public void setUp() {
		builder = new ParameterBuilder();
	}
	
	
	@Test
	public void testAdd_successful_1_oneLevel() {
		String path = "almafa";
		Integer value = 12;
		builder.add(path, value);
		
		assertTrue(builder.getParams().get("almafa") instanceof Integer);
		assertEquals(12, builder.getParams().get("almafa"));
	}
	

	@Test
	public void testAdd_successful_2_threeLevel() {
		String path = "/fold/almafa/ag/";
		String value = "s12";
		builder.add(path, value);
		
		assertTrue(  
					((Map<String,Object>)
						((Map<String,Object>)builder.getParams().get("fold"))
				     .get("almafa")).get("ag") instanceof String);
		assertEquals("s12", 
				((Map<String,Object>)
						((Map<String,Object>)builder.getParams().get("fold"))
				     .get("almafa")).get("ag"));
	}


	@Test
	public void testAdd_successful_3_complex() {
		String path1 = "fold/almafa/ag/";
		String value1 = "s12";
		builder.add(path1, value1);
		String path2 = "levelgo";
		Double value2 = 12.5d;
		builder.add(path2, value2);
		
		assertTrue(builder.getParams().get("levelgo") instanceof Double);
		assertEquals(12.5d, builder.getParams().get("levelgo"));
		assertTrue(  
					((Map<String,Object>)
						((Map<String,Object>)builder.getParams().get("fold"))
				     .get("almafa")).get("ag") instanceof String);
		assertEquals("s12", 
				((Map<String,Object>)
						((Map<String,Object>)builder.getParams().get("fold"))
				     .get("almafa")).get("ag"));
	}
}
