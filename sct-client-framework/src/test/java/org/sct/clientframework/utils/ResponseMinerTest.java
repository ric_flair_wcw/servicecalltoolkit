package org.sct.clientframework.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.sct.clientframework.exception.ClientFrameworkException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ResponseMinerTest {

	
	private ResponseMiner miner = null;
	private String mapString =
					"{"+
					"  \"companies\": {"+
					"    \"desc\": \"list of companies\","+
					"    \"details\": { \"secret\": \"X-files\", \"character\": \"Fox Mulder\"},"+
					"    \"admins\": [\"en\",\"Izibaba\"],"+
					"    \"list\": ["+
					"      {"+
					"        \"company\": {"+
					"          \"name\": \"Nextent\","+
					"          \"address\": {"+
					"            \"city\": \"Budapest\","+
					"            \"street\": \"Gazdagret utca\""+
					"          },"+
					"          \"employees\": ["+
					"            {"+
					"              \"firstName\": \"Laszlo\","+
					"              \"lastName\": \"Dornyei\""+
					"            },"+
					"            {"+
					"              \"firstName\": \"Janos\","+
					"              \"lastName\": \"Szenyo\""+
					"            }"+
					"          ]"+
					"        }"+
					"      },"+
					"      {"+
					"        \"company\": {"+
					"          \"name\": \"LV\","+
					"          \"address\": {"+
					"            \"city\": \"Bournemouth\","+
					"            \"street\": \"County Gate\""+
					"          },"+
					"          \"employees\": ["+
					"            {"+
					"              \"firstName\": \"John\","+
					"              \"lastName\": \"Doe\""+
					"            },"+
					"            {"+
					"              \"firstName\": \"Anna\","+
					"              \"lastName\": \"Smith\""+
					"            },"+
					"            {"+
					"              \"firstName\": \"Peter\","+
					"              \"lastName\": \"Jones\""+
					"            }"+
					"          ]"+
					"        }"+
					"      }"+
					"    ]"+
					"  }"+
					"}";
	private Map<String, ?> map = null;
	
	
	@Before
	public void setUp() throws JsonParseException, JsonMappingException, IOException {
		miner = new ResponseMiner();
		ObjectMapper objectMapper = new ObjectMapper();
		map = objectMapper.readValue(mapString, Map.class);
	}
	
	
	@Test
	public void testGetSimple_successful() throws ClientFrameworkException {
		String path = "/companies/desc";
		
		String name = miner.getSimple(path, map, String.class);
		
		assertEquals("list of companies", name);
	}
	@Test
	public void testGetSimple_unsuccessful_notSimpleButList() throws ClientFrameworkException {
		String path = "/companies/list/company/name";
		
		try {
			String name = miner.getSimple(path, map, String.class);
			fail("ClientFrameworkException should have been thrown!");
		} catch(ClientFrameworkException e) {
			assertEquals("I expected Map and not class java.util.ArrayList at /companies/list!", e.getMessage());
		}
	}
	@Test
	public void testGetSimple_unsuccessful_invalidPath() throws ClientFrameworkException {
		String path = "/company/desc";
		
		try {
			String name = miner.getSimple(path, map, String.class);
			fail("ClientFrameworkException should have been thrown!");
		} catch(ClientFrameworkException e) {
			assertEquals("The path /company is null! Likely the path /company/desc is invalid.", e.getMessage());
		}
	}
	@Test
	public void testGetSimple_unsuccessful_invalidType() throws ClientFrameworkException {
		String path = "/companies/desc";
		
		try {
			Integer nr = miner.getSimple(path, map, Integer.class);
			fail("ClientFrameworkException should have been thrown!");
		} catch(ClientFrameworkException e) {
			assertEquals("The type of the element '/companies/desc' in the map is not 'java.lang.Integer' but 'java.lang.String'!", e.getMessage());
		}
	}


	@Test
	public void testGetStruct_successful() throws ClientFrameworkException {
		String path = "/companies/details";
		
		Map<String, Object> struct = miner.getStruct(path, map);
		
		assertEquals(2, struct.keySet().size());
		assertEquals("Fox Mulder", struct.get("character").toString());
	}
	@Test
	public void testGetStruct_unsuccessful_notStructButSimple() throws ClientFrameworkException {
		String path = "/companies/desc";

		try {
			Map<String, Object> struct = miner.getStruct(path, map);
			fail("ClientFrameworkException should have been thrown!");
		} catch(ClientFrameworkException e) {
			assertEquals("The type of the element '/companies/desc' in the map is not 'java.util.Map' but 'java.lang.String'!", e.getMessage());
		}
	}
	
	
	@Test
	public void testGetSimpleList_successful() throws ClientFrameworkException {
		String path = "/companies/admins";
		
		List<String> simpleList = miner.getSimpleList(path, map, String.class);
		
		assertEquals(2, simpleList.size());
		assertEquals("en", simpleList.get(0));
		assertEquals("Izibaba", simpleList.get(1));
	}
	@Test
	// the getSimpleList cannot extract embedded list -> see the JSON below for understanding the issue
	public void testGetSimpleList_unsuccessful_embeddedList() throws ClientFrameworkException {
		String path = "/companies/list/company/name";
		
		try {
			List<String> simpleList = miner.getSimpleList(path, map, String.class);
			fail("ClientFrameworkException should have been thrown!");
		} catch(ClientFrameworkException e) {
			assertEquals("I expected Map and not class java.util.ArrayList at /companies/list!", e.getMessage());
		}
		
	}
	

	@Test
	public void testGetStructList_successful() throws ClientFrameworkException {
		String path = "/companies/list";
		
		List<Map<String, Object>> structList = miner.getStructList(path, map);
		
		assertEquals(2, structList.size());
		assertEquals("Nextent", ( (Map<String, Object>) structList.get(0).get("company")).get("name").toString());
		assertEquals("LV", ( (Map<String, Object>) structList.get(1).get("company")).get("name").toString());
	}
	@Test
	// the getSimpleList cannot extract embedded list -> see the JSON below for understanding the issue
	public void testGetStructList_unsuccessful_embeddedList() throws ClientFrameworkException {
		String path = "/companies/admins";
		
		try {
			List<Map<String, Object>> structList = miner.getStructList(path, map);
			fail("ClientFrameworkException should have been thrown!");
		} catch(ClientFrameworkException e) {
			assertEquals("The element 'en' cannot be cast to 'Map'!", e.getMessage());
		}
	}


	@Test
	public void testGet_successful_listInLists() throws ClientFrameworkException {
		String path = "/companies/list/company/employees/firstName";
		
		Object v = miner.get(path, map);
		
		assertTrue(v instanceof List);
		List l = (List)v;
		assertEquals(5, l.size());
		assertEquals("Laszlo", l.get(0));
		assertEquals("John", l.get(2));
		assertEquals("Peter", l.get(4));
	}
	@Test
	public void testGet_successful_map() throws ClientFrameworkException {
		String path = "/companies/details";
		
		Object v = miner.get(path, map);
		
		assertTrue(v instanceof Map);
		Map<String, ?> m = (Map)v;
		assertEquals(2, m.keySet().size());
		assertEquals("X-files", m.get("secret").toString());
	}
	@Test
	public void testGet_successful_listOfMaps() throws ClientFrameworkException {
		String path = "/companies/list";
		
		Object v = miner.get(path, map);
		
		assertTrue(v instanceof List);
		List l = (List)v;
		assertEquals(2, l.size());
		assertTrue(l.get(0) instanceof Map);
		assertTrue(l.get(1) instanceof Map);
	}
	@Test
	public void testGet_successful_listOfStrings() throws ClientFrameworkException {
		String path = "/companies/admins";
		
		Object v = miner.get(path, map);
		
		assertTrue(v instanceof List);
		List l = (List)v;
		assertEquals(2, l.size());
		assertTrue(l.get(0) instanceof String);
		assertEquals("en", l.get(0).toString());
		assertTrue(l.get(1) instanceof String);
		assertEquals("Izibaba", l.get(1).toString());
	}
	@Test
	// the getSimpleList cannot extract embedded list -> see the JSON below for understanding the issue
	public void testGet_unsuccessful() throws ClientFrameworkException {
		String path = "/company/admins";
		
		try {
			Object v = miner.get(path, map);
			fail("ClientFrameworkException should have been thrown!");
		} catch(ClientFrameworkException e) {
			assertEquals("The node '/company/admins' cannot be found in the response!", e.getMessage());
		}
		
	}
}
