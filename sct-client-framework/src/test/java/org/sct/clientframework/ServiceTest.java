package org.sct.clientframework;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.client.match.RequestMatchers.method;
import static org.springframework.test.web.client.match.RequestMatchers.requestTo;
import static org.springframework.test.web.client.response.ResponseCreators.withSuccess;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.sct.clientframework.call.CredentialsStorage;
import org.sct.clientframework.config.IConfigurationReaderUtil;
import org.sct.clientframework.exception.ClientFrameworkException;
import org.sct.clientframework.exception.ServerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestClientException;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:clientFramework-applicationContext-test.xml")
public class ServiceTest {

	
	@Autowired
	private ApplicationContext applicationContext;

	@Autowired
	private IConfigurationReaderUtil mockedConfigurationReaderUtil;
	
	private MockRestServiceServer mockRestServiceServer;
	private Service service;
	

	@Before
	public void setUp() throws ClientFrameworkException {
		// defining mockito expectations
		List<String> endpoints = new ArrayList<String>();
		endpoints.add("http://1"); endpoints.add("http://2"); endpoints.add("http://3");
		when(mockedConfigurationReaderUtil.getEndpoints()).thenReturn(endpoints);

		//service = Service.createInstance("admin", "admin");
		// I cannot use the createInstance method as there is an implicit reference to the original context XML. What I can do is to copy the the content of createInstance here
		service = (Service) applicationContext.getBean("service");
		Map<String,Object> credentials = new HashMap<String, Object>();
		credentials.put("username", "admin"); credentials.put("password", "admin");
		CredentialsStorage.get().set(credentials);
		
		service.setEncryptionKey("Bar12345Bar12345");
		// setting up the mocked REST server
		mockRestServiceServer = MockRestServiceServer.createServer(service.getRestClient().getRestTemplate());
	}
	
	
	@Test
	public void testGet_simple_poiAndId_noEncryption_successful() throws RestClientException, ClientFrameworkException, ServerException {
		service.setEncryptionNeeded(false);
		
		// defining the endpoint and http method to be called and set the response -to be sent back
		String response = "simple_string_value";
		mockRestServiceServer.expect(requestTo("http://1"))
                             .andExpect(method(HttpMethod.POST))
                             .andRespond(withSuccess(response, MediaType.TEXT_PLAIN));

		// calling the method
		String poiName = "SAP->HR->employeeNameById";
		String id = "007";
		String value = service.get(poiName, id);
		
		// checking the response
		assertEquals("simple_string_value", value);
	}


	@Test
	public void testGet_simple_poiAndId_noEncryption_unsuccessful_invalidResponse() throws RestClientException, ClientFrameworkException, ServerException {
		service.setEncryptionNeeded(false);
		
		// defining the endpoint and http method to be called and set the response -to be sent back
		String response = "{\"body\": \"somtehing\"}";
		mockRestServiceServer.expect(requestTo("http://1"))
                             .andExpect(method(HttpMethod.POST))
                             .andRespond(withSuccess(response, MediaType.APPLICATION_JSON));

		// calling the method
		String poiName = "SAP->HR->employeeNameById";
		String id = "007";
		try {
			String value = service.get(poiName, id);
			fail("ClientFrameworkException should have been thrown!");
		} catch(ClientFrameworkException e) {
			assertEquals("The /header/status cannot be found in the arrived JSON!", e.getMessage());
		}
	}
	

	@Test
	public void testGet_simple_poiAndId_noEncryption_unsuccessful_validErrorInResponse() throws RestClientException, ClientFrameworkException, ServerException {
		service.setEncryptionNeeded(false);

		// defining the endpoint and http method to be called and set the response -to be sent back
		String response = 
				"{"+
						  "\"body\": {"+
						    "\"class\": \"RuntimeException\","+
						    "\"message\": \"horror...\""+
						  "},"+
						  "\"header\": {"+
							"\"encrypted\" : \"false\"," +
						    "\"status\": \"error\""+
						  "}"+
						"}";
		mockRestServiceServer.expect(requestTo("http://1"))
                             .andExpect(method(HttpMethod.POST))
                             .andRespond(withSuccess(response, MediaType.APPLICATION_JSON));

		// calling the method
		String poiName = "SAP->HR->employeeNameById";
		String id = "007";
		try {
			String value = service.get(poiName, id);
			fail("ClientFrameworkException should have been thrown!");
		} catch(ServerException e) {
			assertEquals("RuntimeException : horror...", e.getMessage());
		}
	}
	
	
	@Test
	public void testGetStruct_poiAndId_noEncryption_successful() throws RestClientException, ClientFrameworkException, ServerException {
		service.setEncryptionNeeded(false);

		// defining the endpoint and http method to be called and set the response -to be sent back
		String response = 
				"{" +
				"	\"body\" : {" +
				"		\"/1/2\" : \"value1\"" +
				"	}," +
				"	\"header\" : {" +
				"		\"encrypted\" : \"false\"," +
				"		\"status\" : \"OK\"" +
				"	}" +
				"}";

		mockRestServiceServer.expect(requestTo("http://1"))
                             .andExpect(method(HttpMethod.POST))
                             .andRespond(withSuccess(response, MediaType.APPLICATION_JSON));

		// calling the method
		String poiName = "SAP->HR->employeeNameById";
		String id = "007";
		Map<String,?> value = service.getStruct(poiName, id, Map.class);
		
		// checking the response
		assertEquals("value1", value.get("/1/2"));
	}
	
	
	@Test
	public void testGetStruct_poiAndId_noEncryption_errorInResponse() throws RestClientException, ClientFrameworkException, ServerException {
		service.setEncryptionNeeded(false);

		// defining the endpoint and http method to be called and set the response -to be sent back
		String response = 
				"{" +
				"	\"body\" : {" +
				"		\"class\" : \"java.lang.RuntimeException\"," +
				"		\"message\" : \"serious\"" +
				"	}," +
				"	\"header\" : {" +
				"		\"encrypted\" : \"false\"," +
				"		\"status\" : \"error\"" +
				"	}" +
				"}";

		mockRestServiceServer.expect(requestTo("http://1"))
                             .andExpect(method(HttpMethod.POST))
                             .andRespond(withSuccess(response, MediaType.APPLICATION_JSON));

		// calling the method
		String poiName = "SAP->HR->employeeNameById";
		String id = "007";
		try {
			Map<String,?> value = service.getStruct(poiName, id, Map.class);
			fail("ServerException should have been thrown!");
		} catch(ServerException e) {
			assertEquals(e.getErrorClass(), "java.lang.RuntimeException");
			assertEquals(e.getErrorMessage(), "serious");
		}
	}

	
	@After
	public void clearMocks() {
		Mockito.reset(mockedConfigurationReaderUtil);
	}
	
}
