package org.sct.clientframework.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.sct.clientframework.exception.ClientFrameworkException;
import org.sct.common.utils.MapUtils;
import org.sct.common.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Helper class to extract a value from the response of the Server Engine
 * 
 * SPath: simplified XPath -> only the character '/' can be used from the XPath standard
 *        For example: /soapenv:Envelope/soapenv:Body/ns:GetAddressResponse/ns:address/ns:streetName
 * 
 * @author      Viktor Horvath
 */
public class ResponseMiner {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(ResponseMiner.class);
			
	
	/**
	 * Getting a simple value from the map
	 *
	 * @param  path The SPath where the value to be retrieved resides
	 * @param  mapAll The response of the Server Engine
	 * @param  type The type of the value
	 * @return The extracted value with specified type
	 */
	public static <T> T getSimple(String path, Map<String, ?> mapAll, Class<T> type) throws ClientFrameworkException {
		return getElement(path, mapAll, type);
	}


	/**
	 * Getting a composite value from the map
	 *
	 * @param  path The SPath where the value to be retrieved resides
	 * @param  mapAll The response of the Server Engine
	 * @return The composite value on the SPath in the response
	 */
	public static Map<String, Object> getStruct(String path, Map<String, ?> mapAll) throws ClientFrameworkException {
		return getElement(path, mapAll, Map.class);
	}

	
	/**
	 * Getting a simple list of values from the map
	 *
	 * @param  path The SPath where the values to be retrieved reside
	 * @param  mapAll The response of the Server Engine
	 * @param  type The type of the values
	 * @return The extracted list of values with specified type
	 */
	public static <T> List<T> getSimpleList(String path, Map<String, ?> mapAll, Class<T> type) throws ClientFrameworkException {
		// getting the list from the map
		List<?> list = getElement(path, mapAll, List.class);
		
		List<T> resultList = new ArrayList<T>();
		// casting the elements of the list to T
		for(Object o : list) {
			try {
				T t = type.cast(o);
				resultList.add(t);
			} catch(ClassCastException cce) {
				throw new ClientFrameworkException("The element '" + o + "' cannot be cast to '" + type + "'!");
			}
		}
		return resultList;
	}
	public static <T> List<T> getSimpleList(String path, List<Map<String, ?>> list, Class<T> type) throws ClientFrameworkException {
		List<T> elements = new ArrayList<T>();
		for(Map<String, ?> m : list) {
			elements.addAll(getSimpleList(path, m, type));
		}
		return elements;
	}


	/**
	 * Getting a list of composite values from the map
	 *
	 * @param  path The SPath where the values to be retrieved reside
	 * @param  mapAll The response of the Server Engine
	 * @return The extracted list of composite values
	 */
	public static List<Map<String, Object>> getStructList(String path, Map<String, ?> mapAll) throws ClientFrameworkException {
		// getting the list from the map
		List<?> list = getElement(path, mapAll, List.class);
		
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		// casting the elements of the list to T
		for(Object o : list) {
			try {
				Map m = Map.class.cast(o);
				resultList.add(m);
			} catch(ClassCastException cce) {
				throw new ClientFrameworkException("The element '" + o + "' cannot be cast to 'Map'!");
			}
		}
		return resultList;
	}
	public static List<Map<String, Object>> getStructList(String path, List<Map<String, ?>> list) throws ClientFrameworkException {
		List<Map<String, Object>> elements = new ArrayList<Map<String, Object>>();
		for(Map<String, ?> m : list) {
			elements.addAll(getStructList(path, m));
		}
		return elements;
	}

	
	// it will retrieve whatever it finds
	// it is different from the others that it can handle the case if it finds a List somewhere in the path
	//	{ aa : [ { bb : [
	//	                   { cc : "11" } ,
	//	                   { cc : "22" }
	//					]
	//	         },
	//	         { bb :
	//	                   { cc : "33" }
	//	         }
	//	       ]
	//	}
	// the aa contains a list of bb elements and the 1st bb contains 2 cc elements 
	//      => the method will return a List with 3 strings if the path is /aa/bb/cc
	/**
	 * Getting the value from the map.
	 * The type doesn't have to be specified as it will retrieve whatever it finds. I.e. the type of the returned value can be a simple value (e.g. String), a map, list with simple values or list with maps.
	 *	{ aa : [ { bb : [
	 *	                   { cc : "11" } ,
	 *	                   { cc : "22" }
	 *					]
	 *	         },
	 *	         { bb :
	 *	                   { cc : "33" }
	 *	         }
	 *	       ]
	 *	}
	 * the aa contains a list of bb elements and the 1st bb contains 2 cc elements 
	 *      => the method will return a List with 3 strings if the path is /aa/bb/cc
	 *
	 * @param  path The SPath where the value to be retrieved resides
	 * @param  mapAll The response of the Server Engine
	 * @return The extracted value
	 */
	public static Object get(String _path, Map<String, ?> mapAll) throws ClientFrameworkException {
		List<Object> values = new ArrayList<Object>();
		_get(_path, mapAll, values);
		if (values.size() == 0) {
			throw new ClientFrameworkException("The node '"+_path+"' cannot be found in the response!");			
		} else if (values.size() == 1) {
			return values.get(0);
		} else {
			return values;
		}
	}
	public static Object get(String path, List<?> list) throws ClientFrameworkException {
		List<Object> elements = new ArrayList<Object>();
		for(Object m : list) {
			if (!(m instanceof Map)) {
				throw new ClientFrameworkException("The upper list must contains Map!");
			}
			Object o = get(path, (Map<String, ?>)m);
			if (o instanceof List) {
				elements.addAll((List)o);
			} else {
				elements.add(o);
			}
		}
		if (elements.size() == 0) {
			throw new ClientFrameworkException("The node '"+path+"' cannot be found in the response!");			
		} else if (elements.size() == 1) {
			return elements.get(0);
		} else {
			return elements;
		}
	}	
	
	
	private static void _get(String _path, Map<String, ?> mapAll, List<Object> values) throws ClientFrameworkException {
		String path = Utils.removeStartingChar(Utils.removeTrailingChar(_path, "/"), "/");
		String topNodeInPath = null;
		String restOfTheNodesInPath = null;
		if (path.indexOf("/") == -1) {
			topNodeInPath = path;
		} else {
			topNodeInPath = path.substring(0, path.indexOf("/"));
			restOfTheNodesInPath = path.substring(path.indexOf("/")+1);
		}
		
		if (mapAll.get(topNodeInPath) instanceof List<?>) {
			for(Object o : (List<?>)mapAll.get(topNodeInPath)) {
				if (restOfTheNodesInPath == null) {
					values.add(o);
				}
				else {
					if (o instanceof Map) {
						_get(restOfTheNodesInPath, (Map<String, ?>)o, values);
					}
				}
			}
		} else {
			if (restOfTheNodesInPath == null) {
				values.add(mapAll.get(topNodeInPath));
			} else {
				if (mapAll.get(topNodeInPath) instanceof Map) {
					_get(restOfTheNodesInPath, (Map<String, ?>)mapAll.get(topNodeInPath), values);
				}
			}
		}
	}
	

	private static <T> T getElement(String path, Map<String, ?> mapAll, Class<T> type) throws ClientFrameworkException {
		Map<String, ?> map = mapAll;
		String[] nodes = MapUtils.trimAndSplit(path);
		StringBuffer currentPath = new StringBuffer();
		if (nodes.length > 1) {
			for(int i = 0; i < nodes.length-1; i++) {
				currentPath.append("/").append(nodes[i]);
				if (map.get(nodes[i]) == null) {
					throw new ClientFrameworkException("The path " + currentPath.toString() + " is null! Likely the path "+path+" is invalid."); 
				}
				if (map.get(nodes[i]) instanceof Map) {
					map = (Map<String, ?>) map.get(nodes[i]);
				} else {
					LOGGER.trace("map: {}", mapAll);
					throw new ClientFrameworkException("I expected Map and not " + map.get(nodes[i]).getClass() + " at "+ currentPath.toString() + "!");
				}
			}
		} else if (nodes.length == 0) {
			throw new ClientFrameworkException("The map seems to be empty!");
		}
		String key = nodes[nodes.length-1];
		
		try {
			return type.cast(map.get(key));
		} catch(ClassCastException cce) {
			throw new ClientFrameworkException("The type of the element '" + path + "' in the map is not '" + type.getName() + "' but '" + map.get(key).getClass().getName() + "'!");
		}
	}

	
}
