package org.sct.clientframework.call;

import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.sct.clientframework.config.IConfigurationReaderUtil;
import org.sct.clientframework.exception.ClientFrameworkException;

/**
 * Queue for storing endpoints
 * Every service call has its own queue
 * 
 * @author      Viktor Horvath
 */
public class EndpointsQueue {

	
	private IConfigurationReaderUtil configurationReaderUtil;
	private Queue<String> endpointsQueue = new ConcurrentLinkedQueue<String>();
	private Map<String, Queue<String>> sessionQueues = new HashMap<String, Queue<String>>();
	
	
	/**
	 * Removing the call-related queue from sessionQueues in order to avoid memory leak.
	 *
	 */
	public void remove(String uuid) {
		sessionQueues.remove(uuid);
	}
	
	
	/**
	 * Getting the next endpoint in the queue
	 *
	 * @return The next endpoint in the queue, null if the queue is empty.
	 */
	public synchronized String next(String uuid) {
		String endpoint = null;
		// if it is first call in a service call then the endpointsQueue has to be added to sessionQueues with the unique id
		// before adding I bounce the queue in order not to call the first endpoint first
		if (sessionQueues.get(uuid) == null) {
			endpoint = endpointsQueue.poll();
			endpointsQueue.add(endpoint);
			sessionQueues.put(uuid, new ConcurrentLinkedQueue<String>(endpointsQueue));
		} else {
			// get the next endpoint from the session queue
			endpoint = sessionQueues.get(uuid).poll();
		}
		return endpoint;
	}


	public void initQueue() throws ClientFrameworkException {
		endpointsQueue.addAll(configurationReaderUtil.getEndpoints());		
	}
	
	
	public IConfigurationReaderUtil getConfigurationReaderUtil() {
		return configurationReaderUtil;
	}

	public void setConfigurationReaderUtil(IConfigurationReaderUtil configurationReaderUtil) {
		this.configurationReaderUtil = configurationReaderUtil;
	}


}
