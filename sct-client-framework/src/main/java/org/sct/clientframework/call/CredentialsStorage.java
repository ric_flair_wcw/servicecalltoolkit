package org.sct.clientframework.call;

import java.util.Map;

public class CredentialsStorage {

	
	private static final ThreadLocal<Map<String,Object>> credentialsStorage = new ThreadLocal<Map<String,Object>>();
	
	
	public static ThreadLocal<Map<String, Object>> get() {
		return credentialsStorage;
	}
	
}
