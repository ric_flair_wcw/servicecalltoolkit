package org.sct.clientframework.call;

import org.sct.clientframework.exception.ClientFrameworkException;
import org.sct.clientframework.exception.ServerException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;

public interface IRestClient {

	ResponseEntity<String> forward(Object objectToBeSent, boolean isEncyptionNeeded) throws RestClientException, ClientFrameworkException, ServerException;

}