package org.sct.clientframework.call;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Map;
import java.util.UUID;

import org.apache.http.HttpHost;
import org.sct.clientframework.exception.ClientFrameworkException;
import org.sct.clientframework.exception.ServerException;
import org.sct.common.call.JSONPostMan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

public class RestClient implements IRestClient {

	
    private static final Logger LOGGER = LoggerFactory.getLogger(RestClient.class);
    
	private RestTemplate restTemplate;
	private EndpointsQueue endpointsQueue;
	private JSONPostMan postMan;
	private String uniqueId;

	
	//http://stackoverflow.com/questions/18328492/resttemplate-jackson
	//http://stackoverflow.com/questions/21316641/how-to-properly-setup-a-post-spring-rest-template-by-passing-in-parameters-and-r
	public ResponseEntity<String> forward(Object objectToBeSent, boolean isEncyptionNeeded) throws RestClientException, ClientFrameworkException, ServerException {
		ResponseEntity<String> response = null;
		
		try {
			// generating a unique id for getting the endpoints
			uniqueId = UUID.randomUUID().toString();
			// call the service with round-robin
			response = callWithLoadBalancing(endpointsQueue.next(uniqueId), 
					                         postMan.createRequest(objectToBeSent, isEncyptionNeeded));
		} finally {
			endpointsQueue.remove(uniqueId);
		}
		
		return response;
	}


	private ResponseEntity<String> callWithLoadBalancing(String _endpoint, Map<String, Object> letter) throws ClientFrameworkException, ServerException {
		String endpoint = _endpoint;
		ResponseEntity<String> response = null;

		if (endpoint == null) {
			throw new ClientFrameworkException("The REST service is not available!");
		}
		
		Calendar startTime = Calendar.getInstance();
		
		try {
			LOGGER.debug("Calling the REST service on {} ...", endpoint);
			// setting the accepted HTTP type
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			// setting the auth info for digest authentication
			UriComponents uriComponents = UriComponentsBuilder.fromUriString(endpoint).build();
			HttpHost host = new HttpHost(uriComponents.getHost(), uriComponents.getPort(), uriComponents.getScheme());
			Map<String,Object> credentials = CredentialsStorage.get().get();
			credentials.put("host", host);
			CredentialsStorage.get().set(credentials);
			
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(letter, headers);
			response = restTemplate.exchange(endpoint, HttpMethod.POST, requestEntity, String.class);
			
			LOGGER.debug("Called the REST service. The time needed to call is {} ms. The response is '{}'", Calendar.getInstance().getTimeInMillis() - startTime.getTimeInMillis(), response);
		} catch(org.springframework.web.client.ResourceAccessException e) {
			LOGGER.warn("The endpoint is not available! Calling the next one. The error message is '{}'.", e.getMessage());
			LOGGER.debug("The exception is as follows", e);
			response = callWithLoadBalancing(endpointsQueue.next(uniqueId), letter);
		} catch(HttpClientErrorException e) {
			LOGGER.warn("The endpoint is not available! Calling the next one. The error message is '{}'.", e.getMessage());
			LOGGER.debug("The exception is as follows", e);
			response = callWithLoadBalancing(endpointsQueue.next(uniqueId), letter);
		}

		return response;
	}

	
	public RestTemplate getRestTemplate() {
		return restTemplate;
	}

	public void setRestTemplate(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	public EndpointsQueue getEndpointsQueue() {
		return endpointsQueue;
	}

	public void setEndpointsQueue(EndpointsQueue endpointsQueue) {
		this.endpointsQueue = endpointsQueue;
	}

	public JSONPostMan getPostMan() {
		return postMan;
	}

	public void setPostMan(JSONPostMan postMan) {
		this.postMan = postMan;
	}

}