package org.sct.clientframework.call;

import java.util.Map;

import org.apache.http.HttpHost;
import org.sct.common.rest.HttpComponentsClientHttpRequestDigestAuthFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestOperations;
import org.springframework.web.client.RestTemplate;

public class PreemptiveAuthenticationRestTemplate extends RestTemplate implements RestOperations  {

	
   public PreemptiveAuthenticationRestTemplate(HttpComponentsClientHttpRequestDigestAuthFactory factory) {
        super();
        setRequestFactory(factory);
    }

   
	@Override
	public <T> ResponseEntity<T> exchange(String url, HttpMethod method, HttpEntity<?> requestEntity, Class<T> responseType, Object... uriVariables) throws RestClientException {
		Map<String,Object> credentials = CredentialsStorage.get().get();
        HttpHost host = (HttpHost) credentials.get("host");
		String username = (String) credentials.get("username");
		String password = (String) credentials.get("password");
		
    	HttpComponentsClientHttpRequestDigestAuthFactory requestFactory = (HttpComponentsClientHttpRequestDigestAuthFactory) getRequestFactory();
		requestFactory.setCredentials(host, username, password);

        return super.exchange(url, method, requestEntity, responseType, uriVariables);
	}
    
    
}
