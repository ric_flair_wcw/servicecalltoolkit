package org.sct.clientframework;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.sct.clientframework.call.CredentialsStorage;
import org.sct.clientframework.call.RestClient;
import org.sct.clientframework.exception.ClientFrameworkException;
import org.sct.clientframework.exception.ServerException;
import org.sct.common.encryption.EnigmaGadget;
import org.sct.common.utils.MapUtils;
import org.sct.common.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;

import com.fasterxml.jackson.databind.ObjectMapper;


/**
 * The class provides simple methods to call the server engine.
 * The org.sct.clientframework.utils.ResponseMiner class can help to extract the value from the response if the getStruct method is used.
 * 
 * @author      Viktor Horvath
 */
public class Service {

	
	private static ClassPathXmlApplicationContext applicationContext = null;
	private static final Logger LOGGER = LoggerFactory.getLogger(Service.class);
	
	private RestClient restClient;
	private ObjectMapper objectMapper;
	private EnigmaGadget enigma;
	
	private String encryptionKey = null;
	private boolean isEncryptionNeeded = false;
	
	
	private Service() { }

	
	/**
	 * Getting a new instance of the Service class. It uses the clientFramework-applicationContext.xml context file to create the object.
	 * The authentication information has to be defined for REST authentication with the Server Engine
	 *
	 * @param  username User name for REST authentication with the Server Engine
	 * @param  password Password for REST authentication with the Server Engine
	 * @return An instance of the Service class
	 */
	public synchronized static Service createInstance(String username, String password) {
		// checking the parameters
		Utils.checkEmpty(username, "user name");
		Utils.checkEmpty(password, "password");
		
		// creating the app context
		if (applicationContext == null) {
			applicationContext = new ClassPathXmlApplicationContext("clientFramework-applicationContext.xml");
		}
		
		// getting the bean
		Service instance = (Service) applicationContext.getBean("service");
		
		// put the credentials into ThreadLocal (The reason why I am using ThreadLocal is that otherwise I couldn't unit test this class and RestClient class -> the MockRestServiceServer.createServer overwrites the ClientHttpRequestFactory of the restTemplate)
		Map<String,Object> credentials = new HashMap<String, Object>();
		credentials.put("username", username); credentials.put("password", password);
		CredentialsStorage.get().set(credentials);
		
		return instance;
	}
	
	
	/**
	 * Closing the Spring application context
	 *
	 */
	public static void closeApplicationContext() {
		applicationContext.close();
	}
	
	
	/**
	 * Setting the key for encryption
	 *
	 * @param  encryptionKey The key for encryption
	 */
	public void setEncryptionKey(String encryptionKey) {
		this.encryptionKey = encryptionKey;
	}
	
	
	// ************************************************** get **************************************************
	
	/**
	 * Getting a simple string value by the POI name.
	 *
	 * @param  poiName The name of the POI
	 * @return string value from the server engine
	 */
	public String get(String poiName) throws ClientFrameworkException, RestClientException, ServerException {
		return get(poiName, (Map<String, Object>)null);
	}
	/**
	 * Getting a simple string value by the POI name and an id.
	 *
	 * @param  poiName The name of the POI
	 * @param  id The identification of the value in the external (being called by the server engine) service
	 * @return string value from the server engine
	 */
	public String get(String poiName, String id) throws ClientFrameworkException, RestClientException, ServerException {
		try {
			Utils.checkEmpty(id, "id");
			
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("id", id);
			
			return get(poiName, params);
		} finally {
			CredentialsStorage.get().remove();
		}
	}
	/**
	 * Getting a simple string value by the POI name and parameters.
	 *
	 * @param  poiName The name of the POI
	 * @param  params a Map that contains the parameters of the call
	 * @return string value from the server engine
	 */
	public String get(String poiName, Map<String, Object> params) throws RestClientException, ClientFrameworkException, ServerException {
		try {
			Utils.checkEmpty(poiName, "poiName");
	
			// creating the map of the body
			Map<String, Object> bodyMap = createBodyMap(poiName, params);
			// encrypting it if necessary
			Object objectToBeSent;
			try {
				objectToBeSent = enigma.encrypt(bodyMap, isEncryptionNeeded, getEncryptionKey());
			} catch (Exception e) {
				throw new ClientFrameworkException("Exception occurred while encrypting!", e);
			}
			
			// calling the server
			ResponseEntity<String> responseEntity = restClient.forward(objectToBeSent, isEncryptionNeeded);
			
			// checking the response -> if its status is 'error' then exception being thrown
			Map<String, Object> rootNode = checkResponse(responseEntity);
			
			// decrypting the single value
			String singleValue = null;
			try {
				String isResponseEncrypted = rootNode == null ? null : MapUtils.goToWithoutNamespace(rootNode, "/header/encrypted", String.class);
				// if the rootNode is not null then we received a JSON => we need the /body from it 
				singleValue = enigma.decrypt(rootNode == null ? responseEntity.getBody() : MapUtils.goToWithoutNamespace(rootNode, "/body", String.class), 
						                     (isResponseEncrypted != null && isResponseEncrypted.equalsIgnoreCase("true")), 
						                     getEncryptionKey(), 
						                     String.class);
			} catch (Exception e) {
				throw new ClientFrameworkException("Exception occurred while decrypting!", e);
			}
			
			return singleValue;
		} finally {
			CredentialsStorage.get().remove();
		}
	}
	/**
	 * Getting a simple value by the POI name and an id. The type of the value has to be defined.
	 *
	 * @param  poiName The name of the POI
	 * @param  id The identification of the value in the external (being called by the server engine) service
	 * @param  clazz The class of the value being retrieved from the external service
	 * @return value from the server engine
	 */
	public <T> T get(String poiName, String id, Class<T> clazz) throws ClientFrameworkException, RestClientException, ServerException {
		return clazz.cast(get(poiName, id));
	}
	/**
	 * Getting a simple value by the POI name and an parameters. The type of the value has to be defined.
	 *
	 * @param  poiName The name of the POI
	 * @param  params a Map that contains the parameters of the call
	 * @param  clazz The class of the value being retrieved from the external service
	 * @return value from the server engine
	 */
	public <T> T get(String poiName, Map<String, Object> params, Class<T> clazz) throws RestClientException, ClientFrameworkException, ServerException {
		return clazz.cast(get(poiName, params));
	}
	
	
	// ************************************************** getStruct **************************************************

	
	/**
	 * Getting a composite value by the POI name.
	 *
	 * @param  poiName The name of the POI
	 * @param  clazz The class of the value being retrieved from the external service (possible values: String.class, Map.class, List.class)
	 * @return composite value from the server engine represented by Map<String,?>
	 */
	public <T> T getStruct(String poiName, Class<T> clazz) throws RestClientException, ClientFrameworkException, ServerException {
		return getStruct(poiName, (Map<String, Object>)null, clazz);
	}
	/**
	 * Getting a composite value by the POI name and an id.
	 *
	 * @param  poiName The name of the POI
	 * @param  id The identification of the value in the external (being called by the server engine) service
	 * @param  clazz The class of the value being retrieved from the external service (possible values: String.class, Map.class, List.class)
	 * @return composite value from the server engine represented by Map<String,?>
	 */
	public <T> T getStruct(String poiName, String id, Class<T> clazz) throws RestClientException, ClientFrameworkException, ServerException {
		try {
			Utils.checkEmpty(id, "id");
			
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("id", id);
	
			return getStruct(poiName, params, clazz);
		} finally {
			CredentialsStorage.get().remove();
		}
	}
	/**
	 * Getting a composite value by the POI name and an parameters.
	 *
	 * @param  poiName The name of the POI
	 * @param  params a Map that contains the parameters of the call
	 * @param  clazz The class of the value being retrieved from the external service (possible values: String.class, Map.class, List.class)
	 * @return composite value from the server engine represented by Map<String,?>
	 */
	public <T> T getStruct(String poiName, Map<String, Object> params, Class<T> clazz) throws RestClientException, ClientFrameworkException, ServerException {
		try {
			Utils.checkEmpty(poiName, "poiName");
			
			// creating the map of the body
			Map<String, Object> bodyMapReq = createBodyMap(poiName, params);
			// encrypting it if necessary
			Object objectToBeSent;
			try {
				objectToBeSent = enigma.encrypt(bodyMapReq, isEncryptionNeeded, getEncryptionKey());
			} catch (Exception e) {
				throw new ClientFrameworkException("Exception occurred while encrypting!", e);
			}
			
			// calling the server
			ResponseEntity<String> responseEntity = restClient.forward(objectToBeSent, isEncryptionNeeded);
	
			// getting the content of the response (the element under the 'body')
			// checking the response -> if its status is 'error' then exception being thrown
			Map<String, Object> rootNode = checkResponse(responseEntity);
			Object bodyNode = rootNode.get("body");
			if (bodyNode == null) {
				LOGGER.debug("The arrived response is {}", responseEntity.getBody());
				throw new ClientFrameworkException("The 'body' root element doesn't exist in the JSON response!");
			}
			
			// decrypting it if necessary
			T bodyMapResp;
			try {
				String isResponseEncrypted = MapUtils.goToWithoutNamespace(rootNode, "/header/encrypted", String.class);
				
				bodyMapResp = enigma.decrypt(bodyNode, 
						                     (isResponseEncrypted != null && isResponseEncrypted.equalsIgnoreCase("true")),
						                     getEncryptionKey(), 
						                     clazz);
			} catch (Exception e) {
				throw new ClientFrameworkException("Exception occurred while decrypting!", e);
			}
			
			// handling the response if database response comes back
			String headerType = MapUtils.goToWithoutNamespace(rootNode, "/header/type", String.class);
			if (headerType != null && headerType.equals("DATABASE")) {
				handleDatabaseTypeResponse(clazz, bodyMapResp);
			}
	
			return bodyMapResp;
		} finally {
			CredentialsStorage.get().remove();
		}
	}


	private <T> void handleDatabaseTypeResponse(Class<T> clazz, T bodyMapResp) throws ClientFrameworkException {
		// the clazz can be only Map if the type is DATABASE in the response
		if (!clazz.equals(Map.class)) {
			throw new ClientFrameworkException("The type of the response is DATABASE which means that only Map can be used as return type!");
		}
		// converting the values
		Map<String,Object> respMap = (Map<String,Object>)bodyMapResp;
		for(String colName : respMap.keySet()) {
			if (!(respMap.get(colName) instanceof List<?>)) {
				LOGGER.info("The response type: {}\nThe response : {}", respMap.get(colName).getClass(), respMap.get(colName));
				throw new ClientFrameworkException("I expected a List at /body/" + colName + " in the response!");
			}
			List<String> values = (List<String>)respMap.get(colName);
			// getting the type
			String type = values.get(0);
			// creating a new list that contains the values with the specific type 
			List<Object> newValues = new ArrayList<Object>();
			for(int i = 1; i < values.size(); i++) {
				// getting the class of the type
				Class newClazz = null;
				try {
					newClazz = Class.forName(type);
				} catch(ClassNotFoundException cnfe) {
					LOGGER.info("The type in the response is {} but such a class doesn't exist!", type);
				}
				// cast the value to type
				Object newValue = convertValue(values.get(i), newClazz);
				// add the casted value to the new list
				newValues.add(newValue);
			}
			respMap.put(colName, newValues);
		}
	}



	public RestClient getRestClient() {
		return restClient;
	}

	public void setRestClient(RestClient restClient) {
		this.restClient = restClient;
	}

	public ObjectMapper getObjectMapper() {
		return objectMapper;
	}

	public void setObjectMapper(ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
	}

	public EnigmaGadget getEnigma() {
		return enigma;
	}

	public void setEnigma(EnigmaGadget enigma) {
		this.enigma = enigma;
	}
	
	public boolean isEncryptionNeeded() {
		return isEncryptionNeeded;
	}

	public void setEncryptionNeeded(boolean isEncryptionNeeded) {
		this.isEncryptionNeeded = isEncryptionNeeded;
	}


	// ************************************************************************************************************
	private Map<String, Object> checkResponse(ResponseEntity<String> responseEntity) throws ClientFrameworkException, ServerException {
		Map<String, Object> rootNode = null;
		
		// if the HTTP status code is not successful
		if (!responseEntity.getStatusCode().is2xxSuccessful()) {
			throw new ServerException("The HTTP request is not successful! HTTP status code:" + responseEntity.getStatusCode().value() + ", Reason:" + responseEntity.getStatusCode().getReasonPhrase());
		}
		
		// the content type is not TEXT/PLAIN
		if (responseEntity.getHeaders().getContentType().getSubtype().equalsIgnoreCase("json")) {
			// transforming the body string to JSON
			try {
				rootNode = objectMapper.readValue(responseEntity.getBody(), Map.class);
			} catch (IOException e) {
				LOGGER.error("The body of the response is {}", responseEntity.getBody());
				throw new ClientFrameworkException("The response is JSON but the root node cannot be read!", e);
			}
			String statusResponse = MapUtils.goToWithoutNamespace(rootNode, "/header/status", String.class);
			// the /header/status must exist always
			if (statusResponse == null) {
				LOGGER.error("The body of the response is {}", responseEntity.getBody());
				throw new ClientFrameworkException("The /header/status cannot be found in the arrived JSON!");
			}
			// if an error arrived
			if (statusResponse.equals("error")) {
				// checking if encryption is needed
				String encryptedResponse = MapUtils.goToWithoutNamespace(rootNode, "/header/encrypted", String.class);
				if (encryptedResponse != null && encryptedResponse.equalsIgnoreCase("true")) {
					String encryptedText = null;
					try {
						encryptedText = MapUtils.goToWithoutNamespace(rootNode, "/body", String.class);
					} catch(ClassCastException cce) {
						LOGGER.error("encryptedText: {}", encryptedText);
						throw new ClientFrameworkException("As per the header of the message encryiption is needed but the body doesn't seem to be encypted!", cce);
					}
					Map<String, Object> body = null;
					try {
						body = enigma.decrypt(encryptedText, true, getEncryptionKey(), Map.class);
					} catch(ClassCastException cce) {
						LOGGER.error("encryptedText: {}", encryptedText);
						throw new ClientFrameworkException("As per the header of the message encryiption is needed but the body doesn't seem to be encypted!", cce);
					} 
					catch (Exception e) {
						throw new ClientFrameworkException("Exception occurred while decrypting!", e);
					}
					rootNode.put("body", body);
				}
				String errorClass = MapUtils.goToWithoutNamespace(rootNode, "/body/class", String.class);
				String errorMessage = MapUtils.goToWithoutNamespace(rootNode, "/body/message", String.class);
				throw new ServerException(errorMessage, errorClass);
			}
		}
		return rootNode;
	}


	private Map<String, Object> createBodyMap(String poiName, Map<String, Object> params) {
		// creating the map of the body to be sent
		Map<String, Object> bodyMap = new HashMap<String, Object>();
		bodyMap.put("poiName", poiName);
		if (!Utils.isEmpty(params)) {
			bodyMap.put("params", params);
		}
		
		return bodyMap;
	}

	
	private String getEncryptionKey() throws ClientFrameworkException {
		if (isEncryptionNeeded && Utils.isEmpty(encryptionKey)) {
			throw new ClientFrameworkException("Encryption is needed but the encryption key is not set (null or empty)!");
		}
		return encryptionKey;
	}
	
	
	private Object convertValue(String value, Class clazz) throws ClientFrameworkException {
		if (clazz.equals(String.class)) {
			return value;
		} 
		else if (clazz.equals(Number.class)) {
			// more number types are accepted and they have different contructor => that constructor has to be found that has only one string parameter
			Constructor[] constructors = clazz.getConstructors();
			for(Constructor constructor : constructors) {
				Class<?>[] pTypes  = constructor.getParameterTypes();
				if (pTypes.length == 1 && pTypes[0].equals(String.class)) {
					try {
						return constructor.newInstance(value);
					} catch (Exception e) {
						throw new ClientFrameworkException("Unknown exception when trying to cast a Number!", e);
					}
				}
			}
			LOGGER.info("Class: {}, value: {}", clazz, value);
			throw new ClientFrameworkException("I haven't found a constructor of the type " + clazz + "' that has only one String parameter!");
		}
		else if (clazz.equals(Date.class)) {
			long ms = -1;
			try {
				ms = Long.valueOf(value);
			} catch(NumberFormatException nfe) {
				LOGGER.info("Class: {}, value: {}", clazz, value);
				throw new ClientFrameworkException("The type of the value is Date but the value is not a long!");
			}
			return new Date(ms);
		} 
		else if (clazz.equals(Boolean.class)) {
			return new Boolean(value);
		}
		else {
			return value.toString();
		}
	}
	

}
