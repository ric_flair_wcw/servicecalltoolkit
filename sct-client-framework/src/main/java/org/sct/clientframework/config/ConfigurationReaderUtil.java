package org.sct.clientframework.config;

import java.io.InputStream;
import java.util.List;

import org.sct.clientframework.beans.ConfigBean;
import org.sct.clientframework.exception.ClientFrameworkException;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.springframework.core.io.ClassPathResource;

public class ConfigurationReaderUtil implements IConfigurationReaderUtil {

	
	private static final Object MONITOR_CONFIG = new Object();
	
	private ConfigBean configBean = null;
	private String pathConfigFile = null;
	
	
	private void initConfig() throws ClientFrameworkException {
		if (configBean == null) {
			synchronized (MONITOR_CONFIG) {
				if (configBean == null) {
					Serializer serializer = new Persister();
			
					try {
						InputStream in = new ClassPathResource(pathConfigFile).getInputStream();
						configBean = serializer.read(ConfigBean.class, in);
					} catch (Exception e) {
						throw new ClientFrameworkException(String.format("The config file %s cannot be read or incorrect config file!", pathConfigFile), e);
					}
				}
			}
		}
	}

	
	public List<String> getEndpoints() throws ClientFrameworkException {
		initConfig();
		
		return configBean.getEndpoints();
	}

	
	public String getPathConfigFile() {
		return pathConfigFile;
	}


	public void setPathConfigFile(String pathConfigFile) {
		this.pathConfigFile = pathConfigFile;
	}
	
}
