package org.sct.clientframework.config;

import java.util.List;

import org.sct.clientframework.exception.ClientFrameworkException;

public interface IConfigurationReaderUtil {

	List<String> getEndpoints() throws ClientFrameworkException;

	String getPathConfigFile();

	void setPathConfigFile(String pathConfigFile);

}