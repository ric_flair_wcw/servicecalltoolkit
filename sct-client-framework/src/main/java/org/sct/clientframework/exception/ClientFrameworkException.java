package org.sct.clientframework.exception;

public class ClientFrameworkException extends Exception {


	private static final long serialVersionUID = 3092324380131986330L;
	

	public ClientFrameworkException(String message) {
		super(message);
	}

	public ClientFrameworkException(String message, Exception e) {
		super(message, e);
	}
	
}
