package org.sct.clientframework.exception;

public class ServerException extends Exception {

	private static final long serialVersionUID = -7796949056025761196L;
	
	private String errorClass = null;
	private String errorMessage = null;

	public ServerException(String errorMessage, String errorClass) {
		super(errorClass + " : " + errorMessage);
		this.errorClass = errorClass;
		this.errorMessage = errorMessage;
	}
	
	public ServerException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}
	
	public String getErrorClass() {
		return errorClass;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

}
