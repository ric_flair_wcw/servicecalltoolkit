package org.sct.clientframework.beans;

import java.util.ArrayList;
import java.util.List;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;


@Root(name="config")
public class ConfigBean {

	
	@ElementList(inline=true, required=true, entry="endpoint")
	private List<String> endpoints = null;

	public ConfigBean() {
	}

	public ConfigBean(ConfigBean source) {
		endpoints = new ArrayList<String>();
		for(String endpoint : source.endpoints) {
			endpoints.add(endpoint);
		}
	}
	
	
	public List<String> getEndpoints() {
		return endpoints;
	}

	public void setEndpoints(List<String> endpoints) {
		this.endpoints = endpoints;
	}


	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.getClass().getName()).append("[");
		if (endpoints != null && endpoints.size() > 0) {
			for(String endpoint : endpoints) {
				sb.append(endpoint).append(",");			
			}
		}
		sb.append("]");
		return sb.toString();
	}
	
}
