package flow;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.containing;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.matching;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.sct.server.common.beans.POIDTO;
import org.sct.server.common.beans.ServiceDTO;
import org.sct.server.common.constants.Constants;
import org.sct.server.common.dao.lowlevel.cache.impl.LocalCache;
import org.sct.server.common.persistence.repository.StatServicePoiDateCntEntityRepository;
import org.sct.server.common.persistence.service.IPOIEntityService;
import org.sct.server.common.persistence.service.IServiceEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.integration.channel.QueueChannel;
import org.springframework.integration.channel.interceptor.WireTap;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.integration.transformer.MessageTransformingHandler;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.junit.WireMockRule;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:SpringIntegration-servlet-test-flow.xml")
public class FlowTest {

	private static final int PORT = 3089;
	@Rule
	public WireMockRule wireMockRule = new WireMockRule(PORT);

	// autowired from SI flow
	@Autowired
	@Qualifier("arrivedRestReq_channel")
	private MessageChannel arrivedRestReqChannel;
	@Autowired
	@Qualifier("logger-channel")
	private MessageChannel loggerChannel;
	@Autowired
	@Qualifier("textContentType-headerEnricher.handler")
	private MessageTransformingHandler textContentTypeHeaderEnricherHandler;
	@Autowired
	@Qualifier("jsonContentType-headerEnricher.handler")
	private MessageTransformingHandler jsonContentTypeHeaderEnricherHandler;
	// autowired from SpringIntegration-servlet-test-flow.xml
	@Autowired
	private LocalCache mockedLocalCache;
	
	private String requestTemplateTextXML =
					"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns=\"http://virginmedia/schema/GetAddressForPostCode/4/0\" xmlns:ns1=\"http://virginmedia/schema/RequestHeader/1/4\" xmlns:ns2=\"http://virginmedia/schema/BaseType/1/3\">" +
					"   <soapenv:Header/>" +
					"   <soapenv:Body>" +
					"      <ns:GetAddressForPostCodeRequest>" +
					"         <ns:requestHeader>" +
					"            <ns1:id>${sctParam.id}</ns1:id>" +
					"            <ns1:user>" +
					"               <ns2:userId>userId</ns2:userId>" +
					"               <ns2:credentials>credentials</ns2:credentials>" +
					"            </ns1:user>" +
					"         </ns:requestHeader>" +
					"         <ns:postalAddress>" +
					"            <ns:buildingName>buildingName</ns:buildingName>" +
					"            <ns:country>HU</ns:country>" +
					"            <ns:postcode>${sctParam.postcode}</ns:postcode>" +
					"            <ns:addressIdentification>" +
					"               <ns:identifierType>identifierType</ns:identifierType>" +
					"               <ns:addressIdentifier>addressIdentifier</ns:addressIdentifier>" +
					"            </ns:addressIdentification>" +
					"         </ns:postalAddress>" +
					"      </ns:GetAddressForPostCodeRequest>" +
					"   </soapenv:Body>" +
					"</soapenv:Envelope>";
	private String responseStringXML =
			    "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns=\"http://virginmedia/schema/GetAddressForPostCode/4/0\" xmlns:ns1=\"http://virginmedia/schema/ResponseHeader/2/1\" xmlns:ns2=\"http://virginmedia/schema/faults/2/1\">" +
				"   <soapenv:Header/>" +
				"   <soapenv:Body>" +
				"      <ns:GetAddressForPostCodeResponse>" +
				"         <ns:responseHeader>" +
				"            <ns1:id>ezt kell visszaadni</ns1:id>" +
				"         </ns:responseHeader>" +
				"         <ns:postalAddress>" +
				"            <ns:buildingName>1_2</ns:buildingName>" +
				"            <ns:county>1_3</ns:county>" +
				"            <ns:apartment>1_4</ns:apartment>" +
				"            <ns:building>1_5</ns:building>" +
				"            <ns:buildingID>1_6</ns:buildingID>" +
				"            <ns:addressIdentification>" +
				"               <ns:identifierType>1_7</ns:identifierType>" +
				"               <ns:identifierType>1_9</ns:identifierType>" +
				"               <ns:identifierType>1_11</ns:identifierType>" +
				"            </ns:addressIdentification>" +
				"         </ns:postalAddress>" +
				"         <ns:postalAddress>" +
				"            <ns:displayAddress>3_1</ns:displayAddress>" +
				"            <ns:buildingName>3_2</ns:buildingName>" +
				"            <ns:county>3_3</ns:county>" +
				"            <ns:apartment>3_4</ns:apartment>" +
				"            <ns:building>3_5</ns:building>" +
				"            <ns:buildingID>3_6</ns:buildingID>" +
				"            <ns:addressIdentification>" +
				"               <ns:identifierType>3_7</ns:identifierType>" +
				"               <ns:addressIdentifier>3_8</ns:addressIdentifier>" +
				"            </ns:addressIdentification>" +
				"            <ns:addressIdentification>" +
				"               <ns:identifierType>3_9</ns:identifierType>" +
				"               <ns:addressIdentifier>3_10</ns:addressIdentifier>" +
				"            </ns:addressIdentification>" +
				"            <ns:addressIdentification>" +
				"               <ns:identifierType>3_11</ns:identifierType>" +
				"               <ns:addressIdentifier>3_12</ns:addressIdentifier>" +
				"            </ns:addressIdentification>" +
				"         </ns:postalAddress>" +
				"         <ns:postalAddress>" +
				"            <ns:displayAddress>2_1</ns:displayAddress>" +
				"            <ns:buildingName>2_2</ns:buildingName>" +
				"            <ns:county>2_3</ns:county>" +
				"            <ns:apartment>2_4</ns:apartment>" +
				"            <ns:building>2_5</ns:building>" +
				"            <ns:buildingID>2_6</ns:buildingID>" +
				"         </ns:postalAddress>" +
				"      </ns:GetAddressForPostCodeResponse>" +
				"   </soapenv:Body>" +
				"</soapenv:Envelope>";
	private String requestTemplateTextJSON =
			"{" +
			"	\"GetAddressForPostCodeRequest\" : {" +
			"		\"responseHeader\" : {" +
			"			\"id\" : \"ezt nem kell visszaadni\"" +
			"		}" +
			"	}," +
			"	\"body\" : {" +
			"		\"customerId\" : \"${sctParam.customerId}\"," +
			"		\"county\" : \"Dorset\"" +
			"	}" +
			"}";
	private String responseStringJSON =
			"{" +
			"	\"GetAddressForPostCodeResponse\" : {" +
			"		\"responseHeader\" : {" +
			"			\"id\" : \"ezt kell visszaadni\"" +
			"		}," +
			"	\"postalAddress\" : [ {" +
			"		\"buildingName\" : \"1_2\"," +
			"		\"county\" : \"1_3\"," +
			"		\"apartment\" : \"1_4\"," +
			"		\"building\" : \"1_5\"," +
			"		\"buildingID\" : \"1_6\"," +
			"		\"addressIdentification\" : {" +
			"			\"identifierType\" : [ \"1_7\", \"1_9\", \"1_11\"]" +

//			"			\"identifierType\" : \"1_7\"," +
//			"			\"identifierType\" : \"1_9\"," +
//			"			\"identifierType\" : \"1_11\"" +
			"		}" +
			"	}," +
			"	{" +
			"		\"displayAddress\" : \"3_1\"," +
			"		\"buildingName\" : \"3_2\"," +
			"		\"county\" : \"3_3\"," +
			"		\"apartment\" : \"3_4\"," +
			"		\"building\" : \"3_5\"," +
			"		\"buildingID\" : \"3_6\"," +
			"		\"addressIdentification\" : [ {" +
			"			\"identifierType\" : \"3_7\", " +
			"			\"addressIdentifier\" : \"3_8\"" +
			"		}," +
			"		{" +
			"			\"identifierType\" : \"3_9\", " +
			"			\"addressIdentifier\" : \"3_10\"" +
			"		}, " +
			"		{" +
			"			\"identifierType\" : \"3_11\", " +
			"			\"addressIdentifier\" : \"3_12\"" +
			"		} ]" +
			"	}," +
			"	{" +
			"		\"displayAddress\" : \"2_1\"," +
			"		\"buildingName\" : \"2_2\"," +
			"		\"county\" : \"2_3\"," +
			"		\"apartment\" : \"2_4\"," +
			"		\"building\" : \"2_5\"," +
			"		\"buildingID\" : \"2_6\"" +
			"	} ]" +
			"}"+
			"}";
	
	@Autowired
	private ApplicationContext applicationContext;
	@Autowired
	private StatServicePoiDateCntEntityRepository statServicePoiDateCntEntityRepository;
	@Autowired
	private IServiceEntityService serviceEntityService;
	@Autowired
	private IPOIEntityService poiEntityService;
	@Autowired
	private ObjectMapper objectMapper;
	
	@Before
	public void setUp() {
		
	}
	
	
	@Test
	public void testFlow_successful_WS_XPATH_single() throws InterruptedException, SQLException, JsonParseException, JsonMappingException, IOException {
		String poiName = "postcode->buildingName";
		String serviceName = "GetAddressForPostCode-GetAddressForPostCode_by_id";
		String url = "/SAP";
		String id = "111";
		String postcode = "BH13EH";
		Calendar now = Calendar.getInstance();
		// getting the serviceDTO from DB
		ServiceDTO serviceDTO = serviceEntityService.getServiceByName(serviceName);
		// getting the poiDTO from DB
		POIDTO poiDTO = poiEntityService.getPOIByName(poiName);
		
		// setting up the expectations of mocked HTTP server
		stubFor( post(urlEqualTo(url))
				 .withHeader("Accept", equalTo("text/xml"))
				 .withHeader("SOAPAction", equalTo(serviceDTO.getSoapAction()))
				 .atPriority(1)
				 .willReturn(aResponse().withStatus(200)
						                .withHeader("Content-Type", "text/xml")
						                .withBody(responseStringXML)));
		// the second stub is for passing back a 400 error if the stubs above don't match (e.g. in unsuccessful tests)
		stubFor( post(urlEqualTo(url))
				 .atPriority(5)
				 .willReturn(aResponse().withStatus(400)));
		
		// defining expectations for mockedLocalCache
		serviceDTO.getEndpoints().set(0, "http://localhost:" + PORT + url);
		serviceDTO.setRequestTemplateText(requestTemplateTextXML);
		poiDTO.getExtracts().clear();
		poiDTO.getExtracts().add("/soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeResponse/ns:responseHeader/ns1:id");
		when(mockedLocalCache.get(Constants.CACHE_KEY_POI + poiName, POIDTO.class)).thenReturn(poiDTO );
		when(mockedLocalCache.get(Constants.CACHE_KEY_SERVICE + serviceName, ServiceDTO.class)).thenReturn(serviceDTO);
		
		// changing the final message channel to pollable channel in order to be able to get the out message
		QueueChannel outputChannel = new QueueChannel();
		textContentTypeHeaderEnricherHandler.setOutputChannel(outputChannel);
		// connecting the logger-channel to the pollable channel to be able to get it tested
		WireTap wireTap = new WireTap(loggerChannel);
		outputChannel.addInterceptor(wireTap);
		
		// creating the message to be sent
		String jsonString = 
						"{" +
						"	\"body\" : {" +
						"		\"poiName\" : \"" + poiName + "\"," +
						"		\"params\" : {" +
						"			\"id\" : \"" + id + "\"," +
						"			\"postcode\" : \"" + postcode + "\"" +
						"		}" +
						"	}," +
						"	\"header\" : {" +
						"		\"encrypted\" : \"false\"" +
						"	}" +
						"}";
		Message<String> messageIn = MessageBuilder.withPayload(jsonString).build();
		// sending the message
		arrivedRestReqChannel.send(messageIn);

		// getting the out message
		// http://stackoverflow.com/questions/29761341/how-to-do-unit-test-in-spring-integration		
		Message<?> messageOut = outputChannel.receive(10000);
		
		// verifying the mocked HTTP server calls
		verify(postRequestedFor(urlMatching(url))
	                            .withRequestBody(containing(requestTemplateTextXML.replace("${sctParam.id}", id).replace("${sctParam.postcode}", postcode)))
	                            .withHeader("Content-Type", matching("text/plain; charset=UTF-8"))
	                            .withHeader("SOAPAction", matching(serviceDTO.getSoapAction())));

		// asserting the message payload
		assertEquals("ezt kell visszaadni", (String)messageOut.getPayload());
		// checking the DB - if the statistics was updated correctly
		Calendar startDate = getPeriodStart(now);
		Calendar endDate = Calendar.getInstance();
		endDate.setTimeInMillis(startDate.getTimeInMillis());
		endDate.add(Calendar.MINUTE, 5);
		Thread.sleep(1000);
		List<Object[]> stat = statServicePoiDateCntEntityRepository.getCntGroupedByStartDateAndSuccessfulForPOI(poiName, startDate, endDate);
		assertEquals(1, Integer.valueOf(stat.get(0)[2].toString()).intValue());
	}
	
	
	@Test
	public void testFlow_successful_WS_XPATH_notSingle() throws InterruptedException, SQLException, JsonParseException, JsonMappingException, IOException {
		String poiName = "postcode->some_strange_value";
		String serviceName = "GetAddressForPostCode-GetAddressForPostCode_by_postcode";
		String url = "/IZI";
		String id = "111";
		String postcode = "BH13EH";
		Calendar now = Calendar.getInstance();
		// getting the serviceDTO from DB
		ServiceDTO serviceDTO = serviceEntityService.getServiceByName(serviceName);
		// getting the poiDTO from DB
		POIDTO poiDTO = poiEntityService.getPOIByName(poiName);		
		
		// setting up the expectations of mocked HTTP server
		stubFor( post(urlEqualTo(url))
				 .withHeader("Accept", equalTo("text/xml"))
				 .withHeader("SOAPAction", equalTo(serviceDTO.getSoapAction()))
				 .atPriority(1)
				 .willReturn(aResponse().withStatus(200)
						                .withHeader("Content-Type", "text/xml")
						                .withBody(responseStringXML)));
		// the second stub is for passing back a 400 error if the stubs above don't match (e.g. in unsuccessful tests)
		stubFor( post(urlEqualTo(url))
				 .atPriority(5)
				 .willReturn(aResponse().withStatus(400)));
		
		// defining expectations for mockedLocalCache
		serviceDTO.getEndpoints().clear();
		serviceDTO.getEndpoints().add("http://localhost:" + PORT + url);
		serviceDTO.setRequestTemplateText(requestTemplateTextXML);
		when(mockedLocalCache.get(Constants.CACHE_KEY_POI + poiName, POIDTO.class)).thenReturn(poiDTO );
		when(mockedLocalCache.get(Constants.CACHE_KEY_SERVICE + serviceName, ServiceDTO.class)).thenReturn(serviceDTO);
		
		// changing the final message channel to pollable channel in order to be able to get the out message
		QueueChannel outputChannel = new QueueChannel();
		jsonContentTypeHeaderEnricherHandler.setOutputChannel(outputChannel);
		// connecting the logger-channel to the pollable channel to be able to get it tested
		WireTap wireTap = new WireTap(loggerChannel);
		outputChannel.addInterceptor(wireTap);
		
		// creating the message to be sent
		String jsonString = 
				"{" +
				"	\"body\" : {" +
				"		\"poiName\" : \"" + poiName + "\"," +
				"		\"params\" : {" +
				"			\"id\" : \"" + id + "\"," +
				"			\"postcode\" : \"" + postcode + "\"" +
				"		}" +
				"	}," +
				"	\"header\" : {" +
				"		\"encrypted\" : \"false\"" +
				"	}" +
				"}";
		Message<String> messageIn = MessageBuilder.withPayload(jsonString).build();
		// sending the message
		arrivedRestReqChannel.send(messageIn);

		// getting the out message
		// http://stackoverflow.com/questions/29761341/how-to-do-unit-test-in-spring-integration		
		Message<?> messageOut = outputChannel.receive(10000);
		
		// verifying the mocked HTTP server calls
		verify(postRequestedFor(urlMatching(url))
				                .withRequestBody(containing(requestTemplateTextXML.replace("${sctParam.id}", id).replace("${sctParam.postcode}", postcode)))
				                .withHeader("Content-Type", matching("text/plain; charset=UTF-8"))
				                .withHeader("SOAPAction", matching(serviceDTO.getSoapAction())));

		// asserting the message payload
		Map<String, Object> payload = (Map<String, Object>)messageOut.getPayload();
		assertEquals("{\"body\":{\"soapenv:Envelope\":{\"soapenv:Body\":{\"ns:GetAddressForPostCodeResponse\":{\"ns:postalAddress\":[{\"ns:addressIdentification\":{\"ns:identifierType\":[\"1_7\",\"1_9\",\"1_11\"]}},{\"ns:addressIdentification\":[{\"ns:identifierType\":\"3_7\"},{\"ns:identifierType\":\"3_9\"},{\"ns:identifierType\":\"3_11\"}],\"ns:displayAddress\":\"3_1\"},{\"ns:displayAddress\":\"2_1\"}]}}}},\"header\":{\"encrypted\":\"false\",\"status\":\"ok\",\"type\":\"N/A\"}}", objectMapper.writeValueAsString(payload).replaceAll("[ \t\n]", ""));

		// checking the DB - if the statistics was updated correctly
		Calendar startDate = getPeriodStart(now);
		Calendar endDate = Calendar.getInstance();
		endDate.setTimeInMillis(startDate.getTimeInMillis());
		endDate.add(Calendar.MINUTE, 5);
		Thread.sleep(1000);
		List<Object[]> stat = statServicePoiDateCntEntityRepository.getCntGroupedByStartDateAndSuccessfulForPOI(poiName, startDate, endDate);
		assertEquals(1, Integer.valueOf(stat.get(0)[2].toString()).intValue());
	}
	
	
	@Test
	public void testFlow_successful_REST_GET_JSON_single() throws InterruptedException, SQLException, JsonParseException, JsonMappingException, IOException {
		String poiName = "customerId->nationalSecurityNumber";
		String serviceName = "NationalSecurityProvider_GET";
		String url = "/NSP/customer/${customerId}";
		String id = "111";
		Calendar now = Calendar.getInstance();
		// getting the serviceDTO from DB
		ServiceDTO serviceDTO = serviceEntityService.getServiceByName(serviceName);
		// getting the poiDTO from DB
		POIDTO poiDTO = poiEntityService.getPOIByName(poiName);		
		
		// setting up the expectations of mocked HTTP server
		stubFor( get(urlEqualTo(url.replace("${customerId}", id)))
				 .withHeader("Accept", equalTo("application/json, text/xml, application/xml"))
				 .atPriority(1)
				 .willReturn(aResponse().withStatus(200)
						                .withHeader("Content-Type", "application/json")
						                .withBody(responseStringJSON)));
		// the second stub is for passing back a 400 error if the stubs above don't match (e.g. in unsuccessful tests)
		stubFor( post(urlEqualTo(url))
				 .atPriority(5)
				 .willReturn(aResponse().withStatus(400)));
		
		// defining expectations for mockedLocalCache
		serviceDTO.getEndpoints().clear();
		serviceDTO.getEndpoints().add("http://localhost:" + PORT + url);
		when(mockedLocalCache.get(Constants.CACHE_KEY_POI + poiName, POIDTO.class)).thenReturn(poiDTO );
		when(mockedLocalCache.get(Constants.CACHE_KEY_SERVICE + serviceName, ServiceDTO.class)).thenReturn(serviceDTO);
		
		// changing the final message channel to pollable channel in order to be able to get the out message
		QueueChannel outputChannel = new QueueChannel();
		textContentTypeHeaderEnricherHandler.setOutputChannel(outputChannel);
		// connecting the logger-channel to the pollable channel to be able to get it tested
		WireTap wireTap = new WireTap(loggerChannel);
		outputChannel.addInterceptor(wireTap);
		
		// creating the message to be sent
		String jsonString = 
				"{" +
				"	\"body\" : {" +
				"		\"poiName\" : \"" + poiName + "\"," +
				"		\"params\" : {" +
				"			\"customerId\" : \"" + id + "\"" +
				"		}" +
				"	}," +
				"	\"header\" : {" +
				"		\"encrypted\" : \"false\"" +
				"	}" +
				"}";
		Message<String> messageIn = MessageBuilder.withPayload(jsonString).build();
		// sending the message
		arrivedRestReqChannel.send(messageIn);

		// getting the out message
		// http://stackoverflow.com/questions/29761341/how-to-do-unit-test-in-spring-integration		
		Message<?> messageOut = outputChannel.receive(10000);
		
		// verifying the mocked HTTP server calls
		verify(getRequestedFor(urlMatching(url.replace("${customerId}", id)))
	                            .withHeader("Accept", matching("application/json, text/xml, application/xml")));

		// asserting the message payload
		assertEquals("ezt kell visszaadni", (String)messageOut.getPayload());

		// checking the DB - if the statistics was updated correctly
		Calendar startDate = getPeriodStart(now);
		Calendar endDate = Calendar.getInstance();
		endDate.setTimeInMillis(startDate.getTimeInMillis());
		endDate.add(Calendar.MINUTE, 5);
		Thread.sleep(1000);
		List<Object[]> stat = statServicePoiDateCntEntityRepository.getCntGroupedByStartDateAndSuccessfulForPOI(poiName, startDate, endDate);
		assertEquals(1, Integer.valueOf(stat.get(0)[2].toString()).intValue());
	}

	
	@Test
	public void testFlow_successful_REST_POST_JSON_notSingle() throws InterruptedException, SQLException, JsonParseException, JsonMappingException, IOException {
		String poiName = "customerId->addressData";
		String serviceName = "NationalSecurityProvider_POST";
		String url = "/NSP";
		String id = "111";
		Calendar now = Calendar.getInstance();
		// getting the serviceDTO from DB
		ServiceDTO serviceDTO = serviceEntityService.getServiceByName(serviceName);
		// getting the poiDTO from DB
		POIDTO poiDTO = poiEntityService.getPOIByName(poiName);		
		
		// setting up the expectations of mocked HTTP server
		stubFor( post(urlEqualTo(url))
				 .withHeader("Accept", equalTo("application/json, text/xml, application/xml"))
				 .atPriority(1)
				 .willReturn(aResponse().withStatus(200)
						                .withHeader("Content-Type", "application/json")
						                .withBody(responseStringJSON)));
		// the second stub is for passing back a 400 error if the stubs above don't match (e.g. in unsuccessful tests)
		stubFor( post(urlEqualTo(url))
				 .atPriority(5)
				 .willReturn(aResponse().withStatus(400)));
		
		// defining expectations for mockedLocalCache
		serviceDTO.getEndpoints().clear();
		serviceDTO.getEndpoints().add("http://localhost:" + PORT + url);
		serviceDTO.setRequestTemplateText(requestTemplateTextJSON);
		when(mockedLocalCache.get(Constants.CACHE_KEY_POI + poiName, POIDTO.class)).thenReturn(poiDTO );
		when(mockedLocalCache.get(Constants.CACHE_KEY_SERVICE + serviceName, ServiceDTO.class)).thenReturn(serviceDTO);
		
		// changing the final message channel to pollable channel in order to be able to get the out message
		QueueChannel outputChannel = new QueueChannel();
		jsonContentTypeHeaderEnricherHandler.setOutputChannel(outputChannel);
		// connecting the logger-channel to the pollable channel to be able to get it tested
		WireTap wireTap = new WireTap(loggerChannel);
		outputChannel.addInterceptor(wireTap);
		
		// creating the message to be sent
		String jsonString = 
				"{" +
				"	\"body\" : {" +
				"		\"poiName\" : \"" + poiName + "\"," +
				"		\"params\" : {" +
				"			\"customerId\" : \"" + id + "\"" +
				"		}" +
				"	}," +
				"	\"header\" : {" +
				"		\"encrypted\" : \"false\"" +
				"	}" +
				"}";
		Message<String> messageIn = MessageBuilder.withPayload(jsonString).build();
		// sending the message
		arrivedRestReqChannel.send(messageIn);

		// getting the out message
		// http://stackoverflow.com/questions/29761341/how-to-do-unit-test-in-spring-integration		
		Message<?> messageOut = outputChannel.receive(10000);
		
		// verifying the mocked HTTP server calls
		verify(postRequestedFor(urlMatching(url.replace("{customerId}", id)))
				                .withRequestBody(containing(requestTemplateTextJSON.replace("${sctParam.customerId}", id)))
	                            .withHeader("Accept", matching("application/json, text/xml, application/xml")));

		// asserting the message payload
		Map<String, Object> payload = (Map<String, Object>)messageOut.getPayload();
		assertEquals("{\"body\":{\"GetAddressForPostCodeResponse\":{\"postalAddress\":[{\"addressIdentification\":{\"identifierType\":[\"1_7\",\"1_9\",\"1_11\"]}},{\"displayAddress\":\"3_1\",\"addressIdentification\":[{\"identifierType\":\"3_7\"},{\"identifierType\":\"3_9\"},{\"identifierType\":\"3_11\"}]},{\"displayAddress\":\"2_1\"}]}},\"header\":{\"encrypted\":\"false\",\"status\":\"ok\",\"type\":\"N/A\"}}", objectMapper.writeValueAsString(payload).replaceAll("[ \t\n]", ""));

		// checking the DB - if the statistics was updated correctly
		Calendar startDate = getPeriodStart(now);
		Calendar endDate = Calendar.getInstance();
		endDate.setTimeInMillis(startDate.getTimeInMillis());
		endDate.add(Calendar.MINUTE, 5);
		Thread.sleep(1000);
		List<Object[]> stat = statServicePoiDateCntEntityRepository.getCntGroupedByStartDateAndSuccessfulForPOI(poiName, startDate, endDate);
		assertEquals(1, Integer.valueOf(stat.get(0)[2].toString()).intValue());
	}

	
	@After
	public void clearMocks() {
		Mockito.reset(mockedLocalCache);
	}
	
	
	private Calendar getPeriodStart(Calendar reqArrivalTime) {
		reqArrivalTime.set(Calendar.MILLISECOND, 0);
		reqArrivalTime.set(Calendar.SECOND, 0);
		reqArrivalTime.set(Calendar.MINUTE, reqArrivalTime.get(Calendar.MINUTE) / 5 * 5);
		
		return reqArrivalTime;
	}

	
}