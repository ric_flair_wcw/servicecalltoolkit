package org.sct.server.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.matchers.JUnitMatchers.containsString;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.sct.server.common.beans.POIDTO;
import org.sct.server.common.beans.ServiceDTO;
import org.sct.server.common.constants.Constants;
import org.sct.server.common.dao.highlevel.cache.ICacheBeanDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:SpringIntegration-servlet-test2.xml")
@WebAppConfiguration
public class CacheControllerTest {

	
	@Autowired
	private ApplicationContext applicationContext;
	@Autowired
	private ICacheBeanDAO mockedCacheBeanDAO;

	private CacheController cacheController = null;
	private MockMvc mockMvc;
	
	String serviceName = "aaa";
	String serviceJsonString = 
					"{" +
					"	\"id\" : 34," +
					"	\"name\" : \"aaa\"," +
					"	\"serviceType\" : \"WEBSERVICE\"," +
					"	\"soapAction\" : \"GetAddressForPostCode\"," +
					"	\"requestTemplateText\" : \"<soapenv:Envelope></soapenv:Envelope>\"," +
					"	\"namespaces\" : {" +
					"		\"ns2\" : \"http://virginmedia/schema/faults/2/1\"," +
					"		\"ns1\" : \"http://virginmedia/schema/ResponseHeader/2/1\"," +
					"		\"soapenv\" : \"http://schemas.xmlsoap.org/soap/envelope/\"," +
					"		\"ns\" : \"http://virginmedia/schema/GetAddressForPostCode/4/0\"" +
					"	}," +
					"	\"endpoints\" : [\"http://lap50215:8083/mockGetAddressForPostCode\"]," +
					"	\"httpMethod\" : null" +
					"}";				
	String poiName = "aaav";
	String poiJsonString = 
					"{" +
					"    \"id\" : 39," +
					"    \"name\" : \"aaav\"," +
					"    \"singleValue\" : \"true\"," +
					"    \"encryptionNeeded\" : true," +
					"    \"serviceDTO\" : {" +
					"        \"id\" : 29," +
					"        \"name\" : \"GetAddressForPostCode-GetAddressForPostCode_by_id\"," +
					"        \"serviceType\" : \"WEBSERVICE\"," +
					"        \"soapAction\" : null," +
					"        \"requestTemplateText\" : null," +
					"        \"namespaces\" : null," +
					"        \"endpoints\" : null," +
					"        \"httpMethod\" : null" +
					"    }," +
					"    \"extracts\" : [\"/soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeResponse/ns:postalAddress/ns:buildingName\"]" +
					"}";	
	
	@Before
	public void setUp() {
		cacheController = (CacheController) applicationContext.getBean("cacheController");
		this.mockMvc = MockMvcBuilders.standaloneSetup(cacheController).build();
	}

	
	@Test
	public void testSetService_successful() throws Exception {
		// defining the mockito expectations
		doNothing().when(mockedCacheBeanDAO).put(eq(serviceName), any(ServiceDTO.class));
		
		mockMvc.perform(post("/cache/service").content(serviceJsonString).contentType(MediaType.APPLICATION_JSON))
						.andExpect(status().isOk());
		
		verify(mockedCacheBeanDAO).put(eq(serviceName), any(ServiceDTO.class));
	}

	
	@Test
	public void testGetService() throws Exception {
		// defining the mockito expectations
		ServiceDTO service = new ServiceDTO(45l, serviceName, null, "soapAction", null, null, null, null, null, null);
		when(mockedCacheBeanDAO.getServiceCacheBean( serviceName )).thenReturn(service);
		
		// calling the controller
		mockMvc.perform(get("/cache/service/" + serviceName ))
						.andExpect(status().isOk())
						.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
						.andExpect(jsonPath("$.name").value(containsString(serviceName)))
						.andExpect(jsonPath("$.soapAction").value(containsString("soapAction")));
	}
	
	
	@Test
	public void testDeleteService_successful() throws Exception {
		// defining the mockito expectations
		doNothing().when(mockedCacheBeanDAO).deleteServiceCacheBean(eq(serviceName));
		
		// calling the controller
		mockMvc.perform(delete("/cache/service/" + serviceName))
						.andExpect(status().isOk());
		
		verify(mockedCacheBeanDAO).deleteServiceCacheBean(eq(serviceName));
	}

	
	@Test
	public void testGetAllServices_successful_3_serviceDTOs() throws Exception {
		List<String> keys = new ArrayList<String>();
		keys.add(Constants.CACHE_KEY_SERVICE + "0"); keys.add(Constants.CACHE_KEY_POI + "4"); keys.add(Constants.CACHE_KEY_SERVICE + "1"); keys.add(Constants.CACHE_KEY_SERVICE + "2"); keys.add(Constants.CACHE_KEY_POI + "46");
		ServiceDTO serviceDTO0 = new ServiceDTO(0l, "0", null, "soapAction0", null, null, null, null, null, null);
		ServiceDTO serviceDTO1 = new ServiceDTO(1l, "1", null, "soapAction1", null, null, null, null, null, null);
		ServiceDTO serviceDTO2 = new ServiceDTO(2l, "2", null, "soapAction2", null, null, null, null, null, null);
		// defining the mockito expectations
		when(mockedCacheBeanDAO.getCacheKeys()).thenReturn(keys);
		when(mockedCacheBeanDAO.getServiceCacheBean( "0" )).thenReturn(serviceDTO0);
		when(mockedCacheBeanDAO.getServiceCacheBean( "1" )).thenReturn(serviceDTO1);
		when(mockedCacheBeanDAO.getServiceCacheBean( "2" )).thenReturn(serviceDTO2);

		// calling the controller
		mockMvc.perform(get("/cache/service/list" ))
						.andExpect(status().isOk())
						.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
						.andExpect(jsonPath("$", hasSize(3)))
						.andExpect(jsonPath("$[0].soapAction", is("soapAction0")))
						.andExpect(jsonPath("$[1].soapAction", is("soapAction1")))
						.andExpect(jsonPath("$[2].soapAction", is("soapAction2")));
	}


	@Test
	public void testGetAllServices_successful_0_serviceDTOs() throws Exception {
		List<String> keys = new ArrayList<String>();
		keys.add(Constants.CACHE_KEY_POI + "4"); keys.add(Constants.CACHE_KEY_POI + "46");
		// defining the mockito expectations
		when(mockedCacheBeanDAO.getCacheKeys()).thenReturn(keys);

		// calling the controller
		mockMvc.perform(get("/cache/service/list" ))
						.andExpect(status().isOk())
						.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
						.andExpect(jsonPath("$", hasSize(0)));
	}


	@Test
	public void testSetPOI_successful() throws Exception {
		// defining the mockito expectations
		doNothing().when(mockedCacheBeanDAO).put(eq(poiName), any(POIDTO.class));
		
		mockMvc.perform(post("/cache/poi").content(poiJsonString).contentType(MediaType.APPLICATION_JSON))
						.andExpect(status().isOk());
		
		verify(mockedCacheBeanDAO).put(eq(poiName), any(POIDTO.class));
	}

	
	@Test
	public void testGetPOI() throws Exception {
		// defining the mockito expectations
		POIDTO poi = new POIDTO(5l, poiName, "true", null, null, null, null);
		when(mockedCacheBeanDAO.getPOICacheBean( poiName )).thenReturn(poi);
		
		// calling the controller
		mockMvc.perform(get("/cache/poi/" + poiName ))
						.andExpect(status().isOk())
						.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
						.andExpect(jsonPath("$.name").value(containsString(poiName)))
						.andExpect(jsonPath("$.singleValue").value(containsString("true")));
	}
	
	
	@Test
	public void testDeletePOI_successful() throws Exception {
		// defining the mockito expectations
		doNothing().when(mockedCacheBeanDAO).deletePOICacheBean(eq(poiName));
		
		// calling the controller
		mockMvc.perform(delete("/cache/poi/" + poiName))
						.andExpect(status().isOk());
		
		verify(mockedCacheBeanDAO, times(1)).deletePOICacheBean(eq(poiName));
	}

	
	@Test
	public void testGetAllPOIs_successful_3_POIDTOs() throws Exception {
		List<String> keys = new ArrayList<String>();
		keys.add(Constants.CACHE_KEY_POI + "0"); keys.add(Constants.CACHE_KEY_SERVICE + "4"); keys.add(Constants.CACHE_KEY_POI + "1"); keys.add(Constants.CACHE_KEY_POI + "2"); keys.add(Constants.CACHE_KEY_SERVICE + "46");
		POIDTO poiDTO0 = new POIDTO(0l, "0", "true", false, null, null, null);
		POIDTO poiDTO1 = new POIDTO(1l, "1", "false", false, null, null, null);
		POIDTO poiDTO2 = new POIDTO(2l, "2", "true", false, null, null, null);
		// defining the mockito expectations
		when(mockedCacheBeanDAO.getCacheKeys()).thenReturn(keys);
		when(mockedCacheBeanDAO.getPOICacheBean( "0" )).thenReturn(poiDTO0);
		when(mockedCacheBeanDAO.getPOICacheBean( "1" )).thenReturn(poiDTO1);
		when(mockedCacheBeanDAO.getPOICacheBean( "2" )).thenReturn(poiDTO2);

		// calling the controller
		mockMvc.perform(get("/cache/poi/list" ))
						.andExpect(status().isOk())
						.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
						.andExpect(jsonPath("$", hasSize(3)))
						.andExpect(jsonPath("$[0].singleValue", is("true")))
						.andExpect(jsonPath("$[1].singleValue", is("false")))
						.andExpect(jsonPath("$[2].singleValue", is("true")));
	}


	@Test
	public void testGetAllPOIs_successful_0_POIDTOs() throws Exception {
		List<String> keys = new ArrayList<String>();
		keys.add(Constants.CACHE_KEY_SERVICE + "4"); keys.add(Constants.CACHE_KEY_SERVICE + "46");
		// defining the mockito expectations
		when(mockedCacheBeanDAO.getCacheKeys()).thenReturn(keys);

		// calling the controller
		mockMvc.perform(get("/cache/poi/list" ))
						.andExpect(status().isOk())
						.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
						.andExpect(jsonPath("$", hasSize(0)));
	}
	
	
	@After
	public void cleanUp() {
		reset(mockedCacheBeanDAO);
	}
}
