package org.sct.server.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.eq;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.sct.server.encryption.EncryptionKeyStorage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:SpringIntegration-servlet-test.xml")
@WebAppConfiguration
public class SetEncryptionKeyControllerTest {

	
	private MockMvc mockMvc;
	@Autowired
	private ApplicationContext applicationContext;
	
	
	@Before
    public void setup() {
		SetEncryptionKeyController setEncryptionKeyController = (SetEncryptionKeyController) applicationContext.getBean("setEncryptionKeyController");
		this.mockMvc = MockMvcBuilders.standaloneSetup(setEncryptionKeyController).build();
    }

	
	@Test
	public void testShowMainSetEncryptionKeyPage_successfulViewRetrieval() throws Exception {
		mockMvc.perform(get("/administration/encryptionKey/main"))
						.andExpect(status().isOk())
						.andExpect(view().name("/administration/setEncryptionKey.tiles"));
	}
	
	
	@Test
	public void testGetStatus_successful() throws Exception {
		EncryptionKeyStorage encryptionKeyStorage = (EncryptionKeyStorage) applicationContext.getBean("encryptionKeyStorage");
		encryptionKeyStorage.loadEncryptionKey("111", false);
		
		mockMvc.perform(get("/administration/encryptionKey/status"))
						.andExpect(status().isOk())
						.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
						//.andExpect(jsonPath("$.encryptionKey").value(containsString("111")))
						.andExpect(jsonPath("$.fromWebApp").value(eq(false)))
						.andExpect(jsonPath("$.loadDate").exists());
	}
	

	@Test
	public void testSetEncryptionKey_successful() throws Exception {
		mockMvc.perform(post("/administration/encryptionKey/set").content("{\"encryptionKey\":\"222\"}").contentType(MediaType.APPLICATION_JSON))
						.andExpect(status().isOk());
		
		EncryptionKeyStorage encryptionKeyStorage = (EncryptionKeyStorage) applicationContext.getBean("encryptionKeyStorage");
		assertEquals("222", encryptionKeyStorage.getEncryptionKey());
	}
}
