package org.sct.server.extract;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.sct.common.objects.map.HierarchialHashMap;
import org.sct.server.beans.workflow.WorkflowBean;
import org.sct.server.common.exceptions.ExternalServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ximpleware.EOFException;
import com.ximpleware.EncodingException;
import com.ximpleware.EntityException;
import com.ximpleware.NavException;
import com.ximpleware.ParseException;
import com.ximpleware.XPathEvalException;
import com.ximpleware.XPathParseException;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:SpringIntegration-servlet-test.xml")
public class XmlResponseExtractorTest {

	
	@Autowired
	private ApplicationContext applicationContext;
	private XmlResponseExtractor xmlResponseExtractor = null;
	private String responseString =
				"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns=\"http://virginmedia/schema/GetAddressForPostCode/4/0\" xmlns:ns1=\"http://virginmedia/schema/ResponseHeader/2/1\" xmlns:ns2=\"http://virginmedia/schema/faults/2/1\">"+
				"   <soapenv:Header/>"+
				"   <soapenv:Body>"+
				"      <ns:GetAddressForPostCodeResponse>"+
				"         <ns:responseHeader>"+
				"            <ns1:id>?</ns1:id>"+
				"         </ns:responseHeader>"+
				"         <ns:postalAddress>"+
				"            <ns:displayAddress>1_1</ns:displayAddress>"+
				"            <ns:buildingName>1_2</ns:buildingName>"+
				"            <ns:county>1_3</ns:county>"+
				"            <ns:apartment>1_4</ns:apartment>"+
				"            <ns:building>1_5</ns:building>"+
				"            <ns:buildingID>1_6</ns:buildingID>"+
				"            <ns:addressIdentification>"+
				"               <ns:identifierType>1_7</ns:identifierType>"+
				"               <ns:identifierType>1_9</ns:identifierType>"+
				"               <ns:identifierType>1_11</ns:identifierType>"+
				"            </ns:addressIdentification>"+
				"         </ns:postalAddress>"+
				"         <ns:postalAddress>"+
				"            <ns:displayAddress>3_1</ns:displayAddress>"+
				"            <ns:buildingName>3_2</ns:buildingName>"+
				"            <ns:county>3_3</ns:county>"+
				"            <ns:apartment>3_4</ns:apartment>"+
				"            <ns:building>3_5</ns:building>"+
				"            <ns:buildingID>3_6</ns:buildingID>"+
				"            <ns:addressIdentification>"+
				"               <ns:identifierType>3_7</ns:identifierType>"+
				"               <ns:addressIdentifier>3_8</ns:addressIdentifier>"+
				"            </ns:addressIdentification>"+
				"            <ns:addressIdentification>"+
				"               <ns:identifierType>3_9</ns:identifierType>"+
				"               <ns:addressIdentifier>3_10</ns:addressIdentifier>"+
				"            </ns:addressIdentification>"+
				"            <ns:addressIdentification>"+
				"               <ns:identifierType>3_11</ns:identifierType>"+
				"               <ns:addressIdentifier>3_12</ns:addressIdentifier>"+
				"            </ns:addressIdentification>"+
				"         </ns:postalAddress>"+
				"         <ns:postalAddress>"+
				"            <ns:displayAddress>2_1</ns:displayAddress>"+
				"            <ns:buildingName>2_2</ns:buildingName>"+
				"            <ns:county>2_3</ns:county>"+
				"            <ns:apartment>2_4</ns:apartment>"+
				"            <ns:building>2_5</ns:building>"+
				"            <ns:buildingID>2_6</ns:buildingID>"+
				"         </ns:postalAddress>"+
				"      </ns:GetAddressForPostCodeResponse>"+
				"   </soapenv:Body>"+
				"</soapenv:Envelope>";
	private String responseStringWithSOAPFault =
					"<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/1999/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/1999/XMLSchema\">"+
					"   <SOAP-ENV:Body>"+
					"      <SOAP-ENV:Fault>"+
					"         <faultcode xsi:type=\"xsd:string\">SOAP-ENV:Client</faultcode>"+
					"         <faultstring xsi:type=\"xsd:string\">It is a very seroius fault!"+
					"really it is...</faultstring>"+
					"      </SOAP-ENV:Fault>"+
					"   </SOAP-ENV:Body>"+
					"</SOAP-ENV:Envelope>";
	
	
	@Before
	public void setUp() {
		xmlResponseExtractor = (XmlResponseExtractor) applicationContext.getBean("xmlResponseExtractor");
    }
	
	
	@Test
	public void testExtract_successful_simpleString_notSingleValue() throws EncodingException, EOFException, EntityException, NavException, ParseException, XPathParseException, XPathEvalException, ExternalServiceException {
		// preparing the WorkflowBean
		WorkflowBean originalBean = new WorkflowBean(null, null);
		List<String> extracts = new ArrayList<String>();
		//extracts.add("/soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeResponse/ns:postalAddress/ns:addressIdentification/ns:identifierType");
		extracts.add("/soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeResponse/ns:responseHeader/ns1:id");
		originalBean.setExtracts(extracts);
		originalBean.setResponseString(responseString);
		Map<String, String> namespaces = new HashMap<String, String>();
		namespaces.put("ns2", "http://virginmedia/schema/faults/2/1");
		namespaces.put("ns1", "http://virginmedia/schema/ResponseHeader/2/1");
		namespaces.put("soapenv", "http://schemas.xmlsoap.org/soap/envelope/");
		namespaces.put("ns", "http://virginmedia/schema/GetAddressForPostCode/4/0");
		originalBean.setNamespaces(namespaces );
		originalBean.setSingleValue("false");
		
		// calling the method
		WorkflowBean newBean = xmlResponseExtractor.extract(originalBean);
		
		// asserting the values
		HierarchialHashMap<String, Object> xmlMap = (HierarchialHashMap<String, Object>) newBean.getExtractedValues();
		
		HierarchialHashMap<String, Object> mapEnvelope = (HierarchialHashMap<String, Object>)xmlMap.get("soapenv:Envelope");
		assertEquals(1, mapEnvelope.keySet().size());
		assertNotNull(mapEnvelope.get("soapenv:Body"));
		
		HierarchialHashMap<String, Object> mapBody = (HierarchialHashMap<String, Object>)mapEnvelope.get("soapenv:Body");
		assertEquals(1, mapBody.keySet().size());
		assertNotNull(mapBody.get("ns:GetAddressForPostCodeResponse"));

		HierarchialHashMap<String, Object> mapGetAddressForPostCodeResponse = (HierarchialHashMap<String, Object>)mapBody.get("ns:GetAddressForPostCodeResponse");
		assertEquals(1, mapGetAddressForPostCodeResponse.keySet().size());
		assertNotNull(mapGetAddressForPostCodeResponse.get("ns:responseHeader"));

		HierarchialHashMap<String, Object> mapResponseHeader = (HierarchialHashMap<String, Object>)mapGetAddressForPostCodeResponse.get("ns:responseHeader");
		assertEquals(1, mapResponseHeader.keySet().size());
		assertNotNull(mapResponseHeader.get("ns1:id"));

		String id = (String) mapResponseHeader.get("ns1:id");
		
//		String id =
//				(String)
//					((HierarchialHashMap<String, Object>)
//						((HierarchialHashMap<String, Object>)
//							((HierarchialHashMap<String, Object>)
//								((HierarchialHashMap<String, Object>)xmlMap.get("soapenv:Envelope"))
//							.get("soapenv:Body"))
//						.get("ns:GetAddressForPostCodeResponse"))
//					.get("ns:responseHeader"))
//				.get("ns1:id");
		assertEquals("?", id);
	}
	

	@Test
	public void testExtract_successful_simpleString_singleValue() throws EncodingException, EOFException, EntityException, NavException, ParseException, XPathParseException, XPathEvalException, ExternalServiceException {
		// preparing the WorkflowBean
		WorkflowBean originalBean = new WorkflowBean(null, null);
		List<String> extracts = new ArrayList<String>();
		//extracts.add("/soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeResponse/ns:postalAddress/ns:addressIdentification/ns:identifierType");
		extracts.add("/soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeResponse/ns:responseHeader/ns1:id");
		originalBean.setExtracts(extracts);
		originalBean.setResponseString(responseString);
		Map<String, String> namespaces = new HashMap<String, String>();
		namespaces.put("ns2", "http://virginmedia/schema/faults/2/1");
		namespaces.put("ns1", "http://virginmedia/schema/ResponseHeader/2/1");
		namespaces.put("soapenv", "http://schemas.xmlsoap.org/soap/envelope/");
		namespaces.put("ns", "http://virginmedia/schema/GetAddressForPostCode/4/0");
		originalBean.setNamespaces(namespaces );
		originalBean.setSingleValue("true");
		
		// calling the method
		WorkflowBean newBean = xmlResponseExtractor.extract(originalBean);
		
		// asserting the values
		String id = (String) newBean.getExtractedValues();
		assertEquals("?", id);
	}


	@Test
	public void testExtract_successful_lists_notSingleValue() throws EncodingException, EOFException, EntityException, NavException, ParseException, XPathParseException, XPathEvalException, ExternalServiceException {
		// preparing the WorkflowBean
		WorkflowBean originalBean = new WorkflowBean(null, null);
		List<String> extracts = new ArrayList<String>();
		extracts.add("/soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeResponse/ns:postalAddress/ns:addressIdentification/ns:identifierType");
		extracts.add("/soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeResponse/ns:postalAddress/ns:displayAddress");
		originalBean.setExtracts(extracts);
		originalBean.setResponseString(responseString);
		Map<String, String> namespaces = new HashMap<String, String>();
		namespaces.put("ns2", "http://virginmedia/schema/faults/2/1");
		namespaces.put("ns1", "http://virginmedia/schema/ResponseHeader/2/1");
		namespaces.put("soapenv", "http://schemas.xmlsoap.org/soap/envelope/");
		namespaces.put("ns", "http://virginmedia/schema/GetAddressForPostCode/4/0");
		originalBean.setNamespaces(namespaces );
		originalBean.setSingleValue("false");
		
		// calling the method
		WorkflowBean newBean = xmlResponseExtractor.extract(originalBean);
		
		// asserting the values
		HierarchialHashMap<String, Object> xmlMap = (HierarchialHashMap<String, Object>) newBean.getExtractedValues();
		
		HierarchialHashMap<String, Object> mapEnvelope = (HierarchialHashMap<String, Object>)xmlMap.get("soapenv:Envelope");
		assertEquals(1, mapEnvelope.keySet().size());
		assertNotNull(mapEnvelope.get("soapenv:Body"));
		
		HierarchialHashMap<String, Object> mapBody = (HierarchialHashMap<String, Object>)mapEnvelope.get("soapenv:Body");
		assertEquals(1, mapBody.keySet().size());
		assertNotNull(mapBody.get("ns:GetAddressForPostCodeResponse"));

		HierarchialHashMap<String, Object> mapGetAddressForPostCodeResponse = (HierarchialHashMap<String, Object>)mapBody.get("ns:GetAddressForPostCodeResponse");
		assertEquals(1, mapGetAddressForPostCodeResponse.keySet().size());
		assertNotNull(mapGetAddressForPostCodeResponse.get("ns:postalAddress"));

		List<HierarchialHashMap<String, Object>> mapPostalAddress = (List<HierarchialHashMap<String, Object>>)mapGetAddressForPostCodeResponse.get("ns:postalAddress");
//		List<HierarchialHashMap<String, Object>> getAddressForPostCodeResponses =
//					(List<HierarchialHashMap<String, Object>>)
//						((HierarchialHashMap<String, Object>)
//							((HierarchialHashMap<String, Object>)
//								((HierarchialHashMap<String, Object>)xmlMap.get("soapenv:Envelope"))
//							.get("soapenv:Body"))
//						.get("ns:GetAddressForPostCodeResponse"))
//					.get("ns:postalAddress");
		// checking the number of GetAddressForPostCodeResponse
		assertEquals(3, mapPostalAddress.size());
		// checking the ns:displayAddresses
		assertEquals("1_1", (String)mapPostalAddress.get(0).get("ns:displayAddress"));
		assertEquals("3_1", (String)mapPostalAddress.get(1).get("ns:displayAddress"));
		assertEquals("2_1", (String)mapPostalAddress.get(2).get("ns:displayAddress"));
		// checking the identifierTypes in the first postalAddress
		HierarchialHashMap<String, Object> mapAddressIdentification = (HierarchialHashMap<String, Object>)mapPostalAddress.get(0).get("ns:addressIdentification");
		assertEquals(1, mapAddressIdentification.keySet().size());
		assertNotNull(mapAddressIdentification.get("ns:identifierType"));
		List<String> identifierTypes1 = (List<String>) (mapAddressIdentification.get("ns:identifierType"));
		assertEquals(3, identifierTypes1.size());
		assertEquals("1_7", identifierTypes1.get(0));
		assertEquals("1_9", identifierTypes1.get(1));
		assertEquals("1_11", identifierTypes1.get(2));
		// checking the identifierTypes in the second postalAddress
		List<HierarchialHashMap<String, Object>> addressIdentifications = (List<HierarchialHashMap<String, Object>>)mapPostalAddress.get(1).get("ns:addressIdentification");
		assertEquals("3_7", (String)addressIdentifications.get(0).get("ns:identifierType"));
		assertEquals("3_9", (String)addressIdentifications.get(1).get("ns:identifierType"));
		assertEquals("3_11", (String)addressIdentifications.get(2).get("ns:identifierType"));
		// checking the identifierTypes in the third postalAddress
		assertNull(mapPostalAddress.get(2).get("ns:addressIdentification"));
	}


	@Test
	public void testExtract_unsuccessful_noExtracts() throws EncodingException, EOFException, EntityException, NavException, ParseException, XPathParseException, XPathEvalException, ExternalServiceException {
		// preparing the WorkflowBean
		WorkflowBean originalBean = new WorkflowBean(null, null);
		List<String> extracts = new ArrayList<String>();
		originalBean.setExtracts(extracts);
		originalBean.setResponseString(responseString);
		Map<String, String> namespaces = new HashMap<String, String>();
		namespaces.put("ns2", "http://virginmedia/schema/faults/2/1");
		namespaces.put("ns1", "http://virginmedia/schema/ResponseHeader/2/1");
		namespaces.put("soapenv", "http://schemas.xmlsoap.org/soap/envelope/");
		namespaces.put("ns", "http://virginmedia/schema/GetAddressForPostCode/4/0");
		originalBean.setNamespaces(namespaces );
		originalBean.setSingleValue("true");
		
		// calling the method
		try {
			WorkflowBean newBean = xmlResponseExtractor.extract(originalBean);
			fail("IllegalArgumentException should have been thrown!");
		} catch(IllegalArgumentException e) {
			assertEquals("There isn't any extract in the POI!", e.getMessage());
		}
	}


	@Test
	public void testExtract_unsuccessful_SOAPFault() throws EncodingException, EOFException, EntityException, NavException, ParseException, XPathParseException, XPathEvalException, ExternalServiceException {
		// preparing the WorkflowBean
		WorkflowBean originalBean = new WorkflowBean(null, null);
		List<String> extracts = new ArrayList<String>();
		extracts.add("/soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeResponse/ns:postalAddress/ns:addressIdentification/ns:identifierType");		
		originalBean.setExtracts(extracts);
		originalBean.setResponseString(responseStringWithSOAPFault );
		Map<String, String> namespaces = new HashMap<String, String>();
		namespaces.put("ns2", "http://virginmedia/schema/faults/2/1");
		namespaces.put("ns1", "http://virginmedia/schema/ResponseHeader/2/1");
		namespaces.put("soapenv", "http://schemas.xmlsoap.org/soap/envelope/");
		namespaces.put("ns", "http://virginmedia/schema/GetAddressForPostCode/4/0");
		originalBean.setNamespaces(namespaces );
		originalBean.setSingleValue("true");
		
		// calling the method
		try {
			WorkflowBean newBean = xmlResponseExtractor.extract(originalBean);
			fail("ExternalServiceException should have been thrown!");
		} catch(ExternalServiceException e) {
			assertEquals("SOAP Fault occurred! faultcode: SOAP-ENV:Client; faultstring: It is a very seroius fault!really it is...", e.getMessage());
		}
	}


}
