package org.sct.server.cache;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.sct.common.constants.ServiceTypeEnum;
import org.sct.server.beans.workflow.WorkflowBean;
import org.sct.server.common.beans.POIDTO;
import org.sct.server.common.beans.ServiceDTO;
import org.sct.server.common.constants.Constants;
import org.sct.server.common.dao.lowlevel.cache.CacheRuntimeException;
import org.sct.server.common.dao.lowlevel.cache.impl.LocalCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:SpringIntegration-servlet-test.xml")
public class ServiceStorageManagerTest {

	
	@Autowired
	private ApplicationContext applicationContext;
	
	@Autowired
	private LocalCache mockedLocalCache;

	private ServiceStorageManager serviceStorageManager = null;
	
	
	@Before
	public void setUp() {
		serviceStorageManager = (ServiceStorageManager) applicationContext.getBean("serviceStorageManager_forTest");
	}
	
	
	@Test
	public void testGetServiceInfo_successful() {
		// creating service DTO for expectation
		Map<String, String> namespaces = new HashMap<String, String>();
		namespaces.put("ns1", "http://virginmedia/schema/ResponseHeader/2/1");
		List<String> endpoints = new ArrayList<String>();
		endpoints.add("http://localhost/1"); endpoints.add("http://localhost/2"); endpoints.add("http://localhost/3");
		ServiceDTO serviceDTO = new ServiceDTO(555l, "testS", ServiceTypeEnum.WEBSERVICE, "GetAddressForPostCode", "<Env>a</Env>", namespaces, endpoints, null, null, "desc1");
		// creating POI DTO for expectation
		List<String> extracts = new ArrayList<String>();
		extracts.add("/soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeResponse/ns:postalAddress/ns:buildingName");
		extracts.add("/soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeResponse/ns:postalAddress/ns:postcodeSuffix");
		POIDTO poiDTO = new POIDTO(444l, "test", "true", false, serviceDTO, extracts, "desc2");
		// defining the mockito expectations
		when(mockedLocalCache.get( Constants.CACHE_KEY_POI + "test", POIDTO.class)).thenReturn(poiDTO);
		when(mockedLocalCache.get( Constants.CACHE_KEY_SERVICE + "testS", ServiceDTO.class)).thenReturn(serviceDTO);
		
		WorkflowBean bean = new WorkflowBean("test", null);
		
		// call the method
		WorkflowBean populatedBean = serviceStorageManager.getServiceInfo(bean);
		
		// checking the POI name
		assertEquals(bean.getPoiName(), populatedBean.getPoiName());
		// checking the service name
		assertEquals("testS", populatedBean.getServiceName());
		// checking the extracts
		assertEquals(2, populatedBean.getExtracts().size());
		assertEquals("/soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeResponse/ns:postalAddress/ns:postcodeSuffix", populatedBean.getExtracts().get(1));
		// checking the single value
		assertEquals("true", populatedBean.getSingleValue());
		// checking the endpoints
		assertEquals(3, populatedBean.getEndpoints().size());
		assertEquals("http://localhost/3", populatedBean.getEndpoints().get(2));
		// checking the request string template
		assertEquals("<Env>a</Env>", populatedBean.getRequestString());
		// checking the service type
		assertEquals(ServiceTypeEnum.WEBSERVICE, populatedBean.getServiceType());
		// checking the SOAP action
		assertEquals("GetAddressForPostCode", populatedBean.getSoapAction());
		// checking the namespaces
		assertEquals(1, populatedBean.getNamespaces().keySet().size());
		assertEquals("http://virginmedia/schema/ResponseHeader/2/1", populatedBean.getNamespaces().get("ns1"));
		// checking the HTTP method
		assertNull(populatedBean.getHttpMethod());
	}


	@Test
	public void testGetServiceInfo_unsuccessful_POINotFound() {
		WorkflowBean bean = new WorkflowBean("test1", null);
		
		try {
			WorkflowBean populatedBean = serviceStorageManager.getServiceInfo(bean);
			fail("CacheRuntimeException should have been thrown!");
		} catch(CacheRuntimeException e) {
			assertEquals("The POI 'test1' is not in the cache!", e.getMessage());
		}
	}
	

	@Test
	public void testGetServiceInfo_unsuccessful_ServiceNotFound() {
		// creating service DTO for expectation
		ServiceDTO serviceDTO = new ServiceDTO(555l, "testXXX", null, null, null, null, null, null, null, null);
		POIDTO poiDTO = new POIDTO(444l, "test2", "true", false, serviceDTO, null, null);
		// defining the mockito expectations
		when(mockedLocalCache.get( Constants.CACHE_KEY_POI + "test2", POIDTO.class)).thenReturn(poiDTO);
		
		WorkflowBean bean = new WorkflowBean("test2", null);
		
		try {
			WorkflowBean populatedBean = serviceStorageManager.getServiceInfo(bean);
			fail("CacheRuntimeException should have been thrown!");
		} catch(CacheRuntimeException e) {
			assertEquals("The service 'testXXX' is not in the cache!", e.getMessage());
		}
	}
	
	
	@After
	public void clearMocks() {
		Mockito.reset(mockedLocalCache);
	}
	
}
