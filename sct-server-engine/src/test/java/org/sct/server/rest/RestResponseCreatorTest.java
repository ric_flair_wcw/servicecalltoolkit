package org.sct.server.rest;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.sct.server.beans.workflow.WorkflowBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHandlingException;
import org.springframework.messaging.MessageHeaders;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:SpringIntegration-servlet-test.xml")
public class RestResponseCreatorTest {

	
	private RestResponseCreator restResponseCreator = null;
	@Autowired
	private ApplicationContext applicationContext;
	
	
	@Before
	public void setUp() {
		restResponseCreator = (RestResponseCreator)applicationContext.getBean("restResponseCreator");
	}
	
	
	@Test
	public void testCreate_successful_singleValue() throws JsonGenerationException, JsonMappingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, IOException {
		WorkflowBean bean = new WorkflowBean(null, null);
		bean.setSingleValue("true");
		bean.setExtractedValues("Izibaba");
		bean.setEncryptionNeeded(false);
		bean.setContentType("application/xml");
		
		Message<Object> message = restResponseCreator.create(bean);
		
		assertEquals(String.class, message.getPayload().getClass());
		assertEquals("Izibaba", (String)message.getPayload());
	}


	@Test
	public void testCreate_successful_notSingleValue() throws JsonGenerationException, JsonMappingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, IOException {
		Map<String, Object> value = new HashMap<String, Object>();
		value.put("v", "Izibaba");
		WorkflowBean bean = new WorkflowBean(null, null);
		bean.setSingleValue("false");
		bean.setExtractedValues(value);
		bean.setEncryptionNeeded(false);
		bean.setContentType("text/xml");
		
		Message<Object> message = restResponseCreator.create(bean);
		
		assertEquals(HashMap.class, message.getPayload().getClass());
		Map<String, Object> payload = (Map<String, Object>) message.getPayload();
		assertEquals("ok", (String)((Map<String, Object>)payload.get("header")).get("status") );
		assertEquals("Izibaba", (String)((Map<String, Object>)payload.get("body")).get("v") );
	}


	@Test
	public void testError_successful_MessageHandlingException() throws JsonGenerationException, JsonMappingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, IOException {
		MessageHandlingException exception = new MessageHandlingException(null, "description", new IllegalArgumentException("illegal"));
		Map<String, Object> headerMap = new HashMap<String, Object>();
		WorkflowBean workflowBean = new WorkflowBean(null, null);
		workflowBean.setEncryptionNeeded(false);
		headerMap.put("storedWorkflowBean", workflowBean );
		MessageHeaders headers = new MessageHeaders(headerMap);
		workflowBean.setContentType("application/json");
		
		Message<Map<String,Object>> message = restResponseCreator.error(headers, exception);
		
		assertEquals(HashMap.class, message.getPayload().getClass());
		Map<String, Object> payload = (Map<String, Object>) message.getPayload();
		assertEquals("error", (String)((Map<String, Object>)payload.get("header")).get("status") );
		assertEquals("java.lang.IllegalArgumentException", (String)((Map<String, Object>)payload.get("body")).get("class") );
		assertEquals("illegal", (String)((Map<String, Object>)payload.get("body")).get("message") );
	}


	@Test
	public void testError_successful_IllegalArgumentException() throws JsonGenerationException, JsonMappingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, IOException {
		IllegalArgumentException exception = new IllegalArgumentException("illegal");
		
		Map<String, Object> headerMap = new HashMap<String, Object>();
		WorkflowBean workflowBean = new WorkflowBean(null, null);
		workflowBean.setEncryptionNeeded(false);
		headerMap.put("storedWorkflowBean", workflowBean );
		MessageHeaders headers = new MessageHeaders(headerMap);
		workflowBean.setContentType("application/json");
		
		Message<Map<String,Object>> message = restResponseCreator.error(headers, exception);
		
		assertEquals(HashMap.class, message.getPayload().getClass());
		Map<String, Object> payload = (Map<String, Object>) message.getPayload();
		assertEquals("error", (String)((Map<String, Object>)payload.get("header")).get("status") );
		assertEquals("java.lang.IllegalArgumentException", (String)((Map<String, Object>)payload.get("body")).get("class") );
		assertEquals("illegal", (String)((Map<String, Object>)payload.get("body")).get("message") );
	}

}
