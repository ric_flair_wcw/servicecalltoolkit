package org.sct.server.encryption;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:SpringIntegration-servlet-test.xml")
public class MyCipherTest {

	
	@Autowired
	private ApplicationContext applicationContext;
	private MyCipher myCipher;

	
	@Before
	public void setUp() {
		myCipher = (MyCipher) applicationContext.getBean("cipher");
    }

	
	@Test
	public void testDecrypt_noEncryption_successful() throws InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, IOException {
		Map<String, Object> header = new HashMap<String, Object>();
		header.put("encrypted", "false");
		Map<String, Object> body = new HashMap<String, Object>();
		body.put("nr", new Integer(32));
		Map<String, Object> payload = new HashMap<String, Object>();
		payload.put("header", header);
		payload.put("body", body);
		
		Map<String, Object> encryptedPayload = myCipher.decrypt(payload);
		
		assertEquals(Integer.class, ((Map<String, Object>)encryptedPayload.get("body")).get("nr").getClass());
		assertEquals(32, ((Integer)(((Map<String, Object>)encryptedPayload.get("body")).get("nr"))).intValue());
	}
	

	@Test
	public void testDecrypt_encryption_successful() throws InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, IOException {
		Map<String, Object> header = new HashMap<String, Object>();
		header.put("encrypted", "true");
		Map<String, Object> payload = new HashMap<String, Object>();
		payload.put("header", header);
		payload.put("body", "7TbnxVELPO3XVkT4UjDlhwSRzHEbG4yiEv0ewbaxMPCPMBjvKp6Zdx//GweZRrEk");
		
		Map<String, Object> encryptedPayload = myCipher.decrypt(payload);
		
		assertEquals(Integer.class, ((Map<String, Object>)encryptedPayload.get("body")).get("nr").getClass());
		assertEquals(12, ((Integer)(((Map<String, Object>)encryptedPayload.get("body")).get("nr"))).intValue());
	}


	@Test
	public void testDecrypt_encryption_unsuccessful() throws InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, IOException {
		Map<String, Object> header = new HashMap<String, Object>();
		header.put("encrypted", "true");
		Map<String, Object> body = new HashMap<String, Object>();
		body.put("nr", new Integer(32));
		Map<String, Object> payload = new HashMap<String, Object>();
		payload.put("header", header);
		payload.put("body", body);

		try {
			Map<String, Object> encryptedPayload = myCipher.decrypt(payload);
			fail("RuntimeException should have been thrown!");
		} catch(RuntimeException e) {
			assertEquals("Encryption is needed but the body is not a String!", e.getMessage());
		}
	}
}
