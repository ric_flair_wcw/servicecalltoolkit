package org.sct.server.call;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.sct.server.common.exceptions.IllegalConfigurationException;

public class EndpointUrlTest {

	
	@Test
	public void test_successful_pathVariable_one() {
		String endpoint = "http://localhost:8080/management-app-1.0.0-SNAPSHOT/administration/service/${serviceId}";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("serviceId", "almafa");
		
		EndpointUrl eu = new EndpointUrl(endpoint, params);
		
		assertEquals("http://localhost:8080/management-app-1.0.0-SNAPSHOT/administration/service/almafa", eu.getUrlString());
	}


	@Test
	public void test_successful_pathVariable_more() {
		String endpoint = "http://localhost:8080/management-app-1.0.0-SNAPSHOT/customer/${continent}/${country}/${customerName}";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("continent", "Europe");
		params.put("country", "Hungary");
		params.put("customerName", "Izibaba");
		
		EndpointUrl eu = new EndpointUrl(endpoint, params);
		
		assertEquals("http://localhost:8080/management-app-1.0.0-SNAPSHOT/customer/Europe/Hungary/Izibaba", eu.getUrlString());
	}
	
	
	@Test
	public void test_successful_queryVariable_one() {
		String endpoint = "http://localhost:8080/management-app-1.0.0-SNAPSHOT/administration/service?status=${status}&kanya=2";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("status", "active");
		
		EndpointUrl eu = new EndpointUrl(endpoint, params);
		
		assertEquals("http://localhost:8080/management-app-1.0.0-SNAPSHOT/administration/service?status=active&kanya=2", eu.getUrlString());
	}
	

	@Test
	public void test_successful_queryVariable_one_2() {
		String endpoint = "http://localhost:8080/management-app-1.0.0-SNAPSHOT/administration/service?${valami}=active&kanya=2";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("valami", "status");
		
		EndpointUrl eu = new EndpointUrl(endpoint, params);
		
		assertEquals("http://localhost:8080/management-app-1.0.0-SNAPSHOT/administration/service?status=active&kanya=2", eu.getUrlString());
	}

	
	@Test
	public void test_successful_queryVariable_one_3() {
		String endpoint = "http://localhost:8080/management-app-1.0.0-SNAPSHOT/administration/service?${valami}=${status}&kanya=2";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("valami", "status");
		params.put("status", "active");
		
		EndpointUrl eu = new EndpointUrl(endpoint, params);
		
		assertEquals("http://localhost:8080/management-app-1.0.0-SNAPSHOT/administration/service?status=active&kanya=2", eu.getUrlString());
	}

	
	@Test
	public void test_successful_queryVariable_more() {
		String endpoint = "http://localhost:8080/management-app-1.0.0-SNAPSHOT/customer?continent=${continent}&country=${country}&customerName=${customerName}";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("continent", "Europe");
		params.put("country", "Hungary");
		params.put("customerName", "Izibaba");
		
		EndpointUrl eu = new EndpointUrl(endpoint, params);
		
		assertEquals("http://localhost:8080/management-app-1.0.0-SNAPSHOT/customer?continent=Europe&country=Hungary&customerName=Izibaba", eu.getUrlString());
	}


	@Test
	public void test_successful_mixed() {
		String endpoint = "http://localhost:8080/management-app-1.0.0-SNAPSHOT/service/${system}/${serviceId}/customer?continent=${continent}&country=${country}&customerName=${customerName}&kanya=2";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("system", "SAP");
		params.put("serviceId", "438");
		params.put("continent", "Europe");
		params.put("country", "Hungary");
		params.put("customerName", "Izibaba");
		
		EndpointUrl eu = new EndpointUrl(endpoint, params);
		
		assertEquals("http://localhost:8080/management-app-1.0.0-SNAPSHOT/service/SAP/438/customer?continent=Europe&country=Hungary&customerName=Izibaba&kanya=2", eu.getUrlString());
	}


	@Test
	public void test_unsuccessful_queryVariable_emptyParameter() {
		String endpoint = "http://localhost:8080/management-app-1.0.0-SNAPSHOT/administration/service?status=${}&kanya=2";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("status", "active");
		
		try {
			EndpointUrl eu = new EndpointUrl(endpoint, params);
			fail("IllegalConfigurationException shoud have been thrown!");
		} catch(IllegalConfigurationException e) {
			assertEquals("The service URL contains an empty parameter in the query parameter! URL is http://localhost:8080/management-app-1.0.0-SNAPSHOT/administration/service?status=${}&kanya=2", e.getMessage());
		}
	}


	@Test
	public void test_unsuccessful_queryVariable_notInParameters() {
		String endpoint = "http://localhost:8080/management-app-1.0.0-SNAPSHOT/administration/service?status=${state}&kanya=2";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("status", "active");
		
		try {
			EndpointUrl eu = new EndpointUrl(endpoint, params);
			fail("RuntimeException shoud have been thrown!");
		} catch(RuntimeException e) {
			assertEquals("The parameters arrived from the client doesn't contain that parameter that exists in the query parameter of the service URL! The parameter in the service URL is state", e.getMessage());
		}
	}
	
	
	@Test
	public void test_unsuccessful_pathVariable_emptyParameter() {
		String endpoint = "http://localhost:8080/management-app-1.0.0-SNAPSHOT/administration/service/${}";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("serviceId", "almafa");
		
		try {
			EndpointUrl eu = new EndpointUrl(endpoint, params);
		} catch(IllegalConfigurationException e) {
			assertEquals("The service URL contains an empty parameter in the path segment! URL is http://localhost:8080/management-app-1.0.0-SNAPSHOT/administration/service/${}", e.getMessage());
		}
	}


	@Test
	public void test_unsuccessful_pathVariable_notInParameters() {
		String endpoint = "http://localhost:8080/management-app-1.0.0-SNAPSHOT/administration/service/${serviceI}";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("serviceId", "almafa");
		
		try {
			EndpointUrl eu = new EndpointUrl(endpoint, params);
			fail("RuntimeException shoud have been thrown!");
		} catch(RuntimeException e) {
			assertEquals("The parameters arrived from the client doesn't contain that parameter that exists in the path segment of the service URL! The parameter in the service URL is serviceI", e.getMessage());
		}
	}
}
