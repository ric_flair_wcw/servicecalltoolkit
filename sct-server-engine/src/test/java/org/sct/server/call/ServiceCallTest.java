package org.sct.server.call;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.matching;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.http.client.ClientProtocolException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.sct.common.constants.ServiceTypeEnum;
import org.sct.server.beans.workflow.WorkflowBean;
import org.sct.server.common.constants.HttpMethodEnum;
import org.sct.server.common.exceptions.ExternalServiceException;
import org.sct.server.common.exceptions.IllegalConfigurationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.github.tomakehurst.wiremock.junit.WireMockRule;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:SpringIntegration-servlet-test.xml")
public class ServiceCallTest {

	private static final int PORT = 3089;
	@Rule
	public WireMockRule wireMockRule = new WireMockRule(PORT);
	
	@Autowired
	private ApplicationContext applicationContext;
	@Autowired
	private InitialContext mockedInitialContext;
	
	private ServiceCall serviceCall = null;
	
	
	@Before
	public void setUp() {
		serviceCall = (ServiceCall) applicationContext.getBean("serviceCall");
    }
	
	
	@Test
	public void testCall_successful_webservice() throws ClientProtocolException, IOException, ExternalServiceException {
		// setting up the expectations of mocked HTTP server
		String soapAction = "jdhjbnf7rfj";
		String url = "/SAP";
		String requestString = "<soap:Envelope>body</soap:Envelope>";
		String responseString = "<soap:Envelope>ez a valasz</soap:Envelope>";
		stubFor( post(urlEqualTo(url))
				 .withHeader("Accept", equalTo("text/xml"))
				 .withHeader("SOAPAction", equalTo(soapAction))
				 .atPriority(1)
				 .willReturn(aResponse().withStatus(200)
						                .withHeader("Content-Type", "text/xml")
						                .withBody(responseString)));
		// the second stub is for passing back a 400 error if the stubs above don't match (e.g. in unsuccessful tests)
		stubFor( post(urlEqualTo(url))
				 .atPriority(5)
				 .willReturn(aResponse().withStatus(400)));
		
		// calling the method
		WorkflowBean originalBean = new WorkflowBean("SAP->customerName",null);
		List<String> endpoints = new ArrayList<String>();
		endpoints.add("http://localhost:" + PORT + url);
		originalBean.setEndpoints(endpoints);
		originalBean.setRequestString(requestString );
		originalBean.setServiceType(ServiceTypeEnum.WEBSERVICE);
		originalBean.setSoapAction(soapAction);
		originalBean.setServiceName("service1");
		WorkflowBean newBean = serviceCall.call(originalBean);
		
		// verifying the mocked HTTP server calls
		verify(postRequestedFor(urlMatching(url))
	                            .withRequestBody(matching(requestString))
	                            .withHeader("Content-Type", matching("text/plain; charset=UTF-8"))
	                            .withHeader("SOAPAction", matching(soapAction)));
		// checking the bean
		assertEquals(responseString, newBean.getResponseString());
		assertNotNull(newBean.getStartCallExternalServiceTime());
		assertNotNull(newBean.getEndCallExternalServiceTime());
	}


	@Test
	public void testCall_unsuccessful_webservice_no_endpoints() throws ClientProtocolException, IOException, ExternalServiceException {
		// setting up the expectations of mocked HTTP server
		String soapAction = "jdhjbnf7rfj";
		String requestString = "<soap:Envelope>body</soap:Envelope>";
		
		// calling the method
		WorkflowBean originalBean = new WorkflowBean("SAP->customerName",null);
		List<String> endpoints = new ArrayList<String>();
		originalBean.setEndpoints(endpoints);
		originalBean.setServiceName("testS");
		originalBean.setRequestString(requestString );
		originalBean.setServiceType(ServiceTypeEnum.WEBSERVICE);
		originalBean.setSoapAction(soapAction);

		try {
			WorkflowBean newBean = serviceCall.call(originalBean);
			fail("IllegalConfigurationException should have been thrown!");
		} catch(IllegalConfigurationException e) {
			assertEquals("No endpoint is defined for the service testS!", e.getMessage());
		}
	}

	
	@Test
	public void testCall_unsuccessful_webservice_empty_request_string() throws ClientProtocolException, IOException, ExternalServiceException {
		// setting up the expectations of mocked HTTP server
		String soapAction = "jdhjbnf7rfj";
		String url = "/SAP";
		
		// calling the method
		WorkflowBean originalBean = new WorkflowBean("SAP->customerName",null);
		List<String> endpoints = new ArrayList<String>();
		endpoints.add("http://localhost:" + PORT + url);
		originalBean.setEndpoints(endpoints);
		originalBean.setServiceName("testS");
		originalBean.setRequestString(null );
		originalBean.setServiceType(ServiceTypeEnum.WEBSERVICE);
		originalBean.setSoapAction(soapAction);

		try {
			WorkflowBean newBean = serviceCall.call(originalBean);
			fail("RuntimeException should have been thrown!");
		} catch(RuntimeException e) {
			assertEquals("The request string cannot be null! It can be null only at REST GET service.", e.getMessage());
		}
	}

	
	@Test
	public void testCall_unsuccessful_webservice_no_service_available() throws ClientProtocolException, IOException, ExternalServiceException {
		// setting up the expectations of mocked HTTP server
		String poiName = "SAP->customerName";
		String soapAction = "jdhjbnf7rfj";
		String url = "/SAP";
		String url2 = "/SAP2";
		String requestString = "<soap:Envelope>body</soap:Envelope>";
		String responseString = "<soap:Envelope>ez a valasz</soap:Envelope>";
		stubFor( post(urlEqualTo(url))
				 .withHeader("Accept", equalTo("text/xml"))
				 .withHeader("SOAPAction", equalTo(soapAction))
				 .atPriority(1)
				 .willReturn(aResponse().withStatus(200)
						                .withHeader("Content-Type", "text/xml")
						                .withFixedDelay(1000)
						                .withBody(responseString)));
		stubFor( post(urlEqualTo(url2))
				 .withHeader("Accept", equalTo("text/xml"))
				 .withHeader("SOAPAction", equalTo(soapAction))
				 .atPriority(1)
				 .willReturn(aResponse().withStatus(200)
						                .withHeader("Content-Type", "text/xml")
						                .withFixedDelay(1000)
						                .withBody(responseString)));
		// the second stub is for passing back a 400 error if the stubs above don't match (e.g. in unsuccessful tests)
		stubFor( post(urlEqualTo(url))
				 .atPriority(5)
				 .willReturn(aResponse().withStatus(400)));
		
		// calling the method
		WorkflowBean originalBean = new WorkflowBean(poiName ,null);
		List<String> endpoints = new ArrayList<String>();
		endpoints.add("http://localhost:" + PORT + url);
		endpoints.add("http://localhost:" + PORT + url2);
		originalBean.setEndpoints(endpoints);
		originalBean.setRequestString(requestString );
		originalBean.setServiceType(ServiceTypeEnum.WEBSERVICE);
		originalBean.setSoapAction(soapAction);
		originalBean.setServiceName("service1");

		try {
			WorkflowBean newBean = serviceCall.call(originalBean);
			fail("ExternalServiceException should have been thrown!");
		} catch(ExternalServiceException e) {
			assertEquals("No service available for the POI '"+poiName+"'!", e.getMessage());
		}
		
		// verifying the mocked HTTP server calls
		verify(postRequestedFor(urlMatching(url))
	                            .withRequestBody(matching(requestString))
	                            .withHeader("Content-Type", matching("text/plain; charset=UTF-8"))
	                            .withHeader("SOAPAction", matching(soapAction)));
		verify(postRequestedFor(urlMatching(url2))
                                .withRequestBody(matching(requestString))
                                .withHeader("Content-Type", matching("text/plain; charset=UTF-8"))
                                .withHeader("SOAPAction", matching(soapAction)));
	}

	
	@Test
	public void testCall_successful_REST_GET_JSON() throws IOException, ExternalServiceException {
		// setting up the expectations of mocked HTTP server
		String responseString = "\\{ 'customerName' : 'Pule'\\}";
		String customerId = "5454";
		String url = "/SAP/" + customerId;
		
		stubFor( get(urlEqualTo(url))
				 .withHeader("Accept", equalTo("application/json, text/xml, application/xml"))
				 .atPriority(1)
				 .willReturn(aResponse().withStatus(200)
						                .withHeader("Content-Type", "application/json")
						                .withBody(responseString)));
		// the second stub is for passing back a 400 error if the stubs above don't match (e.g. in unsuccessful tests)
		stubFor( get(urlEqualTo(url))
				 .atPriority(5)
				 .willReturn(aResponse().withStatus(400)));
		
		// calling the method
		WorkflowBean originalBean = new WorkflowBean("SAP->customerName",null);
		List<String> endpoints = new ArrayList<String>();
		endpoints.add("http://localhost:" + PORT + url);
		originalBean.setEndpoints(endpoints);
		originalBean.setServiceType(ServiceTypeEnum.REST);
		originalBean.setHttpMethod(HttpMethodEnum.GET);
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("customerId", customerId);
		originalBean.setParams(params );
		originalBean.setServiceName("service1");
		WorkflowBean newBean = serviceCall.call(originalBean);
		
		// verifying the mocked HTTP server calls
		verify(getRequestedFor(urlMatching(url))
	                            .withHeader("Accept", matching("application/json, text/xml, application/xml"))
	                            );
		// checking the bean
		assertEquals(responseString, newBean.getResponseString());
		assertNotNull(newBean.getStartCallExternalServiceTime());
		assertNotNull(newBean.getEndCallExternalServiceTime());
	}


	@Test
	public void testCall_successful_REST_POST_JSON() throws IOException, ExternalServiceException {
		// setting up the expectations of mocked HTTP server
		String url = "/SAP";
		String requestString = "\\{ 'customer' : '4764'\\}";
		String responseString = "\\{ 'customerName' : 'Pule'\\}";
		stubFor( post(urlEqualTo(url))
				 .withHeader("Accept", equalTo("application/json, text/xml, application/xml"))
				 .atPriority(1)
				 .willReturn(aResponse().withStatus(200)
						                .withHeader("Content-Type", "application/json")
						                .withBody(responseString)));
		// the second stub is for passing back a 400 error if the stubs above don't match (e.g. in unsuccessful tests)
		stubFor( post(urlEqualTo(url))
				 .atPriority(5)
				 .willReturn(aResponse().withStatus(400)));
		
		// calling the method
		WorkflowBean originalBean = new WorkflowBean("SAP->customerName",null);
		List<String> endpoints = new ArrayList<String>();
		endpoints.add("http://localhost:" + PORT + url);
		originalBean.setRequestString(requestString);
		originalBean.setEndpoints(endpoints);
		originalBean.setServiceType(ServiceTypeEnum.REST);
		originalBean.setHttpMethod(HttpMethodEnum.POST);
		originalBean.setServiceName("service1");
		WorkflowBean newBean = serviceCall.call(originalBean);
		
		// verifying the mocked HTTP server calls
		verify(postRequestedFor(urlMatching(url))
	                            .withHeader("Accept", matching("application/json, text/xml, application/xml"))
	                            );
		// checking the bean
		assertEquals(responseString, newBean.getResponseString());
		assertNotNull(newBean.getStartCallExternalServiceTime());
		assertNotNull(newBean.getEndCallExternalServiceTime());
	}


	@Test
	public void testCall_unsuccessful_REST_POST_JSON_not_success_statusCode() throws IOException, ExternalServiceException {
		// setting up the expectations of mocked HTTP server
		String url = "/SAP";
		String requestString = "\\{ 'customer' : '4764'\\}";
		String responseString = "\\{ 'customerName' : 'Pule'\\}";
		stubFor( post(urlEqualTo(url))
				 .withHeader("Accept", equalTo("application/json, text/xml, application/xml"))
				 .atPriority(1)
				 .willReturn(aResponse().withStatus(300)
						                .withHeader("Content-Type", "application/json")
						                .withBody(responseString)));
		// the second stub is for passing back a 400 error if the stubs above don't match (e.g. in unsuccessful tests)
		stubFor( post(urlEqualTo(url))
				 .atPriority(5)
				 .willReturn(aResponse().withStatus(400)));
		
		// calling the method
		WorkflowBean originalBean = new WorkflowBean("SAP->customerName",null);
		List<String> endpoints = new ArrayList<String>();
		endpoints.add("http://localhost:" + PORT + url);
		originalBean.setRequestString(requestString);
		originalBean.setEndpoints(endpoints);
		originalBean.setServiceType(ServiceTypeEnum.REST);
		originalBean.setHttpMethod(HttpMethodEnum.POST);
		originalBean.setServiceName("service1");
		
		try {
			WorkflowBean newBean = serviceCall.call(originalBean);
			fail("ExternalServiceException should have been thrown!");
		} catch(ExternalServiceException e) {
			assertEquals("The HTTP status code is not Success (2XX)! Status Code:300, Reason:Multiple Choices", e.getMessage());
		}
	}
	
	
	@Test
	public void testCall_successful_DATABASE() throws IOException, ExternalServiceException, NamingException {
		DataSource datasource = (DataSource) applicationContext.getBean("dataSource");
		
		List<String> endpoints = new ArrayList<String>();
		endpoints.add("jdbc/somt");
		
		// defining the mockito expectations
		when(mockedInitialContext.lookup(endpoints.get(0))).thenReturn(datasource);
		
		// calling the method
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("serviceType", "WEBSERVICE");
		WorkflowBean originalBean = new WorkflowBean("sct_DB->just_a_few", params );
		originalBean.setEndpoints(endpoints);
		originalBean.setServiceType(ServiceTypeEnum.DATABASE);
		originalBean.setDatabaseQuery("SELECT * FROM STAT_SERVICE_POI_DATE_CNT WHERE SERVICE_TYPE LIKE 'WEBSERVICE' ORDER BY POI_ID,PERIOD_START");
		WorkflowBean newBean = serviceCall.call(originalBean);
		
		// verifying the call of mocked object
		Mockito.verify(mockedInitialContext, Mockito.times(1)).lookup(endpoints.get(0));
		
		// checking the result
		assertEquals("{ \"SUCCESSFUL\":[\"java.lang.Boolean\",\"true\",\"true\",\"true\",\"true\",\"true\",\"true\",\"true\",\"true\",\"false\",\"false\",\"true\",\"true\",\"true\"],\"POI_ID\":[\"java.lang.Long\",\"38\",\"38\",\"38\",\"38\",\"38\",\"38\",\"39\",\"39\",\"39\",\"39\",\"41\",\"41\",\"41\"],\"POI_NAME\":[\"java.lang.String\",\"postcode->fullAddress\",\"postcode->fullAddress\",\"postcode->fullAddress\",\"postcode->fullAddress\",\"postcode->fullAddress\",\"postcode->fullAddress\",\"postcode->buildingName\",\"postcode->buildingName\",\"postcode->buildingName\",\"postcode->buildingName\",\"postcode->some_strange_value\",\"postcode->some_strange_value\",\"postcode->some_strange_value\"],\"CNT\":[\"java.lang.Integer\",\"22\",\"1\",\"1\",\"1\",\"25\",\"6\",\"25\",\"25\",\"6\",\"1\",\"25\",\"25\",\"6\"],\"ID\":[\"java.lang.Long\",\"166\",\"168\",\"169\",\"170\",\"171\",\"175\",\"165\",\"173\",\"176\",\"177\",\"167\",\"172\",\"174\"],\"SERVICE_ID\":[\"java.lang.Long\",\"29\",\"29\",\"29\",\"29\",\"29\",\"29\",\"29\",\"29\",\"29\",\"29\",\"34\",\"34\",\"34\"],\"SERVICE_TYPE\":[\"java.lang.String\",\"WEBSERVICE\",\"WEBSERVICE\",\"WEBSERVICE\",\"WEBSERVICE\",\"WEBSERVICE\",\"WEBSERVICE\",\"WEBSERVICE\",\"WEBSERVICE\",\"WEBSERVICE\",\"WEBSERVICE\",\"WEBSERVICE\",\"WEBSERVICE\",\"WEBSERVICE\"],\"SERVICE_NAME\":[\"java.lang.String\",\"GetAddressForPostCode-GetAddressForPostCode_by_id\",\"GetAddressForPostCode-GetAddressForPostCode_by_id\",\"GetAddressForPostCode-GetAddressForPostCode_by_id\",\"GetAddressForPostCode-GetAddressForPostCode_by_id\",\"GetAddressForPostCode-GetAddressForPostCode_by_id\",\"GetAddressForPostCode-GetAddressForPostCode_by_id\",\"GetAddressForPostCode-GetAddressForPostCode_by_id\",\"GetAddressForPostCode-GetAddressForPostCode_by_id\",\"GetAddressForPostCode-GetAddressForPostCode_by_id\",\"GetAddressForPostCode-GetAddressForPostCode_by_id\",\"GetAddressForPostCode-GetAddressForPostCode_by_postcode\",\"GetAddressForPostCode-GetAddressForPostCode_by_postcode\",\"GetAddressForPostCode-GetAddressForPostCode_by_postcode\"],\"PERIOD_START\":[\"java.util.Date\",\"1429018800000\",\"1429018800000\",\"1429018800000\",\"1429018800000\",\"1429020900000\",\"1429021200000\",\"1429018800000\",\"1429020900000\",\"1429021200000\",\"1429021200000\",\"1429018800000\",\"1429020900000\",\"1429021200000\"]}", 
				newBean.getResponseString());
		System.out.println(newBean.getResponseString());
		
		Mockito.reset(mockedInitialContext);
	}
	
	
//	@After
//	public void clearMocks() {
//		Mockito.reset(mockedInitialContext);
//	}
	
}
