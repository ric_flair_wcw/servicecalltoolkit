package org.sct.server.call;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.sct.common.constants.ServiceTypeEnum;
import org.sct.server.beans.workflow.WorkflowBean;
import org.sct.server.common.constants.HttpMethodEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import freemarker.template.TemplateException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:SpringIntegration-servlet-test.xml")
public class ServiceRequestCreatorTest {

	
	@Autowired
	private ApplicationContext applicationContext;

	private ServiceRequestCreator serviceRequestCreator = null;

	private String requestString =
			        "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
					"   <soapenv:Header/>" +
					"   <soapenv:Body>" +
					"      <ns:GetAddressForPostCodeRequest>" +
					"         <ns:requestHeader>" +
					"            <ns1:id>${sctParam.id}</ns1:id>" +
					"         </ns:requestHeader>" +
					"         <ns:postalAddress>" +
					"            <ns:displayAddress>kanya2</ns:displayAddress>" +
					"            <ns:buildingID>4</ns:buildingID>" +
					"            <ns:nonUKPostcode>${sctParam.postcode}</ns:nonUKPostcode>" +
					"            <ns:postcode>${sctParam.postcode}</ns:postcode>" +
					"      </ns:GetAddressForPostCodeRequest>" +
					"   </soapenv:Body>" +
					"</soapenv:Envelope>";
	
	
	@Before
	public void setUp() {
		serviceRequestCreator = (ServiceRequestCreator) applicationContext.getBean("serviceRequestCreator");
    }

	
	@Test
	public void testCreate_successful_REST_GET() throws IOException, TemplateException {
		WorkflowBean originalBean = new WorkflowBean(null, null);
		originalBean.setServiceType(ServiceTypeEnum.REST);
		originalBean.setHttpMethod(HttpMethodEnum.GET);
		List<String> endpoints = new ArrayList<String>();
		endpoints.add("http://localhost:8080/management-app-1.0.0-SNAPSHOT/service/${system}/${serviceId}/customer?continent=${continent}&country=${country}&customerName=${customerName}&kanya=2");
		endpoints.add("http://localhost:8080/management-app-2.0.0-SNAPSHOT/customer/${continent}/${country}/${customerName}");
		originalBean.setEndpoints(endpoints);
		Map<String, Object> params = new HashMap<String, Object>();
		params .put("system", "SAP");
		params.put("serviceId", "438");
		params.put("continent", "Europe");
		params.put("country", "Hungary");
		params.put("customerName", "Izibaba");
		originalBean.setParams(params);
		
		WorkflowBean newBean = serviceRequestCreator.create(originalBean);
		
		if (newBean.getEndpoints().get(0).contains("management-app-2.0.0-SNAPSHOT")) {
			assertEquals("http://localhost:8080/management-app-2.0.0-SNAPSHOT/customer/Europe/Hungary/Izibaba", newBean.getEndpoints().get(0));
			assertEquals("http://localhost:8080/management-app-1.0.0-SNAPSHOT/service/SAP/438/customer?continent=Europe&country=Hungary&customerName=Izibaba&kanya=2", newBean.getEndpoints().get(1));
		} else {
			assertEquals("http://localhost:8080/management-app-2.0.0-SNAPSHOT/customer/Europe/Hungary/Izibaba", newBean.getEndpoints().get(1));
			assertEquals("http://localhost:8080/management-app-1.0.0-SNAPSHOT/service/SAP/438/customer?continent=Europe&country=Hungary&customerName=Izibaba&kanya=2", newBean.getEndpoints().get(0));			
		}
	}



	@Test
	public void testCreate_successful_webservice() throws IOException, TemplateException {
		WorkflowBean originalBean = new WorkflowBean(null, null);
		originalBean.setServiceType(ServiceTypeEnum.WEBSERVICE);
		List<String> endpoints = new ArrayList<String>();
		endpoints.add("http://localhost");
		originalBean.setEndpoints(endpoints);
		originalBean.setRequestString(requestString );
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", "4365");
		params.put("postcode", "BH13EH");
		originalBean.setParams(params);
		
		WorkflowBean newBean = serviceRequestCreator.create(originalBean);
		
		assertEquals(
		        "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
				"   <soapenv:Header/>" +
				"   <soapenv:Body>" +
				"      <ns:GetAddressForPostCodeRequest>" +
				"         <ns:requestHeader>" +
				"            <ns1:id>4365</ns1:id>" +
				"         </ns:requestHeader>" +
				"         <ns:postalAddress>" +
				"            <ns:displayAddress>kanya2</ns:displayAddress>" +
				"            <ns:buildingID>4</ns:buildingID>" +
				"            <ns:nonUKPostcode>BH13EH</ns:nonUKPostcode>" +
				"            <ns:postcode>BH13EH</ns:postcode>" +
				"      </ns:GetAddressForPostCodeRequest>" +
				"   </soapenv:Body>" +
				"</soapenv:Envelope>"
				, newBean.getRequestString());
	}


	@Test
	public void testCreate_unsuccessful_webservice_missing_param() throws IOException, TemplateException {
		WorkflowBean originalBean = new WorkflowBean(null, null);
		originalBean.setServiceType(ServiceTypeEnum.WEBSERVICE);
		List<String> endpoints = new ArrayList<String>();
		endpoints.add("http://localhost");
		originalBean.setEndpoints(endpoints);
		originalBean.setRequestString(requestString );
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", "4365");
		originalBean.setParams(params);
		
		try {
			WorkflowBean newBean = serviceRequestCreator.create(originalBean);
		} catch(IllegalArgumentException e) {
			assertTrue(e.getMessage().startsWith("Exception while creating the request string from template!"));
		}
	}


	@Test
	public void testCreate_unsuccessful_webservice_empty_request_template() throws IOException, TemplateException {
		WorkflowBean originalBean = new WorkflowBean(null, null);
		originalBean.setServiceType(ServiceTypeEnum.WEBSERVICE);
		List<String> endpoints = new ArrayList<String>();
		endpoints.add("http://localhost");
		originalBean.setEndpoints(endpoints);
		originalBean.setRequestString(null);
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", "4365");
		originalBean.setParams(params);
		
		try {
			WorkflowBean newBean = serviceRequestCreator.create(originalBean);
		} catch(IllegalArgumentException e) {
			assertEquals("The request template string cannot be null!", e.getMessage());
		}
	}


	@Test
	public void testCreate_successful_extra_parameter() throws IOException, TemplateException {
		WorkflowBean originalBean = new WorkflowBean(null, null);
		originalBean.setServiceType(ServiceTypeEnum.WEBSERVICE);
		List<String> endpoints = new ArrayList<String>();
		endpoints.add("http://localhost");
		originalBean.setEndpoints(endpoints);
		originalBean.setRequestString(requestString );
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", "4365");
		params.put("postcode", "BH13EH");
		params.put("extra", "sdfjlk");
		originalBean.setParams(params);
		
		WorkflowBean newBean = serviceRequestCreator.create(originalBean);
		
	}

}
