package org.sct.server.log;

import java.util.Calendar;
import java.util.Map;

import org.sct.server.beans.workflow.WorkflowBean;
import org.sct.server.common.persistence.service.IStatServicePoiDateCntEntityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.MessageHeaders;


/**
 * Logging the request (WorkflowBean) to DB.
 * 
 * @author Viktor Horvath
 *
 */
public class LogManager {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(LogManager.class);

	@Autowired
	private IStatServicePoiDateCntEntityService service;
	
	
	public void log(MessageHeaders messageHeaders, Object payload) {
		LOGGER.debug("Logging the request (WorkflowBean) to DB.");

		// getting the WorkflowBean from the header of the message
		WorkflowBean workflowBean =  (WorkflowBean) messageHeaders.get("storedWorkflowBean");
		// setting the departure time
		workflowBean.setReqDepartureTime(Calendar.getInstance());
		// setting the successful flag
		if (payload instanceof Map) {
			// getting the status from the header in the message (see in RestResponseCreator)
			Map<String, Object> payloadMap = (Map<String, Object>)payload;
			String status = ((Map<String, String>)payloadMap.get("header")).get("status");
			// if status = error then set successful to false
			if (status.equals("error")) {
				workflowBean.setSuccessful(false);
			}
		}
		
		LOGGER.trace("The workflowBean to be stored: {}", workflowBean);

		// saving to DB
		service.changeStatistics(workflowBean.getPoiName(), workflowBean.getSuccessful(), workflowBean.getReqArrivalTime());
	}

}