package org.sct.server.rest;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.sct.common.call.JSONPostMan;
import org.sct.common.encryption.EnigmaGadget;
import org.sct.server.beans.workflow.WorkflowBean;
import org.sct.server.encryption.EncryptionKeyStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.support.GenericMessage;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;


/**
 * Creating the REST response for the client
 * 
 * @author Viktor Horvath
 *
 */
public class RestResponseCreator {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(RestResponseCreator.class);
	
	private JSONPostMan postMan;
	private EnigmaGadget enigma;
	private EncryptionKeyStorage storage;
	
	
	// create the REST response if everything is awesome
	public Message<Object> create(WorkflowBean bean) throws JsonGenerationException, JsonMappingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, IOException {
		LOGGER.debug("Creating the REST response for the client.");
		
		Message<Object> message = null;
		Object payload = null;
		
		// if only one value has to be sent back then there is a string in the ExtractedValues
		if (bean.getSingleValue().equalsIgnoreCase("true") || !isJsonOrXml(bean)) {
			payload = bean.getExtractedValues().toString();
		}
		// if only json has to be sent back then there is a Map in the ExtractedValues
		else {
			payload = bean.getExtractedValues();
		}
		
		// encrypting the payload
		payload = enigma.encrypt(payload, bean.getEncryptionNeeded(), storage.getEncryptionKey());
		LOGGER.trace("The encrypted payload is '{}' \nIs the encryption needed? {}", payload, bean.getEncryptionNeeded());

		if (!bean.getSingleValue().equalsIgnoreCase("true")) {
			// creating the letter
			payload = postMan.createLetter(payload, "ok", bean.getEncryptionNeeded(), bean.getServiceType());
			
		}
		// creating the message
		message = new GenericMessage<Object>(payload);
		
		return message;
	}

	
	// create the REST response in case of error
	public Message<Map<String,Object>> error(MessageHeaders messageHeaders, Exception exception) throws JsonGenerationException, JsonMappingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, IOException {
		LOGGER.error("The method 'error' has been called.", exception);
		Map<String, Object> exceptionMap = new HashMap<String, Object>();
		String errorClassName = null;
		String errorMessage = null;
		
		// checking if encryption is needed
		Object workflowBean =  messageHeaders.get("storedWorkflowBean");
		boolean encryptionNeeded = false;
		if (workflowBean != null && workflowBean instanceof WorkflowBean && ((WorkflowBean)workflowBean).getEncryptionNeeded() != null) {
			encryptionNeeded = ((WorkflowBean)workflowBean).getEncryptionNeeded();
		} 
		
		// getting the exception class and message
		if (exception.getClass().getName().equals("org.springframework.messaging.MessageHandlingException")) {
			errorClassName = exception.getCause().getClass().getName();
			errorMessage = exception.getCause().getMessage();
		} else {
			errorClassName = exception.getClass().getName();
			errorMessage = exception.getMessage();
		}
		exceptionMap.put("class", errorClassName);
		exceptionMap.put("message", errorMessage);
		
		Object payload = null;
		try {
			payload = enigma.encrypt(exceptionMap, encryptionNeeded, (encryptionNeeded ? storage.getEncryptionKey() : null));
		} catch(Exception e) {
			payload = new HashMap<String, Object>();
			((Map<String, Object>)payload).put("class", errorClassName);
			((Map<String, Object>)payload).put("message", errorMessage);
			encryptionNeeded= false;
		}
		
		// creating a new message to be retrieved
		Message<Map<String,Object>> newMessage = new GenericMessage<Map<String,Object>>(postMan.createLetter(payload, "error", encryptionNeeded, null));

		return newMessage;
	}


	private boolean isJsonOrXml(WorkflowBean bean) {
		return bean.getContentType().equals("application/json") || bean.getContentType().equals("text/xml") || bean.getContentType().equals("application/xml");
	}


	public JSONPostMan getPostMan() {
		return postMan;
	}

	public void setPostMan(JSONPostMan postMan) {
		this.postMan = postMan;
	}

	public EnigmaGadget getEnigma() {
		return enigma;
	}

	public void setEnigma(EnigmaGadget enigma) {
		this.enigma = enigma;
	}

	public EncryptionKeyStorage getStorage() {
		return storage;
	}

	public void setStorage(EncryptionKeyStorage storage) {
		this.storage = storage;
	}

}