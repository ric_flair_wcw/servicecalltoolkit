package org.sct.server.rest;

import java.util.Map;

import org.sct.common.utils.Utils;
import org.sct.server.beans.workflow.WorkflowBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * The class converts the incoming Message<Map<String, Object>> to WorkflowBean
 * 
 * @author Viktor Horvath
 *
 */
public class ConvertRestMessageToWorkflowBean {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(ConvertRestMessageToWorkflowBean.class);
	
	
	public WorkflowBean convert(Map<String, Object> payload) {
		LOGGER.debug("Converting the incoming Message to WorkflowBean. Message : {}", payload);
		
		// getting the poiName property from the body of the payload
		Map<String, Object> body = (Map<String, Object>) payload.get("body");
		String poiName = (String)body.get("poiName");
		Utils.checkEmpty(poiName, "poiName");
		
		// getting the params property from the payload
		Map<String, Object> params = (Map<String, Object>)body.get("params");
		
		// creating the WorkflowBean
		WorkflowBean bean = new WorkflowBean(poiName, params);
		
		LOGGER.trace("The created WorkflowBean: {}", bean);
		
		
		return bean;
	}
	
	
}
