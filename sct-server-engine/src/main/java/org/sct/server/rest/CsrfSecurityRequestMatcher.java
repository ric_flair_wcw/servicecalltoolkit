package org.sct.server.rest;

import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.web.util.matcher.RequestMatcher;


public class CsrfSecurityRequestMatcher implements RequestMatcher {

	private Pattern allowedMethods = Pattern.compile("^(GET|HEAD|TRACE|OPTIONS)$");
			
	@Override
	public boolean matches(HttpServletRequest request) {
		// CSRF not needed at GET,...
		if(allowedMethods.matcher(request.getMethod()).matches()){
            return false;
        }
		
		if (request.getRequestURI().startsWith("/server-engine/cache/service") ||
				request.getRequestURI().startsWith("/server-engine/cache/poi") ||
				request.getRequestURI().startsWith("/server-engine/service/get")) {
			return false;
		} else {
			return true;
		}
	}

}
