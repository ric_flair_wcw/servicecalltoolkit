package org.sct.server.controller;

import java.util.ArrayList;
import java.util.List;

import org.sct.common.utils.Utils;
import org.sct.server.common.beans.POIDTO;
import org.sct.server.common.beans.ServiceDTO;
import org.sct.server.common.constants.Constants;
import org.sct.server.common.controller.ParentController;
import org.sct.server.common.dao.highlevel.cache.ICacheBeanDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;


@Controller
@RequestMapping("/cache")
public class CacheController extends ParentController {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(CacheController.class);
	
	@Autowired
	private ICacheBeanDAO cacheBeanDAO;
	
	
	@RequestMapping(value = "/service", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public void setService(@RequestBody ServiceDTO serviceDTO) {
		LOGGER.debug("Received POST request to set a service in the cache. {}", serviceDTO);
		
		cacheBeanDAO.put(serviceDTO.getName(), serviceDTO);
	}


	@RequestMapping(value = "/service/{serviceName}", method = RequestMethod.GET)
	public @ResponseBody ServiceDTO getService(@PathVariable("serviceName") String serviceName) {
		LOGGER.debug("Received GET request to Get a service from the cache. serviceName:{}", serviceName);
		
		return cacheBeanDAO.getServiceCacheBean(serviceName);
	}


	@RequestMapping(value = "/service/{serviceName}", method = RequestMethod.DELETE)
	@ResponseStatus(value = HttpStatus.OK)
	public void deleteService(@PathVariable("serviceName") String serviceName) {
		LOGGER.debug("Received DELETE request to delete a service from the cache. serviceName:{}", serviceName);
		
		cacheBeanDAO.deleteServiceCacheBean(serviceName);
	}

	
	@RequestMapping(value = "/service/list", method = RequestMethod.GET)
	public @ResponseBody ServiceDTO[] getAllServices() {
		LOGGER.debug("Received GET request to get all the services from the cache.");
		
		List<ServiceDTO> serviceDTOs = new ArrayList<ServiceDTO>();
		List<String> keys = cacheBeanDAO.getCacheKeys();
		for(String key : keys) {
			if (key.startsWith(Constants.CACHE_KEY_SERVICE)) {
				ServiceDTO serviceDTO = cacheBeanDAO.getServiceCacheBean(Utils.removeStartingChar(key, Constants.CACHE_KEY_SERVICE));
				serviceDTOs.add(serviceDTO);
			}
		}
		
		return serviceDTOs.toArray(new ServiceDTO[0]);
	}

	
	@RequestMapping(value = "/poi", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public void setPOI(@RequestBody POIDTO poiDTO) {
		LOGGER.debug("Received POST request to set a POI in the cache. {}", poiDTO);

		cacheBeanDAO.put(poiDTO.getName(), poiDTO);
	}


	@RequestMapping(value = "/poi/{poiName}", method = RequestMethod.GET)
	public @ResponseBody POIDTO getPOI(@PathVariable("poiName") String poiName) {
		LOGGER.debug("Received GET request to get a POI from the cache. poiName:{}", poiName);
		
		return cacheBeanDAO.getPOICacheBean(poiName);
	}

	
	@RequestMapping(value = "/poi/{poiName}", method = RequestMethod.DELETE)
	@ResponseStatus(value = HttpStatus.OK)
	public void deletePOI(@PathVariable("poiName") String poiName) {
		LOGGER.debug("Received DELETE request to delete a POI from the cache. poiName:{}", poiName);
		
		cacheBeanDAO.deletePOICacheBean(poiName);
	}

	
	
	@RequestMapping(value = "/poi/list", method = RequestMethod.GET)
	public @ResponseBody POIDTO[] getAllPOIs() {
		LOGGER.debug("Received GET request to get all the POIs from the cache.");
		
		List<POIDTO> poiDTOs = new ArrayList<POIDTO>();
		List<String> keys = cacheBeanDAO.getCacheKeys();
		for(String key : keys) {
			if (key.startsWith(Constants.CACHE_KEY_POI)) {
				POIDTO poiDTO = cacheBeanDAO.getPOICacheBean(Utils.removeStartingChar(key, Constants.CACHE_KEY_POI));
				poiDTOs.add(poiDTO);
			}
		}
		
		return poiDTOs.toArray(new POIDTO[0]);
	}

	
}
