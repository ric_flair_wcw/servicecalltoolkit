package org.sct.server.controller;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import org.sct.server.common.controller.ParentController;
import org.sct.server.encryption.EncryptionKeyStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/administration/encryptionKey")
public class SetEncryptionKeyController extends ParentController {


	private static final Logger LOGGER = LoggerFactory.getLogger(SetEncryptionKeyController.class);
	
	@Autowired
	private EncryptionKeyStorage encryptionKeyStorage;
	
			
	@RequestMapping(value = "/main", method = RequestMethod.GET)
	public String showMainSetEncryptionKeyPage() {
		LOGGER.debug("Received GET request to show the set encryption key page.");
		return "/administration/setEncryptionKey.tiles";
	}
	
	
	@RequestMapping(value = "/status", method = RequestMethod.GET)
	public @ResponseBody Map<String,Object> getStatus() {
		LOGGER.debug("Received GET request to get the status.");
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		Map<String,Object> status = new HashMap<String, Object>();
		status.put("loadDate", encryptionKeyStorage.getLoadDate() == null ? "N/A" : dateFormat.format(encryptionKeyStorage.getLoadDate().getTime()));
		status.put("fromWebApp", encryptionKeyStorage.getLoadDate() == null ? "N/A" : encryptionKeyStorage.isFromWebApp());
		
		return status;
	}
	

	@RequestMapping(value = "/set", method = RequestMethod.POST)
	public @ResponseBody Object setEncryptionKey(@RequestBody Map<String, String> map) {
		LOGGER.debug("Received POST request to set the encryption key. {}", map);

		String encryptionKey = map.get("encryptionKey");
		encryptionKeyStorage.loadEncryptionKey(encryptionKey, true);

		return null;
	}
	
}
