package org.sct.server.encryption;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.sct.common.encryption.EnigmaGadget;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class MyCipher {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(MyCipher.class);
			
	private EnigmaGadget enigma;
	private EncryptionKeyStorage storage;
	
	
	public Map<String, Object> decrypt(Map<String, Object> payload) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, IOException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException {
		// checking if the body needs to be decrypted
		Map<String, Object> header = (Map<String, Object>) payload.get("header");
		if (header.get("encrypted").toString().equals("false")) {
			LOGGER.debug("Decrypting is not necessary.");
			return payload;
		} else {
			LOGGER.debug("Decrypting the payload. payload:{}", payload);
			if (!(payload.get("body") instanceof String)) {
				LOGGER.warn("The type of body is {}", payload.get("body").getClass());
				throw new RuntimeException("Encryption is needed but the body is not a String!");
			}
			// using the EnigmaGadget to decrypt
			String encryptedText = payload.get("body").toString();
			Map<String, Object> body = null;
			body = enigma.decrypt(encryptedText, true, storage.getEncryptionKey(), Map.class);
			LOGGER.trace("The decrypted body is {}", body);
			// overwriting the body in the payload with the decrypted map
			payload.put("body", body);
			return payload;
		}
	}

	
	public EnigmaGadget getEnigma() {
		return enigma;
	}

	public void setEnigma(EnigmaGadget enigma) {
		this.enigma = enigma;
	}

	public EncryptionKeyStorage getStorage() {
		return storage;
	}

	public void setStorage(EncryptionKeyStorage storage) {
		this.storage = storage;
	}
}
