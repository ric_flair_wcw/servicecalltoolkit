package org.sct.server.encryption;

import java.util.Calendar;

import org.sct.common.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EncryptionKeyStorage {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(EncryptionKeyStorage.class);

	private String encryptionKey;
	private Calendar loadDate; // the date when the encryption key was loaded (from system variable (when the context xml is loaded) or from the web app)
	private boolean fromWebApp; // loaded from system variable (when the application context XML is loaded) - false, via web app - true
	
	
	public void loadEncryptionKey(String encryptionKey, boolean fromWebApp) {
		Utils.checkEmpty(encryptionKey, "encryptionKey");
		
		this.encryptionKey = encryptionKey;
		this.fromWebApp = fromWebApp;
		this.loadDate = Calendar.getInstance();
	}


	public String getEncryptionKey() {
//		if (Utils.isEmpty(encryptionKey)) {
//			throw new IllegalConfigurationException("The encryption key is not set in the server engine!");
//		}
		return encryptionKey;
	}

	
	public void setEncryptionKey(String encryptionKey) {
		LOGGER.info("Read the following value from system variable: encryptionKey={}",encryptionKey);
		if (!Utils.isEmpty(encryptionKey)) {
			loadEncryptionKey(encryptionKey, false);
		}
	}


	public Calendar getLoadDate() {
		return loadDate;
	}

	public boolean isFromWebApp() {
		return fromWebApp;
	}
	
}
