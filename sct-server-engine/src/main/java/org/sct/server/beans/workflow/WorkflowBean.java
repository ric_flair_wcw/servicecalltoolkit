package org.sct.server.beans.workflow;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.sct.common.constants.ServiceTypeEnum;
import org.sct.server.common.constants.HttpMethodEnum;


/**
 * The bean stores all the information to server the request coming from the client
 * 
 * @author Viktor Horvath
 *
 */
public class WorkflowBean {

	
	// original data - coming from REST request
	private String poiName;
	private Map<String, Object> params;
	private String serviceName;
	
	// service call info
	//		containing the request string; e.g. the SOAP request in case of SOAP ws and so on
	private String requestString;
	//		containing the response string that arrived from the called service
	private String responseString;
	//		containing the content type of the arrived response
	private String contentType;
	//		the endpoint of the service to be called
	private List<String> endpoints;
	//		the type of the service (webservice, rest, ...)
	private ServiceTypeEnum serviceType;
	//      it sets if the value to be sent back is just a String or Map
	private String singleValue;
	//		the response needs to be encrypted?
	private Boolean encryptionNeeded;
	//		the path of the elements that have to be retrieved
	private List<String> extracts; // key - type (e.g. xpath), value - expression (e.g. /customer/nameFull)
	//		the namespaces for xpath
	private Map<String,String> namespaces;
	//		soapAction for calling webservice
	private String soapAction;
	//		for calling the REST service (GET, POST, ...)
	private HttpMethodEnum httpMethod;
	//		the extracted value from the response that will be put to the REST response
	//				Map<String, Object> if json has to be sent back
	//				String if only one values has to be sent back
	private Object extractedValues;
	//		the process of the request was successful? 
	private Boolean successful;
	//      the database query to be executed
	private String databaseQuery;
	
	// dates
	//		arrival of the request to server-engine
	private Calendar reqArrivalTime;
	//		departure of the request from server-engine
	private Calendar reqDepartureTime;
	//		start date of calling the external service
	private Calendar startCallExternalServiceTime;
	//		end date of calling the external service
	private Calendar endCallExternalServiceTime;
	
	
	public WorkflowBean(String poiName, Map<String, Object> params) {
		this.poiName = poiName;
		this.params = params;
		this.reqArrivalTime = Calendar.getInstance();
		this.successful = true;
	}

	public WorkflowBean(String poiName, Map<String, Object> params, String requestString, String responseString, String contentType, List<String> endpoints, ServiceTypeEnum serviceType, 
			List<String> extracts, String soapAction, HttpMethodEnum httpMethod, Object extractedValues, Map<String, String> namespaces, Calendar reqArrivalTime, 
			Calendar reqDepartureTime, Calendar startCallExternalServiceTime, Calendar endCallExternalServiceTime, String singleValue, Boolean encryptionNeeded, 
			String serviceName, String databaseQuery) {
		this.poiName = poiName;
		this.params = params;
		this.requestString = requestString;
		this.responseString = responseString;
		this.contentType = contentType;
		this.endpoints = endpoints;
		this.serviceType = serviceType;
		this.extracts = extracts;
		this.soapAction = soapAction;
		this.httpMethod = httpMethod;
		this.extractedValues = extractedValues;
		this.namespaces = namespaces;
		this.reqArrivalTime = reqArrivalTime;
		this.reqDepartureTime = reqDepartureTime;
		this.startCallExternalServiceTime = startCallExternalServiceTime;
		this.endCallExternalServiceTime = endCallExternalServiceTime;
		this.singleValue = singleValue;
		this.encryptionNeeded = encryptionNeeded;
		this.successful = true;
		this.serviceName = serviceName;
		this.databaseQuery = databaseQuery;
	}


	public String getPoiName() {
		return poiName;
	}

	public void setPoiName(String poiName) {
		this.poiName = poiName;
	}

	public Map<String, Object> getParams() {
		return params;
	}

	public void setParams(Map<String, Object> params) {
		this.params = params;
	}

	public String getRequestString() {
		return requestString;
	}

	public void setRequestString(String requestString) {
		this.requestString = requestString;
	}

	public String getResponseString() {
		return responseString;
	}

	public void setResponseString(String responseString) {
		this.responseString = responseString;
	}

	public ServiceTypeEnum getServiceType() {
		return serviceType;
	}

	public void setServiceType(ServiceTypeEnum serviceType) {
		this.serviceType = serviceType;
	}

	public List<String> getExtracts() {
		return extracts;
	}

	public void setExtracts(List<String> extracts) {
		this.extracts = extracts;
	}

	public String getSoapAction() {
		return soapAction;
	}

	public void setSoapAction(String soapAction) {
		this.soapAction = soapAction;
	}

	public Object getExtractedValues() {
		return extractedValues;
	}

	public void setExtractedValues(Object extractedValues) {
		this.extractedValues = extractedValues;
	}

	public Map<String,String> getNamespaces() {
		return namespaces;
	}

	public void setNamespaces(Map<String,String> namespaces) {
		this.namespaces = namespaces;
	}

	public Calendar getReqDepartureTime() {
		return reqDepartureTime;
	}

	public void setReqDepartureTime(Calendar reqDepartureTime) {
		this.reqDepartureTime = reqDepartureTime;
	}

	public Calendar getStartCallExternalServiceTime() {
		return startCallExternalServiceTime;
	}

	public void setStartCallExternalServiceTime(
			Calendar startCallExternalServiceTime) {
		this.startCallExternalServiceTime = startCallExternalServiceTime;
	}

	public Calendar getEndCallExternalServiceTime() {
		return endCallExternalServiceTime;
	}

	public void setEndCallExternalServiceTime(Calendar endCallExternalServiceTime) {
		this.endCallExternalServiceTime = endCallExternalServiceTime;
	}

	public List<String> getEndpoints() {
		return endpoints;
	}

	public void setEndpoints(List<String> endpoints) {
		this.endpoints = endpoints;
	}

	public HttpMethodEnum getHttpMethod() {
		return httpMethod;
	}

	public void setHttpMethod(HttpMethodEnum httpMethod) {
		this.httpMethod = httpMethod;
	}

	public Calendar getReqArrivalTime() {
		return reqArrivalTime;
	}

	public void setReqArrivalTime(Calendar reqArrivalTime) {
		this.reqArrivalTime = reqArrivalTime;
	}

	public String getSingleValue() {
		return singleValue;
	}

	public void setSingleValue(String singleValue) {
		this.singleValue = singleValue;
	}

	public Boolean getSuccessful() {
		return successful;
	}

	public void setSuccessful(Boolean successful) {
		this.successful = successful;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public Boolean getEncryptionNeeded() {
		return encryptionNeeded;
	}

	public void setEncryptionNeeded(Boolean encryptionNeeded) {
		this.encryptionNeeded = encryptionNeeded;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getDatabaseQuery() {
		return databaseQuery;
	}

	public void setDatabaseQuery(String databaseQuery) {
		this.databaseQuery = databaseQuery;
	}

	@Override
	public String toString() {
		DateFormat dateFormat = new SimpleDateFormat("yyy-MM-dd HH:mm:ss.SSS");
		StringBuilder sb = new StringBuilder();
		sb.append(this.getClass().getName()).append("[");
		sb.append("poiName=").append(poiName);
		sb.append("serviceName=").append(serviceName);
		sb.append(", requestString=").append(requestString);
		sb.append(", responseString=").append(responseString);
		sb.append(", contentType=").append(contentType);
		sb.append(", endpoints=").append(endpoints);
		sb.append(", serviceType=").append(serviceType);
		sb.append(", params=").append(params);
		sb.append(", extracts=").append(extracts);
		sb.append(", soapAction=").append(soapAction);
		sb.append(", httpMethod=").append(httpMethod);
		sb.append(", extractedValues=").append(extractedValues);
		sb.append(", namespaces=").append(namespaces);
		sb.append(", reqArrivalTime=").append( reqArrivalTime == null ? "null" : dateFormat.format(reqArrivalTime.getTime()));
		sb.append(", reqDepartureTime=").append( reqDepartureTime == null ? "null" : dateFormat.format(reqDepartureTime.getTime()));
		sb.append(", startCallExternalServiceTime=").append( startCallExternalServiceTime == null ? "null" : dateFormat.format(startCallExternalServiceTime.getTime()));
		sb.append(", endCallExternalServiceTime=").append( endCallExternalServiceTime == null ? "null" : dateFormat.format(endCallExternalServiceTime.getTime()));
		sb.append(", singleValue=").append(singleValue);
		sb.append(", encryptionNeeded=").append(encryptionNeeded);
		sb.append(", successful=").append(successful);
		sb.append(", databaseQuery=").append(databaseQuery);
		sb.append("]");
		return sb.toString();
	}

	public WorkflowBean clone() {
		return new WorkflowBean(poiName, params, requestString, responseString, contentType, endpoints, serviceType, extracts, soapAction, httpMethod, extractedValues, namespaces, reqArrivalTime, 
				reqDepartureTime, startCallExternalServiceTime, endCallExternalServiceTime, singleValue, encryptionNeeded, serviceName, databaseQuery);
	}

	
}
