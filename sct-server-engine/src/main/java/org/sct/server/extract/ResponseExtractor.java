package org.sct.server.extract;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.sct.common.objects.map.HierarchialHashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 * Parent class for JsonResponseExtractor and XmlResponseExtractor
 * 
 * @author Viktor Horvath
 *
 */
public class ResponseExtractor {

	
	protected static Logger LOGGER = LoggerFactory.getLogger(ResponseExtractor.class);

	protected List<String> extracts = null;
	
	// setting the extracts
	protected void setExtracts(List<String> extractsList) {
		extracts = extractsList;
	}
	
	// getting the value as a single value
	protected Object getSingleValueFromMap(Map<String, Object> xmlMap) {
		if (extracts.size() > 1) {
			throw new RuntimeException("The singleValue is true but there are more than one extracts in the POI!");
		}
		String[] nodes = extracts.get(0).split("/");
		Map<String, Object> map = xmlMap;
		for(String node : nodes) {
			if (node == null || node.length() == 0) {
				continue;
			}
			if (map.keySet().size() == 0) {
				LOGGER.info("xmlMap="+xmlMap+"\nmap="+map+"\nnode="+node+"\npath="+extracts.get(0));
				throw new RuntimeException("The map doesn't contain any key!");
			}
//			if (map.keySet().size() > 1) {
//				LOGGER.info("xmlMap="+xmlMap+"\nmap="+map+"\nnode="+node+"\npath="+extracts.get(0));
//				throw new RuntimeException("The map contains more than one keys!");
//			}
			if (map.get(node) == null) {
				LOGGER.info("xmlMap="+xmlMap+"\nmap="+map+"\nnode="+node+"\npath="+extracts.get(0));
				throw new RuntimeException("Cannot find the node '"+node+"' in the map!");
			}
			if (map.get(node) instanceof Map) {
				map = (Map<String, Object>) map.get(node);
			} else {
				if (map.get(node) instanceof List<?>) {
					LOGGER.info("xmlMap="+xmlMap+"\nmap="+map+"\nnode="+node+"\npath="+extracts.get(0));
					throw new RuntimeException("The POI should be a single value but I found a list instead!");
				} else {
					return map.get(node);
				}
			}
		}
		LOGGER.info("xmlMap="+xmlMap+"\npath="+extracts.get(0));
		throw new RuntimeException("Couldn't find the single value!");
	}


	// removing those nodes that don't have to be retrieved
	protected void removeUnnecessaryNodes(HierarchialHashMap<String, Object> map) {
		Iterator<Entry<String, Object>> iterator = map.entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<String, Object> item = iterator.next();
			String key = item.getKey();
			Object value = item.getValue();
			// if mapAll.get(key) contains
			//   - String or
			//   - empty HashMap
			// then the element is candidate to be removed
			if (     value instanceof String || 
					(value instanceof HashMap && ((HashMap<?, ?>)value).isEmpty()) ) {
				// if the extracts list doesn't contain the key then it has to be removed
				if (!extracts.contains(key)) {
					iterator.remove();
					// removing the empty parent nodes above this node
					removeEmptyParentNodes(map);
				}
			}
			// if the value is a HierarchialHashMap then dig deeper (-> recursive call)
			else if (value instanceof HierarchialHashMap) {
				removeUnnecessaryNodes((HierarchialHashMap<String, Object>)value);
			}
			// if the value is a List then all the elements of the list have to be examined
			else if (value instanceof List) {
				List<Object> tempList = new ArrayList<Object>();
				for(Object o : (List<Object>)value) {
					// if a HierarchialHashMap then dig deeper
					if (o instanceof HierarchialHashMap) {
						removeUnnecessaryNodes((HierarchialHashMap<String, Object>)o);
					}
					// if String or empty HashMap then the element is candidate to be removed
					if (     o instanceof String || 
							(o instanceof HashMap && ((HashMap<?, ?>)o).isEmpty()) ) {
						// if the extracts list doesn't contain the key then it has to be removed
						if (!extracts.contains(key)) {
							tempList.add(o);
						}
					}
				}
				((List<Object>)value).removeAll(tempList);
				// if the list has one value then we don't need list...
				if (((List<Object>)value).size() == 1) {
					map.remove(key);
					// not putting back the empty Map (it was already removed in removeEmptyParentNodes but because of the recursive calls it exists here...)
					Object o = ((List<Object>)value).get(0);
					if ( o instanceof Map &&  ((Map<String, Object>)o).isEmpty() ) {
					} else {
						map.put(key, o);
					}
				}
				// if the list is empty then it has to be removed from the map
				if (((List<Object>)value).size() == 0) {
					// removing the empty parent nodes above this node
					map.remove(key);
					removeEmptyParentNodes(map);
				}
			} else {
				throw new RuntimeException("Well I really don't know what this element is... -> " + value.getClass() + " : " + value.toString());
			}
		}
	}


	private void removeEmptyParentNodes(final HierarchialHashMap<String, Object> map) {
		HierarchialHashMap<String, Object> parentMap = map;
		while(parentMap!= null && parentMap.isEmpty()) {
			String keyToBeRemoved = parentMap.getCurrentXpath();
			parentMap = parentMap.getParent();
			if (parentMap != null) {
				if (parentMap.get(keyToBeRemoved) instanceof List) {
					List<Object> list = (List<Object>)parentMap.get(keyToBeRemoved);
					// if there is only one element in the list then the whole key can be deleted
					if (list.size() == 1) {
						parentMap.remove(keyToBeRemoved);
					} else {
						for(Object o : list) {
							if (o instanceof Map) {
								if (((Map)o).isEmpty()) {
									list.remove(o);
								}
							}
						}
						// if all the elements of the list were removed then the key can be removed from the map
						if (list.size() == 0) {
							parentMap.remove(keyToBeRemoved);
						}
					}
				} else {
					parentMap.remove(keyToBeRemoved);
				}
			}
		}
	}
	

	// the xmlMap contains full path (e.g. /soapenv:Header/soapenv:Body/ns:address) as keys but we need only the last station in the path (e.g. ns:address)
	protected void makeKeysSimple(HierarchialHashMap<String, Object> xmlMap) {
		Iterator<Entry<String, Object>> iterator = xmlMap.entrySet().iterator();
		Map<String, Object> temp = new HashMap<String, Object>();
		while (iterator.hasNext()) {
			Entry<String, Object> item = iterator.next();
			String key = item.getKey();
			Object value = item.getValue();
			temp.put(key.substring(key.lastIndexOf("/")+1), value);
			iterator.remove();
			
			if (value instanceof HierarchialHashMap) {
				makeKeysSimple((HierarchialHashMap<String, Object>)value);
			} else if (value instanceof List) {
				List<Object> list = (List<Object>)value;
				for(Object o : list) {
					if (o instanceof HierarchialHashMap) {
						makeKeysSimple((HierarchialHashMap<String, Object>)o);
					}
				}
			}
		}
		xmlMap.putAll(temp);
	}

	
}
