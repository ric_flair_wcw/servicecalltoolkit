package org.sct.server.extract;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import org.sct.common.objects.map.HierarchialHashMap;
import org.sct.server.beans.workflow.WorkflowBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


/**
 * Extracting the values to be retrieved from a JSON response
 * 
 * @author Viktor Horvath
 *
 */
public class JsonResponseExtractor extends ResponseExtractor {


	private static final Logger LOGGER = LoggerFactory.getLogger(JsonResponseExtractor.class);

	private ObjectMapper objectMapper;
	

	public WorkflowBean extract(WorkflowBean _bean) throws JsonParseException, JsonMappingException, IOException {
		LOGGER.debug("Extracting the values to be retrieved from a JSON response.");
		WorkflowBean bean = _bean.clone();

		// converting the extracts to a single List<String>
		setExtracts(bean.getExtracts());
		
		// marshalling the response JSON string to Map
		//    the response JSON can be a Map represantation or it can be an array too; if it is an array that the rootNode cannot be a Map<String, Object> but a List<?> => I'd get a JsonMappingException => the solution is to wrap the bean.getResponseString() into a parent element
		String jsonString = "{\"p\":" + bean.getResponseString() + "}";
		Map<String, Object> content = objectMapper.readValue(jsonString, new TypeReference<Map<String,?>>(){});
		Object rootNode = content.get("p");
		if (rootNode == null || 
				(rootNode instanceof Map && ((Map)rootNode).keySet().size() == 0) ||
				(rootNode instanceof List && ((List)rootNode).size() == 0)) {
			throw new RuntimeException("Well, the marshalled Map is empty.");
		}
		LOGGER.trace("Map representation of the response JSON: {}", rootNode);

		// the Map has to be loaded into HierarchialHashMap
		Object jsonMap = null;
		if (rootNode instanceof Map) {
			jsonMap = loadIntoHierarchialHashMap("", null, (Map<String, Object>)rootNode);
		} else {
			jsonMap = new ArrayList<HierarchialHashMap<String, Object>>();
			for(Map<String,Object> oneOfLeaves : ((List<Map<String,Object>>)rootNode)) {
				((List)jsonMap).add(loadIntoHierarchialHashMap("", null, oneOfLeaves));
			}
		}
		LOGGER.trace("After being loaded into HierarchialHashMap: {}", jsonMap);
		
		// only keeping the elements in the map that are needed to retrieve
		//      if the first element is a list then a loop has to be used (I don't want to modify the removeUnnecessaryNodes method as it is used by XML extractor too...)
		if (jsonMap instanceof List) {
			for(HierarchialHashMap<String, Object> node : (List<HierarchialHashMap<String, Object>>)jsonMap) {
				removeUnnecessaryNodes(node);
			}
		} else {
			removeUnnecessaryNodes((HierarchialHashMap<String, Object>)jsonMap);
		}
		LOGGER.trace("The map after removing the unnecessary nodes: {}", jsonMap);

		// using simple keys in the map (not the whole jsonpath)
		if (jsonMap instanceof List) {
			for(HierarchialHashMap<String, Object> node : (List<HierarchialHashMap<String, Object>>)jsonMap) {
				makeKeysSimple(node);
			}
		} else {
			makeKeysSimple((HierarchialHashMap<String, Object>)jsonMap);
		}
		LOGGER.trace("The map after making the keys simple: {}", jsonMap);
		
		// using the singleValue in POI
		if (bean.getSingleValue().equalsIgnoreCase("true")) {
			if (jsonMap instanceof List) {
				throw new RuntimeException("The POI should be a single value but the JSON is an array!");
			}
			Object o = getSingleValueFromMap((HierarchialHashMap<String, Object>)jsonMap);
			bean.setExtractedValues(o);
		} else {		
			bean.setExtractedValues(jsonMap);
		}
		
		return bean;
	}

	
	private HierarchialHashMap<String, Object> loadIntoHierarchialHashMap(String currentPath, HierarchialHashMap<String, Object> parent, Map<String, Object> map) {
		HierarchialHashMap<String,Object> hierarchialHashMap = new HierarchialHashMap<String, Object>(parent, currentPath);
		
		for(String key : map.keySet()) {
			Object o = map.get(key);
			if (o instanceof Map) {
				o = loadIntoHierarchialHashMap(currentPath+"/"+key, hierarchialHashMap, (Map)o);
			} else if (o instanceof List<?>) {
				List<Object> list = new CopyOnWriteArrayList<Object>();
				for(Object subO : (List<?>)o) {
					if (subO instanceof Map) {
						list.add(loadIntoHierarchialHashMap(currentPath+"/"+key, hierarchialHashMap, (Map)subO));
					} else {
						list.add(subO);
					}
				}
				o = list;
			}
			hierarchialHashMap.put(currentPath+"/"+key, o);	
		}
		
		return hierarchialHashMap;
	}


	public ObjectMapper getObjectMapper() {
		return objectMapper;
	}

	public void setObjectMapper(ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
	}
	
}