package org.sct.server.extract;

import java.util.Map;

import org.sct.common.objects.map.HierarchialHashMap;
import org.sct.common.utils.MapUtils;
import org.sct.server.beans.workflow.WorkflowBean;
import org.sct.server.common.exceptions.ExternalServiceException;
import org.sct.server.common.utils.IXmlToMapConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ximpleware.EOFException;
import com.ximpleware.EncodingException;
import com.ximpleware.EntityException;
import com.ximpleware.NavException;
import com.ximpleware.ParseException;
import com.ximpleware.XPathEvalException;
import com.ximpleware.XPathParseException;


/**
 * Extracting the values to be retrieved from an XML response
 * 
 * @author Viktor Horvath
 *
 */
public class XmlResponseExtractor extends ResponseExtractor {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(XmlResponseExtractor.class);
	
	private IXmlToMapConverter xmlToMapConverter;
	
	
	public WorkflowBean extract(WorkflowBean _bean) throws EncodingException, EOFException, EntityException, NavException, ParseException, XPathParseException, XPathEvalException, ExternalServiceException {
		LOGGER.debug("Extracting the values to be retrieved from an XML response.");
		WorkflowBean bean = _bean.clone();
		
		if (bean.getExtracts().size() == 0) {
			throw new IllegalArgumentException("There isn't any extract in the POI!");
		}
		
		// converting the extracts to a single List<String>
		setExtracts(bean.getExtracts());
		
		// converting the XML to Map
		xmlToMapConverter.start(bean.getResponseString(), bean.getNamespaces());
		HierarchialHashMap<String, Object> xmlMap = xmlToMapConverter.getMap();
		LOGGER.trace("Map representation of the response XML: {}", xmlMap);

		// checking if the response contains SOAP Fault
		checkingSOAPFault(xmlMap, "Envelope");
		
		// only keeping the elements in the map that are needed to be retrieved
		removeUnnecessaryNodes(xmlMap);
		LOGGER.trace("The map after removing the unnecessary nodes: {}", xmlMap);
		
		// using simple keys in the map (not the whole xpath)
		makeKeysSimple(xmlMap);
		LOGGER.trace("The map after making the keys simple: {}", xmlMap);
		
		// using the singleValue in POI
		if (bean.getSingleValue().equalsIgnoreCase("true")) {
			Object o = getSingleValueFromMap(xmlMap);
			bean.setExtractedValues(o);
		} else {		
			bean.setExtractedValues(xmlMap);
		}
		
		return bean;
	}

	
	private void checkingSOAPFault(HierarchialHashMap<String, Object> xmlMap, String nodeName) throws ExternalServiceException {
		Map<String, Object> faultMap = MapUtils.goToWithoutNamespace(xmlMap, "/Envelope/Body/Fault", Map.class);
		if (faultMap != null) {
			String faultcode = (String) MapUtils.goToWithoutNamespace(faultMap, "faultcode", String.class);
			String faultstring = (String) MapUtils.goToWithoutNamespace(faultMap, "faultstring", String.class);
			throw new ExternalServiceException("SOAP Fault occurred! faultcode: " + faultcode + "; faultstring: " + faultstring);
		}
	}


	public IXmlToMapConverter getXmlToMapConverter() {
		return xmlToMapConverter;
	}

	public void setXmlToMapConverter(IXmlToMapConverter xmlToMapConverter) {
		this.xmlToMapConverter = xmlToMapConverter;
	}


}