package org.sct.server.cache;

import org.sct.common.constants.ServiceTypeEnum;
import org.sct.server.beans.workflow.WorkflowBean;
import org.sct.server.common.beans.POIDTO;
import org.sct.server.common.beans.ServiceDTO;
import org.sct.server.common.dao.highlevel.cache.ICacheBeanDAO;
import org.sct.server.common.dao.lowlevel.cache.CacheRuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * The bean is for getting the service/POI details from the cache
 * 
 * @author Viktor Horvath
 *
 */
public class ServiceStorageManager {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(ServiceStorageManager.class);
	
	private ICacheBeanDAO cacheBeanDAO;
	
	
	public ServiceStorageManager() {}
	
	
	public WorkflowBean getServiceInfo(final WorkflowBean _bean) {
		LOGGER.debug("Getting the service details from the cache.");
		WorkflowBean bean = _bean.clone();

		// getting the extracts info from the POI in the cache 
		POIDTO poiDTO = cacheBeanDAO.getPOICacheBean(bean.getPoiName());
		if (poiDTO == null) {
			throw new CacheRuntimeException("The POI '" + bean.getPoiName() + "' is not in the cache!");
		}
		bean.setExtracts(poiDTO.getExtracts());
		bean.setSingleValue(poiDTO.getSingleValue());
		bean.setEncryptionNeeded(poiDTO.getEncryptionNeeded());

		// getting the service cache bean from the cache
		ServiceDTO serviceCacheBean = cacheBeanDAO.getServiceCacheBean(poiDTO.getServiceDTO().getName());
		if (serviceCacheBean == null) {
			throw new CacheRuntimeException("The service '" + poiDTO.getServiceDTO().getName() + "' is not in the cache!");
		}

		// filling the endpoints, request string and service type
		bean.setServiceName(serviceCacheBean.getName());
		bean.setEndpoints(serviceCacheBean.getEndpoints());
		bean.setRequestString(serviceCacheBean.getRequestTemplateText());
		bean.setServiceType(serviceCacheBean.getServiceType());
		
		// filling the service type dependent properties
		if (serviceCacheBean.getServiceType().equals(ServiceTypeEnum.WEBSERVICE)) {
			bean.setSoapAction(serviceCacheBean.getSoapAction());
			bean.setNamespaces(serviceCacheBean.getNamespaces());
		} else if (serviceCacheBean.getServiceType().equals(ServiceTypeEnum.REST)) {
			bean.setHttpMethod(serviceCacheBean.getHttpMethod());
		} else if (serviceCacheBean.getServiceType().equals(ServiceTypeEnum.DATABASE)) {
			bean.setDatabaseQuery(serviceCacheBean.getDatabaseQuery());
		} else {
			throw new RuntimeException("The server engine supports only webservice, REST call and DB query at the moment! The service type of the actual call is " + serviceCacheBean.getServiceType().getLabel());
		}
		
		LOGGER.trace("The WorkflowBean being filled is {}", bean);
		
		return bean;
	}

	
	public ICacheBeanDAO getCacheBeanDAO() {
		return cacheBeanDAO;
	}

	public void setCacheBeanDAO(ICacheBeanDAO cacheBeanDAO) {
		this.cacheBeanDAO = cacheBeanDAO;
	}


}