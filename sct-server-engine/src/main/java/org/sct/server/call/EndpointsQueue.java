package org.sct.server.call;

import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;


public class EndpointsQueue {

	
	private Queue<String> endpointsQueue = new ConcurrentLinkedQueue<String>();
	
	
	public String next() {
		String endpoint = endpointsQueue.poll();
		return endpoint;
	}


	public void initQueue(List<String> endpoints) {
		endpointsQueue.clear();
		endpointsQueue.addAll(endpoints);		
	}
	
	
}
