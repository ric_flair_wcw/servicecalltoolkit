package org.sct.server.call;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class ServiceEndpointsStorage {

	
	private Map<String, Queue<String>> serviceEndpointMap = new HashMap<String, Queue<String>>();
	private Map<String, Queue<String>> sessionQueues = new HashMap<String, Queue<String>>();
	
	
	public synchronized void init(String serviceName, List<String> endpoints) {
		// check if the parameter endpoint list is different from the one in serviceEndpointMap
		if (serviceEndpointMap.get(serviceName) == null || 
				(!(endpoints.containsAll(serviceEndpointMap.get(serviceName)) && serviceEndpointMap.get(serviceName).containsAll(endpoints)))) {
			// if it is then overwrite it
			Queue<String> q = new ConcurrentLinkedQueue<String>(endpoints);
			serviceEndpointMap.put(serviceName, q);
		}
	}

	
	/**
	 * Removing the call-related queue from sessionQueues in order to avoid memory leak.
	 *
	 */
	public void remove(String uuid) {
		sessionQueues.remove(uuid);
	}
	
	
	/**
	 * Getting the next endpoint in the queue
	 *
	 * @return The next endpoint in the queue, null if the queue is empty.
	 */
	public synchronized String next(String serviceName, String uuid) {
		String endpoint = null;
		// if it is first call in a service call then the endpointsQueue has to be added to sessionQueues with the unique id
		// before adding I bounce the queue in order not to call always the first endpoint first
		if (sessionQueues.get(uuid) == null) {
			Queue<String> endpointsQueue = serviceEndpointMap.get(serviceName);
			endpoint = endpointsQueue.poll();
			endpointsQueue.add(endpoint);
			sessionQueues.put(uuid, new ConcurrentLinkedQueue<String>(endpointsQueue));
		}
		// get the next endpoint from the session queue
		endpoint = sessionQueues.get(uuid).poll();
		
		return endpoint;
	}
	
}

