package org.sct.server.call;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.sct.server.common.exceptions.IllegalConfigurationException;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

public class EndpointUrl {

	private UriComponents uc = null;

	
	public EndpointUrl(String endpoint, Map<String, Object> params) {
		UriComponents tempUc = UriComponentsBuilder.fromUriString(endpoint).build();
		
		List<String> pathSegmentList = new ArrayList<String>();
		List<String> queryList = new ArrayList<String>();
		
		// replacing the path variable in the url with the specific param value
		// e.g. /management-app-1.0.0-SNAPSHOT/administration/service/${serviceId}  ->  /management-app-1.0.0-SNAPSHOT/administration/service/SAP
		String[] pathArray = tempUc.getPath().split("/");
		// regular expression pattern: starting with '${', any character, ending with '}'
		Pattern pattern = Pattern.compile("^[\\$]{1}[\\{]{1}.*[\\}]$");
		for(int i = 0; i < pathArray.length; i++) {
			Matcher matcher = pattern.matcher(pathArray[i]);
			// if the patch segment is something to be replaced with the param, i.e. ${serviceId}
			if (matcher.find()) {
				// getting the parameter name from the path segment
				String paramInPathSegment = pathArray[i].substring(2, pathArray[i].length()-1);
				String value = getParameterValue(paramInPathSegment, endpoint, params, "path segment");
//				if (paramInPathSegment.length() == 0) {
//					throw new IllegalConfigurationException("The service URL contains an empty parameter in the path segment! URL is "+endpoint);
//				}
//				// getting the param value
//				Object value = params.get(paramInPathSegment);
//				if (value == null) {
//					throw new RuntimeException("The parameters arrived from the client doesn't contain that parameter that exists in the path segment of the service URL! The parameter in the service URL is "+paramInPathSegment);
//				}
				pathSegmentList.add(value);
			} else {
				pathSegmentList.add(pathArray[i]);
			}
		}
		
		if (tempUc.getQuery() != null) {
			// replacing the query parameter in the url with the specific param value
			// e.g. /management-app-1.0.0-SNAPSHOT/administration/service?status=${serviceStatus}  ->  /management-app-1.0.0-SNAPSHOT/administration/service?status=active
			String[] queryArray = tempUc.getQuery().split("&");
			// regular expression pattern: starting with {, any character, ending with }
			Pattern patternSecond = Pattern.compile("^[^\\=]+[\\=][\\$]{1}[\\{]{1}[^\\}]*[\\}]$");
			Pattern patternFirst = Pattern.compile("^[\\$]{1}[\\{]{1}[^\\}]*[\\}][\\=][^\\=]+$");
			Pattern patternBoth = Pattern.compile("^[\\$]{1}[\\{]{1}[^\\}]*[\\}][\\=][\\$]{1}[\\{]{1}[^\\}]*[\\}]$");
			for(int i = 0; i < queryArray.length; i++) {
//				Matcher matcher = pattern.matcher(queryArray[i]);
				// if the patch segment is something to be replaced with the param, i.e. ${serviceStatus}
				//     .../service?${paramName}=${serviceStatus}
				if (patternBoth.matcher(queryArray[i]).find()) {
					String[] oneQuery = queryArray[i].split("=");
					String paramInQuery_0 = oneQuery[0].substring(2, oneQuery[0].length()-1);
					String value_0 = getParameterValue(paramInQuery_0, endpoint, params, "query parameter");
					String paramInQuery_1 = oneQuery[1].substring(2, oneQuery[1].length()-1);
					String value_1 = getParameterValue(paramInQuery_1, endpoint, params, "query parameter");
					queryList.add(value_0 + "=" + value_1);
				} 
				//     .../service?status=${serviceStatus}				
				else if (patternSecond.matcher(queryArray[i]).find()) {
					String[] oneQuery = queryArray[i].split("=");
					String paramInQuery = oneQuery[1].substring(2, oneQuery[1].length()-1);
					String value = getParameterValue(paramInQuery, endpoint, params, "query parameter");
//					if (paramInQuery.length() == 0) {
//						throw new IllegalConfigurationException("The service URL contains an empty parameter in the query parameters! URL is "+endpoint);
//					}
//					// getting the param value
//					Object value = params.get(paramInQuery);
//					if (value == null) {
//						throw new RuntimeException("The parameters arrived from the client doesn't contain that parameter that exists in the query parameter of service URL! The query parameter in the service URL is "+paramInQuery);
//					}
					queryList.add(oneQuery[0] + "=" + value);
				} 
				//     .../service?${paramName}=active								
				else if (patternFirst.matcher(queryArray[i]).find()) {
					String[] oneQuery = queryArray[i].split("=");
					String paramInQuery = oneQuery[0].substring(2, oneQuery[0].length()-1);
					String value = getParameterValue(paramInQuery, endpoint, params, "query parameter");
					queryList.add(value + "=" + oneQuery[1]);
				} else {
					queryList.add(queryArray[i]);
				}
			}
		}
		
		// building the UriComponents
		UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.newInstance()
                                 .scheme(tempUc.getScheme())
                                 .host(tempUc.getHost())
                                 .port(tempUc.getPort())
                                 .pathSegment(pathSegmentList.toArray(new String[0]));
		for(String query : queryList) {
			uriComponentsBuilder.query(query);
		}
		uc = uriComponentsBuilder.build().encode();
	}


	private String getParameterValue(String paramName, String endpoint, Map<String, Object> params, String exceptionText) {
		if (paramName.length() == 0) {
			throw new IllegalConfigurationException("The service URL contains an empty parameter in the "+exceptionText+"! URL is "+endpoint);
		}
		// getting the param value
		Object value = params.get(paramName);
		if (value == null) {
			throw new RuntimeException("The parameters arrived from the client doesn't contain that parameter that exists in the "+exceptionText+" of the service URL! The parameter in the service URL is "+paramName);
		}
		return value.toString();
	}
	
	
	public String getUrlString() {
		return uc.toUriString();
	}

	
}
