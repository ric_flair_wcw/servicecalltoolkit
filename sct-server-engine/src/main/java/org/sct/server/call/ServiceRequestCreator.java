package org.sct.server.call;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.sct.common.constants.ServiceTypeEnum;
import org.sct.server.beans.workflow.WorkflowBean;
import org.sct.server.common.constants.HttpMethodEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;


/**
 * Creating the request to call the external service
 * 
 * @author Viktor Horvath
 *
 */
public class ServiceRequestCreator {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(ServiceRequestCreator.class);
	
	private Configuration freemarkerConfiguration = null;
	private StringTemplateLoader stringTemplateLoader = null;
	
	
	public WorkflowBean create(WorkflowBean _bean) throws IOException {
		LOGGER.debug("Creating the request to call the external service.");
		
		WorkflowBean bean = _bean.clone();
		
		// if the service is REST and the http method is GET then request is not needed as the endpoint URL will contain all the variables 
		if (bean.getServiceType().equals(ServiceTypeEnum.REST) && bean.getHttpMethod().equals(HttpMethodEnum.GET)) {
			// the endpoint URL(s) contains the path variable name(s) and not the value(s); e.g.: http://.../BLAHBLAGservice/customer/{customerId}?status={customerStatus}
			// the name has to be replaced with value
			List<String> finalEndpointList = new ArrayList<String>();
			for(String endpoint : bean.getEndpoints()) {
				EndpointUrl endpointUrl = new EndpointUrl(endpoint, bean.getParams());
				finalEndpointList.add(endpointUrl.getUrlString());
			}
			bean.setEndpoints(finalEndpointList);
			
		} else {
			// adding the request string or database query to a class (StringTemplateLoader) that will be used as an input for freemarkerConfiguration
			if (bean.getServiceType().equals(ServiceTypeEnum.DATABASE)) {
				if (bean.getDatabaseQuery() == null) {
					throw new IllegalArgumentException("The database query string cannot be null!");
				}
				stringTemplateLoader.putTemplate("requestString", bean.getDatabaseQuery());
				
			} else {
				if (bean.getRequestString() == null) {
					throw new IllegalArgumentException("The request template string cannot be null!");
				}
				stringTemplateLoader.putTemplate("requestString", bean.getRequestString());
			}

			// notTODO check if pass more parameters to freemarker template -> http://stackoverflow.com/questions/30071252/pass-more-parameters-to-freemarker-template
			freemarkerConfiguration.setTemplateLoader(stringTemplateLoader);
			
			// getting the freemarker template
			Template template = freemarkerConfiguration.getTemplate("requestString");
		
			// creating the complete request string
			StringWriter requestStringWriter = new StringWriter();
			try {
				template.process(createDataModel(bean), requestStringWriter);
			} catch (TemplateException e) {
				throw new IllegalArgumentException("Exception while creating the request string from template! " + e.getMessage(), e);
			}
			
			if (bean.getServiceType().equals(ServiceTypeEnum.DATABASE)) {
				bean.setDatabaseQuery(requestStringWriter.toString());
				LOGGER.trace("The request string : {}", bean.getDatabaseQuery());
				
			} else {
				bean.setRequestString(requestStringWriter.toString());
				LOGGER.trace("The request string : {}", bean.getRequestString());
			}
		}
		
		return bean;
	}


	private Map<String, Object> createDataModel(WorkflowBean bean) {
		Map<String, Object> main = new HashMap<String, Object>();
		main.put("sctParam", bean.getParams());
		LOGGER.trace("The Freemarker data model : {}", main);
		return main;
	}


	public Configuration getFreemarkerConfiguration() {
		return freemarkerConfiguration;
	}

	public void setFreemarkerConfiguration(Configuration freemarkerConfiguration) {
		this.freemarkerConfiguration = freemarkerConfiguration;
	}

	public StringTemplateLoader getStringTemplateLoader() {
		return stringTemplateLoader;
	}

	public void setStringTemplateLoader(StringTemplateLoader stringTemplateLoader) {
		this.stringTemplateLoader = stringTemplateLoader;
	}
}
