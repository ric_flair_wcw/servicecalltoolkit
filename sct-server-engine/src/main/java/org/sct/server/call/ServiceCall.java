package org.sct.server.call;


import java.io.IOException;
import java.net.SocketTimeoutException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.sct.common.constants.ServiceTypeEnum;
import org.sct.server.beans.workflow.WorkflowBean;
import org.sct.server.common.constants.HttpMethodEnum;
import org.sct.server.common.exceptions.ExternalServiceException;
import org.sct.server.common.exceptions.IllegalConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;


/**
 * Calling the external service.
 * The service type can be:
 *  - WEBSERVICE
 *  - REST
 *  - DATABASE
 * 
 * @author Viktor Horvath
 *
 */
public class ServiceCall {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(ServiceCall.class);
	
	private HttpClient httpClient = null;
	private ServiceEndpointsStorage serviceEndpointsStorage = null;
	private InitialContext initialContext = null;
	private String uniqueId;
	
	
	public WorkflowBean call(final WorkflowBean _bean) throws IOException, ExternalServiceException {
		LOGGER.debug("Calling the external service.");
		WorkflowBean bean = _bean.clone();
		
		// initializing the endpoint(s) of the service to be called
		if (bean.getEndpoints() == null || bean.getEndpoints().size() == 0) {
			throw new IllegalConfigurationException("No endpoint is defined for the service " + bean.getServiceName() + "!");
		}
		serviceEndpointsStorage.init(bean.getServiceName(), bean.getEndpoints());
		
		bean.setStartCallExternalServiceTime(Calendar.getInstance());
		
		// generating a unique id for getting the endpoints
		uniqueId = UUID.randomUUID().toString();

		try {
			// calling the service
			// DATABASE
			if (bean.getServiceType().equals(ServiceTypeEnum.DATABASE)) {
				StringBuilder jsonResponseBuilder = executeDBQuery(bean);
	        	bean.setContentType("application/json");
	        	bean.setResponseString(jsonResponseBuilder.substring(0, jsonResponseBuilder.length()-1) + "}");
			}
			// REST or WEBSERVICE
			else {
				HttpResponse httpResponse = callWithLoadBalancing(bean);
				bean.setEndCallExternalServiceTime(Calendar.getInstance());
				
				// extracting the response string
				if (httpResponse == null) {
					throw new ExternalServiceException("No response coming back from the service for the POI '"+bean.getPoiName()+"'!");
				}
				HttpEntity httpEntity = httpResponse.getEntity();
		        if (httpEntity != null) {
		        	String responseText = EntityUtils.toString(httpEntity);
		        	LOGGER.trace("The response arrived from the service is {}", responseText);
		        	String contentType = httpEntity.getContentType().getValue().indexOf(";") != -1 ? httpEntity.getContentType().getValue().substring(0, httpEntity.getContentType().getValue().indexOf(";")) : httpEntity.getContentType().getValue();
		        	LOGGER.trace("... and its content type is {}", contentType);
		        	// the Content-Type can be like 'text/xml; charset=utf-8'
		        	bean.setContentType(contentType);
		        	bean.setResponseString(responseText);
		        } else {
					throw new ExternalServiceException("No response coming back from the service for the POI '"+bean.getPoiName()+"'!");
		        }
			}
		} finally {
			serviceEndpointsStorage.remove(uniqueId);
		}
		
        return bean;
	}


	private HttpResponse callWithLoadBalancing(WorkflowBean bean) throws IOException, ClientProtocolException, ExternalServiceException {
		String poiName = bean.getPoiName();
		String endpoint = serviceEndpointsStorage.next(bean.getServiceName(), uniqueId);
		if (endpoint == null) {
			throw new ExternalServiceException("No service available for the POI '"+poiName+"'!");
		}

		String requestString = bean.getRequestString();

		// request parameters and other properties
		StringEntity stringEntity = null;
		if (! (bean.getServiceType().equals(ServiceTypeEnum.REST) && bean.getHttpMethod().equals(HttpMethodEnum.GET)) ) {
			if (requestString == null) {
				LOGGER.info("The workflow bean is {}", bean);
				throw new RuntimeException("The request string cannot be null! It can be null only at REST GET service.");
			}
			stringEntity = new StringEntity(requestString, "UTF-8");
			stringEntity.setChunked(true);
		}

		HttpUriRequest httpUriRequest = null;

		// if webservice
		if (bean.getServiceType().equals(ServiceTypeEnum.WEBSERVICE)) {
			httpUriRequest = new HttpPost(endpoint);
			((HttpPost)httpUriRequest).addHeader("Accept" , "text/xml");
			((HttpPost)httpUriRequest).addHeader("SOAPAction", bean.getSoapAction());
			// set the SOAP request xml
			((HttpPost)httpUriRequest).setEntity(stringEntity);
		}		
		// if REST
		else if (bean.getServiceType().equals(ServiceTypeEnum.REST)) {
			// if GET
			if (bean.getHttpMethod().equals(HttpMethodEnum.GET)) {
				httpUriRequest = new HttpGet(endpoint);
				((HttpGet)httpUriRequest).addHeader("Accept" , "application/json, text/xml, application/xml");
			}
			// if POST
			if (bean.getHttpMethod().equals(HttpMethodEnum.POST)) {
				httpUriRequest = new HttpPost(endpoint);
				// set the request json
				((HttpPost)httpUriRequest).setEntity(stringEntity);
				((HttpPost)httpUriRequest).addHeader("Accept" , "application/json, text/xml, application/xml");
			}
		}
		
		// calling the web service
		HttpResponse httpResponse = null;
		Calendar startTime = Calendar.getInstance();
		LOGGER.trace("Calling the service on {} with \n{}", endpoint, requestString);
		try {
			httpResponse = httpClient.execute(httpUriRequest);
			long ms = Calendar.getInstance().getTimeInMillis() - startTime.getTimeInMillis();
	        LOGGER.debug("Called the service. The time needed to call is {} ms. The response is\n{}", ms, httpResponse);
		} catch(Exception e) {
			LOGGER.warn("The endpoint is not available! Calling the next one. The exception is ", e);
			httpResponse = callWithLoadBalancing(bean);
		} 
		// the status code is not SUCCESS
		if (httpResponse.getStatusLine().getStatusCode() < 200 || httpResponse.getStatusLine().getStatusCode() > 299) {
			throw new ExternalServiceException("The HTTP status code is not Success (2XX)! Status Code:" + httpResponse.getStatusLine().getStatusCode() + ", Reason:" + httpResponse.getStatusLine().getReasonPhrase());
		}
		
        return httpResponse;
	}


	private StringBuilder executeDBQuery(WorkflowBean bean) throws ExternalServiceException {
		String poiName = bean.getPoiName();
		String endpoint = serviceEndpointsStorage.next(bean.getServiceName(), uniqueId);
		if (endpoint == null) {
			throw new ExternalServiceException("No service available for the POI '"+poiName+"'!");
		}

		StringBuilder jsonResponseBuilder = new StringBuilder();
		jsonResponseBuilder.append("{ ");
		
		JdbcTemplate jdbcTemplate = null;
		boolean isExecuted = false;

		// trying to get the datasource from app server
		Object datasource = null;
		try {
			LOGGER.debug("Trying to resolve the '{}' datasource JDNI...", endpoint);
			datasource = (DataSource) initialContext.lookup(endpoint);
		} catch(NamingException ne) {
			LOGGER.warn("Warning, the datasource JNDI '{}' cannot be resolved! Exception:{}" , endpoint, ne.getMessage());
			LOGGER.debug("Full stacktrace:", ne);
		}
		if (datasource != null && datasource instanceof DataSource) {
			// executing the query
			jdbcTemplate =  new JdbcTemplate( (DataSource)datasource );
			List<Map<String,Object>> records = jdbcTemplate.queryForList(bean.getDatabaseQuery());
			if (records.size() > 0) {
				// in order to the generated JSON can be processed by the JSONResponseExtractor I collect the values of each column in an array and add arrays to the final JSON
				// { "NAME" : [
			    //          "java.lang.String", "GetAddressForPostCode-GetAddressForPostCode_by_id", "PensionsEnquiryServices-getPensionsEnquiryService"
			    //      ],
			    //   "SOAP_ACTION": [
			    //          "java.lang.String", "GetAddressForPostCode", "urn:GetFundDetails"
			    //      ]
			    //  }
				Map<String,StringBuilder> jsonColumnBuilders = new HashMap<String,StringBuilder>();
				boolean isTypeSet = false;
				// processing the result
				for(Map<String,Object> record : records) {
					Set<String> keys = record.keySet();
					for(String key : keys) {
						if (!isTypeSet) {
							StringBuilder stringBuilder = new StringBuilder();
							// adding the type
							stringBuilder.append("\"").append(convertType(record.get(key))).append("\"");
							jsonColumnBuilders.put(key, stringBuilder);
						}
						// adding the value
						jsonColumnBuilders.get(key).append(",").append("\"").append(convertValue(record.get(key))).append("\"");
					}
					isTypeSet = true;
				}
				// adding the values to the response JSON
				for(String key : jsonColumnBuilders.keySet()) {
					if (jsonColumnBuilders.get(key) != null && jsonColumnBuilders.get(key).length() > 0) {
						jsonResponseBuilder.append("\"").append(key).append("\":[").append(jsonColumnBuilders.get(key)).append("],");
					}
				}
			}
			// not calling the next datasource JNDI
			isExecuted = true;
		}
		
		// if none of the datasources could be accessed then throw an exception
		if (!isExecuted) {
			jsonResponseBuilder = executeDBQuery(bean);
		}
		return jsonResponseBuilder;
	}


	private String convertType(Object value) {
		if (value instanceof Number) {
			return value.getClass().getName();
		}
		else if (value instanceof java.sql.Date) {
			return Date.class.getName();
		}
		else if (value instanceof java.sql.Time) {
			return String.class.getName();
		}
		else if (value instanceof java.sql.Timestamp) {
			return Date.class.getName();
		}
		else if (value instanceof Character) {
			return String.class.getName();
		}
		else if (value instanceof Boolean) {
			return Boolean.class.getName();
		}
		else if (value instanceof Date) {
			return Date.class.getName();
		} 
		else {
			return String.class.getName();
		}
	}


	private Object convertValue(Object value) {
		if (value instanceof Number) {
			return value.toString();
		}
		else if (value instanceof java.sql.Date) {
			return ((java.sql.Date)value).getTime();
		}
		else if (value instanceof java.sql.Time) {
			DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss.SSS");
			return dateFormat.format((java.sql.Time)value);
		}
		else if (value instanceof java.sql.Timestamp) {
			return ((java.sql.Timestamp)value).getTime();
		}
		else if (value instanceof Character) {
			return ((Character)value).toString();
		}
		else if (value instanceof Boolean) {
			return ((Boolean)value).toString();
		}
		else if (value instanceof Date) {
			return ((Date)value).getTime();
		}
		else {
			return value.toString();
		}
	}


	public HttpClient getHttpClient() {
		return httpClient;
	}

	public void setHttpClient(HttpClient httpClient) {
		this.httpClient = httpClient;
	}

	public ServiceEndpointsStorage getEndpointsQueue() {
		return serviceEndpointsStorage;
	}

	public void setEndpointsQueue(ServiceEndpointsStorage endpointsQueue) {
		this.serviceEndpointsStorage = endpointsQueue;
	}


	public InitialContext getInitialContext() {
		return initialContext;
	}


	public void setInitialContext(InitialContext initialContext) {
		this.initialContext = initialContext;
	}
	
}
