<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="menu_background_gradient">
<table width="100%">
   <sec:authorize access="hasAnyRole('ROLE_ADMINSTRATOR')">
   <tr>
      <td class="menuItem"> 1. Administration </td>
   </tr>
   <tr>
      <td class="menuItem"> <a href="<c:url value='/administration/encryptionKey/main'/>">&nbsp;&nbsp;&nbsp;a) Setting the encryp. key</a> </td>
   </tr>
   </sec:authorize>
</table>
</div>
