<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script src="<c:url value='/resources/js/administration-encryptionKey.controller.js'/>"></script>

<div ng-app="SCTApp" ng-controller="administration-encryptionKey.controller">
    
    <table class="modal-table">
        <tr>
            <td><span class="modal-body-label">The date when the key was altered:</span></td>
            <td>{{status.loadDate}}</td>
        </tr>
        <tr>
            <td><span class="modal-body-label">Altered in this web application?</span></td>
            <td>{{status.fromWebApp}}</td>
        </tr>
    </table>
    
    <hr>
    
    <b>Setting a new encryption key</b>
    <br>
    <ng-form name="mainForm">
        <br>
        <table>
            <tr>
                <td>The new encryption key:</td>
                <td> <input name="encryptionKeyInput" type="text" ng-model="encryptionKey" size="60" placeholder="(required)" required/> </td>
            <tr/>
        </table>
        <br>
		<button type="submit" ng-click="save()" class="btn btn-primary" ng-disabled="mainForm.encryptionKeyInput.$error.required">Save</button>
    </ng-form>
    
    
</div>