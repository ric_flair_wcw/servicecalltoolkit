<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<h3>Welcome to the Server Engine Web Application, <sec:authentication property="principal.username" />!</h3>
