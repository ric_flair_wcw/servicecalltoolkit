app.controller("administration-encryptionKey.controller", function ($rootScope, $scope, $resource, $modal, CommunicationService, PropertyStorage, AKOUtils) {
	// getting the encryption key status info
	$scope.status = CommunicationService.get2("status");
	
	
	$scope.save = function() {
    	CommunicationService.save(
    			"set",
    			{ "encryptionKey": $scope.encryptionKey }		
    	);
	};
	
	
    // when the save happened the page has to be reloaded
    $scope.$on("saveFinishedEvent", function(event){
    	window.location.reload();
    });
	
	
});