// The [] parameter in the module definition can be used to define dependent modules.
var app = angular.module("SCTApp", ["ngResource", "ui.bootstrap"]);

// see in statistics.html
app.filter('range', function() {
	  return function(input, min, max) {
	    min = parseInt(min); //Make string input int
	    max = parseInt(max);
	    for (var i=min; i<max; i++)
	      input.push(i);
	    return input;
	  };
	});
// By default, trailing slashes will be stripped from the calculated URLs, which can pose problems with server backends that do not expect that behavior. This can be disabled by configuring the $resourceProvider like this:
//app.config(['$resourceProvider', function($resourceProvider) {
//  $resourceProvider.defaults.stripTrailingSlashes = false;
//}]);