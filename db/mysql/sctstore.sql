SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sctstore`
--

-- --------------------------------------------------------

--
-- Table structure for table `endpoint`
--

CREATE TABLE IF NOT EXISTS `endpoint` (
`id` bigint(20) NOT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `SERVICE_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=164 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `endpoint`
--

INSERT INTO `endpoint` (`id`, `URL`, `SERVICE_ID`) VALUES
(112, 'http://lap50215:8090/address3', 40),
(124, 'http://lap50215:8090/address', 38),
(139, 'http://localhost:8070/address_url/${county}/${town}', 43),
(140, 'http://localhost:8090/address_url/${county}/${town}', 43),
(149, 'java:/comp/env/jdbc/sctStore', 41),
(150, 'jdbc/sctStore', 41),
(162, 'http://lap50215:8083/mockGetAddressForPostCode', 29),
(163, '1', 29);

-- --------------------------------------------------------

--
-- Table structure for table `extract`
--

CREATE TABLE IF NOT EXISTS `extract` (
`id` bigint(20) NOT NULL,
  `PATH` varchar(255) DEFAULT NULL,
  `POI_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1261 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `extract`
--

INSERT INTO `extract` (`id`, `PATH`, `POI_ID`) VALUES
(942, '/GetAddressForPostCodeResponse/postalAddress/displayAddress', 50),
(952, '/GetAddressForPostCodeResponse/postalAddress/addressIdentification/identifierType', 56),
(1231, '/soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeResponse/ns:postalAddress/ns:buildingName', 39),
(1232, '/soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeResponse/ns:postalAddress/ns:apartment', 38),
(1233, '/soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeResponse/ns:postalAddress/ns:country', 38),
(1234, '/soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeResponse/ns:postalAddress/ns:town', 38),
(1235, '/soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeResponse/ns:postalAddress/ns:buildingName', 38),
(1236, '/soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeResponse/ns:postalAddress/ns:addressIdentification/ns:identifierType', 38),
(1237, '/soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeResponse/ns:postalAddress/ns:addressIdentification/ns:instanceIdentifier', 38),
(1238, '/soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeResponse/ns:postalAddress/ns:addressIdentification/ns:addressIdentifier', 38),
(1239, '/soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeResponse/ns:postalAddress/ns:streetType', 38),
(1240, '/soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeResponse/ns:postalAddress/ns:displayAddress', 38),
(1241, '/soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeResponse/ns:postalAddress/ns:county', 38),
(1242, '/soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeResponse/ns:postalAddress/ns:buildingID', 38),
(1243, '/soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeResponse/ns:postalAddress/ns:subBuildingID', 38),
(1244, '/soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeResponse/ns:postalAddress/ns:locality', 38),
(1245, '/soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeResponse/ns:postalAddress/ns:streetName', 38),
(1246, '/soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeResponse/ns:postalAddress/ns:postcodeSuffix', 38),
(1248, '/SERVICE_NAME', 55),
(1249, '/POI_NAME', 55),
(1250, '/PERIOD_START', 55),
(1251, '/CNT', 55);

-- --------------------------------------------------------

--
-- Table structure for table `poi`
--

CREATE TABLE IF NOT EXISTS `poi` (
`id` bigint(20) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `SINGLE_VALUE` text NOT NULL,
  `SERVICE_ID` bigint(20) DEFAULT NULL,
  `ENCRYPTION_NEEDED` bit(1) DEFAULT NULL,
  `EXTRACT_TYPE` varchar(255) DEFAULT NULL,
  `DESCRIPTION` text
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `poi`
--

INSERT INTO `poi` (`id`, `NAME`, `SINGLE_VALUE`, `SERVICE_ID`, `ENCRYPTION_NEEDED`, `EXTRACT_TYPE`, `DESCRIPTION`) VALUES
(38, 'postcode->fullAddress', 'false', 29, b'0', NULL, NULL),
(39, 'postcode->buildingName', 'true', 29, b'0', NULL, NULL),
(50, 'REST->displayAddress', 'false', 38, b'0', NULL, NULL),
(53, 'REST->simpleString', 'true', 40, b'0', NULL, NULL),
(55, 'DB-protneut->WS-service-data', 'false', 41, b'0', NULL, 'aaaaaaaaaaaaaaaaaaaaaaaaaasdasdsadasdsaderwef'),
(56, 'REST->url_variables->identifierType', 'false', 43, b'0', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `server`
--

CREATE TABLE IF NOT EXISTS `server` (
`id` bigint(20) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `server`
--

INSERT INTO `server` (`id`, `NAME`, `URL`) VALUES
(1, 'local', 'http://localhost:8080/sct-server-engine'),
(3, 'local-Weblogic', 'http://localhost:7001/server-engine');

-- --------------------------------------------------------

--
-- Table structure for table `service`
--

CREATE TABLE IF NOT EXISTS `service` (
`id` bigint(20) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `NAMESPACE_JSON` varchar(255) DEFAULT NULL,
  `REQUEST_TEMPLATE_TEXT` text,
  `SERVICE_TYPE` varchar(255) DEFAULT NULL,
  `SOAP_ACTION` varchar(255) DEFAULT NULL,
  `HTTP_METHOD` varchar(255) DEFAULT NULL,
  `DATABASE_QUERY` text,
  `DESCRIPTION` text
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `service`
--

INSERT INTO `service` (`id`, `NAME`, `NAMESPACE_JSON`, `REQUEST_TEMPLATE_TEXT`, `SERVICE_TYPE`, `SOAP_ACTION`, `HTTP_METHOD`, `DATABASE_QUERY`, `DESCRIPTION`) VALUES
(29, 'GetAddressForPostCode-GetAddressForPostCode_by_id', '{"ns2":"ns2","ns1":"ns1","soapenv":"http://schemas.xmlsoap.org/soap/envelope/","ns":"ns"}', '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="ns" xmlns:ns1="ns1" xmlns:ns2="ns2">\n   <soapenv:Header/>\n   <soapenv:Body>\n      <ns:GetAddressForPostCodeRequest>\n         <ns:requestHeader>\n            <ns1:id>${sctParam.id}</ns1:id>\n            <!--Optional:-->\n            <ns1:source>?</ns1:source>\n            <ns1:user>\n               <ns2:userId>?</ns2:userId>\n               <ns2:credentials>?</ns2:credentials>\n            </ns1:user>\n         </ns:requestHeader>\n         <!--Optional:-->\n         <ns:postalAddress>\n            <!--Optional:-->\n            <ns:displayAddress>?</ns:displayAddress>\n            <!--Optional:-->\n            <ns:buildingID>?</ns:buildingID>\n            <!--Optional:-->\n            <ns:building>?</ns:building>\n            <!--Optional:-->\n            <ns:apartment>?</ns:apartment>\n            <!--Optional:-->\n            <ns:buildingName>?</ns:buildingName>\n            <!--Optional:-->\n            <ns:county>?</ns:county>\n            <!--Optional:-->\n            <ns:locality>?</ns:locality>\n            <!--Optional:-->\n            <ns:nonUKPostcode>BH1 3EH</ns:nonUKPostcode>\n            <!--Optional:-->\n            <ns:postcodeSuffix>?</ns:postcodeSuffix>\n            <!--Optional:-->\n            <ns:streetName>?</ns:streetName>\n            <!--Optional:-->\n            <ns:streetType>?</ns:streetType>\n            <!--Optional:-->\n            <ns:subBuildingID>?</ns:subBuildingID>\n            <!--Optional:-->\n            <ns:town>?</ns:town>\n            <!--Optional:-->\n            <ns:country>HU</ns:country>\n            <!--Optional:-->\n            <ns:postcode>BH1 3EH</ns:postcode>\n            <!--Zero or more repetitions:-->\n            <ns:addressIdentification>\n               <ns:identifierType>?</ns:identifierType>\n               <ns:addressIdentifier>?</ns:addressIdentifier>\n               <!--Optional:-->\n               <ns:instanceIdentifier>?</ns:instanceIdentifier>\n            </ns:addressIdentification>\n            <!--Optional:-->\n            <ns:handoverPointDetails>\n               <ns:L2SID>?</ns:L2SID>\n               <ns:HandoverPortID>?</ns:HandoverPortID>\n               <ns:SVLANID>?</ns:SVLANID>\n               <ns:exchangeRef>?</ns:exchangeRef>\n            </ns:handoverPointDetails>\n         </ns:postalAddress>\n         <!--Optional:-->\n         <ns:serviceDirective>?</ns:serviceDirective>\n      </ns:GetAddressForPostCodeRequest>\n   </soapenv:Body>\n</soapenv:Envelope>', 'WEBSERVICE', 'GetAddressForPostCode', NULL, '', NULL),
(38, 'get_address_REST', '{}', '', 'REST', NULL, 'GET', '', NULL),
(40, 'get_address_REST_simpleString', '{}', NULL, 'REST', NULL, 'GET', '', NULL),
(41, 'DB-protneut', '{}', NULL, 'DATABASE', NULL, 'GET', 'SELECT \n	*\nFROM\n	stat_service_poi_date_cnt\nWHERE\n	successful = ${sctParam.successful}', NULL),
(43, 'REST_URL_variables', '{}', NULL, 'REST', NULL, 'GET', NULL, 'asd');

-- --------------------------------------------------------

--
-- Table structure for table `stat_service_poi_date_cnt`
--

CREATE TABLE IF NOT EXISTS `stat_service_poi_date_cnt` (
`id` bigint(20) NOT NULL,
  `CNT` int(11) DEFAULT NULL,
  `PERIOD_START` datetime DEFAULT NULL,
  `POI_NAME` varchar(255) DEFAULT NULL,
  `SERVICE_NAME` varchar(255) DEFAULT NULL,
  `SERVICE_TYPE` varchar(255) DEFAULT NULL,
  `SUCCESSFUL` bit(1) DEFAULT NULL,
  `POI_ID` bigint(20) DEFAULT NULL,
  `SERVICE_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=603 DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `endpoint`
--
ALTER TABLE `endpoint`
 ADD PRIMARY KEY (`id`), ADD KEY `FK_46hrsm47d4qip46e61oyn3b1i` (`SERVICE_ID`);

--
-- Indexes for table `extract`
--
ALTER TABLE `extract`
 ADD PRIMARY KEY (`id`), ADD KEY `FK_jg5i6vresdmyyl1fmrw91gxrm` (`POI_ID`);

--
-- Indexes for table `poi`
--
ALTER TABLE `poi`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `NAME` (`NAME`), ADD KEY `FK_da9euo7r61k9kqmei8j8pq2gm` (`SERVICE_ID`);

--
-- Indexes for table `server`
--
ALTER TABLE `server`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `NAME` (`NAME`);

--
-- Indexes for table `service`
--
ALTER TABLE `service`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `NAME` (`NAME`);

--
-- Indexes for table `stat_service_poi_date_cnt`
--
ALTER TABLE `stat_service_poi_date_cnt`
 ADD PRIMARY KEY (`id`), ADD KEY `FK_1qhdxw18iirv46dhlv4uow34v` (`POI_ID`), ADD KEY `FK_85mif4kxdcv3otudxardhkcew` (`SERVICE_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `endpoint`
--
ALTER TABLE `endpoint`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=164;
--
-- AUTO_INCREMENT for table `extract`
--
ALTER TABLE `extract`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1261;
--
-- AUTO_INCREMENT for table `poi`
--
ALTER TABLE `poi`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT for table `server`
--
ALTER TABLE `server`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `service`
--
ALTER TABLE `service`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `stat_service_poi_date_cnt`
--
ALTER TABLE `stat_service_poi_date_cnt`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=603;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `endpoint`
--
ALTER TABLE `endpoint`
ADD CONSTRAINT `FK_46hrsm47d4qip46e61oyn3b1i` FOREIGN KEY (`SERVICE_ID`) REFERENCES `service` (`id`);

--
-- Constraints for table `extract`
--
ALTER TABLE `extract`
ADD CONSTRAINT `FK_jg5i6vresdmyyl1fmrw91gxrm` FOREIGN KEY (`POI_ID`) REFERENCES `poi` (`id`);

--
-- Constraints for table `poi`
--
ALTER TABLE `poi`
ADD CONSTRAINT `FK_da9euo7r61k9kqmei8j8pq2gm` FOREIGN KEY (`SERVICE_ID`) REFERENCES `service` (`id`);

--
-- Constraints for table `stat_service_poi_date_cnt`
--
ALTER TABLE `stat_service_poi_date_cnt`
ADD CONSTRAINT `FK_1qhdxw18iirv46dhlv4uow34v` FOREIGN KEY (`POI_ID`) REFERENCES `poi` (`id`),
ADD CONSTRAINT `FK_85mif4kxdcv3otudxardhkcew` FOREIGN KEY (`SERVICE_ID`) REFERENCES `service` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
