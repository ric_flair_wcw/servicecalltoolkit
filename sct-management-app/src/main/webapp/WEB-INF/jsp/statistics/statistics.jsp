<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<script src="<c:url value='/resources/js/statistics.controller.js'/>"></script>
<script src="<c:url value='/resources/js/chart/jquery.flot.js'/>"></script>
<script src="<c:url value='/resources/js/chart/jquery.flot.time.js'/>"></script>
<script src="<c:url value='/resources/js/chart/jquery.flot.selection.js'/>"></script>

<script>
    $(function() {
    	var d = new Date();
    	var ds = d.getFullYear() + "-" + ("0" + (d.getMonth()+1)).slice(-2) + "-" + ("0" + d.getDate()).slice(-2);
        $( "#startdate-picker-service" ).datepicker({ changeMonth: true, changeYear: true, showAnim: "slide", dateFormat: "yy-mm-dd", showWeek: true, firstDay: 1 });
        $( "#startdate-picker-service" ).val(ds);
        $( "#enddate-picker-service" ).datepicker({	changeMonth: true, changeYear: true, showAnim: "slide", dateFormat: "yy-mm-dd", showWeek: true, firstDay: 1 });
        $( "#enddate-picker-service" ).val(ds);
        $( "#startdate-picker-poi" ).datepicker({ changeMonth: true, changeYear: true, showAnim: "slide", dateFormat: "yy-mm-dd", showWeek: true, firstDay: 1 });
        $( "#startdate-picker-poi" ).val(ds);
        $( "#enddate-picker-poi" ).datepicker({ changeMonth: true, changeYear: true, showAnim: "slide", dateFormat: "yy-mm-dd", showWeek: true, firstDay: 1 });
        $( "#enddate-picker-poi" ).val(ds);
        $( "#startdate-picker-comparison" ).datepicker({ changeMonth: true, changeYear: true, showAnim: "slide", dateFormat: "yy-mm-dd", showWeek: true, firstDay: 1 });
        $( "#startdate-picker-comparison" ).val(ds);
        $( "#enddate-picker-comparison" ).datepicker({ changeMonth: true, changeYear: true, showAnim: "slide", dateFormat: "yy-mm-dd", showWeek: true, firstDay: 1 });
        $( "#enddate-picker-comparison" ).val(ds);
    });
</script>
  
<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/flot-chart.css'/>"/>


<div ng-app="SCTApp" ng-controller="statistics.controller">

    <tabset>
        
        <tab heading="Service" active="true">
	
            <p>
            <span class="modal-body-label">Start date:</span> <input type="text" id="startdate-picker-service" style="width:120px;" readonly>&nbsp;<span class="modal-body-label">hour:</span><select ng-model="store.selectedStartTimeService" ng-options="n for n in [] | range:0:24"></select>
            &nbsp;&nbsp;-&nbsp;&nbsp;
            <span class="modal-body-label">End date:</span> <input type="text" id="enddate-picker-service" style="width:120px;" readonly>&nbsp;<span class="modal-body-label">hour:</span><select ng-model="store.selectedEndTimeService" ng-options="n for n in [] | range:0:24"></select>
            </p>
			<span class="modal-body-label">Service:</span> <select ng-model="store.selectedServiceName" ng-options="serv.n as serv.n for serv in services"></select>
			<button type="submit" ng-click="loadGraphService()" class="btn btn-primary">Load the graph!</button>
			
            <div style="box-sizing: border-box; width: 850px; margin: 15px auto 30px auto; border: 1px solid #ddd;">
                <div id="legend-container-service"></div>
            </div>
		    <div id="parent-graph-placeholder-service" class="demo-container">
		        <div id="graph-placeholder-service" class="demo-placeholder"></div>
		    </div>
		    <div class="demo-container" style="height:150px;">
		        <div id="graph-overview-service" class="demo-placeholder"></div>
		    </div>

        </tab>
        
        
        
        <tab heading="POI" active="false">
            <p>
            <span class="modal-body-label">Start date:</span> <input type="text" id="startdate-picker-poi" style="width:120px;" readonly>&nbsp;<span class="modal-body-label">hour:</span><select ng-model="store.selectedStartTimePOI" ng-options="n for n in [] | range:0:24"></select>
            &nbsp;&nbsp;-&nbsp;&nbsp;
            <span class="modal-body-label">End date:</span> <input type="text" id="enddate-picker-poi" style="width:120px;" readonly>&nbsp;<span class="modal-body-label">hour:</span><select ng-model="store.selectedEndTimePOI" ng-options="n for n in [] | range:0:24"></select>
            </p>
            <span class="modal-body-label">POI:</span> <select ng-model="store.selectedPOIName" ng-options="poi.n as poi.n for poi in pois"></select>
            <button type="submit" ng-click="loadGraphPOI()" class="btn btn-primary">Load the graph!</button>
            
            <div style="box-sizing: border-box; width: 850px; margin: 15px auto 30px auto; border: 1px solid #ddd;">
                <div id="legend-container-POI"></div>
            </div>
            <div id="parent-graph-placeholder-POI" class="demo-container">
                <div id="graph-placeholder-POI" class="demo-placeholder"></div>
            </div>
            <div class="demo-container" style="height:150px;">
                <div id="graph-overview-POI" class="demo-placeholder"></div>
            </div>
        </tab>

    
        <tab heading="Comparison" active="false">
            <p><form name="formInComparison">
                <input type="radio" ng-model="store.typeInComparison" ng-change="changeTypeInComparison()" value="service">Service
                <input type="radio" ng-model="store.typeInComparison" ng-change="changeTypeInComparison()" value="POI">POI<br>
            </form></p>
            <div ng-show="store.typeInComparison == 'service'">
                <table style="border-collapse: separate; border-spacing: 10px;"><tr>
                    <td><select id="select-from-service" multiple size="7" ng-model="store.selectedNamesSelectFromService" ng-options="s for s in store.selectFromServiceData" style="overflow: auto; width:500px;"></select></td>
                    <td><button type="submit" ng-click="addToSelectedServices()" class="btn btn-primary" ng-disabled="store.selectedNamesSelectFromService.length == 0">Add >></button> <br>
                        <button type="submit" ng-click="removeFromSelectedServices()" class="btn btn-primary" ng-disabled="store.selectedNamesSelectToService.length == 0">&lt;&lt; Remove</button></td>
                    <td><select id="select-to-service" multiple size="7" ng-model="store.selectedNamesSelectToService" ng-options="o for o in store.selectedNamesSelectTo" style="overflow: auto; width:500px;"></select></td>
                </tr></table>
            </div>
            <div ng-show="store.typeInComparison == 'POI'">
                <table style="border-collapse: separate; border-spacing: 10px;"><tr>
                    <td><select id="select-from-poi" multiple size="7" ng-model="store.selectedNamesSelectFromPOI" ng-options="s for s in store.selectFromPOIData" style="overflow: auto; width:500px;"></select></td>
                    <td><button type="submit" ng-click="addToSelectedPOIs()" class="btn btn-primary" ng-disabled="store.selectedNamesSelectFromPOI.length == 0">Add >></button> <br>
                        <button type="submit" ng-click="removeFromSelectedPOIs()" class="btn btn-primary" ng-disabled="store.selectedNamesSelectToPOI.length == 0">&lt;&lt; Remove</button></td>
                    <td><select id="select-to-service" multiple size="7" ng-model="store.selectedNamesSelectToPOI" ng-options="o for o in store.selectedNamesSelectTo" style="overflow: auto; width:500px;"></select></td>
                </tr></table>
            </div>
            <p>
            <span class="modal-body-label">Start date:</span> <input type="text" id="startdate-picker-comparison" style="width:120px;" readonly>&nbsp;<span class="modal-body-label">hour:</span><select ng-model="store.selectedStartTimeComparison" ng-options="n for n in [] | range:0:24"></select>
            &nbsp;&nbsp;-&nbsp;&nbsp;
            <span class="modal-body-label">End date:</span> <input type="text" id="enddate-picker-comparison" style="width:120px;" readonly>&nbsp;<span class="modal-body-label">hour:</span><select ng-model="store.selectedEndTimeComparison" ng-options="n for n in [] | range:0:24"></select>
            </p>
            <button type="submit" ng-click="loadGraphComparison()" class="btn btn-primary" ng-disabled="store.selectedNamesSelectTo.length == 0">Load the graph!</button>
            
            <div style="box-sizing: border-box; width: 850px; margin: 15px auto 30px auto; border: 1px solid #ddd;">
                <div id="legend-container-comparison"></div>
            </div>
            <div class="demo-container">
                <div id="graph-placeholder-comparison" class="demo-placeholder"></div>
            </div>
            <div class="demo-container" style="height:150px;">
                <div id="graph-overview-comparison" class="demo-placeholder"></div>
            </div>
        </tab>
    
    </tabset>
    
</div>