<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<h3>Welcome back, <sec:authentication property="principal.username" />!</h3>
