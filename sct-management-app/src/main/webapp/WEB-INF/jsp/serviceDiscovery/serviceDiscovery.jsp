<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script src="<c:url value='/resources/js/serviceDiscovery.controller.js'/>"></script>
<script src="<c:url value='/resources/js/jquery/jquery.checkboxtree.js'/>" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/jquery.checkboxtree.css'/>"/>

<div ng-app="SCTApp" ng-controller="serviceDiscovery.controller">

	<tabset>
        
		<tab heading="Service" active="is1stActive" disabled="true">
            <br>
			<input type="radio" ng-model="data.first.newOrExisting" value="new" ng-click="changeNewOrExisting()"></input>New service<br/>
			<input type="radio" ng-model="data.first.newOrExisting" value="existing" ng-click="changeNewOrExisting()"></input>Existing service<br/>
			
			<br>
			<div ng-show="data.first.newOrExisting == 'new'">
                <input id="selectWsdlUrl" type="text" ng-model="data.first.wsdlUrl" size="50"></input> 
                <button type="submit" ng-click="loadWsdl()" ng-disabled="!(!!data.first.wsdlUrl)" class="btn btn-primary">Load it!</button>
				<div ng-show="data.first.canOperationsBeDisplayed">
				    <br>
					WSDL operations:<select id="selectWsdlOperation" ng-model="data.first.selectedWsdlOperation" ng-options="wsdlOperation for wsdlOperation in data.wsdlOperations.operations" ng-change="wsdlOperationChanged()"></select>
				</div>
			</div>
			
			<div ng-show="data.first.newOrExisting == 'existing'">
				Service: <select id="selectService" ng-model="data.first.selectedService" ng-options="serv.id as serv.n for serv in services" ng-change="selectedServiceChanged()"></select>
				<div ng-show="canWsdlInputBeDisplayed()">
				    <br>
					WSDL url:<input type="text" ng-model="data.first.wsdlUrlExistingService" size="50">
                    <button type="submit" ng-click="loadExistingServiceWsdl()" ng-disabled="!(!!data.first.wsdlUrlExistingService)" class="btn btn-primary">Load it!</button>
				</div>
			</div>
            
            <br>
            <button type="submit" ng-click="next('service')" ng-disabled="isNextDisable('Service')" class="btn btn-primary">Next >></button>
        </tab>
		
		
        
		<tab heading="Service details" active="is2ndActive" ng-show="data.first.newOrExisting == 'new'" disabled="true">
			<table class="modal-table">
				<tr>
					<td><span class="modal-body-label">Service name:</span></td>
					<td> <input id="serviceNameInput" type="text" ng-model="data.serviceDetails.serviceName" size="60" placeholder="(required)" required/> </td>
				</tr>
				<tr>
					<td><span class="modal-body-label">Service type:</span></td>
					<td>Webservice</td>
				</tr>
				<tr>
					<td><span class="modal-body-label">SOAP action:</span></td>
					<td> {{data.serviceDetails.soapAction}} </td>
				</tr>
				<tr>
					<td valign="top"><span class="modal-body-label">Endpoint:</span></td>
					<td> <input id="serviceWsdlUrlInput" type="text" ng-model="data.serviceEndpoint" size="60" placeholder="(required)" required/> </td>
				</tr>
				<tr>
					<td valign="top"><span class="modal-body-label">Namespaces:</span></td>
					<td>
						<div style="max-height:150px; max-width:620px; overflow:auto;">
							<ul>
								<li ng-repeat="prefixNamespace in notSorted(data.serviceDetails.namespaces)">
								    {{prefixNamespace}} : {{data.serviceDetails.namespaces[prefixNamespace]}}
								</li>
							</ul>
						</div>
					</td>
				</tr>
				<tr>
					<td><span class="modal-body-label">Request Template Text:</span></td>
					<td> <button type="submit" ng-click="showEditRequestTemplateText()">...</button> </td>
				<tr/>
			</table>
		
            <button type="submit" ng-click="prev('serviceDetails')" class="btn btn-primary"><< Prev</button>
            <button type="submit" ng-click="next('serviceDetails')" ng-disabled="isNextDisable('Service details')" class="btn btn-primary">Next >></button>
        </tab>
		
		
        
		<tab heading="POI" active="is3rdActive" disabled="true">
		    <br>
			<span class="modal-body-label">Service name:</span>&nbsp;<input id="poiNameInput" type="text" ng-model="data.poi.name" size="60" placeholder="(required)" required/><br>
			<br>
			<span class="modal-body-label">Single value?</span>&nbsp;<input name="poiSingleValueInput" type="checkbox" ng-model="data.poi.singleValue" ng-disabled="disablePoiSingleValueInput"/>
			
			<br><br>
			<div style="max-height:550px; max-width:920px; overflow:auto;" id="divForCheckboxTreeForPOI"></div>
            
            <br>
            <button type="submit" ng-click="prev('POI')" class="btn btn-primary"><< Prev</button>
            <button type="submit" ng-click="finish()" ng-disabled="isFinishDisable()" class="btn btn-primary">Finish</button>            
		</tab>
		
	</tabset>
   
</div>