<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script src="<c:url value='/resources/js/administration-server-list.controller.js'/>"></script>
<script src="<c:url value='/resources/js/administration-server-data.controller.js'/>"></script>
<script src="<c:url value='/resources/js/directives/customTable.js'/>"></script>

<div ng-app="SCTApp" ng-controller="administration-server-list.controller">

    <div class="list_main_title">Servers</div>
    
   <custom-table></custom-table>

</div>