<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script src="<c:url value='/resources/js/administration-service-list.controller.js'/>"></script>
<script src="<c:url value='/resources/js/administration-service-data.controller.js'/>"></script>
<script src="<c:url value='/resources/js/directives/customTable.js'/>"></script>
<script src="<c:url value='/resources/js/directives/editableCellTextbox.js'/>"></script>

<div ng-app="SCTApp" ng-controller="administration-service-list.controller">

    <div class="list_main_title">Services</div>
    
    <custom-table></custom-table>

</div>

