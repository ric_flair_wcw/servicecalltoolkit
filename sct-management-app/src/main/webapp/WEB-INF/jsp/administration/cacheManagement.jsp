<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script src="<c:url value='/resources/js/administration-cacheManagement.controller.js'/>"></script>

<div ng-app="SCTApp" ng-controller="administration-cacheManagement.controller">

   <div class="list_main_title">Cache management</div>

   View type:<select id="selectCacheManagementView" ng-model="cacheManagementView" ng-options="rec.value as rec.label for rec in cacheManagementViews" ng-change="updateCacheManagementViewSelect()"></select>
   
   <table border="1" class="modal-table">
      <tr>
         <td></td>
         <td ng-repeat="server in servers" >
            <span ng-show="matrix[1][$index] != 'OFFLINE'" ng-click="showRefreshCacheWindow(servers[$index].n, null, null, 'SERVER' )" 
                  style="cursor: pointer; font-weight: bold; word-wrap: break-word; max-width: 80px; text-align: center;">{{server.n}}</span>
            <span ng-show="matrix[1][$index] == 'OFFLINE'" style="word-wrap: break-word; max-width: 80px; text-align: center;">{{server.n}}</span>
         </td>
      </tr>
      <tr ng-repeat="row in matrix">
         <td ng-show="cacheManagementView == 'SERVICE'">{{services[$index].n}}</td>
         <td ng-show="cacheManagementView == 'POI'">{{pois[$index].n}}</td>
         <td ng-show="cacheManagementView == 'BOTH' && servicesAndPOIs[$index][0] == 'SERVICE'"><b>{{servicesAndPOIs[$index][1]}}</b></td>
         <td ng-show="cacheManagementView == 'BOTH' && servicesAndPOIs[$index][0] == 'POI'"><i>&nbsp;&nbsp;&nbsp; - &nbsp;{{servicesAndPOIs[$index][1]}}</i></td>
         <td ng-repeat="cellValue in row" style="text-align: center;">
            <span ng-show="cellValue == 'OFFLINE'">OFFLINE</span>
            <img ng-show="cacheManagementView == 'SERVICE' && (cellValue == 'OLD' || cellValue == 'NOT_FOUND')" src="<c:url value='/resources/img/cacheManagement_{{cellValue}}.jpg'/>" ng-click="showRefreshCacheWindow(servers[$index].n, services[$parent.$index].n, cellValue, 'SERVICE' )" style="cursor: pointer" />
            <img ng-show="cacheManagementView == 'POI' && (cellValue == 'OLD' || cellValue == 'NOT_FOUND')" src="<c:url value='/resources/img/cacheManagement_{{cellValue}}.jpg'/>" ng-click="showRefreshCacheWindow(servers[$index].n, pois[$parent.$index].n, cellValue, 'POI' )" style="cursor: pointer" />
            <img ng-show="cacheManagementView == 'BOTH' && (cellValue == 'OLD' || cellValue == 'NOT_FOUND')" src="<c:url value='/resources/img/cacheManagement_{{cellValue}}.jpg'/>" ng-click="showRefreshCacheWindow(servers[$index].n, servicesAndPOIs[$parent.$index][1], cellValue, servicesAndPOIs[$parent.$index][0] )" style="cursor: pointer" />
            <img ng-show="cellValue == 'UP_TO_DATE'" src="<c:url value='/resources/img/cacheManagement_{{cellValue}}.jpg'/>" />
         </td>
      </tr>
      <tr style='background-color: rgb(207,205,177);'>
         <td><font color="red"><b>The forgotten ones</b></font></td>
         <td ng-show="forgottenOne == 'true'" ng-repeat="forgottenOne in forgottenOnes track by $index" ng-click="showForgottenOnes(servers[$index].n)" style="cursor: pointer; text-align: center;">Check!</td>
         <td ng-show="forgottenOne == 'false'" ng-repeat="forgottenOne in forgottenOnes track by $index" style="text-align: center;">none</td>
      </tr>
   </table>
</div>