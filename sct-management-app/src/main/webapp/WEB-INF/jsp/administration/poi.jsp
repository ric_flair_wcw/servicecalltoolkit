<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script src="<c:url value='/resources/js/administration-poi-list.controller.js'/>"></script>
<script src="<c:url value='/resources/js/administration-poi-data.controller.js'/>"></script>
<script src="<c:url value='/resources/js/directives/customTable.js'/>"></script>
<script src="<c:url value='/resources/js/directives/editableCellTextbox.js'/>"></script>
<script src="<c:url value='/resources/js/directives/editableCellCombobox.js'/>"></script>

<div ng-app="SCTApp" ng-controller="administration-poi-list.controller">

    <div class="list_main_title">POIs</div>

   <custom-table></custom-table>

</div>