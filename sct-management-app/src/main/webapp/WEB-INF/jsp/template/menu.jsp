<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%-- <link rel="stylesheet" href="<c:url value='/resources/css/menu.css'/>"/> --%>

<div class="menu_background_gradient">
<table width="100%">
   <sec:authorize access="hasAnyRole('ROLE_ADMINSTRATOR')">
   <tr>
      <td class="menuItem"> 1. Administration </td>
   </tr>
   <tr>
      <td class="menuItem"> <a href="<c:url value='/administration/service/main'/>" class="menuItem">&nbsp;&nbsp;&nbsp;a) Services</a> </td>
   </tr>
   <tr>
      <td class="menuItem"> <a href="<c:url value='/administration/poi/main'/>" class="menuItem">&nbsp;&nbsp;&nbsp;b) POIs</a> </td>
   </tr>
   <tr>
      <td class="menuItem"> <a href="<c:url value='/administration/server/main'/>" class="menuItem">&nbsp;&nbsp;&nbsp;c) Servers</a> </td>
   </tr>
   <tr>
      <td class="menuItem"> <a href="<c:url value='/administration/cacheManagement/main'/>" class="menuItem">&nbsp;&nbsp;&nbsp;d) Cache management</a> </td>
   </tr>
   <tr>
      <td class="menuItem"> <a href="<c:url value='/serviceDiscovery/serviceDiscovery/main'/>" class="menuItem">2. Service discovery</a> </td>
   </tr>
   </sec:authorize>
   <tr>
      <td class="menuItem"> <a href="<c:url value='/statistics/statistics/main'/>" class="menuItem">3. Monitoring</a> </td>
   </tr>
</table>
</div>