<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="footer_background_gradient">
<table width="100%" height="100%">
    <tr>
        <td align="left" valign="middle" width="50%" style="padding: 8px;"><%= new java.text.SimpleDateFormat("HH:mm:ss  dd/MM/yyyy").format(new java.util.Date()) %></td>
        <sec:authorize access="authenticated">
        <td align="right" valign="middle" width="50%" style="padding: 4px;">
            User: <b><sec:authentication property="principal.username" /></b>
            <c:url var="logoutUrl" value="/logout"/>
			<form action="${logoutUrl}" method="post">
			  <input type="submit" value="Log out" />
<%-- 			  <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/> --%>
			</form>
        </td>
        </sec:authorize>
    </tr>
</table>
</div>