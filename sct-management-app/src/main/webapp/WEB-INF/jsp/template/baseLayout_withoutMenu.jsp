<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<html>

<head>
   <title><tiles:insertAttribute name="title" ignore="true"/></title>
   <link rel="stylesheet" href="<c:url value='/resources/css/bootstrap.css'/>"/>
   <link rel="stylesheet" href="<c:url value='/resources/css/directive.css'/>"/>
   <link rel="stylesheet" href="<c:url value='/resources/css/general.css'/>"/>

   <script src="<c:url value='/resources/js/jquery/jquery-1.8.3.min.js'/>" type="text/javascript"></script>
   <script src="<c:url value='/resources/js/jquery/jquery-ui-1.11.4.custom/jquery-ui.js'/>" type="text/javascript"></script>
   <link rel="stylesheet" type="text/css" href="<c:url value='/resources/js/jquery/jquery-ui-1.11.4.custom/jquery-ui.css'/>"/>
   
   <sec:authorize access="authenticated">
   <script src="<c:url value='/resources/js/angularjs/angular.min.js'/>"></script>
   <script src="<c:url value='/resources/js/angularjs/angular-resource.min.js'/>"></script>
   <script src="<c:url value='/resources/js/angularjs/ui-bootstrap-tpls-0.13.3.js'/>"></script>
<%--    <script src="<c:url value='/resources/js/angularjs/bootstrap.js'/>"></script> --%>
   
   <script src="<c:url value='/resources/js/SCTApp.js'/>"></script>
   <script src="<c:url value='/resources/js/utils/utils.js'/>"></script>
   <script src="<c:url value='/resources/js/utils/communication.js'/>"></script>
   </sec:authorize>
   
<%--    <sec:csrfMetaTags /> --%>
</head>

<body>

   <table align="center" width="100%" height="100%">
      <tr height="90px">
         <td align="center"><tiles:insertAttribute name="header"/></td>
      </tr>
      <tr>
         <td valign="top" align="center">
            <div style="position:relative;">
               <div id="loadingDiv" style="display:none" ng-show=false>
                  <img src="<c:url value='/resources/img/loading.gif'/>" class="ajax-loader"/>
               </div>
               <tiles:insertAttribute name="body"/>
            </div>
         </td>
      </tr>
      <tr height="70px">
         <td align="center"><tiles:insertAttribute name="footer"/></td>
      </tr>
   </table>

</body>

</html>