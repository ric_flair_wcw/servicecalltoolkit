
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<style>
table {
    border-collapse:collapse;
    border-spacing: 0; 
    margin: 0 auto;
}

table td {
    padding: 4px;
}
</style>

<br><br><br><br>

<table border="1" ><tr><td style="padding: 20px;">
<form name="loginForm" id="loginForm" action="<c:url value='/login' />" method="POST">

    <table width="100%">
        <tr>
            <td colspan="2" align="center" width="90%"><h3>Please log in!</h3></td>                
        </tr>
        <tr>
            <td align="right" width="40%">User:</td>
            <td align="left" width="50%"><input type="text" name="username" value=""></td>
        </tr>
        <tr>
            <td align="right">Password:</td>
            <td align="left"><input type="password" name="password" /></td>
        </tr>
        <tr>
            <td colspan="2" align="center" width="100%">
                <input name="submit" type="submit" value="Login" />
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center" width="100%"><b>${loginErrorText}${logoutText}</b></td>
        </tr>
    </table>
    
<%--     <input type="hidden" id="_csrf" name="${_csrf.parameterName}" value="" />  --%>
</form>
</td></tr></table>


<script>
//     var token = getMetaContent("_csrf");

//     document.getElementById("_csrf").value = token;

    function getMetaContent(propName) {
        var metas = document.getElementsByTagName('meta');
        for (i = 0; i < metas.length; i++) {
            if (metas[i].getAttribute("name") == propName) {
                return metas[i].getAttribute("content");
            }
        }
        return "";
    }
</script>