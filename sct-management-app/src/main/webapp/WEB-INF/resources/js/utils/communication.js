// service class for handling the communication details
app.service("CommunicationService", function($rootScope, $resource, $modal, PropertyStorage) {

    // *******************************************************************
    //                              util methods
    // *******************************************************************
	// method for handling error
    this.handleError = function(error) {
    	// http://weblogs.asp.net/dwahlin/building-an-angularjs-modal-service
    	var modalDefaults = {
    		backdrop: false,
            keyboard: false,
            modalFade: true,
            size: "lg",
            templateUrl : '../../jsp/_other/showException.html',
        };
        modalDefaults.controller = function ($scope, $modalInstance) {
            xceptionClass = "";
            xceptionMessage = "";
            xceptionStackTrace = "";
            if (error.data != null) { xceptionClass = error.data.exceptionClass; }
            if (error.data != null) { xceptionMessage = error.data.exceptionMessage; }
            if (error.data != null) { xceptionStackTrace = error.data.exceptionStackTrace; }
            $scope.error = {status: error.status,
                            statusText: error.statusText,
                            exceptionClass: xceptionClass,
                            exceptionMessage: xceptionMessage,
                            exceptionStackTrace: xceptionStackTrace};
            $scope.closeShowExceptionModal = function() {
            	PropertyStorage.setErrorWindowDisplayed(false);
                $modalInstance.dismiss('cancel');
            };
        };
    	
        if (!PropertyStorage.isErrorWindowDisplayed()) {
        	$modal.open(modalDefaults);
        	PropertyStorage.setErrorWindowDisplayed(true);
        }
    	
    };
    
    // to show the loading modal
    this.showLoadingModal = function() {
        var howMany = PropertyStorage.getHowManyTimesRequestWasFired();
        if (howMany == 0) {
            $('#loadingDiv').show();
        }
        howMany++;
        PropertyStorage.setHowManyTimesRequestWasFired(howMany);
    };
    
    // to hide the loading modal
    this.hideLoadingModal = function() {
        var howMany = PropertyStorage.getHowManyTimesRequestWasFired();
        howMany--;
        if (howMany == 0) {
            $('#loadingDiv').hide();
        }
        PropertyStorage.setHowManyTimesRequestWasFired(howMany);
    };

    var ms = this;
    
    // *******************************************************************
    //                          communication methods
    // *******************************************************************
    // method for saving
    this.save  = function(url, request) {
        return ms.save2(url, request, "saveFinishedEvent");
    };
    this.save2 = function(url, request, broadcastString) {
    	return ms.save3(url, request, broadcastString, null);
	};
    this.save3 = function(url, request, successfulBroadcastString, unsuccessfulBroadcastString) {
    	ms.showLoadingModal();
//    	// handling the CSRF token
//    	var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
//    	var csrfHeader = $("meta[name='_csrf_header']").attr("content");
//		var csrfToken = $("meta[name='_csrf']").attr("content");
//		// calling the save
//    	var communicationManager = $resource(url, {}, {
//            save: {
//                method: "POST",
//                headers: { 
//                	"X-CSRF-TOKEN": csrfToken
//                }
//            }
//    	});
    	
    	var communicationManager = $resource(url);
    	var valueOfTheSave = communicationManager.save(request);
        valueOfTheSave.$promise.then(
            //success
            function( value ) {
                ms.hideLoadingModal();
                // firing an event that is caught in the list controller
                $rootScope.$broadcast(successfulBroadcastString);
            },
            //error
            function( error ) {
                ms.hideLoadingModal();
                ms.handleError(error);
                if (unsuccessfulBroadcastString != null) {
                	$rootScope.$broadcast(unsuccessfulBroadcastString);
                }
            }
        );
        return valueOfTheSave;
	};

    // method for querying (no method callback)	
	this.query = function(url) {
        return ms.query2(url, function() {});
	};
    // method for querying (with method callback)	
	this.query2 = function(url, funcToBeCalled) {
		ms.showLoadingModal();
    	var communicationManager = $resource(url);
    	var valueOfTheQuery = communicationManager.query();
        valueOfTheQuery.$promise.then(
            //success
            function( value ) {
                ms.hideLoadingModal();
                funcToBeCalled();
            },
            //error
            function( error ) {
            	ms.hideLoadingModal();
                ms.handleError(error);
            }
        );
        return valueOfTheQuery;
	};
    
    // method for deleting
	this.remove = function(url, request) {
		ms.showLoadingModal();
		
//    	// handling the CSRF token
//    	var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
//    	var csrfHeader = $("meta[name='_csrf_header']").attr("content");
//		var csrfToken = $("meta[name='_csrf']").attr("content");
//		// calling the save
//    	var communicationManager = $resource(url, {}, {
//    		remove: {
//                method: "DELETE",
//                headers: { 
//                	"X-CSRF-TOKEN": csrfToken
//                }
//            }
//    	});
		
		var communicationManager = $resource(url);
    	communicationManager.remove(request).$promise.then(
            //success
            function( value ) {
            	ms.hideLoadingModal();
                // firing an event that is caught in the list controller
                $rootScope.$broadcast("removeFinishedEvent");
            },
            //error
            function( error ) {
            	ms.hideLoadingModal();
                ms.handleError(error);
            }
        );
	};

    // method for getting
	this.get2 = function(url) {
        return ms.get(url, null, function() {});
	};
	this.get = function(url, request, funcToBeCalled) {
		ms.showLoadingModal();
    	var communicationManager = $resource(url);
    	if (request == null) {
    		var valueOfTheGet = communicationManager.get();
    	} else {
    		var valueOfTheGet = communicationManager.get(request);
    	}
        valueOfTheGet.$promise.then(
            //success
            function( value ) {
            	ms.hideLoadingModal();
                funcToBeCalled();
            },
            //error
            function( error ) {
            	ms.hideLoadingModal();
                ms.handleError(error);
            }
        );
        return valueOfTheGet;
	};
	
});