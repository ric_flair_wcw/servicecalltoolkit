// service class for String operations
app.service("StringUtils", function() {

    this.isEmpty = function(value) {
        if (value === undefined || value.length === 0) {
            return true;
        } else {
            return false;
        }
    };
    
});


// I don't know ... for any kind of util methods
app.service("AKOUtils", function() {

    this.getToken = function() {
        return new Date().getTime();
    };
    
});


// for sharing variable between controllers
app.service('PropertyStorage', function () {
    var property = 'First';
    var howManyTimesRequestWasFired = 0;
    var isErrorWindowDisplayed = false;

    return {
        getProperty: function () {
            return property;
        },
        setProperty: function(value) {
            property = value;
        },
        getHowManyTimesRequestWasFired: function() {
            return howManyTimesRequestWasFired;
        },
        setHowManyTimesRequestWasFired: function(value) {
            howManyTimesRequestWasFired = value;
        },
        isErrorWindowDisplayed: function() {
        	return isErrorWindowDisplayed;
        },
        setErrorWindowDisplayed: function(value) {
        	isErrorWindowDisplayed = value;
        }
    };
});