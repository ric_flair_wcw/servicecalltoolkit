app.controller("administration-server-data.controller", function ($scope, $resource, $modalInstance, $modal, dataWindowParams, CommunicationService, PropertyStorage) {
    $scope.errorMessage = "";
	$scope.operation = dataWindowParams.operation;
	
	if ($scope.operation === "edit") {
		// querying the server
		$scope.server = CommunicationService.get(":serverId", 
				                                  {serverId: dataWindowParams.id},
				                                  function() {} );
	} else if ($scope.operation === "new") {
		$scope.server = {};
	}
        
    /* ************************************************************
                           modal window events
       ************************************************************ */
    $scope.save = function() {
        // save
    	CommunicationService.save(
    			"save",
    			{ "id": $scope.server.id,
    			  "name": $scope.server.name,
		          "url": $scope.server.url}		
    	);
    	
        $modalInstance.close("ok");
    };
    
    $scope.cancel = function() {
        $modalInstance.dismiss("cancel");
    };

});
