app.controller("statistics.controller", function ($scope, CommunicationService, AKOUtils) {

	
    /* ************************************************************
                        other functions
       ************************************************************ */
	function format(datee) {
	    var dd = datee.getDate();
	    var mm = datee.getMonth()+1; //January is 0!
	    var yyyy = datee.getFullYear();
	    
	    if (dd < 10){
	        dd= "0" + dd
	    } 
	    if (mm < 10){
	        mm= "0" + mm
	    } 
	    return yyyy + "-" + mm + "-" + dd;
	}

	function parse(dateString,hour) {
		try {
			var datee = new Date(dateString);
		} catch(err) {
			window.alert("The string '" + dateString + "' cannot be converted to date!");
		}
		datee.setHours(hour);
		
	    return datee;
	}
	
	$scope.prettyPrintArray = function(tomb) {
		if (tomb == null) {
			return "";
		}
		var result = "" + tomb;
		return result.replace(/,\n$/, "");
	}
	
	$scope.changeTypeInComparison = function() {
		// clear the right list
		$scope.store.selectedNamesSelectTo = [];
		// clear the the left list (service)
		$scope.store.selectFromServiceData = [];
		for (var i = 0; i < $scope.services.length; i++) {
			$scope.store.selectFromServiceData.push($scope.services[i].n);
		}
		// clear the the left list (POI)
		$scope.store.selectFromPOIData = [];
		for (var i = 0; i < $scope.pois.length; i++) {
			$scope.store.selectFromPOIData.push($scope.pois[i].n);
		}
	}
	
	function getKeys(obj) {
        if (!obj) {
            return [];
        }
        return Object.keys(obj);
    };
    
    $scope.addToSelectedServices = function() {
    	// add to right array
    	for (var i = 0; i < $scope.store.selectedNamesSelectFromService.length; i++) {
    		$scope.store.selectedNamesSelectTo.push($scope.store.selectedNamesSelectFromService[i]);
    	}
    	// remove from left array
    	for (var i = 0; i < $scope.store.selectedNamesSelectFromService.length; i++) {
    		var index = $scope.store.selectFromServiceData.indexOf($scope.store.selectedNamesSelectFromService[i]);
    		$scope.store.selectFromServiceData.splice(index, 1);
    	}
    	$scope.store.selectedNamesSelectFromService = [];
    };
    
    $scope.removeFromSelectedServices = function() {
    	// add to left array
    	for (var i = 0; i < $scope.store.selectedNamesSelectToService.length; i++) {
    		$scope.store.selectFromServiceData.push($scope.store.selectedNamesSelectToService[i]);
    	}
    	// remove right left array
    	for (var i = 0; i < $scope.store.selectedNamesSelectToService.length; i++) {
    		var index = $scope.store.selectedNamesSelectTo.indexOf($scope.store.selectedNamesSelectToService[i]);
    		$scope.store.selectedNamesSelectTo.splice(index, 1);
    	}
    	$scope.store.selectedNamesSelectToService = [];
    };

    $scope.addToSelectedPOIs = function() {
    	// add to right array
    	for (var i = 0; i < $scope.store.selectedNamesSelectFromPOI.length; i++) {
    		$scope.store.selectedNamesSelectTo.push($scope.store.selectedNamesSelectFromPOI[i]);
    	}
    	// remove from left array
    	for (var i = 0; i < $scope.store.selectedNamesSelectFromPOI.length; i++) {
    		var index = $scope.store.selectFromPOIData.indexOf($scope.store.selectedNamesSelectFromPOI[i]);
    		$scope.store.selectFromPOIData.splice(index, 1);
    	}
    	$scope.store.selectedNamesSelectFromPOI = [];
    };
    
    $scope.removeFromSelectedPOIs = function() {
    	// add to left array
    	for (var i = 0; i < $scope.store.selectedNamesSelectToPOI.length; i++) {
    		$scope.store.selectFromPOIData.push($scope.store.selectedNamesSelectToPOI[i]);
    	}
    	// remove right left array
    	for (var i = 0; i < $scope.store.selectedNamesSelectToPOI.length; i++) {
    		var index = $scope.store.selectedNamesSelectTo.indexOf($scope.store.selectedNamesSelectToPOI[i]);
    		$scope.store.selectedNamesSelectTo.splice(index, 1);
    	}
    	$scope.store.selectedNamesSelectToPOI = [];
    };

    /* ************************************************************
                              graph functions
       ************************************************************ */
	
	// loading the graph for service
	$scope.loadGraphService = function() {
		var startDatee = parse($("#startdate-picker-service").val(), $scope.store.selectedStartTimeService);
		var endDatee = parse($("#enddate-picker-service").val(), $scope.store.selectedEndTimeService);
		if (startDatee.getTime() >= endDatee.getTime()) {
			alert("The end date must be earlier the end date!");
			return;
		}
		if ((endDatee.getTime() - startDatee.getTime()) / 1000 / 60 / 60 / 24 > 366) {
			alert("The difference between the end date and start date cannot be more the 366 days!");
			return;
		}
		$scope.statsService = CommunicationService.get("getStatistics/service/" + $scope.store.selectedServiceName + "/" + 
				                                       $("#startdate-picker-service").val() + " " + $scope.store.selectedStartTimeService + "/" + 
				                                       $("#enddate-picker-service").val() + " " + $scope.store.selectedEndTimeService, 
				                                null,
                                                function() {
													$scope.showGraph("service");
                                                 });
	};
	
	// loading the graph for POI
	$scope.loadGraphPOI = function() {
		var startDatee = parse($("#startdate-picker-poi").val(), $scope.store.selectedStartTimeService);
		var endDatee = parse($("#enddate-picker-poi").val(), $scope.store.selectedEndTimeService);
		if (startDatee.getTime() >= endDatee.getTime()) {
			alert("The end date must be earlier the end date!");
			return;
		}
		if ((endDatee.getTime() - startDatee.getTime()) / 1000 / 60 / 60 / 24 > 366) {
			alert("The difference between the end date and start date cannot be more the 366 days!");
			return;
		}
		$scope.statsPOI = CommunicationService.get("getStatistics/POI/" + $scope.store.selectedPOIName + "/" + 
				                                    $("#startdate-picker-poi").val() + " " + $scope.store.selectedStartTimePOI + "/" + 
				                                    $("#enddate-picker-poi").val() + " " + $scope.store.selectedEndTimePOI, 
				                                null,
                                                function() {
													$scope.showGraph("POI");
                                                 });
	};

	// loading the graph for POI
	$scope.loadGraphComparison = function() {
		var startDatee = parse($("#startdate-picker-comparison").val(), $scope.store.selectedStartTimeComparion);
		var endDatee = parse($("#enddate-picker-comparison").val(), $scope.store.selectedEndTimeComparion);
		if (startDatee.getTime() >= endDatee.getTime()) {
			alert("The end date must be earlier the end date!");
			return;
		}
		if ((endDatee.getTime() - startDatee.getTime()) / 1000 / 60 / 60 / 24 > 30) {
			alert("The difference between the end date and start date cannot be more the 30 days!");
			return;
		}
		if ($scope.store.selectedNamesSelectTo.length > 5) {
			alert("You cannot select more than 5 services / POIs!");
			return;
		}
		
		$scope.statsComparison = CommunicationService.save2("getStatisticsForComparison", 
                                   {startDate: $("#startdate-picker-comparison").val() + " " + $scope.store.selectedStartTimeComparison, 
                                	endDate: $("#enddate-picker-comparison").val() + " " + $scope.store.selectedEndTimeComparison,
                                	type: $scope.store.typeInComparison,
                                	names: $scope.store.selectedNamesSelectTo},
                                   "getStatisticsForComparisonFinishedEvent" + $scope.token);
	};
	
	$scope.showGraph = function(type) {
		var suffix = "-" + type;
		var plotData = [];
		
		if (type == "comparison") {
			var keys = getKeys($scope.statsComparison);
			for (var key in keys) {
				if (keys[key] != "$promise" && keys[key] != "$resolved") {
					plotData.push({ data: $scope.statsComparison[keys[key]], label: keys[key]});
				}
			}
		} else {
			if (type == "service") {
				var successfulList = $scope.statsService["successfulList"];
				var unsuccessfulList = $scope.statsService["unsuccessfulList"];
			} else if (type == "POI") {
				var successfulList = $scope.statsPOI["successfulList"];
				var unsuccessfulList = $scope.statsPOI["unsuccessfulList"];
			}
			plotData = [ { data: successfulList, label: "successful"},
		       			 { data: unsuccessfulList, label: "unsuccessful"}
			           ];			
		}
		var plot = $.plot("#graph-placeholder" + suffix, plotData , {
   			series: {
   				lines: {
   					show: true
   				},
   				points: {
   					show: true
   				}
   			},
   			legend: {
   			    show: true,
   			    container: "#legend-container-"+type
   			},
   			grid: {
   				hoverable: true,
   				clickable: true,
   				markings: weekendAreas
   			},
			xaxis: {
				mode: "time",
				tickLength: 5
			},
			yaxis: {
				min: 0
			},			
			selection: {
				mode: "x"
			},
   		});

		var overview = $.plot("#graph-overview" + suffix, plotData, {
			series: {
				lines: {
					show: true,
					lineWidth: 1
				},
				shadowSize: 1
			},
			xaxis: {
				mode: "time"
			},
			legend: {
   			    show: false
   			},
			yaxis: {
				ticks: [],
				min: 0,
				autoscaleMargin: 0.1
			},
			selection: {
				mode: "x"
			}
		});
		
		$("#graph-placeholder" + suffix).bind("plotselected", function (event, ranges) {
			// do the zooming
			$.each(plot.getXAxes(), function(_, axis) {
				var opts = axis.options;
				opts.min = ranges.xaxis.from;
				opts.max = ranges.xaxis.to;
			});
			plot.setupGrid();
			plot.draw();
			plot.clearSelection();

			// don't fire event on the overview to prevent eternal loop
			overview.setSelection(ranges, true);
		});

		$("#graph-overview" + suffix).bind("plotselected", function (event, ranges) {
			plot.setSelection(ranges);
		});		

		$("<div id='graph-tooltip" + suffix + "'></div>").css({
			position: "absolute",
			display: "none",
			border: "1px solid #fdd",
			padding: "2px",
			"background-color": "#fee",
			opacity: 0.80
		}).appendTo("body");		

		$("#graph-placeholder" + suffix).bind("plothover", function (event, pos, item) {
			if (item) {
				var x = item.datapoint[0].toFixed(2), y = item.datapoint[1].toFixed(2);
				var date = new Date(parseInt(x)); 
				var dateString = date.getFullYear()+"-"+(date.getMonth()+1)+"-"+date.getDate()+" "+date.getHours()+":"+date.getMinutes();

				$("#graph-tooltip" + suffix).html(dateString + " : " + parseInt(y) + " '" + item.series.label + "' request(s)")
					.css({top: item.pageY+5, left: item.pageX+5})
					.fadeIn(200);
			} else {
				$("#graph-tooltip" + suffix).hide();
			}
		});
	};
	
	
	function weekendAreas(axes) {
		var markings = [], d = new Date(axes.xaxis.min);
		// go to the first Saturday
		d.setUTCDate(d.getUTCDate() - ((d.getUTCDay() + 1) % 7))
		d.setUTCSeconds(0);
		d.setUTCMinutes(0);
		d.setUTCHours(0);
		var i = d.getTime();
		// when we don't set yaxis, the rectangle automatically
		// extends to infinity upwards and downwards
		do {
			markings.push({ xaxis: { from: i, to: i + 2 * 24 * 60 * 60 * 1000 } });
			i += 7 * 24 * 60 * 60 * 1000;
		} while (i < axes.xaxis.max);
		return markings;
	};
	
    /* ************************************************************
                                   main
       ************************************************************ */
	$scope.store = {};
	$scope.store.selectedStartTimeService = 8;
	$scope.store.selectedEndTimeService = 20;
	$scope.store.selectedStartTimePOI = 8;
	$scope.store.selectedEndTimePOI = 20;
	$scope.store.selectedStartTimeComparison = 8;
	$scope.store.selectedEndTimeComparison = 20;
	$scope.store.typeInComparison = "service";
	$scope.store.selectedNamesSelectTo = [];
	$scope.store.selectedNamesSelectFromService = [];
	$scope.store.selectedNamesSelectToService = [];
	$scope.store.selectedNamesSelectFromPOI = [];
	$scope.store.selectedNamesSelectToPOI = [];
	$scope.token = AKOUtils.getToken();
	
	// querying the service list
	$scope.services = CommunicationService.query2("../../administration/service/list",
                                                  function() {
													  $scope.store.selectedServiceName = $scope.services[0].n;
													  $scope.store.selectFromServiceData = [];
													  for (var i = 0; i < $scope.services.length; i++) {
														  $scope.store.selectFromServiceData.push($scope.services[i].n);
                                                      }
                                                  });
	// getting the poi list
	$scope.pois = CommunicationService.query2("../../administration/poi/list",
										        function() {
												  $scope.store.selectedPOIName = $scope.pois[0].n;
												  $scope.store.selectFromPOIData = [];
												  for (var i = 0; i < $scope.pois.length; i++) {
													  $scope.store.selectFromPOIData.push($scope.pois[i].n);
										          }
    });
	
    // showing the graph when the data arrived
    $scope.$on("getStatisticsForComparisonFinishedEvent" + $scope.token, function(event) {
    	$scope.showGraph("comparison");
    });
	
});
