app.controller("administration-poi-list.controller", function ($scope, $resource, CommunicationService) {

	// defining the info to build the custom table
	$scope.customTable = new Object();
	$scope.customTable.headers = [
		{column: "n", width: "150px", label: "Name"},
		{column: "cnt", width: "100px", label: "Count of extracts"},
		{column: "s", width: "200px", label: "Service"}
	];
	$scope.customTable.filters = {};
	$scope.customTable.data = "poi";
	$scope.customTable.dataWindow = "../../jsp/administration/poiDataWindow.html";
	
	
	// refreshing the poi list
    $scope.reloadList = function() {
        $scope.customTableData = CommunicationService.query("list");
    };
    
    // deleting a poi
    $scope.remove = function(selectedId) {
    	// deleting the item
    	CommunicationService.remove("delete/:id", {id: selectedId});
    };
    
    // when the save happened (the response arrived from the server) an event is fired that is captured here
    $scope.$on("saveFinishedEvent", function(event){
    	$scope.reloadList();
    });

    // similarly at delete
    $scope.$on("removeFinishedEvent", function(event){
    	$scope.reloadList();
    });

	// querying the list of POIs
	$scope.reloadList();
    
});