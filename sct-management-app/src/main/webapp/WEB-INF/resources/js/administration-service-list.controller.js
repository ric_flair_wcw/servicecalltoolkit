app.controller("administration-service-list.controller", function ($scope, $resource, CommunicationService) {

	// defining the info to build the custom table
	$scope.customTable = new Object();
	$scope.customTable.headers = [
		{column: "n", width: "150px", label: "Name"},
		{column: "st", width: "100px", label: "Service Type"}
	];
	$scope.customTable.filters = {};
	$scope.customTable.data = "service";
	$scope.customTable.dataWindow = "../../jsp/administration/serviceDataWindow.html";
	
	
	// refreshing the service list
    $scope.reloadList = function() {
        $scope.customTableData = CommunicationService.query("list");
    };
    
    // deleting a service
    $scope.remove = function(selectedId) {
    	// deleting the item
    	CommunicationService.remove("delete/:id", {id: selectedId});
    };
    
    // when the save happened (the response arrived from the server) an event is fired that is captured here
    $scope.$on("saveFinishedEvent", function(event){
    	$scope.reloadList();
    });

    // similarly at delete
    $scope.$on("removeFinishedEvent", function(event){
    	$scope.reloadList();
    });

	// querying the list of services
	$scope.reloadList();
    
});