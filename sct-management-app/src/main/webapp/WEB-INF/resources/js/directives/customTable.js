app.filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int
        return input.slice(start);
    };
});

app.directive("customTable", function($compile, $document, $modal) {

    function link($scope, $element, $attrs) {
        // variables for pagination
        $scope.currentPage = 0;
        $scope.pageSize = 20;
        $scope.numberOfPages=function(){
            return Math.ceil($scope.filteredCustomTableData.length/$scope.pageSize);                
        };
        
        // for sorting
        $scope.sort = {
            column: '',
            descending: false
        };
        
        $scope.changeSorting = function(columnName, colId) {
            var sort = $scope.sort;
            if (sort.column == columnName) {
                sort.descending = !sort.descending;
            } else {
                sort.column = columnName;
                sort.descending = false;
            }
            // adding the the arrow
            $("#ajdiof_sorticon").remove();
            if (!sort.descending) {
            	$('#'+colId).append("<span id='ajdiof_sorticon'>&nbsp;<img src='../../resources/img/sortAsc.gif'/></span>");
            } else {
            	$('#'+colId).append("<span id='ajdiof_sorticon'>&nbsp;<img src='../../resources/img/sortDesc.gif'/></span>");
            }
        };
        
        // for selecting a row
        $scope.idSelectedRecord = null;
        $scope.setSelected = function(idSelectedRecord) {
            $scope.idSelectedRecord = idSelectedRecord;
        };
        
        // for showing the data modal window for new and edit
        $scope.showDataWindow = function(operation) {
            $scope.opts = {
                backdrop: false,
                backdropClick: true,
                dialogFade: true,
                keyboard: false,
                size: "lg",
                //templateUrl : '../../resources/' + $scope.customTable.data.toLowerCase() + 'DataWindow.html',
                templateUrl : $scope.customTable.dataWindow,
                controller : 'administration-' + $scope.customTable.data.toLowerCase() + '-data.controller',
                resolve: {} // empty storage
            };
            $scope.opts.resolve.dataWindowParams = function() {
                return angular.copy( {id: $scope.idSelectedRecord,
                                      operation: operation} 
                );
            };
            var modalInstance = $modal.open($scope.opts);
            modalInstance.result.then(function() {
                //on ok button press
                //$scope.reloadList();   -> I use broadcast-emit
            }, function() {
                //on cancel button press
            });
        };
        
        // deleting an item
        $scope.deleteItem = function()  {
            if (confirm("Are you sure?")) {
                $scope.remove($scope.idSelectedRecord);
            }
        };
        
        var arrayLength = $scope.customTable.headers.length;
        var html = "<table class='custom_table_directive' > <tr>";
        // 1. building the header
        for (var i = 0; i < arrayLength; i++) {
            html += "<th id='customTable_col" + i + "' ng-click='changeSorting(\"" + $scope.customTable.headers[i].column + "\", \"customTable_col" + i + "\")'> " + $scope.customTable.headers[i].label + " </th>";
        }
        // 2. building the filters
        html += "</tr> <tr> <td style='width: {{ h.width }} px;white-space: nowrap;' ng-repeat='h in customTable.headers'>";
        html += "<input style='width: 100%; display: block;' type='text' ng-model='customTable.filters[h.column]'> </td> </tr>";
        // 3. building the table rows
        // sample: <tr ng-repeat='record in (filteredCustomTableData = (customTableData | filter:{name:customTable.filters.name} | filter:{serviceType:customTable.filters.serviceType}) | startFrom:currentPage*pageSize | limitTo:pageSize | orderBy:sort.column:sort.descending')>"
        html += "<tr ng-repeat='record in (filteredCustomTableData = (customTableData";
        
        var htmlTableTd = "";
        // first creating the filters in the ngRepeat (| filter:{name:customTable.filters.name} | filter:{serviceType:customTable.filters.serviceType}| ...)
        // and after that adding the real table rows -> 
        for (var i = 0; i < arrayLength; i++) {
            html += " | filter:{" + $scope.customTable.headers[i].column + ":customTable.filters." + $scope.customTable.headers[i].column + "}";
            htmlTableTd += "<td>{{ record." + $scope.customTable.headers[i].column + " }}</td>";
        }
        html += " | orderBy:sort.column:sort.descending) | startFrom:currentPage*pageSize | limitTo:pageSize)' ";
        html += "ng-click='setSelected(record.id)' ng-class='{selected: record.id === idSelectedRecord}'>";
        html += htmlTableTd;
        
        // adding the New, Edit, Delete buttons
        // http://stackoverflow.com/questions/16265844/invoking-modal-window-in-angularjs-bootstrap-ui-using-javascript
        html += "</tr> <tr style='background-color: rgb(207,205,177);'> <td colspan='" + arrayLength + "'> <div style='float:left;width:50%;text-align:left'>";
        
        html += "<button type='submit' ng-click='showDataWindow(\"new\")' class='btn btn-primary'>New</button>&nbsp;";
        html += "<button type='submit' ng-disabled='idSelectedRecord == null' ng-click='showDataWindow(\"edit\")' class='btn btn-primary'>Edit</button>&nbsp;";
        html += "<button type='submit' ng-disabled='idSelectedRecord == null' ng-click='deleteItem()' class='btn btn-primary'>Delete</button>";
        html += "</div> <div style='float:right;width:50%;text-align:right'>";

        // adding the pagination buttons
        html += "<button type='submit' ng-disabled='currentPage == 0' ng-click='currentPage=currentPage-1' class='btn btn-primary'>Previous</button>&nbsp;{{currentPage+1}}/{{numberOfPages()}}&nbsp;";
        html += "<button type='submit' ng-disabled='currentPage >= filteredCustomTableData.length/pageSize - 1' ng-click='currentPage=currentPage+1' class='btn btn-primary'>Next</button>";
        html += "</div></td> </tr> </table>";
        
        $element.html(html);
        $compile($element.contents())($scope);
    }
    
    function capitalize(s) {
        return s && s[0].toUpperCase() + s.slice(1);
    }
    
    return {
        restrict: "E",
        link: link
    };
    
});