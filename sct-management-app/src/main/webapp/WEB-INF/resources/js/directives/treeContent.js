app.directive("treeContent", function($compile, $document) {
	
	function link($scope, $element, $attrs) {
		$scope.value.$promise.then(
            //success
            function( value ) {
               funcToBeCalled();
            },
            //error
            function( error ) {
                ms.handleError(error);
            }
        );
		var html = getHtml($scope.value, true);

        $element.html(html);
        $compile($element.contents())($scope);
	}
	
	function getHtml(node, isAdd) {
		if (isAdd) {
			var html = "<ul id='checkboxTreeForPOI'>";
		} else {
			var html = "<ul>";
		}
		for(var key in getKeys(node)) {
			html += "<li><input type='checkbox' id='" + key + "'><label>" + key + "</label>";
			html += getHtml(node[key], false);
		}
		html += "</ul>";
		return html;
	}
	
    function getKeys(obj) {
        if (!obj) {
            return [];
        }
        return Object.keys(obj);
    };
	
	
	
	return {
		restrict: "E",
		scope: {
            value: "="
        },		
		link: link
	};
});