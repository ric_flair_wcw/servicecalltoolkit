// http://jsfiddle.net/joshdmiller/NDFHg/
app.directive("editableCellTextbox", function($compile, $document, $modal, StringUtils) {

	function link($scope, $element, $attrs) {
        $scope.oldValue = $scope.value;
        // getting the main div element (see the template below)
        var mainDiv = angular.element( $element.children()[0] );
        // getting the input box element (see the template below)
        var inputElement = angular.element( $element.children()[1] );
        // getting the delete icon element (see the template below)
        var xElement = angular.element( mainDiv.children()[1] );
        $element.addClass( "editableCellTextbox" );
        // it shows if delete icon has to be displayed (mouse is on the cell)
        $scope.deleteIconVisible = false;
        
        // when clicking on the cell
        $scope.editTheCell = function() {
            $element.addClass("active");
            hossz = inputElement[0].value.length * 12;
            if (hossz > 560) {
            	hossz = 560;
            }
            inputElement[0].style.width = hossz + "px"
            inputElement[0].focus();
        };
        
        // when the mouse is being moved on the cell (the delete icon has to be displayed)
        $scope.showDeleteIcon = function(show) {
            if ($scope.displayDeleteIcon === undefined || $scope.displayDeleteIcon == true) {
                $scope.deleteIconVisible = show;
            }
        };
        
        // mouse event -> clicked outside the cell (input box) -> TODO: Enter cannot be used the the moment
        inputElement.prop( "onblur", function() {
            if (StringUtils.isEmpty($scope.value)) {
                alert("It cannot be empty!");
                inputElement.val($scope.oldValue);
                $scope.value = $scope.oldValue;
                inputElement[0].focus();
            } else {
                //the list has to be refreshed
                $scope.update({val: $scope.value});
                $element.removeClass("active");
            }
        });
        // supporting the Enter keypress
        inputElement[0].onkeypress = function(event){
            if(event.keyCode == 13){
            	//the list has to be refreshed
                $scope.update({val: $scope.value});
                $element.removeClass("active");
            }
        };
	}
	
    
	return {
		restrict: "E",
        scope: {
            value: "=",
            delete: "&",
            update: "&",
            displayDeleteIcon: "="
        },
        template: 
            "<span ng-mouseover='showDeleteIcon(true)' ng-mouseleave='showDeleteIcon(false)' style='white-space:nowrap;'>"+
            "  <span ng-click='editTheCell()' ng-bind='value'></span>"+
            "  <span ng-click='delete()' style='color:gray' ng-show='deleteIconVisible'>X</span>"+
            "  <span ng-show='!deleteIconVisible'>&nbsp;</span>"+
            "</span>"+
            "<input ng-model='value'></input>",
		link: link
	};
	
});