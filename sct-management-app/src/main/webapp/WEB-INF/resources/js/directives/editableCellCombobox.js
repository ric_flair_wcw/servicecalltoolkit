// http://jsfiddle.net/joshdmiller/NDFHg/
app.directive("editableCellCombobox", function($compile, $document, $modal, StringUtils) {

	function link($scope, $element, $attrs) {
        $scope.oldValue = $scope.value;
        // getting the main div element (see the template below)
        var mainDiv = angular.element( $element.children()[0] );
        // getting the combo box element (see the template below)
        var comboElement = angular.element( $element.children()[1] );
        $element.addClass( "editableCellCombobox" );
        // it shows if delete icon has to be displayed (mouse is on the cell)
        $scope.deleteIconVisible = false;
        
        // when clicking on the cell
        $scope.editTheCell = function() {
            $element.addClass("active");
            comboElement[0].focus();
        };
        
        // when the mouse is being moved on the cell (the delete icon has to be displayed)
        $scope.showDeleteIcon = function(show) {
            if ($scope.displayDeleteIcon === undefined || $scope.displayDeleteIcon == true) {
                $scope.deleteIconVisible = show;
            }
        };
        
        // mouse event -> clicked outside the cell (input box) -> TODO: Enter cannot be used the the moment
        comboElement.prop( "onblur", function() {
            if (StringUtils.isEmpty($scope.value)) {
                alert("It cannot be empty!");
                comboElement[0].focus();
            } else {
                //the list has to be refreshed
                $scope.update({val: $scope.value});
                $element.removeClass("active");
            }
        });
	}
	
    
	return {
		restrict: "E",
        scope: {
            value: "=" ,
            list: "=",
            delete: "&",
            update: "&",
            displayDeleteIcon: "="
        },
        template: 
            "<span ng-mouseover='showDeleteIcon(true)' ng-mouseleave='showDeleteIcon(false)'>"+
            "  <span ng-click='editTheCell()' ng-bind='value'></span>"+
            "  <span ng-click='delete()' style='color:gray' ng-show='deleteIconVisible'>X</span>"+
            "  <span ng-show='!deleteIconVisible'>&nbsp;</span>"+
            "</span>"+
            "<select ng-model='value' ng-options='rec.value as rec.label for rec in list' required/>",
		link: link
	};
	
});