app.controller("administration-service-data.controller", function ($scope, $resource, $modalInstance, $modal, dataWindowParams, CommunicationService, PropertyStorage) {
    $scope.errorMessage = "";
	$scope.operation = dataWindowParams.operation;
	
    // querying the service type list
	$scope.serviceTypes = CommunicationService.query("../../common/masterdata/serviceType/list");
    
    // querying the http method list
	$scope.httpMethods = CommunicationService.query2("../../common/masterdata/httpMethod/list",
                                                     function() {
		                                                 $scope.service.httpMethod = $scope.httpMethods[0].value;
	                                                 });

	if ($scope.operation === "edit") {
		// querying the service
		$scope.service = CommunicationService.get(":serviceId", 
				                                  {serviceId: dataWindowParams.id},
				                                  function() {} );
	} else if ($scope.operation === "new") {
		$scope.service = {serviceType:"WEBSERVICE",
				          endpoints: [],
				          namespaces: {}};
	}
        
    /* ************************************************************
                       Endpoint operations
       ************************************************************ */
    // adding a new endpoint
    // http://plnkr.co/edit/2UFfaG?p=preview
    $scope.addEndpoint = function() {
        for (var i = 0; i < $scope.service.endpoints.length; i++) {
            if ($scope.service.endpoints[i] === $scope.newEndpoint) {
                alert("The endpoint is already in the list!");
                return;
            }
        }
        $scope.service.endpoints.push($scope.newEndpoint);
        $scope.newEndpoint = '';
    };
    
    // deleting an endpoint from the list
    $scope.deleteEndpoint = function(indexToBeDeleted) {
        $scope.service.endpoints.splice(indexToBeDeleted, 1);
    };
    
    // updating an endpoint to the endpoint list
    $scope.updateEndpoint = function(indexToBeChanged, val) {
        $scope.service.endpoints[indexToBeChanged] = val;
    };

    /* ************************************************************
                       Namespace operations
       ************************************************************ */
    // adding a new namespace
    $scope.addNamespace = function() {
        // checking if namespace list already contains it
        for(var prefix in $scope.service.namespaces) {
            if (prefix === $scope.newNamespacePrefix) {
                alert("The prefix is already in the list!");
                return;
            }
        }
        $scope.service.namespaces[$scope.newNamespacePrefix] = $scope.newNamespaceValue;
        $scope.newNamespacePrefix = '';
        $scope.newNamespaceValue = '';
    };
    
    // deleting a namespace from the list
    $scope.deleteNamespace = function(prefixToBeDeleted) {
        delete $scope.service.namespaces[prefixToBeDeleted];
    };
    
    // updating a namespace prefix
    $scope.updateNamespacePrefix = function(index, newPrefix) {
        // finding the old prefix by the index
        var ind = 0;
        for(var prefix in $scope.service.namespaces) {
            if (ind == index) {
                var oldPrefix = prefix;
            }
            ind++;
        }
        
        // storing the value (namespace URI) of the old prefix, deleting the old one and creating a new
        var namespaceURI = $scope.service.namespaces[oldPrefix];
        delete $scope.service.namespaces[oldPrefix];
        $scope.service.namespaces[newPrefix] = namespaceURI;
    };
    
    // updating a namespace URI
    $scope.updateNamespaceValue = function(prefix, namespaceURI) {
        $scope.service.namespaces[prefix] = namespaceURI;
    };
    
    /* ************************************************************
		             request template operations
       ************************************************************ */
    $scope.showEditRequestTemplateText = function() {
    	var modalDefaults = {
        		backdrop: false,
                keyboard: false,
                modalFade: true,
                size: "lg",
                templateUrl : "../../jsp/administration/editRequestTemplateText.html",
            };
        PropertyStorage.setProperty($scope.service.requestTemplateText);
        //var service_requestTemplateText = $scope.service.requestTemplateText;
        modalDefaults.controller = function ($scope, $modalInstance) {
            //$scope.requestTemplateText = service_requestTemplateText;
            $scope.requestTemplateText = PropertyStorage.getProperty();
            $scope.cancelEditRequestTemplateText = function() {
                $modalInstance.dismiss("cancel");
            };
            $scope.okEditRequestTemplateText = function() {
                PropertyStorage.setProperty($scope.requestTemplateText);
                $modalInstance.close("ok");
            };
        };
        
        var modalIinstance = $modal.open(modalDefaults);
        modalIinstance.result.then(function() {
            //on ok button press
            $scope.service.requestTemplateText = PropertyStorage.getProperty();
        }, function() {
            //on cancel button press
        });
    };
    
    /* ************************************************************
                           modal window events
       ************************************************************ */
    $scope.save = function() {
        // save
    	CommunicationService.save(
    			"save",
    			{ "id": $scope.service.id,
    			  "name": $scope.service.name,
		          "serviceType": $scope.service.serviceType,
		          "soapAction": $scope.service.soapAction,
		          "httpMethod": $scope.service.httpMethod,
		          "requestTemplateText": $scope.service.requestTemplateText,
		          "namespaces": $scope.service.namespaces,
		          "endpoints": $scope.service.endpoints,
		          "databaseQuery": $scope.service.databaseQuery,
		          "description": $scope.service.description}
    	);
    	
        $modalInstance.close("ok");
    };
    
    $scope.cancel = function() {
        $modalInstance.dismiss("cancel");
    };

    /* ************************************************************
                               Utils
       ************************************************************ */
    $scope.notSorted = function(obj){
        if (!obj) {
            return [];
        }
        return Object.keys(obj);
    };
});
