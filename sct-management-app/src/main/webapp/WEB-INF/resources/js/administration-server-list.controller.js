app.controller("administration-server-list.controller", function ($scope, $resource, CommunicationService) {

	// defining the info to build the custom table
	$scope.customTable = new Object();
	$scope.customTable.headers = [
		{column: "n", width: "150px", label: "Name"},
		{column: "u", width: "300px", label: "Endpoint URL"}
	];
	$scope.customTable.filters = {};
	$scope.customTable.data = "server";
	$scope.customTable.dataWindow = "../../jsp/administration/serverDataWindow.html";
	
	
	// refreshing the server list
    $scope.reloadList = function() {
        $scope.customTableData = CommunicationService.query("list");
    };
    
    // deleting a server
    $scope.remove = function(selectedId) {
    	// deleting the item
    	CommunicationService.remove("delete/:id", {id: selectedId});
    };
    
    // when the save happened (the response arrived from the server) an event is fired that is captured here
    $scope.$on("saveFinishedEvent", function(event){
    	$scope.reloadList();
    });

    // similarly at delete
    $scope.$on("removeFinishedEvent", function(event){
    	$scope.reloadList();
    });

	// querying the list of servers
	$scope.reloadList();
    
});