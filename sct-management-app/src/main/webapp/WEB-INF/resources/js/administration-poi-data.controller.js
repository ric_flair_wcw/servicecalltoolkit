app.controller("administration-poi-data.controller", function ($scope, $resource, $modalInstance, $modal, dataWindowParams, CommunicationService, PropertyStorage) {
    $scope.errorMessage = "";
	$scope.operation = dataWindowParams.operation;

	// querying the service list
	$scope.services = CommunicationService.query("../../administration/service/list");
	
	if ($scope.operation === "edit") {
		// querying the POI
		$scope.poi = CommunicationService.get(":poiId", 
                                              {poiId: dataWindowParams.id},
                                              function() {
                                                  $scope.poi.serviceId = $scope.poi.serviceDTO.id;
                                              });
	} else if ($scope.operation === "new") {
		$scope.poi = {singleValue: "false",
				      encryptionNeeded: "false",
			          extracts: [],
				      serviceDTO: { id: {} }};
	}
        
    
    /* ************************************************************
                       Extract operations
       ************************************************************ */
    // adding a new extract
    $scope.addExtract = function() {
        for (var i = 0; i < $scope.poi.extracts.length; i++) {
            if ($scope.poi.extracts[i] === $scope.newExtractValue) {
                alert("The extract is already in the list!");
                return;
            }
        }
        $scope.poi.extracts.push( $scope.newExtractValue );
        $scope.newExtractValue = '';
        // if 2 or more extracts belong to the POI then the POI cannot be a single value
        if ($scope.poi.extracts.length > 1) {
        	$scope.poi.singleValue = "false";
        }
    };
    
    // deleting an extract from the list
    $scope.deleteExtract = function(indexToBeDeleted) {
        $scope.poi.extracts.splice(indexToBeDeleted, 1);
    };
    
    // updating the extract value to the list
    $scope.updateExtractValue = function(indexToBeChanged, val) {
        $scope.poi.extracts[indexToBeChanged] = val;
    };

    
    /* ************************************************************
                           modal window events
       ************************************************************ */
    $scope.save = function() {
        // save
    	CommunicationService.save(
    			"save",
    			{ "id": $scope.poi.id,
    			  "name": $scope.poi.name,
    			  "singleValue": $scope.poi.singleValue,
    			  "encryptionNeeded": $scope.poi.encryptionNeeded,
    			  "serviceDTO": { 
    				  "id": $scope.poi.serviceId
    			  },
		          "extracts": $scope.poi.extracts,
		          "description": $scope.poi.description}
    	);
    	
        $modalInstance.close("ok");
    };
    
    $scope.cancel = function() {
        $modalInstance.dismiss("cancel");
    };

});
