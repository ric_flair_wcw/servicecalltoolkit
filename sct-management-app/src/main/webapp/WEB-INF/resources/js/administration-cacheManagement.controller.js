app.controller("administration-cacheManagement.controller", function ($rootScope, $scope, $resource, $modal, CommunicationService, PropertyStorage, AKOUtils) {
	var token = AKOUtils.getToken();
	// getting the matrix to be displayed
	$scope.matrix = CommunicationService.query("matrix/BOTH");
	// getting the server list
	$scope.servers = CommunicationService.query("../../administration/server/list");
	// getting the service list
	$scope.services = CommunicationService.query("../../administration/service/list");
	// getting the poi list
	$scope.pois = CommunicationService.query("../../administration/poi/list");
	// getting the poi list
	$scope.servicesAndPOIs = CommunicationService.query("serviceAndPOI/list");
	// getting the forgotten services (those services that exist in the cache but not in the DB -> after refresh they were deleted from the DB)
	$scope.forgottenOnes = CommunicationService.query("forgottenOnes/BOTH");
	// querying the cache management view list
	$scope.cacheManagementViews = CommunicationService.query2("../../common/masterdata/cacheManagementView/list",
                                                     function() {
                                                         $scope.cacheManagementView = $scope.cacheManagementViews[2].value;
                                                     });
	
	// show the window that displays the forgotten ones
	$scope.showForgottenOnes = function(server) {
        var modalDefaults = {
                backdrop: false,
                keyboard: false,
                modalFade: true,
                size: "lg",
                templateUrl : '../../jsp/administration/showForgottenOnes.html',
            };
        PropertyStorage.setProperty({server: server, cacheManagementView: $scope.cacheManagementView});
        modalDefaults.controller = function ($scope, $modalInstance) {
        	$scope.data = PropertyStorage.getProperty();
        	$scope.forgottenList = CommunicationService.get("forgottenOnes/details/" + $scope.data.server + "/" + $scope.data.cacheManagementView);
            // when closing the window
            $scope.close = function() {
                $modalInstance.dismiss("cancel");
            };
            // when deleting the forgotten ones
            $scope.deleteForgottenOnes = function() {
            	CommunicationService.save2("forgottenOnes/delete", 
            			                   {server: $scope.data.server, 
            		                        cacheManagementView: $scope.data.cacheManagementView},
            	                           "deleteForgottenOnesEvent"+token);
            };
            // it is needed to use ng-repeat on map
            $scope.notSorted = function(obj){
                if (!obj) {
                    return [];
                }
                return Object.keys(obj);
            };
        };
		
        var modalIinstance = $modal.open(modalDefaults);
        modalIinstance.result.then(function() {
            //on ok button press
        }, function() {
            //on cancel button press
        });
	};
	
    // show modal window when the user clicked on a cell in the table -> refreshing cache
    $scope.showRefreshCacheWindow = function(server, service_or_poi, status, itIsAServiceOrPoiOrServer) {
        var modalDefaults = {
            backdrop: false,
            keyboard: false,
            modalFade: true,
            size: "lg",
            templateUrl : '../../jsp/administration/cacheManagementRefresh.html',
        };
        PropertyStorage.setProperty({server: server, service_or_poi: service_or_poi, status: status, cacheManagementView: $scope.cacheManagementView, itIsAServiceOrPoiOrServer: itIsAServiceOrPoiOrServer});
        modalDefaults.controller = function ($scope, $modalInstance) {
            $scope.data = PropertyStorage.getProperty();
            // if the cache is OLD then get the service compare matrix            
            if ($scope.data.status == "OLD" && $scope.data.cacheManagementView != "BOTH") {
                $scope.map = CommunicationService.get("serviceComparison/" + server + "/" + $scope.data.cacheManagementView + "/" + $scope.data.service_or_poi);
            }
            // when closing the window
            $scope.closeRefreshCache = function() {
                $modalInstance.dismiss("cancel");
            };
            // when clicking on the 'Do it!' button
            $scope.refreshCache = function() {
            	CommunicationService.save2("refresh", 
                                          {server: $scope.data.server,
                                           service_or_poi: $scope.data.service_or_poi,
                                           cacheManagementView: $scope.data.cacheManagementView,
                                           itIsAServiceOrPoiOrServer: $scope.data.itIsAServiceOrPoiOrServer},
                                          "refreshCacheEvent"+token);
            };
        };
        
        var modalIinstance = $modal.open(modalDefaults);
        modalIinstance.result.then(function() {
            //on ok button press
        }, function() {
            //on cancel button press
        });
    };
    
    // when deleting the forgotten ones the matrix has to be refreshed
    $scope.$on("deleteForgottenOnesEvent"+token, function(event) {
    	$scope.matrix = CommunicationService.query("matrix/" + $scope.cacheManagementView);
        alert("The operation completed successfuly!");
    });

    // when refreshing the cache the matrix has to be refreshed
    $scope.$on("refreshCacheEvent"+token, function(event) {
    	$scope.matrix = CommunicationService.query("matrix/" + $scope.cacheManagementView);
        alert("The operation completed successfuly!");
    });

    // when the value of the updateCacheManagementView select is being changed
    $scope.updateCacheManagementViewSelect = function() {
    	// getting the matrix to be displayed
    	$scope.matrix = CommunicationService.query("matrix/" + $scope.cacheManagementView);
    	// getting the forgotten ones (those services/POIs/both that exist in the cache but not in the DB -> after refresh they were deleted from the DB)
    	$scope.forgottenOnes = CommunicationService.query("forgottenOnes/" + $scope.cacheManagementView);
    };
    
});
