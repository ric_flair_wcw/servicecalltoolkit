app.controller("serviceDiscovery.controller", function ($rootScope, $scope, $resource, $compile, $modal, CommunicationService, PropertyStorage, AKOUtils) {

	// some init
    $scope.data = {};
    $scope.data.first = {};
	$scope.data.first.canOperationsBeDisplayed = false;
    $scope.data.first.newOrExisting = "new";
    $scope.is1stActive = true;
    $scope.is2ndActive = false;
    $scope.is3rdActive = false;
    $scope.isExistingServiceWsdlLoaded = false;
    $scope.disablePoiSingleValueInput = false;
    $scope.data.poi = {singleValue: false};
    $('#loadingDiv').hide();
	
	
    /* ************************************************************
                    for building the SOAP response tree
       ************************************************************ */
    // build the tree
    $scope.buildTree = function(html) {
        // adding the content the of tree to the HTML
        var myEl = angular.element( document.querySelector( "#divForCheckboxTreeForPOI" ) );
        myEl.html(html);
        $compile(myEl.contents())($scope);
        
        // building the tree
        $('#checkboxTreeForPOI').checkboxTree({
            onCheck: {
                ancestors: 'checkIfFull',
                descendants: 'check'
            },
            onUncheck: {
                ancestors: 'uncheck',
                descendants: 'uncheck'
            }
        });
    };

       // building the content of the tree with recursive call
    $scope.buildTreeContent = function () {
        var html = "";

        // building the HTML text with recursive call
		html += $scope.getHtml($scope.responseMap, true);

        $scope.buildTree(html);
    };
	// the recursive method to build the tree content
	$scope.getHtml = function(node, isAdd) {
        // the id attribute has to be added at the first call
		if (isAdd) {
			var html = "<ul id='checkboxTreeForPOI'>";
		} else {
			var html = "<ul>";
		}

        angular.forEach(node, function(value, key) {
            var value = node[key];
            if (key != "$promise" && key != "$resolved") {
                var labelKey = key.slice(key.lastIndexOf("/")+1);
                html += "<li><input type='checkbox' id='" + key + "'><label>" + labelKey + "</label>";
                if (Object.prototype.toString.call(value) != "[object String]") {
                    html += $scope.getHtml(value, false);
                }
            }
        });
        
		html += "</ul>";
        delete node;
		return html;
	};
	
	
    /* ************************************************************
                           button events
       ************************************************************ */
	// RADIO BUTTONS : when the new / existing radio buttons changes
	$scope.changeNewOrExisting = function() {
		$scope.data.first.canOperationsBeDisplayed = false;
		$scope.data.first.wsdlUrl = "";
		if ($scope.data.first.newOrExisting == "existing") {
			// querying the service list
			$scope.services = CommunicationService.query("../../administration/service/list");
		}
	};
	
	// Load it! BUTTON by new service: when the user click on the loadWsdl button
	$scope.loadWsdl = function() {
        $scope.data.serviceEndpoint = $scope.data.first.wsdlUrl;
        var token = AKOUtils.getToken();
        $scope.data.wsdlOperations = CommunicationService.save2("getOperationsFromWsdl", {wsdlUrl : $scope.data.first.wsdlUrl}, "wsdlLoadedEvent"+token);
        $scope.$on("wsdlLoadedEvent"+token, function(event) {
            $scope.data.first.canOperationsBeDisplayed = true;
        });
	};
    
	// Load it! BUTTON by existing service: when the user click on the loadWsdl button
	$scope.loadExistingServiceWsdl = function() {
        // build the SOAP response map
        $scope.getResponseMap(true);
	};
	
    // Next >> BUTTON : when the user click on the Next >> button
    $scope.next = function(tab) {
        $scope.is1stActive = false;
        $scope.is2ndActive = false;
        $scope.is3rdActive = false;
        if (tab == "service" && $scope.data.first.newOrExisting == "new") {
            $scope.is2ndActive = true;
        } else if (tab == "service" && $scope.data.first.newOrExisting == "existing") {
            $scope.is3rdActive = true;
        } else if (tab == "serviceDetails") {
            $scope.is3rdActive = true;
        }
    };
    
    // << Prev BUTTON : when the user clicks on the << Prev button
    $scope.prev = function(tab) {
        $scope.is1stActive = false;
        $scope.is2ndActive = false;
        $scope.is3rdActive = false;
        if (tab == "POI" && $scope.data.first.newOrExisting == "existing") {
            $scope.is1stActive = true;
        } else if (tab == "POI" && $scope.data.first.newOrExisting == "new") {
            $scope.is2ndActive = true;
        } else if (tab == "serviceDetails") {
            $scope.is1stActive = true;
        } else {
            $scope.is2ndActive = true;
        }
    };
    
    // Finish BUTTON : when the user clicks on the finish button
    $scope.finish = function() {
        var checkedCheckboxes = $('#checkboxTreeForPOI input[type="checkbox"]:checked');
    	if (checkedCheckboxes.length == 0) {
            alert("No extract is chosen!");
        } else if (checkedCheckboxes.length > 1 && $scope.data.poi.singleValue) {
            alert("You cannot mark the POI as single value if you selected more than one extract!");
        }
        else {
            var token = AKOUtils.getToken();
            if ($scope.data.first.newOrExisting == "new") {
                // save the service
                var endpoints = [];
                endpoints.push($scope.data.serviceEndpoint);
                $scope.sserviceId = CommunicationService.save2(
                        "../../administration/service/save",
                        { "name": $scope.data.serviceDetails.serviceName,
                          "serviceType": "WEBSERVICE",
                          "soapAction": $scope.data.serviceDetails.soapAction,
                          "requestTemplateText": $scope.data.serviceDetails.request,
                          "namespaces": $scope.data.serviceDetails.namespaces,
                          "endpoints": endpoints},
                        "successfulServiceSaveEvent"+token
                );
            } else {
                $scope.sserviceId = {serviceId: $scope.data.first.selectedService};
            }            
            
            // if successful then save the POI
            var extracts = [];
            for(var i=0; i<checkedCheckboxes.length; i++) {
                extracts.push(checkedCheckboxes[i].id);
            }
            if ($scope.data.first.newOrExisting == "new") {
                $scope.$on("successfulServiceSaveEvent"+token, function(event) {
                    $scope.savePOI(extracts, token);
                });
            } else {
                $scope.savePOI(extracts, token);
            }
            // if saving the POI wasn't successful then warning the user the user is saved already
            $scope.$on("unsuccessfulPOISaveEvent"+token, function(event) {
                if ($scope.data.first.newOrExisting == "new") {
                    alert("Warning, the service ("+$scope.data.serviceDetails.serviceName+", "+$scope.sserviceId.serviceId+") is saved but the POI is not!!");
                } else {
                    alert("Warning, the POI couldn't be saved!!");
                }
            });
            // if everything is fine
            $scope.$on("successfulPOISaveEvent"+token, function(event) {
            	alert("Successful save.");
            });
        }
    };

    /* ************************************************************
                    request template modal operations
       ************************************************************ */
    $scope.showEditRequestTemplateText = function() {
		var modalDefaults = {
			backdrop: false,
			keyboard: false,
			modalFade: true,
			size: "lg",
			templateUrl : "../../jsp/serviceDiscovery/editRequestTemplateText.html",
		};
		PropertyStorage.setProperty($scope.data.serviceDetails.request);
		modalDefaults.controller = function ($scope, $modalInstance) {
			$scope.requestTemplateText = PropertyStorage.getProperty();
			$scope.cancelEditRequestTemplateText = function() {
				$modalInstance.dismiss("cancel");
			};
			$scope.okEditRequestTemplateText = function() {
				PropertyStorage.setProperty($scope.requestTemplateText);
				$modalInstance.close("ok");
			};
		};

		var modalIinstance = $modal.open(modalDefaults);
		modalIinstance.result.then(function() {
			// on ok button press
			$scope.data.serviceDetails.request = PropertyStorage.getProperty();
		}, function() {
			//on cancel button press
		});
    };
    
    
    /* ************************************************************
                               Utils
       ************************************************************ */
    $scope.notSorted = function(obj){
        if (!obj) {
            return [];
        }
        return Object.keys(obj);
    };

    // loading the map of the SOAP response to build the tree
    $scope.getResponseMap = function(needResponseToLoad) {
        if (needResponseToLoad) {
            // getting the service details from WSDL
            var token = AKOUtils.getToken();
            $scope.data.serviceDetails = CommunicationService.save2("getServiceDetailsFromWsdl", {wsdlUrl : $scope.data.first.wsdlUrlExistingService, serviceId: $scope.data.first.selectedService}, "serviceDetailsLoadedEvent"+token);
            $scope.$on("serviceDetailsLoadedEvent"+token, function(event) {
            	$scope.getResponseMap2();
            });
        } else {
        	$scope.getResponseMap2();
        }
    };
    $scope.getResponseMap2 = function() {
		// getting the response map from REST ; not saving anything but a POST request has to be sent
        var token = AKOUtils.getToken();
	    $scope.responseMap = CommunicationService.save2("responseTree", {reponseString : $scope.data.serviceDetails.response}, "responseMapArrivedEvent"+token);
	    // build the tree when the response map arrived 
	    $scope.$on("responseMapArrivedEvent"+token, function(event) {
	        $scope.buildTreeContent();
            $scope.isExistingServiceWsdlLoaded = true;
	    });
    };
    
    
    // checking if the next button has to be disabled on the Service tab
    $scope.isNextDisable = function(tab) {
        if (tab == "Service") {
            if ($scope.data.first.newOrExisting == "existing" && $("#selectService").val() != "?" && $scope.isExistingServiceWsdlLoaded) {
                return false;
            } else if ($scope.data.first.newOrExisting == "new" && $("#selectWsdlUrl").val().length > 0 && $("#selectWsdlOperation").val() != "?") {
                return false;
            }
            return true;
        } else if (tab == "Service details") {
            if ($("#serviceNameInput").val() && $("#serviceWsdlUrlInput").val()) {
                return false;
            } else {
                return true;
            }
        }
    };
    
    // in order to build the SOAP response tree in case of existing service the endpoint has to be got
    $scope.selectedServiceChanged = function() {
        $scope.isExistingServiceWsdlLoaded = false;
		// querying the service
		$scope.existingService = CommunicationService.get("../../administration/service/:serviceId", 
				                                  {serviceId: $scope.data.first.selectedService},
				                                  function() {
                                                      // set the WSDL URL in existing service WSDL URL input
                                                      $scope.data.first.wsdlUrlExistingService = $scope.existingService.endpoints[0] + "?WSDL";
                                                  } );
    }
    
    // checking if wsdl URL input has to be displayed for an existing service
    $scope.canWsdlInputBeDisplayed = function() {
        if ($scope.data.first.newOrExisting == "existing" && $("#selectService").val() != "?") {
            return true;
        } else {
            return false;
        }
    };

    // checking if the finish button has to be disabled on the POI tab
    $scope.isFinishDisable = function() {
        if ($("#poiNameInput").val()) {
            return false;
        } else {
            return true;
        }
    };
    
    $scope.wsdlOperationChanged = function() {
        // getting the service details from WSDL
        var token = new Date().getTime();
        $scope.data.serviceDetails = CommunicationService.save2("getServiceDetailsFromWsdl", 
        		                                                {wsdlUrl : $scope.data.first.wsdlUrl, operation: $scope.data.first.selectedWsdlOperation}, 
        		                                                "serviceDetailsLoadedEvent"+token);
        $scope.$on("serviceDetailsLoadedEvent"+token, function(event) {
            $scope.getResponseMap(false);
        });
    };
    
    $scope.savePOI = function(extracts, token) {
        CommunicationService.save3(
                "../../administration/poi/save",
                { "name": $scope.data.poi.name,
                  "singleValue": $scope.data.poi.singleValue,
                  "serviceDTO": { 
                      "id": $scope.sserviceId.serviceId
                  },
                  "encryptionNeeded": "false",
                  "extracts": extracts},
                "successfulPOISaveEvent"+token,
                "unsuccessfulPOISaveEvent"+token);
    };
    
});
