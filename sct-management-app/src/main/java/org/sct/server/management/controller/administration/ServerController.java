package org.sct.server.management.controller.administration;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.sct.server.common.beans.ServerDTO;
import org.sct.server.common.controller.ParentController;
import org.sct.server.management.persistence.service.IServerEntityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;


@Controller
@RequestMapping("/administration/server")
public class ServerController extends ParentController {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(ServerController.class);
	
	@Autowired
	private IServerEntityService serverDAO;
	
	
	@RequestMapping(value = "/main", method = RequestMethod.GET)
	public String showMainServerPage() throws SQLException {
		LOGGER.debug("Received GET request to show the server records main (list) page.");

		return "/administration/server.tiles";
	}
	
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public @ResponseBody Object getServers() throws JsonParseException, JsonMappingException, IOException {
		LOGGER.debug("Received GET request to get the server list for displaying them in a table.");
		
		// getting the server list
		List<ServerDTO> serverDTOs = serverDAO.getServers();
		LOGGER.debug("Retrieved {} server record(s)", serverDTOs.size());
		
		// creating the list that will be sent back to angular js
		Object[] simplifiedServerList = new Object[serverDTOs.size()];
		int ind = 0;
		for(ServerDTO serverDTO : serverDTOs) {
			Map<String, Object> record = new HashMap<String, Object>();
			record.put("id", serverDTO.getId());
			record.put("n", serverDTO.getName());
			record.put("u", serverDTO.getUrl());
			simplifiedServerList[ind++] = record;
		}
		
		return simplifiedServerList;
	}

	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public @ResponseBody Object getServer(@PathVariable("id") Long id) throws JsonParseException, JsonMappingException, IOException {
		LOGGER.debug("Received GET request to get the details of one server for editing it.");
		
		// getting the server
		ServerDTO serverDTO = serverDAO.getServer(id);
		LOGGER.debug("The server to be retrieved is {}", serverDTO);
		
		return serverDTO;
	}
	

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public @ResponseBody Object deleteServer(@PathVariable("id") Long id) {
		LOGGER.debug("Received POST request to delete a server.");

		// deleting a server
		serverDAO.deleteServer(id);
		return null;
	}


	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public @ResponseBody Object saveServer(@RequestBody ServerDTO serverDTO) throws JsonGenerationException, JsonMappingException, IOException {
		LOGGER.debug("Received POST request to save a server.");

		// saving the server
		serverDAO.saveServer(serverDTO);
		
		return null;
	}
	

}
