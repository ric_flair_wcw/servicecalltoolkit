package org.sct.server.management.controller.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.sct.common.constants.ServiceTypeEnum;
import org.sct.server.common.constants.CacheManagementViewEnum;
import org.sct.server.common.constants.HttpMethodEnum;
import org.sct.server.common.controller.ParentController;
import org.sct.server.management.contants.CacheManagementVerticalEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/common/masterdata")
public class MasterDataController extends ParentController {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(MasterDataController.class);
	
	
	@RequestMapping(value = "/serviceType/list", method = RequestMethod.GET)
	public @ResponseBody Object getServiceTypes() {
		LOGGER.debug("Received GET request to get the service type list.");
		
		List<Map<String,String>> result = new ArrayList<Map<String,String>>();
		for(ServiceTypeEnum serviceType : ServiceTypeEnum.values()) {
			Map<String,String> record = new HashMap<String, String>();
			record.put("value", serviceType.name());
			record.put("label", serviceType.getLabel());
			result.add(record);
		}
		return result;
	}
	

	@RequestMapping(value = "/httpMethod/list", method = RequestMethod.GET)
	public @ResponseBody Object getHttpMethods() {
		LOGGER.debug("Received GET request to get the http method list.");
		
		List<Map<String,String>> result = new ArrayList<Map<String,String>>();
		for(HttpMethodEnum httpMethod : HttpMethodEnum.values()) {
			Map<String,String> record = new HashMap<String, String>();
			record.put("value", httpMethod.name());
			record.put("label", httpMethod.name());
			result.add(record);
		}
		return result;
	}

	
	@RequestMapping(value = "/cacheManagementVerticals", method = RequestMethod.GET)
	public @ResponseBody Object getCacheManagementVertical() {
		LOGGER.debug("Received GET request to get the vertical values for Cache Management.");
		
		List<Map<String,String>> result = new ArrayList<Map<String,String>>();
		for(CacheManagementVerticalEnum cacheManagementVertical : CacheManagementVerticalEnum.values()) {
			Map<String,String> record = new HashMap<String, String>();
			record.put("value", cacheManagementVertical.name());
			record.put("label", cacheManagementVertical.getLabel());
			result.add(record);
		}
		return result;
	}


	@RequestMapping(value = "/cacheManagementView/list", method = RequestMethod.GET)
	public @ResponseBody Object getCacheManagementViews() {
		LOGGER.debug("Received GET request to get the cache management view list.");
		
		List<Map<String,String>> result = new ArrayList<Map<String,String>>();
		for(CacheManagementViewEnum cacheManagementView : CacheManagementViewEnum.values()) {
			Map<String,String> record = new HashMap<String, String>();
			record.put("value", cacheManagementView.name());
			record.put("label", cacheManagementView.getLabel());
			result.add(record);
		}
		return result;
	}

}
