package org.sct.server.management.controller.administration;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.sct.common.utils.Utils;
import org.sct.server.common.beans.POIDTO;
import org.sct.server.common.beans.ServerDTO;
import org.sct.server.common.beans.ServiceDTO;
import org.sct.server.common.constants.CacheManagementViewEnum;
import org.sct.server.common.persistence.service.IPOIEntityService;
import org.sct.server.common.persistence.service.IServiceEntityService;
import org.sct.server.management.persistence.service.IServerEntityService;
import org.sct.server.management.rest.RestServiceManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;


@Controller
@RequestMapping("/administration/cacheManagement/forgottenOnes")
public class ForgottenOnesController extends CacheManagementController {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(ForgottenOnesController.class);
	
	
	@Autowired
	private IServerEntityService serverDAO;
	@Autowired
	private IServiceEntityService serviceDAO;
	@Autowired
	private IPOIEntityService poiDAO;
	@Autowired
	private RestServiceManager restManager;
	
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public @ResponseBody Object deleteForgottenOnes(@RequestBody Map<String, String> requestBody) throws JsonParseException, JsonMappingException, IOException {
		LOGGER.debug("Received POST request to delete the forgotten ones. requestBody:{}", requestBody);
		
		String serverName = requestBody.get("server");
		String cacheManagementView = requestBody.get("cacheManagementView");

		ServerDTO serverDTO = serverDAO.getServerByName(serverName);
		
		if (CacheManagementViewEnum.SERVICE.name().equals(cacheManagementView)) {
			List<ServiceDTO> forgottenServiceDTOs = getForgottenServices(serverDTO);
			for(ServiceDTO serviceDTO : forgottenServiceDTOs) {
				String url = Utils.removeTrailingChar(serverDTO.getUrl(), "/") + "/cache/service/" + serviceDTO.getName();
				LOGGER.trace("Calling {}", url);
				restManager.deleteFromCacheViaREST(url);
			}
		}
		else if (CacheManagementViewEnum.POI.name().equals(cacheManagementView)) {
			List<POIDTO> forgottenPOIDTOs = getForgottenPOIs(serverDTO);
			for(POIDTO poiDTO : forgottenPOIDTOs) {
				String url = Utils.removeTrailingChar(serverDTO.getUrl(), "/") + "/cache/poi/" + poiDTO.getName();
				LOGGER.trace("Calling {}", url);
				restManager.deleteFromCacheViaREST(url);
			}
		}
		else if (CacheManagementViewEnum.BOTH.name().equals(cacheManagementView)) {
			List<ServiceDTO> forgottenServiceDTOs = getForgottenServices(serverDTO);
			for(ServiceDTO serviceDTO : forgottenServiceDTOs) {
				String url = Utils.removeTrailingChar(serverDTO.getUrl(), "/") + "/cache/service/" + serviceDTO.getName();
				LOGGER.trace("Calling {}", url);
				restManager.deleteFromCacheViaREST(url);
			}

			List<POIDTO> forgottenPOIDTOs = getForgottenPOIs(serverDTO);
			for(POIDTO poiDTO : forgottenPOIDTOs) {
				String url = Utils.removeTrailingChar(serverDTO.getUrl(), "/") + "/cache/poi/" + poiDTO.getName();
				LOGGER.trace("Calling {}", url);
				restManager.deleteFromCacheViaREST(url);
			}
		}
		return null;
	}
	

	@RequestMapping(value = "/details/{server}/{cacheManagementView}", method = RequestMethod.GET)
	public @ResponseBody Object getForgottenOnesDetails(@PathVariable("server") String serverName, 
			                                            @PathVariable("cacheManagementView") String cacheManagementView) throws JsonParseException, JsonMappingException, IOException {
		LOGGER.debug("Received GET request to get the forgotten ones details. server:{}, cacheManagementView:{}", serverName, cacheManagementView);

		Map<String, Object> result = new HashMap<String,Object>();

		ServerDTO serverDTO = serverDAO.getServerByName(serverName);
		
		if (CacheManagementViewEnum.SERVICE.name().equals(cacheManagementView)) {
			List<ServiceDTO> forgottenServiceDTOs = getForgottenServices(serverDTO);
			result.put("SERVICE", forgottenServiceDTOs.toArray());
		}
		else if (CacheManagementViewEnum.POI.name().equals(cacheManagementView)) {
			List<POIDTO> forgottenPOIDTOs = getForgottenPOIs(serverDTO);
			result.put("POI", forgottenPOIDTOs.toArray());
		}
		else if (CacheManagementViewEnum.BOTH.name().equals(cacheManagementView)) {
			
			List<ServiceDTO> forgottenServiceDTOs = getForgottenServices(serverDTO);
			List<POIDTO> forgottenPOIDTOs = getForgottenPOIs(serverDTO);

			result.put("SERVICE", forgottenServiceDTOs.toArray());
			result.put("POI", forgottenPOIDTOs.toArray());
		}
		return result;
	}
	

	@RequestMapping(value = "/{cacheManagementView}", method = RequestMethod.GET)
	public @ResponseBody Object getForgottenOnes(@PathVariable("cacheManagementView") String cacheManagementView) throws JsonParseException, JsonMappingException, IOException {
		LOGGER.debug("Received GET request to get the forgotten ones. cacheManagementView:{}", cacheManagementView);
		
		// getting the serverDTOs from DB
		List<ServerDTO> serverDTOs = serverDAO.getServers();
		String[] result = new String[serverDTOs.size()];
		
		if (CacheManagementViewEnum.SERVICE.name().equals(cacheManagementView)) {
			int ind = 0;
			for(ServerDTO serverDTO : serverDTOs) {
				List<ServiceDTO> forgottenServiceDTOs = getForgottenServices(serverDTO);
				if (forgottenServiceDTOs.size() > 0) {
					result[ind++] = "true";
				} else {
					result[ind++] = "false";	
				}
			}
		}
		else if (CacheManagementViewEnum.POI.name().equals(cacheManagementView)) {
			int ind = 0;
			for(ServerDTO serverDTO : serverDTOs) {
				List<POIDTO> forgottenPOIDTOs = getForgottenPOIs(serverDTO);
				if (forgottenPOIDTOs.size() > 0) {
					result[ind++] = "true";
				} else {
					result[ind++] = "false";	
				}
			}
		}
		else if (CacheManagementViewEnum.BOTH.name().equals(cacheManagementView)) {
			int ind = 0;
			for(ServerDTO serverDTO : serverDTOs) {
				List<ServiceDTO> forgottenServiceDTOs = getForgottenServices(serverDTO);
				if (forgottenServiceDTOs.size() > 0) {
					result[ind++] = "true";
					continue;
				}
				List<POIDTO> forgottenPOIDTOs = getForgottenPOIs(serverDTO);
				if (forgottenPOIDTOs.size() > 0) {
					result[ind++] = "true";
				} else {
					result[ind++] = "false";	
				}
			}
		}
		
		return result;
	}
	
		
	private List<ServiceDTO> getForgottenServices(ServerDTO serverDTO) throws JsonParseException, JsonMappingException, IOException {
		List<ServiceDTO> result = new ArrayList<ServiceDTO>();

		// getting the serviceDTOs from cache
		String url = Utils.removeTrailingChar(serverDTO.getUrl(), "/") + "/cache/service/list";
		List<ServiceDTO> serviceDTOsInCache = new ArrayList<ServiceDTO>();
		try {
			serviceDTOsInCache = restManager.getServiceDTOsFromREST(url);
		} catch(Exception e) {
			LOGGER.warn("The service on {} is unavailable.", url);
			LOGGER.debug("The exception:", e);
		}
		
		// getting the serviceDTOs from DB
		List<ServiceDTO> serviceDTOs = serviceDAO.getServices();

		boolean found = false;
		for (ServiceDTO serviceDTOInCache : serviceDTOsInCache) {
			found = false;
			for (ServiceDTO serviceDTO : serviceDTOs) {
				if (serviceDTOInCache.getId().longValue() == serviceDTO.getId().longValue()) {
					found = true;
					break;
				}
			}
			if (!found) {
				result.add(serviceDTOInCache);
			}
		}
		return result;
	}


	private List<POIDTO> getForgottenPOIs(ServerDTO serverDTO) throws JsonParseException, JsonMappingException, IOException {
		List<POIDTO> result = new ArrayList<POIDTO>();

		// getting the serviceDTOs from cache
		String url = Utils.removeTrailingChar(serverDTO.getUrl(), "/") + "/cache/poi/list";
		List<POIDTO> poiDTOsInCache = new ArrayList<POIDTO>();
		try {
			poiDTOsInCache = restManager.getPOIDTOsFromREST(url);
		} catch(Exception e) {
			LOGGER.warn("The service on {} is unavailable.", url);
			LOGGER.debug("The exception:", e);
		}
		
		// getting the serviceDTOs from DB
		List<POIDTO> poiDTOs = poiDAO.getPOIs();

		boolean found = false;
		for (POIDTO poiDTOInCache : poiDTOsInCache) {
			found = false;
			for (POIDTO poiDTO : poiDTOs) {
				if (poiDTOInCache.getId().longValue() == poiDTO.getId().longValue()) {
					found = true;
					break;
				}
			}
			if (!found) {
				result.add(poiDTOInCache);
			}
		}
		return result;
	}
	
	
	
}
