package org.sct.server.management.persistence.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.sct.server.common.beans.ServerDTO;
import org.sct.server.common.persistence.model.ServerEntity;
import org.sct.server.common.persistence.utils.Converter;
import org.sct.server.management.persistence.repository.ServerEntityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class RepositoryServerEntityService implements IServerEntityService {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(RepositoryServerEntityService.class);
	
	@Resource
	private ServerEntityRepository serverEntityRepository;
	@Autowired
	private Converter converter;
	
	
	@Transactional
	public List<ServerDTO> getServers() {
		LOGGER.debug("Getting the server list from DB");
		List<ServerDTO> serverDTOs = new ArrayList<ServerDTO>();
		try {
			// getting the service entities
			List<ServerEntity> serverEntities = serverEntityRepository.findAll(new Sort(Sort.Direction.ASC, "name"));
			// converting them to ServiceDTOs
			for(ServerEntity serverEntity : serverEntities) {
				serverDTOs.add(converter.convertToServerDTO(serverEntity));
			}
			return serverDTOs;
		} finally {
			LOGGER.debug("Retrieved {} server record(s) from DB", serverDTOs.size());
		}
	}


	@Transactional
	public void deleteServer(Long id) {
		LOGGER.debug("Deleting a server record (id={}) from DB", id);
		// delete the service
		serverEntityRepository.delete(id);
	}


	@Transactional
	public void saveServer(ServerDTO serverDTO) {
		if (serverDTO != null) {
			// insert
			if (serverDTO.getId() == null) {
				LOGGER.debug("Creating a server record in DB. {}", serverDTO);
				// saving the service entity
				ServerEntity serverEntity = serverEntityRepository.save(converter.convertToServerEntity(serverDTO, new ServerEntity()));
				LOGGER.debug("The id of the new server record is {}.", serverEntity.getId());
			}
			// update
			else {
				LOGGER.debug("Updating a server record in DB. {}", serverDTO);
				// getting the entity from DB
				ServerEntity serverEntity = serverEntityRepository.findOne(serverDTO.getId());
				// updating the values of the entity
				converter.convertToServerEntity(serverDTO, serverEntity);
			}
		}
	}


	@Transactional
	public ServerDTO getServer(Long id) {
		LOGGER.debug("Getting the server record (id={}) from DB", id);
		// getting the service entity
		ServerEntity serverEntity = serverEntityRepository.findOne(id);
		// converting it to ServiceDTO
		return converter.convertToServerDTO(serverEntity);
	}


	@Transactional
	public ServerDTO getServerByName(String serverName) {
		LOGGER.debug("Getting the server record by name (name={}) from DB", serverName);
		// getting the service entity by its name
		ServerEntity serverEntity = serverEntityRepository.findByName(serverName);
		// converting it to ServiceDTO
		return converter.convertToServerDTO(serverEntity);
	}
	
}
