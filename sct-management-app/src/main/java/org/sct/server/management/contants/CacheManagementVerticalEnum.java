package org.sct.server.management.contants;


public enum CacheManagementVerticalEnum {

	SERVICE("Service"), 
	POI("POI"),
	BOTH("Both");
	
	private String label = null;

	CacheManagementVerticalEnum(String label) {
		this.label = label;
	}
	
	public String getLabel() {
		return this.label;
	}

}
