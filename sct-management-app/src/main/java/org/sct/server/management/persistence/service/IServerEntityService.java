package org.sct.server.management.persistence.service;

import java.util.List;

import org.sct.server.common.beans.ServerDTO;

public interface IServerEntityService {

	
	List<ServerDTO> getServers();
	
	void deleteServer(Long id);
	
	void saveServer(ServerDTO serviceBean);

	ServerDTO getServer(Long id);
	
	ServerDTO getServerByName(String serverName);
	
}
