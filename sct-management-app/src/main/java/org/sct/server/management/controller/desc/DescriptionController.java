package org.sct.server.management.controller.desc;

import java.io.IOException;

import org.sct.server.common.beans.POIDTO;
import org.sct.server.common.beans.ServiceDTO;
import org.sct.server.common.controller.ParentController;
import org.sct.server.common.persistence.service.IPOIEntityService;
import org.sct.server.common.persistence.service.IServiceEntityService;
import org.sct.server.management.controller.administration.ServiceController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;


@Controller
@RequestMapping("/desc")
public class DescriptionController extends ParentController {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(ServiceController.class);
	
	@Autowired
	private IServiceEntityService serviceDAO;
	@Autowired
	private IPOIEntityService poiDAO;

	
	@RequestMapping(value = "/service/{serviceName}", method = RequestMethod.GET, produces = "text/html; charset=utf-8")
	public @ResponseBody Object getService(@PathVariable("serviceName") String serviceName) throws JsonParseException, JsonMappingException, IOException {
		LOGGER.debug("Received GET request to get the description of the service '{}'.", serviceName);
		
		// getting the service
		ServiceDTO serviceBean = serviceDAO.getServiceByName(serviceName);
		LOGGER.debug("The service to be retrieved is {}", serviceBean);
		
		if (serviceBean == null) {
			return "The service '" + serviceName + "' cannot be found in the system!";
		} else {
			return "<html><body>" + serviceBean.getDescription() + "</body></html>";
		}
	}


	@RequestMapping(value = {"/poi/{poiName}", "/POI/{poiName}"}, method = RequestMethod.GET, produces = "text/html; charset=utf-8")
	public @ResponseBody Object getPOI(@PathVariable("poiName") String poiName) throws JsonParseException, JsonMappingException, IOException {
		LOGGER.debug("Received GET request to get the description of the POI '{}'.", poiName);
		
		// getting the service
		POIDTO poiBean = poiDAO.getPOIByName(poiName);
		LOGGER.debug("The POI to be retrieved is {}", poiName);
		
		if (poiBean == null) {
			return "The service '" + poiName + "' cannot be found in the system!";
		} else {
			return "<html><body>" + poiBean.getDescription() + "</body></html>";
		}
	}

}