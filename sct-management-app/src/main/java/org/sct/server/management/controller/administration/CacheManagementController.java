package org.sct.server.management.controller.administration;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.sct.common.utils.MapUtils;
import org.sct.common.utils.Utils;
import org.sct.server.common.beans.POIDTO;
import org.sct.server.common.beans.ServerDTO;
import org.sct.server.common.beans.ServiceDTO;
import org.sct.server.common.constants.CacheManagementViewEnum;
import org.sct.server.common.controller.ParentController;
import org.sct.server.common.exceptions.ExceptionUtils;
import org.sct.server.common.persistence.service.IPOIEntityService;
import org.sct.server.common.persistence.service.IServiceEntityService;
import org.sct.server.management.contants.CacheManagementVerticalEnum;
import org.sct.server.management.persistence.service.IServerEntityService;
import org.sct.server.management.rest.RestServiceManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;


@Controller
@RequestMapping("/administration/cacheManagement")
public class CacheManagementController extends ParentController {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(CacheManagementController.class);
	
	@Autowired
	private IServerEntityService serverDAO;
	@Autowired
	private IServiceEntityService serviceDAO;
	@Autowired
	private IPOIEntityService poiDAO;
	@Autowired
	private RestServiceManager restManager;
	
	
	@RequestMapping(value = "/main", method = RequestMethod.GET)
	public String showMainCacheManagementPage() {
		LOGGER.debug("Received GET request to show the POI main (list) page.");

		return "/administration/cacheManagement.tiles";
	}
	
	
	@RequestMapping(value = "/serviceAndPOI/list", method = RequestMethod.GET)
	public @ResponseBody Object getServiceAndPOIList() throws JsonParseException, JsonMappingException, IOException {
		LOGGER.debug("Received GET request to get the service and POI list.");
		
		// getting the serviceDTOs and poiDTOs from DB
		List<ServiceDTO> serviceDTOs = serviceDAO.getServices();
		List<POIDTO> poiDTOs = poiDAO.getPOIs();
		
		String[][] serviceAndPOI = new String[serviceDTOs.size() + poiDTOs.size()][];
		
		int ind = 0;
		for (ServiceDTO serviceDTO : serviceDTOs) {
			serviceAndPOI[ind] = new String[2];
			serviceAndPOI[ind][0] = CacheManagementViewEnum.SERVICE.name();
			serviceAndPOI[ind++][1] = serviceDTO.getName();
			
			// getting the POIs that belongs to the service
			List<POIDTO> poiDTOsOfServiceDTO = getPOIsForService(serviceDTO, poiDTOs);
			for (POIDTO poiDTOOfServiceDTO : poiDTOsOfServiceDTO) {
				serviceAndPOI[ind] = new String[2];
				serviceAndPOI[ind][0] = CacheManagementViewEnum.POI.name();
				serviceAndPOI[ind++][1] = poiDTOOfServiceDTO.getName();		
			}
		}
		
		return serviceAndPOI;
	}
	
	
	@RequestMapping(value = "/serviceComparison/{serverName}/{cacheManagementView}/{serviceOrPOIName}", method = RequestMethod.GET)
	public @ResponseBody Object getServiceComparison(@PathVariable("serverName") String serverName,
			                                         @PathVariable("cacheManagementView") String cacheManagementView,
			                                         @PathVariable("serviceOrPOIName") String serviceOrPOIName) throws JsonParseException, JsonMappingException, IOException {
		LOGGER.debug("Received GET request to get the service comparison. serverName:{}, cacheManagementView:{}, serviceOrPOIName:{}", serverName, cacheManagementView, serviceOrPOIName);
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		ServerDTO serverDTO = serverDAO.getServerByName(serverName);
		// building the url to call the REST
		if (CacheManagementViewEnum.SERVICE.name().equals(cacheManagementView)) {
			String url = Utils.removeTrailingChar(serverDTO.getUrl(), "/") + "/cache/service/" + serviceOrPOIName;
			
			// getting the service from DB
			ServiceDTO serviceDTO = serviceDAO.getServiceByName(serviceOrPOIName);
	
			// calling the REST to get the service in the cache
			ServiceDTO serviceDTOInCache = restManager.getServiceDTOFromREST(url);
			
			// building the map
			map.put("name", new String[]{serviceDTOInCache.getName(),  serviceDTO.getName()});
			map.put("serviceType", new String[]{serviceDTOInCache.getServiceType().getLabel(),  serviceDTO.getServiceType().getLabel()});
			map.put("soapAction", new String[]{serviceDTOInCache.getSoapAction(),  serviceDTO.getSoapAction()});
			map.put("namespaces", new Object[]{convertToArray(serviceDTOInCache.getNamespaces()),  convertToArray(serviceDTO.getNamespaces())});
			map.put("endpoints", new Object[]{serviceDTOInCache.getEndpoints().toArray(), serviceDTO.getEndpoints().toArray() });
			map.put("requestTemplateText", new String[]{serviceDTOInCache.getRequestTemplateText(), serviceDTO.getRequestTemplateText() });
			map.put("httpMethod", new String[]{
					serviceDTOInCache.getHttpMethod() == null ? "" : serviceDTOInCache.getHttpMethod().toString(), 
					serviceDTO.getHttpMethod() == null ? "" : serviceDTO.getHttpMethod().toString() });
			map.put("databaseQuery", new String[]{serviceDTOInCache.getDatabaseQuery(), serviceDTO.getDatabaseQuery() });
		} 
		
		else if (CacheManagementViewEnum.POI.name().equals(cacheManagementView)) {
			String url = Utils.removeTrailingChar(serverDTO.getUrl(), "/") + "/cache/poi/" + serviceOrPOIName;
			
			// getting the POI from DB
			POIDTO poiDTO = poiDAO.getPOIByName(serviceOrPOIName);
	
			// calling the REST to get the POI in the cache
			POIDTO poiDTOInCache = restManager.getPOIDTOFromREST(url);
			
			// building the map
			map.put("name", new String[]{poiDTOInCache.getName(),  poiDTO.getName()});
			map.put("singleValue", new String[]{poiDTOInCache.getSingleValue(),  poiDTO.getSingleValue()});
			map.put("encryptionNeeded", new String[]{poiDTOInCache.getEncryptionNeeded().toString(),  poiDTO.getEncryptionNeeded().toString()});
			map.put("serviceDTO_id", new String[]{poiDTOInCache.getServiceDTO().getId().toString(), poiDTO.getServiceDTO().getId().toString()});
			map.put("serviceDTO_name", new String[]{poiDTOInCache.getServiceDTO().getName(), poiDTO.getServiceDTO().getName()});
			map.put("extracts", new Object[]{ poiDTOInCache.getExtracts().toArray(new String[0]),  poiDTO.getExtracts().toArray(new String[0])});
		}
		
		else if (CacheManagementViewEnum.BOTH.name().equals(cacheManagementView)) {
			// the comparison is not displyed if cacheManagementView = BOTH
		}
		
		else {
			throw new RuntimeException("Unknown cacheManagementView value: " + cacheManagementView);
		}

		return map;
	}
	
	
	@RequestMapping(value = "/refresh", method = RequestMethod.POST)
	public @ResponseBody Object refreshCache(@RequestBody Map<String, String> requestBody) throws JsonParseException, JsonMappingException, IOException {
		LOGGER.debug("Received POST request to refresh a cache. requestBody:{}", requestBody);
		
		String serviceOrPOIName = requestBody.get("service_or_poi");
		String serverName = requestBody.get("server");
		String cacheManagementView = requestBody.get("cacheManagementView");
		String itIsAServiceOrPoiOrServer = requestBody.get("itIsAServiceOrPoiOrServer");
		
		ServerDTO serverDTO = serverDAO.getServerByName(serverName);
		
		// if the user clicked on the header (on the name of the server) then all the POIs/services/both have to be refreshed
		if (itIsAServiceOrPoiOrServer != null && itIsAServiceOrPoiOrServer.equals("SERVER")) {
			// if we are on the SERVICE view then only the services have to be refreshed
			if (cacheManagementView.equals(CacheManagementViewEnum.SERVICE.name()) || cacheManagementView.equals(CacheManagementViewEnum.BOTH.name())) {
				// getting all the services
				List<ServiceDTO> serviceDTOs = serviceDAO.getServices();
				// refreshing the services
				String url = Utils.removeTrailingChar(serverDTO.getUrl(), "/") + "/cache/service";
				for(ServiceDTO serviceDTO : serviceDTOs) {
					// calling the REST service
					restManager.sendObjectToREST(url, serviceDTO);
				}
			}
			// if we are on the POI view then only the POIs have to be refreshed
			if (cacheManagementView.equals(CacheManagementViewEnum.POI.name()) || cacheManagementView.equals(CacheManagementViewEnum.BOTH.name())) {
				// getting all the services and POIs
				List<POIDTO> poiDTOs = poiDAO.getPOIs();
				// refreshing the POIs
				String url = Utils.removeTrailingChar(serverDTO.getUrl(), "/") + "/cache/poi";
				for(POIDTO poiDTO : poiDTOs) {
					// calling the REST service
					restManager.sendObjectToREST(url, poiDTO);
				}
			}
		}
		// if the service should be refreshed
		else if (cacheManagementView.equals(CacheManagementViewEnum.SERVICE.name())) {
			// building the url to call the REST 
			String url = Utils.removeTrailingChar(serverDTO.getUrl(), "/") + "/cache/service";
			// getting the service from DB
			ServiceDTO serviceDTO = serviceDAO.getServiceByName(serviceOrPOIName);
			// calling the REST service
			restManager.sendObjectToREST(url, serviceDTO);
		} 
		// if the POI should be refreshed
		else if (cacheManagementView.equals(CacheManagementViewEnum.POI.name())) {
			// building the url to call the REST 
			String url = Utils.removeTrailingChar(serverDTO.getUrl(), "/") + "/cache/poi";
			// getting the POI from DB
			POIDTO poiDTO = poiDAO.getPOIByName(serviceOrPOIName);
			// calling the REST service
			restManager.sendObjectToREST(url, poiDTO);
		}
		// if the BOTH should be refreshed
		else if (cacheManagementView.equals(CacheManagementViewEnum.BOTH.name())) {
			if (itIsAServiceOrPoiOrServer.equals("POI")) {
				// building the url to call the REST 
				String url = Utils.removeTrailingChar(serverDTO.getUrl(), "/") + "/cache/poi";
				// getting the POI from DB
				POIDTO poiDTO = poiDAO.getPOIByName(serviceOrPOIName);
				// calling the REST service
				restManager.sendObjectToREST(url, poiDTO);
			} 
			else if (itIsAServiceOrPoiOrServer.equals("SERVICE")) {
				// refreshing the service and all the POIs that belong to the service
				
				// getting the service from DB
				ServiceDTO serviceDTO = serviceDAO.getServiceByName(serviceOrPOIName);
				// getting all the POIs that belong to the service
				List<POIDTO> poiDTOs = poiDAO.getPOIs();
				List<POIDTO> poiDTOsOfServiceDTO = getPOIsForService(serviceDTO, poiDTOs);
				// refreshing the service
				String url = Utils.removeTrailingChar(serverDTO.getUrl(), "/") + "/cache/service";
				restManager.sendObjectToREST(url, serviceDTO);
				// refreshing the POIs
				for(POIDTO poiDTOOfServiceDTO : poiDTOsOfServiceDTO) {
					// building the url to call the REST 
					url = Utils.removeTrailingChar(serverDTO.getUrl(), "/") + "/cache/poi";
					// calling the REST service
					restManager.sendObjectToREST(url, poiDTOOfServiceDTO);					
				}
				
			} else {
				throw new RuntimeException("Not supported! itIsAServiceOrPoi:" + itIsAServiceOrPoiOrServer);
			}
		}
		else {
			throw new RuntimeException("Not supported! cacheManagementView:" + cacheManagementView);
		}
		return null;
	}
	
	
	@RequestMapping(value = "/matrix/{vertical}", method = RequestMethod.GET)
	// possible verticalValues: service, POI, both
	public @ResponseBody Object buildMatrix(@PathVariable("vertical") String verticalValue) throws JsonParseException, JsonMappingException, IOException {
		LOGGER.debug("Received GET request to build the services/POISs - server matrix. verticalValue:{}", verticalValue);
		
		List<ServerDTO> serverDTOs = serverDAO.getServers();
		
		// building the SERVICE - SERVER matrix 
		if (verticalValue.equals(CacheManagementVerticalEnum.SERVICE.name())) {
			List<ServiceDTO> serviceDTOs = serviceDAO.getServices();
			
			String[][] matrix = new String[serverDTOs.size()][];

			int x = 0;
			for(ServerDTO serverDTO : serverDTOs) {
				String url = Utils.removeTrailingChar(serverDTO.getUrl(), "/") + "/cache/service/list";
				List<ServiceDTO> serviceDTOsInCache = null;
				try {
					serviceDTOsInCache = restManager.getServiceDTOsFromREST(url);
				} catch(Exception e) {
					LOGGER.warn("The service on {} is unavailable.", url);
					LOGGER.debug("The exception:", e);
				}
				
				int y = 0;
				matrix[x] = new String[serviceDTOs.size()];
				for (ServiceDTO serviceDTO : serviceDTOs) {
					matrix[x][y++] = getCacheStatusByService(serviceDTO, serviceDTOsInCache);
				}
				x++;
			}
			
			return reverseXandY(matrix);
		}

		// building the POI - SERVER matrix 
		else if (verticalValue.equals(CacheManagementVerticalEnum.POI.name())) {
			List<POIDTO> poiDTOs = poiDAO.getPOIs();
			
			String[][] matrix = new String[serverDTOs.size()][];
			
			int x = 0;
			for(ServerDTO serverDTO : serverDTOs) {
				String url = Utils.removeTrailingChar(serverDTO.getUrl(), "/") + "/cache/poi/list";
				List<POIDTO> poiDTOsInCache = null;
				try {
					poiDTOsInCache = restManager.getPOIDTOsFromREST(url);
				} catch(Exception e) {
					LOGGER.warn("The service on {} is unavailable.", url);
					LOGGER.debug("The exception:", e);
				}
				
				int y = 0;
				matrix[x] = new String[poiDTOs.size()];
				for (POIDTO poiDTO : poiDTOs) {
					matrix[x][y++] = getCacheStatusByPOI(poiDTO, poiDTOsInCache);
				}
				x++;
			}
			
			return reverseXandY(matrix);
			
		} 
		
		// building the SERVICE/POI - SERVER matrix 
		else if (verticalValue.equals(CacheManagementVerticalEnum.BOTH.name())) {
			// getting the serviceDTOs and poiDTOs from DB
			List<ServiceDTO> serviceDTOs = serviceDAO.getServices();
			List<POIDTO> poiDTOs = poiDAO.getPOIs();
			
			String[][] matrix = new String[serverDTOs.size()][];
			
			int x = 0;
			for(ServerDTO serverDTO : serverDTOs) {
				// getting the serviceDTOs and poiDTOs from cache
				String url = Utils.removeTrailingChar(serverDTO.getUrl(), "/") + "/cache/service/list";
				List<ServiceDTO> serviceDTOsInCache = null;
				try {
					serviceDTOsInCache = restManager.getServiceDTOsFromREST(url);
				} catch(Exception e) {
					LOGGER.warn("The service on {} is unavailable.", url);
					LOGGER.debug("The exception:", e);
				}
				url = Utils.removeTrailingChar(serverDTO.getUrl(), "/") + "/cache/poi/list";
				List<POIDTO> poiDTOsInCache = null;
				try {
					poiDTOsInCache = restManager.getPOIDTOsFromREST(url);
				} catch(Exception e) {
					LOGGER.warn("The service on {} is unavailable.", url);
					LOGGER.debug("The exception:", e);
				}
				
				int y = 0;
				matrix[x] = new String[serviceDTOs.size() + poiDTOs.size()];
				// putting the service status to the matrix
				for (ServiceDTO serviceDTO : serviceDTOs) {
					matrix[x][y++] = getCacheStatusByService(serviceDTO, serviceDTOsInCache);
					
					// putting the POI status that belongs to the service to the matrix
					List<POIDTO> poiDTOsOfServiceDTO = getPOIsForService(serviceDTO, poiDTOs);
					for (POIDTO poiDTOOfServiceDTO : poiDTOsOfServiceDTO) {
						matrix[x][y++] = getCacheStatusByPOI(poiDTOOfServiceDTO, poiDTOsInCache);
					}
				}
				x++;
			}
			
			return reverseXandY(matrix);

			
		} else {
			throw new RuntimeException("Unknown vertical value: " + verticalValue);
		}
	}
	
	
	private List<POIDTO> getPOIsForService(ServiceDTO serviceDTO, List<POIDTO> poiDTOs) {
		List<POIDTO> result = new ArrayList<POIDTO>();
		for(POIDTO poiDTO : poiDTOs) {
			if (poiDTO.getServiceDTO().getId().longValue() == serviceDTO.getId().longValue()) {
				result.add(poiDTO);
			}
		}
		return result;
	}


	private String[][] reverseXandY(String[][] matrix) {
		if (matrix.length == 0) {
			return matrix;
		}
		int xLength = matrix.length;
		int yLength = matrix[0].length;
		String[][] reversedMatrix = new String[yLength][xLength];
		
		for (int i = 0; i < yLength; i++) {
			reversedMatrix[i] = new String[xLength];
			for (int j = 0; j < xLength; j++) {
				reversedMatrix[i][j] = matrix[j][i];
			}
		}
		
		return reversedMatrix;
	}


	private String getCacheStatusByService(ServiceDTO serviceDTO, List<ServiceDTO> serviceDTOsInCache) {
		if (serviceDTOsInCache == null) {
			return "OFFLINE";
		}
		if (serviceDTOsInCache.size() == 0) {
			return "NOT_FOUND";
		}
		else {
			for(ServiceDTO serviceDTOInCache : serviceDTOsInCache) {
				if (serviceDTOInCache.getName().equals(serviceDTO.getName())) {
					if (isEqualServiceDTOs(serviceDTO, serviceDTOInCache)) {
						return "UP_TO_DATE";
					} else {
						return "OLD";
					}
				}
			}
			return "NOT_FOUND";
		}
	}


	private String getCacheStatusByPOI(POIDTO poiDTO, List<POIDTO> poiDTOsInCache) {
		if (poiDTOsInCache == null) {
			return "OFFLINE";
		}
		if (poiDTOsInCache.size() == 0) {
			return "NOT_FOUND";
		}
		for(POIDTO poiDTOInCache : poiDTOsInCache) {
			if (poiDTOInCache.getName().equals(poiDTO.getName())) {
				if (isEqualPOIDTOs(poiDTO, poiDTOInCache)) {
					return "UP_TO_DATE";
				} else {
					return "OLD";
				}
			}
		}
		return "NOT_FOUND";
	}

	


	private boolean isEqualServiceDTOs(ServiceDTO sDTO1, ServiceDTO sDTO2) {
		if (    sDTO1.getName().equals(sDTO2.getName()) && 
				sDTO1.getServiceType().equals(sDTO2.getServiceType()) && 
				((sDTO1.getSoapAction() == null && sDTO2.getSoapAction() == null) || sDTO1.getSoapAction().equals(sDTO2.getSoapAction())) &&
				((sDTO1.getHttpMethod() == null && sDTO2.getHttpMethod() == null) || sDTO1.getHttpMethod().equals(sDTO2.getHttpMethod())) &&
				((sDTO1.getDatabaseQuery() == null && sDTO2.getDatabaseQuery() == null) || sDTO1.getDatabaseQuery().equals(sDTO2.getDatabaseQuery()))
				) {
			if ((sDTO1.getRequestTemplateText() == null && sDTO2.getRequestTemplateText() == null) || (sDTO1.getRequestTemplateText().equals(sDTO2.getRequestTemplateText()))) {
				// namespace
				if (MapUtils.equals(sDTO1.getNamespaces(), sDTO2.getNamespaces())) {
					// endpoints
					String[] a1 = sDTO1.getEndpoints().toArray(new String[0]);
					Arrays.sort(a1);
					String[] a2 = sDTO2.getEndpoints().toArray(new String[0]);
					Arrays.sort(a2);
					return Arrays.equals(a1, a2);
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	
	private boolean isEqualPOIDTOs(POIDTO pDTO1, POIDTO pDTO2) {
		if (    pDTO1.getName().equals(pDTO2.getName()) && 
				pDTO1.getServiceDTO().getName().equals(pDTO2.getServiceDTO().getName()) &&
				pDTO1.getSingleValue().equals(pDTO2.getSingleValue()) &&
				!(pDTO1.getEncryptionNeeded().booleanValue() ^ pDTO2.getEncryptionNeeded().booleanValue()) 
			) {
			// extracts
			for(String rec1 : pDTO1.getExtracts()) {
				boolean found = false;
				for(String rec2 : pDTO2.getExtracts()) {
					if (rec1.equals(rec2)) {
						found = true;
						break;
					}
				}
				if (!found) {
					return false;
				}
			}
			return true;
		} else {
			return false;
		}
	}

	
	private String[] convertToArray(Map<String, String> map) {
		String[] result = new String[map.keySet().size()];
		int i = 0;
		for(String key : map.keySet()) {
			result[i++] = key + "  :  " + map.get(key);
		}
		return result;
	}


}
