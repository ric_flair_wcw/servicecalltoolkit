package org.sct.server.management.controller.administration;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.sct.server.common.beans.ServiceDTO;
import org.sct.server.common.controller.ParentController;
import org.sct.server.common.persistence.service.IServiceEntityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;


@Controller
@RequestMapping("/administration/service")
public class ServiceController extends ParentController {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(ServiceController.class);
	
	@Autowired
	private IServiceEntityService serviceDAO;
	
	
	@RequestMapping(value = "/main", method = RequestMethod.GET)
	public String showMainServicePage() throws SQLException {
		LOGGER.debug("Received GET request to show the service main (list) page.");

		return "/administration/service.tiles";
	}
	
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public @ResponseBody Object getServices() throws JsonParseException, JsonMappingException, IOException {
		LOGGER.debug("Received GET request to get the service list for displaying them in a table.");
		
		// getting the service list
		List<ServiceDTO> serviceDTOs = serviceDAO.getServices();
		LOGGER.debug("Retrieved {} service(s)", serviceDTOs.size());
		
		// creating the list that will be sent back to angular js
		Object[] simplifiedServiceList = new Object[serviceDTOs.size()];
		int ind = 0;
		for(ServiceDTO serviceDTO : serviceDTOs) {
			Map<String, Object> record = new HashMap<String, Object>();
			record.put("id", serviceDTO.getId());
			record.put("n", serviceDTO.getName());
			record.put("st", serviceDTO.getServiceType().toString());
			simplifiedServiceList[ind++] = record;
		}
		
		return simplifiedServiceList;
	}

	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public @ResponseBody Object getService(@PathVariable("id") Long id) throws JsonParseException, JsonMappingException, IOException {
		LOGGER.debug("Received GET request to get the details of one service for editing it.");
		
		// getting the service
		ServiceDTO serviceBean = serviceDAO.getService(id);
		LOGGER.debug("The service to be retrieved is {}", serviceBean);
		
		return serviceBean;
	}
	

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public @ResponseBody Object deleteService(@PathVariable("id") Long id) {
		LOGGER.debug("Received POST request to delete a service.");

		// deleting a service
		serviceDAO.deleteService(id);
		
		return null;
	}


	// notTODO at the moment the update and create happen here => the id has to be sent to JS and getting back to
	//   decide if a new record has to be created or update an existing one
	// notTODO sending back a status??? (the modified record doesn't have to be sent back as it is already there in JS)
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public @ResponseBody Object saveService(@RequestBody ServiceDTO serviceDTO) throws JsonGenerationException, JsonMappingException, IOException {
		LOGGER.debug("Received POST request to save a service.");

		// saving the service
		Long serviceId = serviceDAO.saveService(serviceDTO);
		
		Map<String, String> response = new HashMap<String, String>();
		response.put("serviceId", serviceId.toString());
		return response;
	}
	
	
}
