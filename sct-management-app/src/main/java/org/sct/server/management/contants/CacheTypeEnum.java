package org.sct.server.management.contants;

public enum CacheTypeEnum {

	
	MEMCACHED_1_4_21("Memcached", "1.4.21");
	
	private String cacheType;
	private String version;
	
	CacheTypeEnum(String cacheType, String version) {
		this.cacheType = cacheType;
		this.version = version;
	}

	public String getCacheType() {
		return cacheType;
	}

	public String getVersion() {
		return version;
	}
	
	public String toString() {
		return cacheType + "v" + version;
	}
	
}
