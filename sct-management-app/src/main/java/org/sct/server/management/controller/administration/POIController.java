package org.sct.server.management.controller.administration;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.sct.server.common.beans.POIDTO;
import org.sct.server.common.controller.ParentController;
import org.sct.server.common.exceptions.ExceptionUtils;
import org.sct.server.common.persistence.service.IPOIEntityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;


@Controller
@RequestMapping("/administration/poi")
public class POIController extends ParentController {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(POIController.class);
	
	@Autowired
	private IPOIEntityService poiDAO;

	
	@RequestMapping(value = "/main", method = RequestMethod.GET)
	public String showMainServicePage() {
		LOGGER.debug("Received GET request to show the POI main (list) page.");

		return "/administration/poi.tiles";
	}
	
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public @ResponseBody Object getPOIs() throws JsonParseException, JsonMappingException, IOException {
		LOGGER.debug("Received GET request to get the POI list for displaying them in a table.");
		
		// getting the POI list
		List<POIDTO> poiDTOs = poiDAO.getPOIs();
		LOGGER.debug("Retrieved {} POI(s)", poiDTOs.size());
		
		// creating the list that will be sent back to angular js
		Object[] simplifiedPOIList = new Object[poiDTOs.size()];
		int ind = 0;
		for(POIDTO poiDTO : poiDTOs) {
			Map<String, Object> record = new HashMap<String, Object>();
			record.put("id", poiDTO.getId());
			record.put("n", poiDTO.getName());
			record.put("cnt", poiDTO.getExtracts().size());
			record.put("s", poiDTO.getServiceDTO().getName() + " (" + poiDTO.getServiceDTO().getServiceType().toString() + ")");
			simplifiedPOIList[ind++] = record;
		}
		
		return simplifiedPOIList;
	}

	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public @ResponseBody Object getPOI(@PathVariable("id") Long id) throws JsonParseException, JsonMappingException, IOException {
		LOGGER.debug("Received GET request to get the details of one POI for editing it.");
		
		// getting the service
		POIDTO poiDTO = poiDAO.getPOI(id);
		LOGGER.debug("The POI to be retrieved is {}", poiDTO);
		
		return poiDTO;
	}
	

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public @ResponseBody Object deletePOI(@PathVariable("id") Long id) {
		LOGGER.debug("Received POST request to delete a POI.");

		// deleting the POI
		poiDAO.deletePOI(id);
		
		return null;
	}


	// notTODO at the moment the update and create happen here => the id has to be sent to JS and getting back to
	//   decide if a new record has to be created or update an existing one
	// notTODO sending back a status??? (the modified record doesn't have to be sent back as it is already there in JS)
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public @ResponseBody Object savePOI(@RequestBody POIDTO poiDTO) throws JsonGenerationException, JsonMappingException, IOException {
		LOGGER.debug("Received POST request to save a POI.");

		// more extracts might come as needed (from service discovery) that have to be removed
		//  e.g.  /a/b, /a/b/1, /a/b/2  => no need the /a/b
		List<String> toRemove = new ArrayList<String>();
		for(int i = 0; i < poiDTO.getExtracts().size(); i++) {
			String v = poiDTO.getExtracts().get(i);
			for(int j = 0; j < poiDTO.getExtracts().size(); j++) {
				if (i != j) {
					String v2 = poiDTO.getExtracts().get(j);
					if (v2.contains(v)) {
						toRemove.add(v);
					}
				}
			}
		}
		poiDTO.getExtracts().removeAll(toRemove);
		
		// saving the POI
		poiDAO.savePOI(poiDTO);
		
		return null;
	}
	
	
}
