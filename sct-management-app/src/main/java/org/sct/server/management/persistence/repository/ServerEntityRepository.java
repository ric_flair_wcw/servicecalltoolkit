package org.sct.server.management.persistence.repository;

import org.sct.server.common.persistence.model.ServerEntity;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ServerEntityRepository extends JpaRepository<ServerEntity, Long> {

	
	/**
     * Finds the ServerEntity by using its last name as a search criteria.
     * @param name the custom name of the server
     * @return  the ServerEntity
     */
	public ServerEntity findByName(String name);
	
}
