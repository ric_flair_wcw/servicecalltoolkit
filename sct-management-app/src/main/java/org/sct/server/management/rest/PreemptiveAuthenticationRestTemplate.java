package org.sct.server.management.rest;

import org.apache.http.HttpHost;
import org.sct.common.rest.HttpComponentsClientHttpRequestDigestAuthFactory;
import org.springframework.web.client.RestOperations;
import org.springframework.web.client.RestTemplate;

public class PreemptiveAuthenticationRestTemplate extends RestTemplate implements RestOperations  {

	
   public PreemptiveAuthenticationRestTemplate(HttpComponentsClientHttpRequestDigestAuthFactory factory) {
        super();
        setRequestFactory(factory);
    }

   
    public void setCredentials(HttpHost host, String username, String password) {
    	HttpComponentsClientHttpRequestDigestAuthFactory requestFactory = (HttpComponentsClientHttpRequestDigestAuthFactory) getRequestFactory();
        requestFactory.setCredentials(host, username, password);
    }


    
}
