package org.sct.server.management.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpHost;
import org.sct.server.common.beans.POIDTO;
import org.sct.server.common.beans.ServiceDTO;
import org.sct.server.common.persistence.utils.Converter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


public class RestServiceManager {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(RestServiceManager.class);
			
	private PreemptiveAuthenticationRestTemplate restTemplate;
	private Converter converter;
	protected ObjectMapper objectMapper;
	
	
	public void deleteFromCacheViaREST(String url) throws JsonParseException, JsonMappingException, IOException {
		LOGGER.debug("Deleting a resource via REST service (DELETE) on {}.", url);
		
		try {
			callRest(url, null, HttpMethod.DELETE);
		} catch(HttpServerErrorException hsee) {
			Map<String, String> exceptionMap = objectMapper.readValue(hsee.getResponseBodyAsString(), Map.class); ;
			throw new RuntimeException("Exception arrived from the REST service! url: " + url + ", exception class: " + 
					exceptionMap.get("exceptionClass") + ", exception message: " + exceptionMap.get("exceptionMessage"));
		}
	}
	
	
	public List<ServiceDTO> getServiceDTOsFromREST(String url) throws JsonParseException, JsonMappingException, IOException {
		LOGGER.debug("Gettting serviceDTOs from REST service (GET) on {}", url);
		
		try {
			ResponseEntity<Object> responseEntity = callRest(url, null, HttpMethod.GET);
			
			List<ServiceDTO> serviceDTOs = new ArrayList<ServiceDTO>();
			for(Map<String, Object> record : (List<Map<String, Object>>)responseEntity.getBody()) {
				serviceDTOs.add(converter.convertToServiceDTOFromMap(record));
			}
			
			return serviceDTOs;
		} catch(HttpServerErrorException hsee) {
			Map<String, String> exceptionMap = objectMapper.readValue(hsee.getResponseBodyAsString(), Map.class); ;
			throw new RuntimeException("Exception arrived from the REST service! url: " + url + ", exception class: " + 
					exceptionMap.get("exceptionClass") + ", exception message: " + exceptionMap.get("exceptionMessage"));
		}
	}


	public List<POIDTO> getPOIDTOsFromREST(String url) throws JsonParseException, JsonMappingException, IOException {
		LOGGER.debug("Getting poiDTOs from REST service (GET) on {}", url);
		
		try {
			ResponseEntity<Object> responseEntity = callRest(url, null, HttpMethod.GET);
			
			List<POIDTO> poiDTOs = new ArrayList<POIDTO>();
			for(Map<String, Object> record : (List<Map<String, Object>>)responseEntity.getBody()) {
				poiDTOs.add(converter.convertToPOIDTOFromMap(record));
			}
			
			return poiDTOs;
		} catch(HttpServerErrorException hsee) {
			Map<String, String> exceptionMap = objectMapper.readValue(hsee.getResponseBodyAsString(), Map.class); ;
			throw new RuntimeException("Exception arrived from the REST service! url: " + url + ", exception class: " + 
					exceptionMap.get("exceptionClass") + ", exception message: " + exceptionMap.get("exceptionMessage"));
		}
	}


	public ServiceDTO getServiceDTOFromREST(String url) throws JsonParseException, JsonMappingException, IOException {
		LOGGER.debug("Gettting serviceDTO from REST service (GET) on {}", url);
		
		try {
			ResponseEntity<Object> responseEntity = callRest(url, null, HttpMethod.GET);
			
			Map<String, Object> jsonMap = (Map)responseEntity.getBody();
			return converter.convertToServiceDTOFromMap(jsonMap);
		} catch(HttpServerErrorException hsee) {
			Map<String, String> exceptionMap = objectMapper.readValue(hsee.getResponseBodyAsString(), Map.class); ;
			throw new RuntimeException("Exception arrived from the REST service! url: " + url + ", exception class: " + 
					exceptionMap.get("exceptionClass") + ", exception message: " + exceptionMap.get("exceptionMessage"));
		}
	}

	
	public POIDTO getPOIDTOFromREST(String url) throws JsonParseException, JsonMappingException, IOException {
		LOGGER.debug("Gettting poiDTO from REST service (GET) on {}", url);
		
		try {
			ResponseEntity<Object> responseEntity = callRest(url, null, HttpMethod.GET);
			
			Map<String, Object> jsonMap = (Map)responseEntity.getBody();
			return converter.convertToPOIDTOFromMap(jsonMap);
		} catch(HttpServerErrorException hsee) {
			Map<String, String> exceptionMap = objectMapper.readValue(hsee.getResponseBodyAsString(), Map.class); ;
			throw new RuntimeException("Exception arrived from the REST service! url: " + url + ", exception class: " + 
					exceptionMap.get("exceptionClass") + ", exception message: " + exceptionMap.get("exceptionMessage"));
		}
	}

	
	public void sendObjectToREST(String url, Object objecToBeSent) throws JsonParseException, JsonMappingException, IOException {
		LOGGER.debug("Sending object to REST service (POST) on {}, object: {}", url, objecToBeSent);
		
		try {
			ResponseEntity<Object> responseEntity = callRest(url, objecToBeSent, HttpMethod.POST);
		} catch(HttpServerErrorException hsee) {
			Map<String, String> exceptionMap = objectMapper.readValue(hsee.getResponseBodyAsString(), Map.class); ;
			throw new RuntimeException("Exception arrived from the REST service! url: " + url + ", exception class: " + 
					exceptionMap.get("exceptionClass") + ", exception message: " + exceptionMap.get("exceptionMessage"));
		}
	}
	

	private ResponseEntity<Object> callRest(String url, Object objecToBeSent, HttpMethod httpMethod) {
		// setting the accepted HTTP type
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		// setting the auth info for digest authentication
		UriComponents uriComponents = UriComponentsBuilder.fromUriString(url).build();
		HttpHost host = new HttpHost(uriComponents.getHost(), uriComponents.getPort(), uriComponents.getScheme());
		restTemplate.setCredentials(host, SecurityContextHolder.getContext().getAuthentication().getName(), SecurityContextHolder.getContext().getAuthentication().getCredentials().toString());
		// creating the HttpEntity to be sent
		HttpEntity<Object> requestEntity = new HttpEntity<Object>(objecToBeSent == null ? "parameters" : objecToBeSent, headers);

		// call
		if (httpMethod.compareTo(HttpMethod.DELETE) != 0) {
			return restTemplate.exchange(url, httpMethod, requestEntity, Object.class);
		} else {
			restTemplate.delete(url);
			return null;
		}
	}


	public PreemptiveAuthenticationRestTemplate getRestTemplate() {
		return restTemplate;
	}

	public void setRestTemplate(PreemptiveAuthenticationRestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	public Converter getConverter() {
		return converter;
	}

	public void setConverter(Converter converter) {
		this.converter = converter;
	}

	public ObjectMapper getObjectMapper() {
		return objectMapper;
	}

	public void setObjectMapper(ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
	}

}
