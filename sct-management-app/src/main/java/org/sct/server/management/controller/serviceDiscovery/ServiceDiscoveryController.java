package org.sct.server.management.controller.serviceDiscovery;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.sct.common.objects.map.HierarchialHashMap;
import org.sct.server.common.beans.ServiceDTO;
import org.sct.server.common.controller.ParentController;
import org.sct.server.common.persistence.service.IServiceEntityService;
import org.sct.server.common.utils.IXmlToMapConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.eviware.soapui.SoapUI;
import com.eviware.soapui.impl.wsdl.WsdlInterface;
import com.eviware.soapui.impl.wsdl.WsdlOperation;
import com.eviware.soapui.impl.wsdl.WsdlProject;
import com.eviware.soapui.impl.wsdl.support.http.SoapUIMultiThreadedHttpConnectionManager;
import com.eviware.soapui.impl.wsdl.support.wsdl.WsdlImporter;
import com.eviware.soapui.model.iface.Operation;
import com.ximpleware.EOFException;
import com.ximpleware.EncodingException;
import com.ximpleware.EntityException;
import com.ximpleware.NavException;
import com.ximpleware.ParseException;
import com.ximpleware.XPathEvalException;
import com.ximpleware.XPathParseException;

@Controller
@RequestMapping("/serviceDiscovery/serviceDiscovery")
public class ServiceDiscoveryController extends ParentController {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(ServiceDiscoveryController.class);

	@Autowired
	private IXmlToMapConverter xmlToMapConverter;
	@Autowired
	private IServiceEntityService serviceDAO;

	
	@RequestMapping(value = "/main", method = RequestMethod.GET)
	public String showMainPage() {
		LOGGER.debug("Received GET request to show the service discovery main page.");

		return "/serviceDiscovery/serviceDiscovery.tiles";
	}

	
	@RequestMapping(value="getServiceDetailsFromWsdl", method = RequestMethod.POST)
	public @ResponseBody Object getServiceDetailsFromWsdl(@RequestBody Map<String, String> requestBody) throws Exception {
		LOGGER.debug("Received POST request to get the service details of a WSDL from an URL. requestBody={}.", requestBody);
		
		Map<String, Object> result = new HashMap<String, Object>();
		
		// getting the WSDL URL and the operation from the request body
		String wsdlUrl = requestBody.get("wsdlUrl");
		String operation = requestBody.get("operation");
		String soapAction = null;
		if (operation == null) {
			String serviceId = requestBody.get("serviceId");
			// getting the service from DB
			ServiceDTO serviceBean = serviceDAO.getService(Long.valueOf(serviceId));
			soapAction = serviceBean.getSoapAction();
		}
		
		// reading the WSDL
		try {
			WsdlProject project = new WsdlProject();
		    WsdlInterface[] wsdls = loadWsdl(project, wsdlUrl);
		    WsdlInterface wsdl = wsdls[0];
		    WsdlOperation wsdlOperation = getWsdlOperation(operation, wsdl, soapAction, wsdlUrl);
		    // creating the SOAP request
		    result.put("request", wsdlOperation.createRequest(true));
		    // creating the SOAP response
		    result.put("response", wsdlOperation.createResponse(true));
		    // getting the service name
		    result.put("serviceName", getServiceName(wsdl) + "-" + operation);
		    // getting the SOAP action
		    result.put("soapAction", wsdlOperation.getAction());
		    // getting the namespaces
		    xmlToMapConverter.start(result.get("response").toString());
		    result.put("namespaces", xmlToMapConverter.getNamespaces());
		} finally {
			shutdownSoapUI();
		}
	    
		return result;
	}


	@RequestMapping(value = "/getOperationsFromWsdl", method = RequestMethod.POST)
	public @ResponseBody Object getOperationsFromWsdl(@RequestBody Map<String, String> requestBody) throws Exception {
		LOGGER.debug("Received POST request to get the operations of a WSDL from an URL. requestBody={}.", requestBody);
		
		Map<String, Object> result = new HashMap<String, Object>();
		
		// getting the WSDL URL from the request body
		String wsdlUrl = requestBody.get("wsdlUrl");
		
		// reading the WSDL
		try {
			WsdlProject project = new WsdlProject();
			WsdlInterface[] wsdls = loadWsdl(project, wsdlUrl);
		    WsdlInterface wsdl = wsdls[0];
		    // this call is only to check if the WSDL contains one service definition
		    getServiceName(wsdl);
		    // getting the operation names
		    String[] operations = new String[wsdl.getOperationList().size()];
		    int i = 0;
		    for (Operation operation : wsdl.getOperationList()) {
				WsdlOperation wsdlOperation = (WsdlOperation) operation;
		        operations[i++] = wsdlOperation.getName();
		    }
		    
		    result.put("operations", operations);
		} 
		finally {
			shutdownSoapUI();
		}
	    
	    return result;
	}
	
	
	private WsdlInterface[] loadWsdl(WsdlProject project, String wsdlUrl) {
		try {
			WsdlInterface[] wsdls = WsdlImporter.importWsdl(project, wsdlUrl);
			return wsdls;
		} catch(Throwable t) {
			throw new RuntimeException("Cannot load the WSDL from " + wsdlUrl, t);
		}
	}


	@RequestMapping(value = "/responseTree", method = RequestMethod.POST)
	public @ResponseBody Object getResponseTree(@RequestBody Map<String, String> requestBody) throws EncodingException, EOFException, EntityException, NavException, ParseException, XPathParseException, XPathEvalException {
		LOGGER.debug("Received POST request to build the tree of the SOAP response of the service. requestBody={}.", requestBody);
		
		// getting the response XML string from the request body
		String responseString = requestBody.get("reponseString");//"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns=\"http://virginmedia/schema/GetAddressForPostCode/4/0\" xmlns:ns1=\"http://virginmedia/schema/ResponseHeader/2/1\" xmlns:ns2=\"http://virginmedia/schema/faults/2/1\"><soapenv:Header/><soapenv:Body><ns:GetAddressForPostCodeResponse><ns:responseHeader><ns1:id>?</ns1:id><ns1:fault><ns2:faultcode>?</ns2:faultcode><ns2:detail>?</ns2:detail></ns1:fault></ns:responseHeader><ns:postalAddress><ns:displayAddress>?</ns:displayAddress><ns:buildingID>?</ns:buildingID><ns:building>?</ns:building><ns:apartment>?</ns:apartment><ns:buildingName>?</ns:buildingName><ns:county>?</ns:county><ns:locality>?</ns:locality><ns:nonUKPostcode>?</ns:nonUKPostcode><ns:postcodeSuffix>?</ns:postcodeSuffix><ns:streetName>?</ns:streetName><ns:streetType>?</ns:streetType><ns:subBuildingID>?</ns:subBuildingID><ns:town>?</ns:town><ns:country>?</ns:country><ns:postcode>?</ns:postcode><ns:addressIdentification><ns:identifierType>?</ns:identifierType><ns:addressIdentifier>?</ns:addressIdentifier><ns:instanceIdentifier>?</ns:instanceIdentifier></ns:addressIdentification><ns:handoverPointDetails><ns:L2SID>?</ns:L2SID><ns:HandoverPortID>?</ns:HandoverPortID><ns:SVLANID>?</ns:SVLANID><ns:exchangeRef>?</ns:exchangeRef></ns:handoverPointDetails></ns:postalAddress><ns:serviceDirective>?</ns:serviceDirective><ns:maximumAddressMatches>?</ns:maximumAddressMatches></ns:GetAddressForPostCodeResponse></soapenv:Body></soapenv:Envelope>";//requestBody.get("response");
		
		// building the response map
		xmlToMapConverter.start(responseString);
		HierarchialHashMap<String, Object> responseMap = xmlToMapConverter.getMap();
		
		return responseMap;
	}
	
	
	private String getServiceName(WsdlInterface wsdl) throws Exception {
		String serviceName = null;
	    for(Object o : wsdl.getWsdlContext().getDefinition().getServices().keySet()) {
	    	if (serviceName != null) {
	    		throw new RuntimeException("The WSDL has to contain only one service definition! Multiple service definitions are not supported at the moment...");
	    	}
	    	serviceName = ((javax.xml.namespace.QName)o).getLocalPart();
	    }

		return serviceName;
	}


	private void shutdownSoapUI() {
		// Need to shutdown all the threads invoked by each SoapUI
		SoapUI.getThreadPool().shutdown();
		try {
			SoapUI.getThreadPool().awaitTermination(1, TimeUnit.SECONDS);
		} catch (InterruptedException e) {			
			LOGGER.warn("Exception when trying to shutdown soapUI!", e);
		}

		// Now to shutdown the monitor thread setup by SoapUI
		Thread[] tarray = new Thread[Thread.activeCount()];
		Thread.enumerate(tarray);
		for (Thread t : tarray) {
			if (t instanceof SoapUIMultiThreadedHttpConnectionManager.IdleConnectionMonitorThread) {
				((SoapUIMultiThreadedHttpConnectionManager.IdleConnectionMonitorThread) t).shutdown();
			}
		}

		// Finally Shutdown SoapUI itself.
		SoapUI.shutdown();
	}


	private WsdlOperation getWsdlOperation(String operationName, WsdlInterface wsdl, String soapAction, String wsdlUrl) {
		if (operationName != null) {
			return (WsdlOperation) wsdl.getOperationByName(operationName);
		} else {
			List<Operation> operations = wsdl.getOperationList();
			for (Operation operation : operations) {
				if (((WsdlOperation)operation).getAction().equals(soapAction)) {
					return (WsdlOperation)operation;
				}
			}
			throw new RuntimeException("I couldn't find an operation with the SOAP action '"+soapAction+"' in '"+wsdlUrl+"'!");
		}
	}
	
	
}
