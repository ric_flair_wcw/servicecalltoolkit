package org.sct.server.management.controller.statistics;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.sct.server.common.controller.ParentController;
import org.sct.server.common.persistence.service.IStatServicePoiDateCntEntityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/statistics/statistics")
public class StatisticsController extends ParentController {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(StatisticsController.class);
	
	@Autowired
	private IStatServicePoiDateCntEntityService service;
	
	
	@RequestMapping(value = "/main", method = RequestMethod.GET)
	public String showMainPage() {
		LOGGER.debug("Received GET request to show the statistics main page.");

		return "/statistics/statistics.tiles";
	}
	
	
	@RequestMapping(value="/getStatistics/{type}/{serviceName}/{startDate}/{endDate}", method = RequestMethod.GET)
	public @ResponseBody Map<String, List<Object[]>> getStatistics(@PathVariable("type") String type,
																   @PathVariable("serviceName") String serviceName,
																   @PathVariable("startDate") String startDateString,
																   @PathVariable("endDate") String endDateString) throws ParseException {
		LOGGER.debug("Received GET request to get the statistics for the {} {} starting from {} ending at {}.", type, serviceName, startDateString, endDateString); 
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd H");
		Calendar startDateCalendar = Calendar.getInstance();
		startDateCalendar.setTime(dateFormat.parse(startDateString));
		Calendar endDateCalendar = Calendar.getInstance();
		endDateCalendar.setTime(dateFormat.parse(endDateString));

		dateFormat = new SimpleDateFormat("HH:mm");
		
		// getting the statistics from DB
		List<Object[]> stat = service.getStatistics(type, serviceName, startDateCalendar, endDateCalendar);
		
		// converting the list
		List<Object[]> successfulList = new ArrayList<Object[]>();
		List<Object[]> unsuccessfulList = new ArrayList<Object[]>();
		for(int i = 0; i < stat.size(); i++) {
			Object[] recStat = stat.get(i);
			Object[] record = new Object[2];
			// if successful
			record[0] = ((Calendar)recStat[0]).getTime();
			record[1] = Integer.valueOf(String.valueOf(recStat[2]));
			if ( ((Boolean)recStat[1]).booleanValue() ) {
				successfulList.add(record);
			} else {
				unsuccessfulList.add(record);
			}
		}

		// adding the 3 lists to a map and return it
		Map<String, List<Object[]>> result = new HashMap<String, List<Object[]>>();
		result.put("successfulList", successfulList);
		result.put("unsuccessfulList", unsuccessfulList);
		
		return result;
	}


	@RequestMapping(value="/getStatisticsForComparison", method = RequestMethod.POST)
	public @ResponseBody Map<String, List<Object[]>> getStatisticsForComparison(@RequestBody Map<String, Object> requestBody) throws ParseException {
		LOGGER.debug("Received POST request to get the statistics for comparison. requestBody: {}", requestBody);
		
		String startDateString = (String) requestBody.get("startDate");
		String endDateString = (String) requestBody.get("endDate");
		String type = (String) requestBody.get("type");
		List<String> nameArray = (List<String>) requestBody.get("names");
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd H");
		Calendar startDateCalendar = Calendar.getInstance();
		startDateCalendar.setTime(dateFormat.parse(startDateString));
		Calendar endDateCalendar = Calendar.getInstance();
		endDateCalendar.setTime(dateFormat.parse(endDateString));

		dateFormat = new SimpleDateFormat("HH:mm");
		
		// getting the statistics from DB
		List<Object[]> stat = service.getStatisticsForComparison(type, nameArray, startDateCalendar, endDateCalendar);
		
		// converting the list
		Map<String, List<Object[]>> result = new HashMap<String, List<Object[]>>();
		String name = null;
		List<Object[]> list = new ArrayList<Object[]>();
		Object[] rec = null;
		for(int i = 0; i < stat.size(); i++) {
			// initialising the name
			if (name == null) {
				name = (String) stat.get(0)[0];
			}
			// if a service/POI ended then it has to be added to the result
			if (!stat.get(i)[0].toString().equals(name)) {
				result.put(name, list);
				list = new ArrayList<Object[]>();
				name = stat.get(i)[0].toString();
			}
			// filling the list
			rec = new Object[2];
			rec[0] = stat.get(i)[1];
			rec[1] = stat.get(i)[2];
			list.add(rec);
		}
		result.put(name, list);
		
		return result;
		
	}
	
}
