package org.sct.server.management.controller.administration;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.sct.common.constants.ServiceTypeEnum;
import org.sct.common.utils.Utils;
import org.sct.server.common.beans.POIDTO;
import org.sct.server.common.beans.ServerDTO;
import org.sct.server.common.beans.ServiceDTO;
import org.sct.server.common.constants.CacheManagementViewEnum;
import org.sct.server.common.persistence.service.IPOIEntityService;
import org.sct.server.common.persistence.service.IServiceEntityService;
import org.sct.server.management.persistence.service.IServerEntityService;
import org.sct.server.management.rest.RestServiceManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:management-app-context-test.xml")
@WebAppConfiguration
public class CacheManagementControllerTest {

	
	private MockMvc mockMvc;
	@Autowired
	private ApplicationContext applicationContext;
	@Autowired
	private IServiceEntityService mockedServiceDAO;
	@Autowired
	private IPOIEntityService mockedPoiDAO;
	@Autowired
	private IServerEntityService mockedServerDAO;
	@Autowired
	private RestServiceManager mockedRestManager;
	
	
	@Before
    public void setup() {
		CacheManagementController cacheManagementController = (CacheManagementController) applicationContext.getBean("cacheManagementController");
		this.mockMvc = MockMvcBuilders.standaloneSetup(cacheManagementController).build();
    }

	
	@Test
	public void testShowMainCacheManagementPage_successfulViewRetrieval() throws Exception {
		mockMvc.perform(get("/administration/cacheManagement/main"))
						.andExpect(status().isOk())
						.andExpect(view().name("/administration/cacheManagement.tiles"));
	}
	
	
	@Test
	public void testGetServiceAndPOIList_successful() throws Exception {
		// defining the mockito expectations
		List<ServiceDTO> serviceDTOs = new ArrayList<ServiceDTO>();
		serviceDTOs.add(new ServiceDTO(12l, "servcie0", null, null, null, null, null, null, null, null));
		serviceDTOs.add(new ServiceDTO(15l, "servcie1", null, null, null, null, null, null, null, null));
		when(mockedServiceDAO.getServices()).thenReturn(serviceDTOs);
		List<POIDTO> poiDTOs = new ArrayList<POIDTO>();
		poiDTOs.add(new POIDTO(34l, "poi0", null, null, new ServiceDTO(12l, "servcie0", null, null, null, null, null, null, null, null), null, null));
		poiDTOs.add(new POIDTO(35l, "poi1", null, null, new ServiceDTO(12l, "servcie0", null, null, null, null, null, null, null, null), null, null));
		poiDTOs.add(new POIDTO(65l, "poi2", null, null, new ServiceDTO(15l, "servcie1", null, null, null, null, null, null, null, null), null, null));
		when(mockedPoiDAO.getPOIs()).thenReturn(poiDTOs);

		// calling the controller
		mockMvc.perform(get("/administration/cacheManagement/serviceAndPOI/list" ))
						.andExpect(status().isOk())
						.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
						.andExpect(jsonPath("$", hasSize(5)))
						.andExpect(jsonPath("$[0][0]", is("SERVICE")))
						.andExpect(jsonPath("$[0][1]", is("servcie0")))
						.andExpect(jsonPath("$[1][0]", is("POI")))
						.andExpect(jsonPath("$[1][1]", is("poi0")))
						.andExpect(jsonPath("$[2][0]", is("POI")))
						.andExpect(jsonPath("$[2][1]", is("poi1")))
						.andExpect(jsonPath("$[3][0]", is("SERVICE")))
						.andExpect(jsonPath("$[3][1]", is("servcie1")))
						.andExpect(jsonPath("$[4][0]", is("POI")))
						.andExpect(jsonPath("$[4][1]", is("poi2")));
		
		verify(mockedServiceDAO, times(1)).getServices();
		verify(mockedPoiDAO, times(1)).getPOIs();
	}
	
	
	@Test
	public void testGetServiceComparison_successful_service() throws Exception {
		String serverName = "localServer";
		String cacheManagementView = CacheManagementViewEnum.SERVICE.name();
		String serviceOrPOIName = "service1";
		
		// defining the mockito expectations
		when(mockedServerDAO.getServerByName(serverName)).thenReturn(new ServerDTO(3l, serverName, "http://localhost"));
		
		Map<String, String> namespaces = new HashMap<String, String>(); namespaces.put("env", "http:valami");
		List<String> endpoints = new ArrayList<String>(); endpoints.add("145"); endpoints.add("146");
		ServiceDTO serviceDTO = new ServiceDTO(4l, serviceOrPOIName, ServiceTypeEnum.WEBSERVICE, "soapAction", "requestTemplateText", namespaces, endpoints, null, null, "desc1");
		when(mockedServiceDAO.getServiceByName(serviceOrPOIName)).thenReturn(serviceDTO);
		
		String url = Utils.removeTrailingChar("http://localhost", "/") + "/cache/service/" + serviceOrPOIName;
		ServiceDTO serviceDTOInCache = new ServiceDTO(5l, serviceOrPOIName, ServiceTypeEnum.WEBSERVICE, "soapAction"+"_", "requestTemplateText"+"_", namespaces, endpoints, null, null, "desc1_");
		when(mockedRestManager.getServiceDTOFromREST(url)).thenReturn(serviceDTOInCache);
		
		// calling the controller
		mockMvc.perform(get("/administration/cacheManagement/serviceComparison/" + serverName  + "/" + cacheManagementView + "/" + serviceOrPOIName))
						.andExpect(status().isOk())
						.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
						.andExpect(jsonPath("$.name[0]", is(serviceOrPOIName)))
						.andExpect(jsonPath("$.name[1]", is(serviceOrPOIName)))
						.andExpect(jsonPath("$.serviceType[0]", is(ServiceTypeEnum.WEBSERVICE.getLabel())))
						.andExpect(jsonPath("$.serviceType[1]", is(ServiceTypeEnum.WEBSERVICE.getLabel())))
						.andExpect(jsonPath("$.soapAction[0]", is("soapAction_")))
						.andExpect(jsonPath("$.soapAction[1]", is("soapAction")))
						.andExpect(jsonPath("$.namespaces[0]", hasSize(1)))
						.andExpect(jsonPath("$.namespaces[0][0]", is("env  :  http:valami")))
						.andExpect(jsonPath("$.namespaces[1]", hasSize(1)))
						.andExpect(jsonPath("$.namespaces[1][0]", is("env  :  http:valami")))
						.andExpect(jsonPath("$.endpoints[0]", hasSize(2)))
						.andExpect(jsonPath("$.endpoints[1]", hasSize(2)))
						.andExpect(jsonPath("$.endpoints[0][1]", is("146")))
						.andExpect(jsonPath("$.endpoints[1][0]", is("145")))
						.andExpect(jsonPath("$.requestTemplateText[0]", is("requestTemplateText_")))
						.andExpect(jsonPath("$.requestTemplateText[1]", is("requestTemplateText")));

		verify(mockedServerDAO, times(1)).getServerByName(serverName);
		verify(mockedServiceDAO, times(1)).getServiceByName(serviceOrPOIName);
		verify(mockedRestManager, times(1)).getServiceDTOFromREST(url);
	}


	@Test
	public void testGetServiceComparison_successful_POI() throws Exception {
		String serverName = "localServer";
		String cacheManagementView = CacheManagementViewEnum.POI.name();
		String serviceOrPOIName = "poi1";
		
		// defining the mockito expectations
		when(mockedServerDAO.getServerByName(serverName)).thenReturn(new ServerDTO(3l, serverName, "http://localhost"));
		
		Map<String, String> namespaces = new HashMap<String, String>(); namespaces.put("env", "http:valami");
		List<String> endpoints = new ArrayList<String>(); endpoints.add("145"); endpoints.add("146");
		ServiceDTO serviceDTO = new ServiceDTO(4l, "serv1", ServiceTypeEnum.WEBSERVICE, "soapAction", "requestTemplateText", namespaces, endpoints, null, null, "desc1");
		List<String> extracts = new ArrayList<String>();
		extracts.add("/a/b"); extracts.add("/ad"); 
		POIDTO poiDTO = new POIDTO(65l, serviceOrPOIName, "true", true, serviceDTO, extracts, "desc2");
		when(mockedPoiDAO.getPOIByName(serviceOrPOIName)).thenReturn(poiDTO);
		
		String url = Utils.removeTrailingChar("http://localhost", "/") + "/cache/poi/" + serviceOrPOIName;
		POIDTO poiDTOInCache = new POIDTO(65l, serviceOrPOIName, "true", true, serviceDTO, extracts, "desc2");
		when(mockedRestManager.getPOIDTOFromREST(url)).thenReturn(poiDTOInCache);
		
		// calling the controller
		mockMvc.perform(get("/administration/cacheManagement/serviceComparison/" + serverName  + "/" + cacheManagementView + "/" + serviceOrPOIName))
						.andExpect(status().isOk())
						.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
						.andExpect(jsonPath("$.name[0]", is(serviceOrPOIName)))
						.andExpect(jsonPath("$.name[1]", is(serviceOrPOIName)))
						.andExpect(jsonPath("$.singleValue[0]", is("true")))
						.andExpect(jsonPath("$.singleValue[1]", is("true")))
						.andExpect(jsonPath("$.encryptionNeeded[0]", is("true")))
						.andExpect(jsonPath("$.encryptionNeeded[1]", is("true")))
						.andExpect(jsonPath("$.serviceDTO_id[0]", is("4")))
						.andExpect(jsonPath("$.serviceDTO_id[1]", is("4")))
						.andExpect(jsonPath("$.serviceDTO_name[0]", is("serv1")))
						.andExpect(jsonPath("$.serviceDTO_name[1]", is("serv1")))
						.andExpect(jsonPath("$.extracts[0]", hasSize(2)))
						.andExpect(jsonPath("$.extracts[1]", hasSize(2)));

		verify(mockedServerDAO, times(1)).getServerByName(serverName);
		verify(mockedPoiDAO, times(1)).getPOIByName(serviceOrPOIName);
		verify(mockedRestManager, times(1)).getPOIDTOFromREST(url);
	}


	@Test
	public void testGetServiceComparison_successful_Both() throws Exception {
		String serverName = "localServer";
		String cacheManagementView = CacheManagementViewEnum.BOTH.name();
		String serviceOrPOIName = "XXX";
		
		// defining the mockito expectations
		when(mockedServerDAO.getServerByName(serverName)).thenReturn(new ServerDTO(3l, serverName, "http://localhost"));
		
		// calling the controller
		mockMvc.perform(get("/administration/cacheManagement/serviceComparison/" + serverName  + "/" + cacheManagementView + "/" + serviceOrPOIName))
						.andExpect(status().isOk())
						.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
						.andExpect(jsonPath("$.soapAction").doesNotExist());

		verify(mockedServerDAO, times(1)).getServerByName(anyString());
		verify(mockedPoiDAO, times(0)).getPOIByName(anyString());
		verify(mockedRestManager, times(0)).getPOIDTOFromREST(anyString());
	}
	
	
	@Test
	public void testRefreshCache_successful_SERVICE() throws Exception {
		String serverName = "server1";
		String serviceOrPOIName = "serv1";

		// defining the mockito expectations
		when(mockedServerDAO.getServerByName(serverName)).thenReturn(new ServerDTO(3l, serverName, "http://localhostt"));

		Map<String, String> namespaces = new HashMap<String, String>(); namespaces.put("env", "http:valami");
		List<String> endpoints = new ArrayList<String>(); endpoints.add("145"); endpoints.add("146");
		ServiceDTO serviceDTO = new ServiceDTO(4l, serviceOrPOIName, ServiceTypeEnum.WEBSERVICE, "soapAction", "requestTemplateText", namespaces, endpoints, null, null, "desc1");
		when(mockedServiceDAO.getServiceByName(serviceOrPOIName)).thenReturn(serviceDTO);
		
		doNothing().when(mockedRestManager).sendObjectToREST(eq("http://localhostt/cache/service"), anyObject());

		// calling the controller
		String reqJson = 
				"{" +
				"	\"service_or_poi\" : \"" + serviceOrPOIName + "\"," +
				"	\"server\" : \"" + serverName + "\"," +
				"	\"cacheManagementView\" : \"SERVICE\"," +
				"	\"itIsAServiceOrPoiOrServer\" : \"SERVICE\"" +
				"}";
		mockMvc.perform(post("/administration/cacheManagement/refresh").content(reqJson).contentType(MediaType.APPLICATION_JSON))
						.andExpect(status().isOk())
						.andExpect(content().string(isEmptyOrNullString()));
		
		verify(mockedServerDAO, times(1)).getServerByName(serverName);
		verify(mockedServiceDAO, times(1)).getServiceByName(serviceOrPOIName);
		verify(mockedRestManager, times(1)).sendObjectToREST(eq("http://localhostt/cache/service"), anyObject());
	}
	
	
	@Test
	public void testRefreshCache_successful_POI() throws Exception {
		String serverName = "server1";
		String serviceOrPOIName = "poi1";

		// defining the mockito expectations
		when(mockedServerDAO.getServerByName(serverName)).thenReturn(new ServerDTO(3l, serverName, "http://localhostt"));

		Map<String, String> namespaces = new HashMap<String, String>(); namespaces.put("env", "http:valami");
		List<String> endpoints = new ArrayList<String>(); endpoints.add("145"); endpoints.add("146");
		ServiceDTO serviceDTO = new ServiceDTO(4l, "serv2", ServiceTypeEnum.WEBSERVICE, "soapAction", "requestTemplateText", namespaces, endpoints, null, null, "desc1");
		List<String> extracts = new ArrayList<String>();
		extracts.add("/a/b"); extracts.add("/ad");
		POIDTO poiDTO = new POIDTO(6l, serviceOrPOIName, "false", true, serviceDTO, extracts, "desc2");
		when(mockedPoiDAO.getPOIByName(serviceOrPOIName)).thenReturn(poiDTO );
		
		doNothing().when(mockedRestManager).sendObjectToREST(eq("http://localhostt/cache/service"), anyObject());


		// calling the controller
		String reqJson = 
				"{" +
				"	\"service_or_poi\" : \"" + serviceOrPOIName + "\"," +
				"	\"server\" : \"" + serverName + "\"," +
				"	\"cacheManagementView\" : \"POI\"," +
				"	\"itIsAServiceOrPoiOrServer\" : \"POI\"" +
				"}";
		mockMvc.perform(post("/administration/cacheManagement/refresh").content(reqJson).contentType(MediaType.APPLICATION_JSON))
						.andExpect(status().isOk())
						.andExpect(content().string(isEmptyOrNullString()));
		
		verify(mockedServerDAO, times(1)).getServerByName(serverName);
		verify(mockedPoiDAO, times(1)).getPOIByName(serviceOrPOIName);
		verify(mockedRestManager, times(1)).sendObjectToREST(eq("http://localhostt/cache/poi"), anyObject());
	}

	
	@Test
	public void testRefreshCache_successful_BOTH_SERVICE() throws Exception {
		String serverName = "server1";
		String serviceOrPOIName = "serv1";

		// defining the mockito expectations
		when(mockedServerDAO.getServerByName(serverName)).thenReturn(new ServerDTO(3l, serverName, "http://localhostt"));

		Map<String, String> namespaces = new HashMap<String, String>(); namespaces.put("env", "http:valami");
		List<String> endpoints = new ArrayList<String>(); endpoints.add("145"); endpoints.add("146");
		ServiceDTO serviceDTO = new ServiceDTO(4l, serviceOrPOIName, ServiceTypeEnum.WEBSERVICE, "soapAction", "requestTemplateText", namespaces, endpoints, null, null, "desc1");
		when(mockedServiceDAO.getServiceByName(serviceOrPOIName)).thenReturn(serviceDTO);
		
		List<POIDTO> poiDTOs = new ArrayList<POIDTO>();
		poiDTOs.add(new POIDTO(34l, "poi0", null, null, serviceDTO, null, "desc2"));
		poiDTOs.add(new POIDTO(35l, "poi1", null, null, serviceDTO, null, "desc2"));
		poiDTOs.add(new POIDTO(65l, "poi2", null, null, new ServiceDTO(15l, "servcie1", null, null, null, null, null, null, null, null), null, "desc2"));
		when(mockedPoiDAO.getPOIs()).thenReturn(poiDTOs);
		
		doNothing().when(mockedRestManager).sendObjectToREST(eq("http://localhostt/cache/service"), anyObject());
		doNothing().when(mockedRestManager).sendObjectToREST(eq("http://localhostt/cache/poi"), anyObject());


		// calling the controller
		String reqJson = 
				"{" +
				"	\"service_or_poi\" : \"" + serviceOrPOIName + "\"," +
				"	\"server\" : \"" + serverName + "\"," +
				"	\"cacheManagementView\" : \"BOTH\"," +
				"	\"itIsAServiceOrPoiOrServer\" : \"SERVICE\"" +
				"}";
		mockMvc.perform(post("/administration/cacheManagement/refresh").content(reqJson).contentType(MediaType.APPLICATION_JSON))
						.andExpect(status().isOk())
						.andExpect(content().string(isEmptyOrNullString()));
		
		verify(mockedServerDAO, times(1)).getServerByName(serverName);
		verify(mockedServiceDAO, times(1)).getServiceByName(serviceOrPOIName);
		verify(mockedPoiDAO, times(1)).getPOIs();
		verify(mockedRestManager, times(1)).sendObjectToREST(eq("http://localhostt/cache/service"), anyObject());
		verify(mockedRestManager, times(2)).sendObjectToREST(eq("http://localhostt/cache/poi"), anyObject());
	}
	
	
	@Test
	public void testRefreshCache_successful_BOTH_POI() throws Exception {
		String serverName = "server1";
		String serviceOrPOIName = "poi1";

		// defining the mockito expectations
		when(mockedServerDAO.getServerByName(serverName)).thenReturn(new ServerDTO(3l, serverName, "http://localhostt"));

		Map<String, String> namespaces = new HashMap<String, String>(); namespaces.put("env", "http:valami");
		List<String> endpoints = new ArrayList<String>(); endpoints.add("145"); endpoints.add("146");
		ServiceDTO serviceDTO = new ServiceDTO(4l, "serv2", ServiceTypeEnum.WEBSERVICE, "soapAction", "requestTemplateText", namespaces, endpoints, null, null, "desc1");
		List<String> extracts = new ArrayList<String>();
		extracts.add("/a/b"); extracts.add("/ad");
		POIDTO poiDTO = new POIDTO(6l, serviceOrPOIName, "false", true, serviceDTO, extracts, "desc2");
		when(mockedPoiDAO.getPOIByName(serviceOrPOIName)).thenReturn(poiDTO );
		
		doNothing().when(mockedRestManager).sendObjectToREST(eq("http://localhostt/cache/service"), anyObject());


		// calling the controller
		String reqJson = 
				"{" +
				"	\"service_or_poi\" : \"" + serviceOrPOIName + "\"," +
				"	\"server\" : \"" + serverName + "\"," +
				"	\"cacheManagementView\" : \"BOTH\"," +
				"	\"itIsAServiceOrPoiOrServer\" : \"POI\"" +
				"}";
		mockMvc.perform(post("/administration/cacheManagement/refresh").content(reqJson).contentType(MediaType.APPLICATION_JSON))
						.andExpect(status().isOk())
						.andExpect(content().string(isEmptyOrNullString()));
		
		verify(mockedServerDAO, times(1)).getServerByName(serverName);
		verify(mockedPoiDAO, times(1)).getPOIByName(serviceOrPOIName);
		verify(mockedRestManager, times(1)).sendObjectToREST(eq("http://localhostt/cache/poi"), anyObject());
	}

	
	@Test
	public void testRefreshCache_successful_SERVER_SERVICE() throws Exception {
		String serverName = "server1";

		// defining the mockito expectations
		when(mockedServerDAO.getServerByName(serverName)).thenReturn(new ServerDTO(3l, serverName, "http://localhostt"));

		Map<String, String> namespaces = new HashMap<String, String>(); namespaces.put("env", "http:valami");
		List<String> endpoints = new ArrayList<String>(); endpoints.add("145"); endpoints.add("146");
		List<ServiceDTO> serviceDTOs = new ArrayList<ServiceDTO>();
		serviceDTOs.add(new ServiceDTO(12l, "servcie0", null, null, null, namespaces, endpoints, null, null, null));
		serviceDTOs.add(new ServiceDTO(15l, "servcie1", null, null, null, namespaces, endpoints, null, null, null));
		when(mockedServiceDAO.getServices()).thenReturn(serviceDTOs);
		
		doNothing().when(mockedRestManager).sendObjectToREST(eq("http://localhostt/cache/service"), anyObject());

		// calling the controller
		String reqJson = 
				"{" +
				"	\"service_or_poi\" : \"not important\"," +
				"	\"server\" : \"" + serverName + "\"," +
				"	\"cacheManagementView\" : \"SERVICE\"," +
				"	\"itIsAServiceOrPoiOrServer\" : \"SERVER\"" +
				"}";
		mockMvc.perform(post("/administration/cacheManagement/refresh").content(reqJson).contentType(MediaType.APPLICATION_JSON))
						.andExpect(status().isOk())
						.andExpect(content().string(isEmptyOrNullString()));
		
		verify(mockedServerDAO, times(1)).getServerByName(serverName);
		verify(mockedServiceDAO, times(1)).getServices();
		verify(mockedRestManager, times(2)).sendObjectToREST(eq("http://localhostt/cache/service"), anyObject());
	}

	
	@Test
	public void testRefreshCache_successful_SERVER_POI() throws Exception {
		String serverName = "server1";

		// defining the mockito expectations
		when(mockedServerDAO.getServerByName(serverName)).thenReturn(new ServerDTO(3l, serverName, "http://localhostt"));

		Map<String, String> namespaces = new HashMap<String, String>(); namespaces.put("env", "http:valami");
		List<String> endpoints = new ArrayList<String>(); endpoints.add("145"); endpoints.add("146");
		List<POIDTO> poiDTOs = new ArrayList<POIDTO>();
		poiDTOs.add(new POIDTO(34l, "poi0", null, null, new ServiceDTO(12l, "servcie0", null, null, null, null, null, null, null, null), null, null));
		poiDTOs.add(new POIDTO(35l, "poi1", null, null, new ServiceDTO(12l, "servcie0", null, null, null, null, null, null, null, null), null, null));
		poiDTOs.add(new POIDTO(65l, "poi2", null, null, new ServiceDTO(15l, "servcie1", null, null, null, null, null, null, null, null), null, null));
		when(mockedPoiDAO.getPOIs()).thenReturn(poiDTOs);
		
		doNothing().when(mockedRestManager).sendObjectToREST(eq("http://localhostt/cache/poi"), anyObject());

		// calling the controller
		String reqJson = 
				"{" +
				"	\"service_or_poi\" : \"not important\"," +
				"	\"server\" : \"" + serverName + "\"," +
				"	\"cacheManagementView\" : \"POI\"," +
				"	\"itIsAServiceOrPoiOrServer\" : \"SERVER\"" +
				"}";
		mockMvc.perform(post("/administration/cacheManagement/refresh").content(reqJson).contentType(MediaType.APPLICATION_JSON))
						.andExpect(status().isOk())
						.andExpect(content().string(isEmptyOrNullString()));
		
		verify(mockedServerDAO, times(1)).getServerByName(serverName);
		verify(mockedPoiDAO, times(1)).getPOIs();
		verify(mockedRestManager, times(3)).sendObjectToREST(eq("http://localhostt/cache/poi"), anyObject());
	}
	
	
	@Test
	public void testBuildMatrix_successful_SERVICE_match() throws Exception {
		// defining the mockito expectations
		List<ServerDTO> serverDTOs = new ArrayList<ServerDTO>();
		serverDTOs.add(new ServerDTO(1l, "serv0", "http://local"));
		serverDTOs.add(new ServerDTO(2l, "serv1", "http://remote"));
		when(mockedServerDAO.getServers()).thenReturn(serverDTOs);
		
		List<ServiceDTO> serviceDTOs = new ArrayList<ServiceDTO>();
		Map<String, String> namespaces = new HashMap<String, String>(); namespaces.put("env", "http:valami");
		List<String> endpoints = new ArrayList<String>(); endpoints.add("145"); endpoints.add("146");
		serviceDTOs.add(new ServiceDTO(4l, "serv0", ServiceTypeEnum.WEBSERVICE, "soapAction", "requestTemplateText", namespaces, endpoints, null, null, null));
		serviceDTOs.add(new ServiceDTO(5l, "serv1", ServiceTypeEnum.WEBSERVICE, "soapAction", "requestTemplateText", namespaces, endpoints, null, null, null));
		serviceDTOs.add(new ServiceDTO(6l, "serv2", ServiceTypeEnum.WEBSERVICE, "soapAction", "requestTemplateText", namespaces, endpoints, null, null, null));
		when(mockedServiceDAO.getServices()).thenReturn(serviceDTOs);
		
		when(mockedRestManager.getServiceDTOsFromREST(eq("http://local/cache/service/list"))).thenReturn(serviceDTOs);
		when(mockedRestManager.getServiceDTOsFromREST(eq("http://remote/cache/service/list"))).thenReturn(serviceDTOs);
		
		// calling the controller
		mockMvc.perform(get("/administration/cacheManagement//matrix/SERVICE" ))
						.andExpect(status().isOk())
						.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
						.andExpect(jsonPath("$", hasSize(3)))
						.andExpect(jsonPath("$[0][0]", is("UP_TO_DATE")))
						.andExpect(jsonPath("$[0][1]", is("UP_TO_DATE")))
						.andExpect(jsonPath("$[1][0]", is("UP_TO_DATE")))
						.andExpect(jsonPath("$[1][1]", is("UP_TO_DATE")))
						.andExpect(jsonPath("$[2][0]", is("UP_TO_DATE")))
						.andExpect(jsonPath("$[2][1]", is("UP_TO_DATE")));
	}
	
	
	@Test
	public void testBuildMatrix_successful_SERVICE_doesNotMatch() throws Exception {
		// defining the mockito expectations
		List<ServerDTO> serverDTOs = new ArrayList<ServerDTO>();
		serverDTOs.add(new ServerDTO(1l, "serv0", "http://local"));
		serverDTOs.add(new ServerDTO(2l, "serv1", "http://remote"));
		when(mockedServerDAO.getServers()).thenReturn(serverDTOs);
		
		List<ServiceDTO> serviceDTOs = new ArrayList<ServiceDTO>();
		Map<String, String> namespaces = new HashMap<String, String>(); namespaces.put("env", "http:valami");
		List<String> endpoints = new ArrayList<String>(); endpoints.add("145"); endpoints.add("146");
		serviceDTOs.add(new ServiceDTO(4l, "serv0", ServiceTypeEnum.WEBSERVICE, "soapAction", "requestTemplateText", namespaces, endpoints, null, null, null));
		serviceDTOs.add(new ServiceDTO(5l, "serv1", ServiceTypeEnum.WEBSERVICE, "soapAction", "requestTemplateText", namespaces, endpoints, null, null, null));
		serviceDTOs.add(new ServiceDTO(6l, "serv2", ServiceTypeEnum.WEBSERVICE, "soapAction", "requestTemplateText", namespaces, endpoints, null, null, null));
		when(mockedServiceDAO.getServices()).thenReturn(serviceDTOs);
		
		// http:valamii is the difference
		Map<String, String> localNamespaces = new HashMap<String, String>(); namespaces.put("env", "http:valamii");
		// 146 is missing
		List<String> localEndpoints = new ArrayList<String>(); endpoints.add("145");
		// serv_0 is the difference
		List<ServiceDTO> localServiceDTOs = new ArrayList<ServiceDTO>();
		localServiceDTOs.add(new ServiceDTO(4l, "serv_0", ServiceTypeEnum.WEBSERVICE, "soapAction", "requestTemplateText", namespaces, endpoints, null, null, null));
		localServiceDTOs.add(new ServiceDTO(5l, "serv1", ServiceTypeEnum.WEBSERVICE, "soapAction", "requestTemplateText", localNamespaces, endpoints, null, null, null));
		localServiceDTOs.add(new ServiceDTO(6l, "serv2", ServiceTypeEnum.WEBSERVICE, "soapAction", "requestTemplateText", namespaces, localEndpoints, null, null, null));
		when(mockedRestManager.getServiceDTOsFromREST(eq("http://local/cache/service/list"))).thenReturn(localServiceDTOs);
		List<ServiceDTO> remoteServiceDTOs = new ArrayList<ServiceDTO>();
		// contains more namespaces
		Map<String, String> remoteNamespaces = new HashMap<String, String>(); namespaces.put("env", "http:valami"); namespaces.put("env2", "http:valami2");
		// 147 instead of 146
		List<String> remoteEndpoints = new ArrayList<String>(); endpoints.add("145"); endpoints.add("147");
		remoteServiceDTOs.add(new ServiceDTO(4l, "serv0", ServiceTypeEnum.WEBSERVICE, "soapAction", "requestTemplateText", remoteNamespaces, endpoints, null, null, null));
		remoteServiceDTOs.add(new ServiceDTO(5l, "serv1", ServiceTypeEnum.WEBSERVICE, "soapAction", "requestTemplateText", namespaces, remoteEndpoints, null, null, null));
		when(mockedRestManager.getServiceDTOsFromREST(eq("http://remote/cache/service/list"))).thenReturn(remoteServiceDTOs);
		
		// calling the controller
		mockMvc.perform(get("/administration/cacheManagement/matrix/SERVICE" ))
						.andExpect(status().isOk())
						.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
						.andExpect(jsonPath("$", hasSize(3)))
						.andExpect(jsonPath("$[0][0]", is("NOT_FOUND")))
						.andExpect(jsonPath("$[0][1]", is("OLD")))
						.andExpect(jsonPath("$[1][0]", is("OLD")))
						.andExpect(jsonPath("$[1][1]", is("OLD")))
						.andExpect(jsonPath("$[2][0]", is("OLD")))
						.andExpect(jsonPath("$[2][1]", is("NOT_FOUND")));
	}

	
	@Test
	public void testBuildMatrix_successful_POI_doesNotMatch() throws Exception {
		// defining the mockito expectations
		List<ServerDTO> serverDTOs = new ArrayList<ServerDTO>();
		serverDTOs.add(new ServerDTO(1l, "serv0", "http://local"));
		serverDTOs.add(new ServerDTO(2l, "serv1", "http://remote"));
		when(mockedServerDAO.getServers()).thenReturn(serverDTOs);
		
		List<POIDTO> poiDTOs = new ArrayList<POIDTO>();
		List<String> extracts = new ArrayList<String>();
		extracts.add("/a/b"); extracts.add("/ad");		
		poiDTOs.add(new POIDTO(34l, "poi0", "true", true, new ServiceDTO(12l, "servcie0", null, null, null, null, null, null, null, null), extracts, null));
		poiDTOs.add(new POIDTO(35l, "poi1", "false", true, new ServiceDTO(12l, "servcie0", null, null, null, null, null, null, null, null), extracts, null));
		poiDTOs.add(new POIDTO(65l, "poi2", "true", false, new ServiceDTO(13l, "servcie1", null, null, null, null, null, null, null, null), extracts, null));
		when(mockedPoiDAO.getPOIs()).thenReturn(poiDTOs);
		
		List<POIDTO> localServiceDTOs = new ArrayList<POIDTO>();
		List<String> localExtracts = new ArrayList<String>();
		localExtracts.add("/a/b"); extracts.add("/ag");
		// using different name
		localServiceDTOs.add(new POIDTO(34l, "poi_0", "true", true, new ServiceDTO(12l, "servcie0", null, null, null, null, null, null, null, null), extracts, null));
		// using ag instead of ad
		localServiceDTOs.add(new POIDTO(35l, "poi1", "false", true, new ServiceDTO(12l, "servcie0", null, null, null, null, null, null, null, null), localExtracts, null));
		// using different serviceDTO
		localServiceDTOs.add(new POIDTO(65l, "poi2", "true", false, new ServiceDTO(12l, "servcie0", null, null, null, null, null, null, null, null), extracts, null));
		when(mockedRestManager.getPOIDTOsFromREST(eq("http://local/cache/poi/list"))).thenReturn(localServiceDTOs);

		List<POIDTO> remoteServiceDTOs = new ArrayList<POIDTO>();
		// UP_TO_DATE
		remoteServiceDTOs.add(new POIDTO(34l, "poi0", "true", true, new ServiceDTO(12l, "servcie0", null, null, null, null, null, null, null, null), extracts, null));
		// differrent encryptionNeeded
		remoteServiceDTOs.add(new POIDTO(35l, "poi1", "false", false, new ServiceDTO(12l, "servcie0", null, null, null, null, null, null, null, null), extracts, null));
		when(mockedRestManager.getPOIDTOsFromREST(eq("http://remote/cache/poi/list"))).thenReturn(remoteServiceDTOs);
		
		// calling the controller
		mockMvc.perform(get("/administration/cacheManagement/matrix/POI" ))
						.andExpect(status().isOk())
						.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
						.andExpect(jsonPath("$", hasSize(3)))
						.andExpect(jsonPath("$[0][0]", is("NOT_FOUND")))
						.andExpect(jsonPath("$[0][1]", is("UP_TO_DATE")))
						.andExpect(jsonPath("$[1][0]", is("OLD")))
						.andExpect(jsonPath("$[1][1]", is("OLD")))
						.andExpect(jsonPath("$[2][0]", is("OLD")))
						.andExpect(jsonPath("$[2][1]", is("NOT_FOUND")));
	}

	
	@After
	public void cleanUp() {
		reset(mockedServerDAO);
		reset(mockedPoiDAO);
		reset(mockedRestManager);
		reset(mockedServiceDAO);
	}
}