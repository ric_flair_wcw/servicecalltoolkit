Project homepage: https://servicecalltoolkit.bitbucket.io

### What is this repository for? ###

* Quick summary

The system is for making the service calls easy. Well some configuration is needed but the application can provide you with some tool.    

Also it can give some other equipment that might make your (IT) life easy (e.g. monitoring, security, etc.).

The main goal of the system is to get rid of handling different protocols when calling services. It seems a wishful thinking and it's partly true, it cannot be achieved without some configuration. The idea is that on the client side a specific framework has to be used (Client Framework) and all you have to do is to execute 

`
String customerName = Service.createInstance().get("CRM->customerName_byCustomerId", customerId);
`

What it does is to use the POI (see later) that is defined in the server engine and you have the name of customer! The server engine calls an existing service and it extracts the customer name from the response of the existing service by the POI.    
So what is this POI?? It is the acronym of 'piece of information'. Usually on the client side you need only one information (e.g. the customer name) but you'll get much more information when you call a service. Then you have to write a lot of boring code to marshall the response and extract the information. And you have to write the same code in different applications/systems. What you can do is to define the POI in the server engine and just use it (see above).

When you define the POI you have to set :    
- the name of the POI (CRM->customerName_byCustomerId)    
- the extract(s) (like /soapenv:Envelope/soapenv:Body/ns:CRMResponse/ns:persoanalInfo/ns:customerName)    
- some other info but they are not important now    

The extract looks like an XPATH but it is not you cannot use the full XPATH syntax.
I you define more extracts then you'll get back a Map<String,Object>.

At the moment 2 protocols are supported: SOAP and REST. 


The architecture is:

![Architecture.jpg](https://bitbucket.org/repo/9nMLMX/images/4200433285-Architecture.jpg)

I presume some explanation is needed... So there are 3 main parts of the system:    
- Client Framework    
- Server Engine    
- Management Application

I'll explain the parts with an example. Let's assume I need the customer name and I only have the customer id. 
The first station is the Client Framework. I'll execute the 'get' method call (see above) which will send a REST request to one the server engines (defined in a config file on the client).    

The Server Engine receives the request, looks for the POI in its cache and load the metadata needed to call the existing service. There are 2 metadata that we need to call the service:    
- POI    
- metadata about the service itself    
The following info is stored about the service:    
- service type (WS or REST)    
- request text (a full request text is stored with markings where the data (the customer id in this example) coming from client should be put into -> when firing a request the request text is just sent to the service via HTTP)    
The Server Engine creates the request and sends to the existing service. When the response arrives it tries to extract the info defined in the POI from the it and sends back to the client.    
It is important to note that the body of the requests (between the Client Framework and Server Engine) can be encrypted (not SSL). It means that an existing service can be protected without having certificates and making HTTPS!    

The responsibility of the Management Applications is    
- providing GUI to administer the metadata and stores them in DB    
- refreshing the caches of the Server Engines    
- automatic SOAP service discovery    
- monitoring the traffic    