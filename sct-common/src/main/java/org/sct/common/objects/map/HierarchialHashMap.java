package org.sct.common.objects.map;

import java.util.concurrent.ConcurrentHashMap;


public class HierarchialHashMap<K,V> extends ConcurrentHashMap<K, V> {

	
	private static final long serialVersionUID = 6932661986086793017L;
	
	
	private HierarchialHashMap<K, V> parent = null;
	private String currentXpath;


	public HierarchialHashMap(HierarchialHashMap<K,V> parent, String currentXpath) {
		this.parent = parent;
		this.currentXpath = currentXpath;
	}

	
	public HierarchialHashMap<K, V> getParent() {
		return parent;
	}

	public String getCurrentXpath() {
		return currentXpath;
	}

	@Override
	public String toString() {
		return super.toString();
	}
	
}
