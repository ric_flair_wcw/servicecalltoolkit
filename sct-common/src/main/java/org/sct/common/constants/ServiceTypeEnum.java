package org.sct.common.constants;

public enum ServiceTypeEnum {
	
	WEBSERVICE("Webservice"), 
	REST("REST"),
	DATABASE("Database");
	
	private String label = null;

	ServiceTypeEnum(String label) {
		this.label = label;
	}
	
	public String getLabel() {
		return this.label;
	}
	
}
