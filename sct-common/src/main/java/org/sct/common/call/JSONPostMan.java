package org.sct.common.call;

import java.util.HashMap;
import java.util.Map;

import org.sct.common.constants.ServiceTypeEnum;


/**
 * Creating JSON letter
 * 
 * @author Viktor Horvath
 *
 */
public class JSONPostMan {

	
	public Map<String, Object> createLetter(Object bodyObject, String status, boolean isEncrypted, ServiceTypeEnum serviceType) {
		Map<String, Object> letterMap = new HashMap<String, Object>();
		Map<String, String> headerMap = new HashMap<String, String>();
		
		if (status != null) {
			headerMap.put("status", status);
		}
		headerMap.put("encrypted", isEncrypted ? "true" : "false");
		if (serviceType != null) {
			headerMap.put("type", serviceType.equals(ServiceTypeEnum.DATABASE) ? "DATABASE" : "N/A");
		}
		
		letterMap.put("header", headerMap);
		letterMap.put("body", bodyObject);
		
		return letterMap;
	}


	public Map<String, Object> createRequest(Object bodyObject, boolean isEncrypted) {
		return createLetter(bodyObject, null, isEncrypted, null);
	}
}
