package org.sct.common.utils;

import java.util.List;
import java.util.Map;


public class MapUtils {

	
	// removing the starting and trailing '/' character and split by '/'
	public static String[] trimAndSplit(String s) {
		if (s == null) {
			return null;
		}
		if (s.length() == 0) {
			return new String[0];
		}
		return s.replaceAll("([\\/])+$", "").replaceAll("^[\\/]", "").split("/");
	}
	
	
	public static <T> T goToWithoutNamespace(Map<String, Object> xmlMap, String path, Class<T> type) {
		// removing the starting and trailing '/' character
		String[] nodes = MapUtils.trimAndSplit(path);
		
		Map<String, Object> pointerMap = xmlMap;
		Map<String, Object> foundMap = null;
		String lastKey = null;
		boolean found = false;
		for (String nodeName : nodes) {
			found = false;
			foundMap = pointerMap;
			for(String nodeNameInMap : pointerMap.keySet()) {
				// getting the nodename from the path without namespace (/aa:aa/bb:bb -> bb  ;  /aa:aa/bb -> bb)
				String originalKey = nodeNameInMap;
				String key = simplifyKey(originalKey);
				// if the name of the key is equal to nodeName
				if (key.equals(nodeName)) {
					// if the value in the map is a HierarchialHashMap
					if (pointerMap.get(originalKey) != null) {
						if (pointerMap.get(originalKey) instanceof Map) {
							pointerMap = (Map<String, Object>)pointerMap.get(originalKey);
						}
						if (pointerMap.get(originalKey) instanceof List<?>) {
							throw new RuntimeException("Cannot handle Lists at the moment!");
						}
						found = true;
						lastKey = nodeNameInMap;
						break;
					}
				}
			}
			if (!found) {
				break;
			}
		}
		if (!found) {
			return null;
		} else {
			return type.cast(foundMap.get(lastKey));
		}
	}

	
	private static String simplifyKey(final String _key) {
		String key = _key;
		if (key.lastIndexOf("/") != -1) {
			key = key.substring(key.lastIndexOf("/") + 1);
			if (key.lastIndexOf(":") != -1) {
				key = key.substring(key.lastIndexOf(":") + 1);
			}
		} else if (key.lastIndexOf(":") != -1) {
			key = key.substring(key.lastIndexOf(":") + 1);
		}

		return key;
	}
	
	
	public static boolean equals(Map<String, String> map1, Map<String, String> map2) {
		if (map1 == null && map2 == null) {
			return true;
		}
		if ((map1 == null && map2 != null) || (map2 == null && map1 != null)) {
			return false;
		}
		if (map1.keySet().size() != map2.keySet().size()) {
			return false;
		}
		for(String key1 : map1.keySet()) {
			if (!map2.keySet().contains(key1)) {
				return false;
			}
			if (!map1.get(key1).equals(map2.get(key1))) {
				return false;
			}
		}
		return true;
	}
}
