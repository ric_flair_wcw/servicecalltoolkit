package org.sct.common.encryption;

import java.io.IOException;
import java.io.StringWriter;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.util.List;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.bouncycastle.util.encoders.Base64;
import org.sct.common.utils.Utils;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


/**
 * Encrypting/decrypting map
 * 
 * https://github.com/bcgit/bc-java/blob/master/prov/src/test/java/org/bouncycastle/jce/provider/test/AESTest.java
 * 
 * @author Viktor Horvath
 *
 */
public class EnigmaGadget {

	
	private static final String CHECK_TEXT = "CHECKTEXT:RICFLAIRISTHEPRESIDENTOFWCW";
	
	private ObjectMapper objectMapper;

	{
		Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
	}

	
	public Object encrypt(Object objectToBeEncrypyted, boolean isEncryptionNeeded, String encryptionKey) throws JsonGenerationException, JsonMappingException, IOException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException {
		if (!isEncryptionNeeded) {
			return objectToBeEncrypyted;
		} else {
			if (Utils.isEmpty(encryptionKey)) {
				throw new RuntimeException("Encryption is needed but the encryption key is null or empty!!");
			}

			String stringToBeEncrypyted = null;
			
			// if single value
			if (objectToBeEncrypyted instanceof String) {
				stringToBeEncrypyted = (String) objectToBeEncrypyted;
			} 
			// if map
			if (objectToBeEncrypyted instanceof Map) {
				// converting the map to JSON string
				StringWriter stringWriter = new StringWriter();
				objectMapper.writeValue(stringWriter, (Map)objectToBeEncrypyted);
				stringToBeEncrypyted = stringWriter.toString();
			} else {
				throw new RuntimeException("Unexpected type in encryption! type:" + objectToBeEncrypyted.getClass());
			}
			
			// initialising the cipher
			IvParameterSpec iv = new IvParameterSpec((encryptionKey.substring(1)+"F").getBytes("UTF-8"));
			Key key = new SecretKeySpec(encryptionKey.getBytes("UTF-8"), "AES");
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding", "BC");
			cipher.init(Cipher.ENCRYPT_MODE, key, iv);
			
			// encrypting the JSON string
			byte[] encrypted = cipher.doFinal(addCheckText(stringToBeEncrypyted).getBytes());
		    return Base64.toBase64String(encrypted);
		}
	}
	
	
	public <T> T decrypt(Object encryptedObject, boolean isEncryptionNeeded, String encryptionKey, Class<T> type) throws NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, InvalidKeyException, IOException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException {
		if (!isEncryptionNeeded) {
            if (type.equals(Map.class) || type.equals(List.class) || type.equals(String.class)) {
            	return type.cast(encryptedObject);
            } else {
            	throw new RuntimeException("The getStruct method can handle only Map, List and String!");
            }
		} else {
			if (Utils.isEmpty(encryptionKey)) {
				throw new RuntimeException("Encryption is needed but the encryption key is null or empty!");
			}

			byte[] input = encryptedObject.toString().getBytes("UTF-8");
			
			// initialising the cipher
			IvParameterSpec iv = new IvParameterSpec((encryptionKey.substring(1)+"F").getBytes("UTF-8"));
			Key key = new SecretKeySpec(encryptionKey.getBytes("UTF-8"), "AES");
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding", "BC");
			try {
				cipher.init(Cipher.DECRYPT_MODE, key, iv);
			} catch(Exception e) {
				throw new RuntimeException("There is a problem with the encryption key on the server! " + e.getMessage(), e);
			}
			
			// decrypting
			byte[] decodedBytes = Base64.decode(input);
			byte[] original = null;
			try {
				original = cipher.doFinal(decodedBytes);
			} catch(javax.crypto.BadPaddingException e) {
				throw new RuntimeException("Likely different encryption keys are being used on the client and server side! " + e.getMessage(), e);
			}

		    // verifying the checktext
		    String decryptedText = new String(original, "UTF-8");
		    decryptedText = verifyCheckText(decryptedText); 
		    
            // getting the decrypted object
            if (type.equals(Map.class) || type.equals(List.class)) {
            	T decryptedObject = objectMapper.readValue(decryptedText, type);
            	return type.cast(decryptedObject);
            } else if (type.equals(String.class)) {
            	return type.cast(decryptedText);
            } else {
            	throw new RuntimeException("The getStruct method can handle only Map, List and String!");
            }
		}
		
	}

	
	private String verifyCheckText(String decryptedText) {
		if (!decryptedText.startsWith(CHECK_TEXT)) {
			throw new RuntimeException("The CHECKTEXT is missing from the decrypted text, the encryiption wasn't successful on the other side!");
		}
		return decryptedText.substring(CHECK_TEXT.length());
	}


	private String addCheckText(String stringToBeEncrypyted) {
		return CHECK_TEXT + stringToBeEncrypyted;
	}


	public ObjectMapper getObjectMapper() {
		return objectMapper;
	}

	public void setObjectMapper(ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
	}
	
}
