package org.sct.common.encryption;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class EnigmaGadgetTest {

	
	private String encryptionKey = "Bar12345Bar12345";
	private EnigmaGadget instance = null;
	
	
	@Before
	public void setUp() {
		instance = new EnigmaGadget();
		instance.setObjectMapper(new ObjectMapper());
	}
	
	
	@Test
	public void testEncrypt_successful_noEncryptionNeeded() throws JsonGenerationException, JsonMappingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, IOException {
		Object objectToBeEncrypyted = new Integer(1);
		boolean isEncryptionNeeded = false;
		
		Object intege = instance.encrypt(objectToBeEncrypyted, isEncryptionNeeded, encryptionKey);
		
		assertEquals(Integer.class, intege.getClass());
		assertEquals("1", intege.toString());
	}

	
	@Test
	public void testEncrypt_successful_encryptionNeeded() throws JsonGenerationException, JsonMappingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, IOException {
		Map<String,Object> objectToBeEncrypyted = new HashMap<String,Object>();
		objectToBeEncrypyted.put("nr", new Integer(12));
		boolean isEncryptionNeeded = true;
		
		Object encryptedObject = instance.encrypt(objectToBeEncrypyted, isEncryptionNeeded, encryptionKey);
		
		assertEquals(String.class, encryptedObject.getClass());
		assertEquals("7TbnxVELPO3XVkT4UjDlhwSRzHEbG4yiEv0ewbaxMPCPMBjvKp6Zdx//GweZRrEk", encryptedObject.toString());
	}

	
	@Test
	public void testEncrypt_unsuccessful_unexpectedType() throws JsonGenerationException, JsonMappingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, IOException {
		Integer objectToBeEncrypyted = new Integer(12);
		boolean isEncryptionNeeded = true;

		try {
			Object encryptedObject = instance.encrypt(objectToBeEncrypyted, isEncryptionNeeded, encryptionKey);
			fail("RuntimeException should have been thrown!");
		} catch(RuntimeException e) {
			assertEquals("Unexpected type in encryption! type:class java.lang.Integer", e.getMessage());
		}		
	}

	
	@Test
	public void testDecrypt_successful_noEncryptionNeeded() throws InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, IOException {
		String encryptedObject = new String("22");
		boolean isEncryptionNeeded = false;
		
		String intege = instance.decrypt(encryptedObject, isEncryptionNeeded, encryptionKey, String.class);

		assertEquals(String.class, intege.getClass());
		assertEquals("22", intege);

	}
	

	@Test
	public void testDecrypt_successful_deryptionNeeded() throws InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, IOException {
		String encryptedObject = "7TbnxVELPO3XVkT4UjDlhwSRzHEbG4yiEv0ewbaxMPCPMBjvKp6Zdx//GweZRrEk";
		boolean isEncryptionNeeded = true;
		
		Map<String,Object> decryptedObject = instance.decrypt(encryptedObject, isEncryptionNeeded, encryptionKey, Map.class);

		assertEquals(1, decryptedObject.keySet().size());
		assertEquals(Integer.class, decryptedObject.get("nr").getClass());
		assertEquals("12", decryptedObject.get("nr").toString());
	}
}
