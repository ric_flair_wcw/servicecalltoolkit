package org.sct.common.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


//@RunWith(SpringJUnit4ClassRunner.class)
public class MapUtilsTest {

	
	private static final String JSON_SOURCE = 
	"{"+
		  "\"/Earth\": {"+
		    "\"/Earth/name\": \"our Earth\","+
		    "\"/Earth/Hungary\": {"+
		      "\"/Earth/Hungary/name\": \"our country\","+
		      "\"/Earth/Hungary/County GY-M-S\": {"+
		        "\"/Earth/Hungary/County GY-M-S/Ujker\": {"+
		          "\"/Earth/Hungary/County GY-M-S/Ujker/name\": \"my village\","+
		          "\"/Earth/Hungary/County GY-M-S/Ujker/population\": 1001"+
		        "},"+
		        "\"Egyhazasfalu\": {"+
		          "\"name\": \"the village of my mother\","+
		          "\"population\": 1002"+
		        "}"+
		      "},"+
		      "\"County Vas\": {"+
		        "\"Csepreg\": {"+
		          "\"name\": \"where I studied\","+
		          "\"population\": 7001,"+
		          "\"schools\": ["+
		            "{\"name\" : \"Nadasdy\"},"+
		            "{\"name\" : \"egy masik\"}"+
		          "]"+
		        "}"+
              "}"+
		    "}"+
		  "}"+
		"}";


	@Test
	public void testTrimAndSplit_successful1() {
		String s = "/fa/ag/alma/";
		String[] sArray = MapUtils.trimAndSplit(s);
		
		assertEquals(3, sArray.length);
		assertEquals("fa", sArray[0]);
		assertEquals("ag", sArray[1]);
		assertEquals("alma", sArray[2]);
	}

	@Test
	public void testTrimAndSplit_successful2() {
		String s = "fa/ag/alma";
		String[] sArray = MapUtils.trimAndSplit(s);
		
		assertEquals(3, sArray.length);
		assertEquals("fa", sArray[0]);
		assertEquals("ag", sArray[1]);
		assertEquals("alma", sArray[2]);
	}

	@Test
	public void testTrimAndSplit_successful3() {
		String s = "";
		String[] sArray = MapUtils.trimAndSplit(s);
		
		assertEquals(0, sArray.length);
	}

	@Test
	public void testTrimAndSplit_successful4() {
		String s = null;
		String[] sArray = MapUtils.trimAndSplit(s);
		
		assertNull(sArray);
	}
	
	
	@Test
	public void testGoToWithoutNamespace_successful1() throws JsonParseException, JsonMappingException, IOException {
		String path = "/Earth/Hungary/County GY-M-S/Ujker/population";
		Integer population = MapUtils.goToWithoutNamespace(createXmlMap(), path, Integer.class);
		
		assertEquals(1001, population.intValue());
	}

	@Test
	public void testGoToWithoutNamespace_successful2() throws JsonParseException, JsonMappingException, IOException {
		String path = "/Earth/Hungary/County GY-M-S/Ujker/name";
		String name = MapUtils.goToWithoutNamespace(createXmlMap(), path, String.class);
		
		assertEquals("my village", name);
	}

	@Test
	public void testGoToWithoutNamespace_unsuccessful1() throws JsonParseException, JsonMappingException, IOException {
		String path = "/Earth/Hungary/County Vas/Csepreg/schools/name";
		try {
			List<String> name = MapUtils.goToWithoutNamespace(createXmlMap(), path, List.class);
			fail("RuntimeException should have been thrown!");
		} catch(RuntimeException re) {
			assertEquals("Cannot handle Lists at the moment!", re.getMessage());
		}
	}

	@Test
	public void testGoToWithoutNamespace_unsuccessful2() throws JsonParseException, JsonMappingException, IOException {
		String path = "/Earth/Hungary/County GY-M-S/PUjker/name";
		String name = MapUtils.goToWithoutNamespace(createXmlMap(), path, String.class);
		
		assertNull(name);
	}

	
	@Test
	public void testEquals_equal1() {
		Map<String, String> map1 = null;
		Map<String, String> map2 = null;
		boolean eq = MapUtils.equals(map1, map2);
		
		assertTrue(eq);
	}
	
	@Test
	public void testEquals_equal2() {
		Map<String, String> map1 = new HashMap<String, String>();
		Map<String, String> map2 = new HashMap<String, String>();
		map1.put("a", "1"); map1.put("b", "2"); map1.put("c", "3");
		map2.put("c", "3"); map2.put("b", "2"); map2.put("a", "1");
		boolean eq = MapUtils.equals(map1, map2);
		
		assertTrue(eq);
	}

	@Test
	public void testEquals_notequal1() {
		Map<String, String> map1 = new HashMap<String, String>();
		Map<String, String> map2 = new HashMap<String, String>();
		map1.put("a", "1"); map1.put("d", "2"); map1.put("c", "3");
		map2.put("c", "3"); map2.put("b", "2"); map2.put("a", "1");
		boolean eq = MapUtils.equals(map1, map2);
		
		assertFalse(eq);
	}

	@Test
	public void testEquals_notequal2() {
		Map<String, String> map1 = null;
		Map<String, String> map2 = new HashMap<String, String>();
		map2.put("c", "3"); map2.put("b", "2"); map2.put("a", "1");
		boolean eq = MapUtils.equals(map1, map2);
		
		assertFalse(eq);
	}

	@Test
	public void testEquals_notequal3() {
		Map<String, String> map1 = new HashMap<String, String>();
		Map<String, String> map2 = new HashMap<String, String>();
		map1.put("a", "1"); map1.put("b", "2"); map1.put("c", "3");
		map2.put("c", "3"); map2.put("b", "2"); map2.put("a", "1"); map2.put("d", "4");
		boolean eq = MapUtils.equals(map1, map2);
		
		assertFalse(eq);
	}

	
	private Map<String, Object> createXmlMap() throws JsonParseException, JsonMappingException, IOException {
		Map<String,Object> result = new ObjectMapper().readValue(JSON_SOURCE, HashMap.class);
		return result;
	}
}
