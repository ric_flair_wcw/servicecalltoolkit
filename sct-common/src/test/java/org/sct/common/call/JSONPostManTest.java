package org.sct.common.call;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.sct.common.call.JSONPostMan;
import org.sct.common.constants.ServiceTypeEnum;

public class JSONPostManTest {

	
	private JSONPostMan instance = null;
	
	
	@Before
	public void setUp() {
		instance = new JSONPostMan();
	}
	
	
	@Test
	public void testCreateLetter() {
		Map<String,Object> bodyObject = new HashMap<String, Object>();
		bodyObject.put("nr", new Integer(1));
		String status = "ok";
		boolean isEncrypted = true;
		
		Map<String, Object> letter = instance.createLetter(bodyObject, status, isEncrypted, ServiceTypeEnum.REST);
		
		assertEquals(2, letter.keySet().size());
		assertEquals(3, ((Map<String, Object>)letter.get("header")).keySet().size());
		assertEquals("true", ((Map<String, Object>)letter.get("header")).get("encrypted"));
		assertEquals("ok", ((Map<String, Object>)letter.get("header")).get("status"));
		assertEquals("N/A", ((Map<String, Object>)letter.get("header")).get("type"));

		assertEquals(Integer.class, ((Map<String, Object>)letter.get("body")).get("nr").getClass());
		assertEquals("1", ((Map<String, Object>)letter.get("body")).get("nr").toString());
	}

	@Test
	public void testCreateRequest() {
		Map<String,Object> bodyObject = new HashMap<String, Object>();
		bodyObject.put("nr", new Integer(1));
		boolean isEncrypted = true;
		
		Map<String, Object> letter = instance.createRequest(bodyObject, isEncrypted );
		
		assertEquals(2, letter.keySet().size());
		assertEquals(1, ((Map<String, Object>)letter.get("header")).keySet().size());
		assertEquals("true", ((Map<String, Object>)letter.get("header")).get("encrypted"));
		assertEquals(Integer.class, ((Map<String, Object>)letter.get("body")).get("nr").getClass());
		assertEquals("1", ((Map<String, Object>)letter.get("body")).get("nr").toString());
	}
}
