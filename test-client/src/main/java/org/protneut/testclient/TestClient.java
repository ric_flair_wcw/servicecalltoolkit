package org.protneut.testclient;

import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.sct.clientframework.Service;
import org.sct.clientframework.exception.ClientFrameworkException;
import org.sct.clientframework.exception.ServerException;
import org.sct.clientframework.utils.ParameterBuilder;
import org.sct.clientframework.utils.ResponseMiner;
import org.springframework.web.client.RestClientException;

public class TestClient {

	
	private DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");
	
	
	public void get_return_String__String() throws  RestClientException, ClientFrameworkException, ServerException{
		String customerId = "ksdf87432hjdsf879";
		
		String buildingName = Service.createInstance("service_consumer", "service_consumer").get("postcode->buildingName", customerId);
		
		System.out.println("buildingName: "+buildingName);
	}
	
	
	public void get_return_Map__String() throws ClientFrameworkException, RestClientException, ServerException {
		String customerId = "2";
		
		Map<String, ?> customerAddress = Service.createInstance("service_consumer", "service_consumer").getStruct("postcode->fullAddress", customerId, Map.class);
		
		//String contact_postcode = (String) ((Map<String, ?>)customerAddress.get("contactAddress")).get("postcode");
		Object identifierType = ResponseMiner.get("/soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeResponse/ns:postalAddress/ns:addressIdentification/ns:identifierType", customerAddress);
		System.out.println(identifierType);
	}

	
	public void get_REST(int i) throws ClientFrameworkException, RestClientException, ServerException {
		System.out.println(" +++++++++++++++++++++++++++++++++++ get_REST START +++++++++++++++++++++++++++++++++++ "+i+ " ++++++++ "+dateFormat.format(new Date()));

		Map<String, ?> customerAddress = Service.createInstance("service_consumer", "service_consumer").getStruct("REST->displayAddress", Map.class);
		
		Object identifierType = ResponseMiner.get("/GetAddressForPostCodeResponse/postalAddress/displayAddress", customerAddress);
		System.out.println(identifierType);
		System.out.println(" +++++++++++++++++++++++++++++++++++ get_REST END +++++++++++++++++++++++++++++++++++ "+i+ " ++++++++ "+dateFormat.format(new Date()));
	}

	
	public void get_REST_only_list() throws ClientFrameworkException, RestClientException, ServerException {
		List<?> response = Service.createInstance("service_consumer", "service_consumer").getStruct("REST->list/value", List.class);
		
		Object values = ResponseMiner.get("/value", response);
		System.out.println(values);
	}
	

	public void get_return_String() throws ClientFrameworkException, RestClientException, ServerException {
		ParameterBuilder builder = new ParameterBuilder();
		builder.add("postcode", "BH1 3EH");	// MANDATORY
		builder.add("country", "HU");  // optional
		builder.add("addressIdentification/identifierType", "nemtom");  // optional
		builder.add("addressIdentification/addressIdentifier", "eztse");  // optional
		
		String id = Service.createInstance("service_consumer", "service_consumer").get("responseHeader-id", builder.getParams());
		
		System.out.println(id);
	}
	
	
	public void get_return_401() throws ClientFrameworkException, RestClientException, ServerException {
		ParameterBuilder builder = new ParameterBuilder();
		builder.add("postcode", "BH1 3EH");	// MANDATORY
		builder.add("country", "HU");  // optional
		
		String id = Service.createInstance("service_consumer", "service_consumer").get("REST->displayAddress_401", builder.getParams());
		
		System.out.println(id);
	}

	
	public void get_return_REST_simpleString() throws ClientFrameworkException, RestClientException, ServerException {
		ParameterBuilder builder = new ParameterBuilder();
		builder.add("postcode", "BH1 3EH");	// MANDATORY
		builder.add("country", "HU");  // optional
		
		String sg = Service.createInstance("service_consumer", "service_consumer").get("REST->simpleString", builder.getParams());
		
		System.out.println(sg);
	} 
	
	
	public void get_return_REST_urlVariables(int i) throws ClientFrameworkException, RestClientException, ServerException {
		System.out.println(" +++++++++++++++++++++++++++++++++++ get_return_REST_urlVariables START +++++++++++++++++++++++++++++++++++ "+i+ " ++++++++ "+dateFormat.format(new Date()));
		ParameterBuilder builder = new ParameterBuilder();
		builder.add("county", "Dorset");
		builder.add("town", "Bournemouth");
		
		Map<String,Object> identifierType = Service.createInstance("service_consumer", "service_consumer").getStruct("REST->url_variables->identifierType", builder.getParams(), Map.class);
		System.out.println(identifierType);
		System.out.println(" +++++++++++++++++++++++++++++++++++ get_return_REST_urlVariables END +++++++++++++++++++++++++++++++++++ "+i+ " ++++++++ "+dateFormat.format(new Date()));
	} 
	
	
	public void get_return_ListOfMap__Map(int i) throws ClientFrameworkException, ServerException {
		System.out.println(" +++++++++++++++++++++++++++++++++++ get_return_ListOfMap__Map START +++++++++++++++++++++++++++++++++++ "+i+ " ++++++++ "+dateFormat.format(new Date()));
		ParameterBuilder builder = new ParameterBuilder();
		builder.add("postcode", "BH1 3EH");	// MANDATORY
		builder.add("country", "HU");  // optional
		builder.add("addressIdentification/identifierType", "nemtom");  // optional
		builder.add("addressIdentification/addressIdentifier", "eztse");  // optional
		
		// the customers who became clients between two dates and they are from Bournemouth
		//		address : the theoretical data name
		//		getAddressForPostCode : the service name
		//		postalAddress_short : the name of the data group in address->getAddressForPostCode
		Service service = Service.createInstance("service_consumer", "service_consumer");
		service.setEncryptionKey("Bar12345Bar12345");
		service.setEncryptionNeeded(true);
		
//		System.out.println("    service engine Start - " + dateFormat.format(new Date()));
		Map<String, ?> adresses = service.getStruct("postcode->some_strange_value", builder.getParams(),Map.class);
//		System.out.println("    service engine End - " + dateFormat.format(new Date()));
		Object addressIdentification = ResponseMiner.get("/soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeResponse/ns:postalAddress/ns:addressIdentification", adresses);
		System.out.println(addressIdentification);
		Object identifierType = ResponseMiner.get("/soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeResponse/ns:postalAddress/ns:addressIdentification/ns:identifierType", adresses);
		System.out.println(identifierType);
		System.out.println(" +++++++++++++++++++++++++++++++++++ get_return_ListOfMap__Map END +++++++++++++++++++++++++++++++++++ "+i+ " ++++++++ "+dateFormat.format(new Date()));
//		Object id = ResponseMiner.get("/soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeResponse/ns:responseHeader/ns1:id", adresses);
//		System.out.println(id);
	}

	
	public void get_from_DB_by_serviceTye(int i) throws ClientFrameworkException, RestClientException, ServerException {
		System.out.println(" +++++++++++++++++++++++++++++++++++ get_from_DB_by_serviceTye START +++++++++++++++++++++++++++++++++++ "+i+ " ++++++++ "+dateFormat.format(new Date()));
		ParameterBuilder builder = new ParameterBuilder();
		builder.add("successful", "1");
		
		Map<String,List<String>> services = Service.createInstance("service_consumer", "service_consumer").getStruct("DB-protneut->WS-service-data", builder.getParams(), Map.class);
		
		System.out.println(services.get("CNT").get(2));
		System.out.println(" +++++++++++++++++++++++++++++++++++ get_from_DB_by_serviceTye END +++++++++++++++++++++++++++++++++++ "+i+ " ++++++++ "+dateFormat.format(new Date()));
//		System.out.println(services.get("POI_NAME"));
//		System.out.println(services.get("POI_NAME").get(1));
	}
	
	
//	public void get_return_String__Long() throws ClientFrameworkException, RestClientException, ServerException {
//		Long customerId = 9786239873259807l;
//		
//		String customerName = Service.createInstance().get("customer->name", customerId.toString());
//	}
//	
//	
//	public void get_return_Integer__String() throws ClientFrameworkException, RestClientException, ServerException {
//		String customerId = "ksdf87432hjdsf879";
//		
//		//Integer periodOfmembership = service.get("periodOfmembership", customerId);
//		Integer periodOfmembership = Service.createInstance().get("customer->membership->period", customerId, Integer.class);
//	}
	
	
//	public void get_return_ListOfString__Map() {
//		ParameterBuilder builder = new ParameterBuilder();
//		builder.add("/customer/location", "Bournemouth");
//		builder.add("/customer/startDate", Calendar.getInstance());
//		builder.add("/customer/endDate", Calendar.getInstance());
//		
//		// the customers who became clients between two dates and they are from Bournemouth
//		List<String> customerNames = Service.createInstance().getList("customer->names@membershipBetween", builder.getParams());
//	}
	
	
//	public void get_return_Map__Map() throws RestClientException, ClientFrameworkException, ServerException {
//		ParameterBuilder builder = new ParameterBuilder();
//		builder.add("postcode", "BH1 3EH");	// MANDATORY
//		builder.add("country", "HU");  // optional
//		builder.add("addressIdentification/identifierType", "nemtom");  // optional
//		builder.add("addressIdentification/addressIdentifier", "eztse");  // optional
//		
//		Map<String, ?> customerAddress = Service.createInstance().getStruct("address->getAddressForPostCode->postalAddress_short", builder.getParams());
//		
//		List<String> identifierTypes = ResponseMiner.getSimpleList("/soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeResponse/ns:postalAddress/ns:addressIdentification/ns:identifierType", customerAddress, String.class);
//		System.out.println(identifierTypes);
//	}

	
	public static void main(String[] args) throws ClientFrameworkException, RestClientException, ServerException, InterruptedException, NoSuchAlgorithmException {
		final TestClient testClient = new TestClient();
		
		System.out.println("Start - " + testClient.dateFormat.format(new Date()));
		
//		testClient.get_from_DB_by_serviceTye(0);
		
//		KeyGenerator keyGen = KeyGenerator.getInstance("AES");
//		keyGen.init(128);
//		SecretKey secretKey = keyGen.generateKey();
//		byte[] encoded = secretKey.getEncoded(); 
//		    System.out.println("key: "+encoded);
		
		// !!! Important: the first call is always slow as the local JVM has to load the Spring bean definitions !!!
		
		try {
			testClient.get_return_String__String();
		} catch(Throwable t) {System.out.println(t); }
//		try {
//			testClient.get_return_ListOfMap__Map(0);
//		} catch(Throwable t) {System.out.println(t); }
//		try {
//			testClient.get_REST(0);
//		} catch(Throwable t) {System.out.println(t); }
//		try {
//			testClient.get_return_String();
//		} catch(Throwable t) {System.out.println(t); }
//		try {
//			testClient.get_return_401();
//		} catch(Throwable t) {System.out.println(t); }
//		try {
//			testClient.get_return_REST_simpleString();
//		} catch(Throwable t) {System.out.println(t); }
//		try {
//			testClient.get_REST_only_list();
//		} catch(Throwable t) {System.out.println(t); }
//		try {
//			testClient.get_from_DB_by_serviceTye(0);
//		} catch(Throwable t) {System.out.println(t); }
//		try {
//			testClient.get_return_REST_urlVariables(0);
//		} catch(Throwable t) {System.out.println(t); }
		
//		for(int k = 0; k < 10000; k++) {
//			for(int j=0; j<60; j++) {
//				final int i = j;
//				new Thread(new Runnable() {
//					public void run() {
//						try {
//							testClient.get_return_ListOfMap__Map(i);
//						} catch (Throwable t) { throw new RuntimeException(t); }
//					}
//				}).start();
//				new Thread(new Runnable() {
//					public void run() {
//						try {
//							testClient.get_return_REST_urlVariables(i);
//						} catch (Throwable t) { throw new RuntimeException(t); }
//					}
//				}).start();
//				new Thread(new Runnable() {
//					public void run() {
//						try {
//							testClient.get_from_DB_by_serviceTye(i);
//						} catch (Throwable t) { throw new RuntimeException(t); }
//					}
//				}).start();
//			}
//			System.out.println("------------------------------- Waiting for 10sec -------------------------------");
//			Thread.sleep(1100);
//			for(int j=0; j<10; j++) {
//				final int i = j;
//				testClient.get_return_ListOfMap__Map(i);
//				testClient.get_return_REST_urlVariables(i);
//				testClient.get_from_DB_by_serviceTye(i);
//			}
//		}
		
//		for(int i=0; i<4; i++) {
//			new Thread(new Runnable() {
//				public void run() {
//					for(int j=0; j<2; j++) {
//						try {
//							new TestClient().get_return_String__String();
//							new TestClient().get_return_Map__String();
//							new TestClient().get_return_ListOfMap__Map();
//						} catch(Exception e) {
//							System.out.println(e);
//						}
//					}
//				}
//			}).start();
//		}

//		for(int i=0; i<10; i++) {
//			new TestClient().get_return_String__String();
//			new TestClient().get_return_Map__String();
//			new TestClient().get_return_ListOfMap__Map();
//		}
		
		System.out.println("End - " + testClient.dateFormat.format(new Date()));
	}
	
}
