package org.sct.server.common.beans;


public class ServerDTO {

	
	private Long id;
	private String name;
	//private CacheTypeEnum cacheType;
	private String url;
//	private String port;

	
	public ServerDTO() { }
	
	public ServerDTO(Long id, String name, /*CacheTypeEnum cacheType, */String url/*, String port*/) {
		super();
		this.id = id;
		this.name = name;
//		this.cacheType = cacheType;
		this.url = url;
//		this.port = port;
	}

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

//	public CacheTypeEnum getCacheType() {
//		return cacheType;
//	}
//
//	public void setCacheType(CacheTypeEnum cacheType) {
//		this.cacheType = cacheType;
//	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

//	public String getPort() {
//		return port;
//	}
//
//	public void setPort(String port) {
//		this.port = port;
//	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.getClass().getName()).append("[");
		sb.append("id=").append(id);
		sb.append(", name=").append(name);
//		sb.append(", cacheType=").append(cacheType.toString());
		sb.append(", url=").append(url);
//		sb.append(", port=").append(port);
		sb.append("]");
		return sb.toString();
	}

	
	
}
