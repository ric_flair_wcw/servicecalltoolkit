package org.sct.server.common.persistence.repository;

import java.util.Calendar;
import java.util.List;

import org.sct.server.common.persistence.model.POIEntity;
import org.sct.server.common.persistence.model.ServiceEntity;
import org.sct.server.common.persistence.model.StatServicePoiDateCntEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface StatServicePoiDateCntEntityRepository extends JpaRepository<StatServicePoiDateCntEntity, Long> {

	
	//@Lock(LockModeType.PESSIMISTIC_READ)
	@Query(value = "SELECT * " +
           "FROM STAT_SERVICE_POI_DATE_CNT s " +
           "WHERE s.PERIOD_START = :periodStart AND s.POI_NAME = :poiName " +
           "FOR UPDATE",
           nativeQuery = true)
	List<StatServicePoiDateCntEntity> findByPoiNameAndPeriodStart(@Param("poiName") String poiName, 
                                                                  @Param("periodStart") Calendar periodStart);

	
	List<StatServicePoiDateCntEntity> findByPoiEntity(POIEntity poiEntity);
	
	
	List<StatServicePoiDateCntEntity> findByServiceEntity(ServiceEntity serviceEntity);
	

	/**
     * Getting the counts of a service between two dates and grouped by periodStart and successful 
     * 
     * @param serviceName the name of the service
     * @param startDate the start date for filtering 
     * @param endDate the end date for filtering
     * @return 
     */
    @Query("SELECT s.periodStart, s.successful, SUM(s.cnt) " +
           "FROM StatServicePoiDateCntEntity s " +
           "WHERE s.serviceName = :serviceName AND s.periodStart BETWEEN :startDate AND :endDate " +
           "GROUP BY s.periodStart, s.successful " +
           "ORDER BY s.periodStart, s.successful")
    public List<Object[]> getCntGroupedByStartDateAndSuccessfulForService(@Param("serviceName") String serviceName, 
    											                          @Param("startDate") Calendar startDate,
    											                          @Param("endDate") Calendar endDate);

	/**
     * Getting the counts of a POI between two dates and grouped by periodStart and successful 
     * 
     * @param poiName the name of the POI
     * @param startDate the start date for filtering 
     * @param endDate the end date for filtering
     * @return 
     */
    @Query("SELECT s.periodStart, s.successful, SUM(s.cnt) " +
           "FROM StatServicePoiDateCntEntity s " +
           "WHERE s.poiName  = :poiName AND s.periodStart BETWEEN :startDate AND :endDate " +
           "GROUP BY s.periodStart, s.successful " +
           "ORDER BY s.periodStart, s.successful")
    public List<Object[]> getCntGroupedByStartDateAndSuccessfulForPOI(@Param("poiName") String poiName, 
    											                      @Param("startDate") Calendar startDate,
    											                      @Param("endDate") Calendar endDate);


	/**
     * Getting the counts of services between two dates and grouped by service name and periodStart 
     * 
     * @param serviceNames the list of the names of the services
     * @param startDate the start date for filtering 
     * @param endDate the end date for filtering
     * @return 
     */
    @Query("SELECT s.serviceName, s.periodStart, SUM(s.cnt) " +
           "FROM StatServicePoiDateCntEntity s " +
           "WHERE s.serviceName IN (:serviceNames) AND s.periodStart BETWEEN :startDate AND :endDate " +
           "GROUP BY s.serviceName, s.periodStart " +
           "ORDER BY s.serviceName, s.periodStart")
    public List<Object[]> getCntGroupedByServiceAndStartDateForService(@Param("serviceNames") List<String> serviceNames, 
                                                                       @Param("startDate") Calendar startDate,
                                                                       @Param("endDate") Calendar endDate);

	/**
     * Getting the counts of POIs between two dates and grouped by service name and periodStart 
     * 
     * @param poiNames the list of the names of the POIs
     * @param startDate the start date for filtering 
     * @param endDate the end date for filtering
     * @return 
     */
    @Query("SELECT s.poiName, s.periodStart, SUM(s.cnt) " +
           "FROM StatServicePoiDateCntEntity s " +
           "WHERE s.poiName IN (:poiNames) AND s.periodStart BETWEEN :startDate AND :endDate " +
           "GROUP BY s.poiName, s.periodStart " +
           "ORDER BY s.poiName, s.periodStart")
    public List<Object[]> getCntGroupedByServiceAndStartDateForPOI(@Param("poiNames") List<String> poiNames, 
                                                                   @Param("startDate") Calendar startDate,
                                                                   @Param("endDate") Calendar endDate);
    
}
