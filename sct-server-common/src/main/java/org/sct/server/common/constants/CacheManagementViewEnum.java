package org.sct.server.common.constants;

public enum CacheManagementViewEnum {

	SERVICE("Service"),
	POI("POI"),
	BOTH("Both");
	
	private String label;
	
	private CacheManagementViewEnum(String label) {
		this.label = label;
	}
	
	public String getLabel() {
		return this.label;
	}

}
