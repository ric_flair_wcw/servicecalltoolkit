package org.sct.server.common.persistence.service;

import java.util.Calendar;
import java.util.List;

public interface IStatServicePoiDateCntEntityService {

	void changeStatistics(String poiName, Boolean successful, Calendar reqArrivalTime);
	
	List<Object[]> getStatistics(String type, String serviceName, Calendar startDate, Calendar endDate);

	List<Object[]> getStatisticsForComparison(String type, List<String> stringArray, Calendar startDateCalendar, Calendar endDateCalendar);
	
}
