package org.sct.server.common.persistence.model;


import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.sct.common.constants.ServiceTypeEnum;



@Entity
@Table(name = "STAT_SERVICE_POI_DATE_CNT")
public class StatServicePoiDateCntEntity {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "SERVICE_ID")
	private ServiceEntity serviceEntity;
	
	@Column(name="SERVICE_NAME")
	private String serviceName;
	
	@Column(name="SERVICE_TYPE")
	@Enumerated(EnumType.STRING)
	private ServiceTypeEnum serviceType;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "POI_ID")
	private POIEntity poiEntity;
	
	@Column(name="POI_NAME")
	private String poiName;

	@Column(name="PERIOD_START")
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar periodStart;
	
	@Column(name="SUCCESSFUL")
	private Boolean successful;	
	
	@Column(name="CNT")
	private Integer cnt;
	
	
	public StatServicePoiDateCntEntity() { }
	
	public StatServicePoiDateCntEntity(ServiceEntity serviceEntity, String serviceName, ServiceTypeEnum serviceType, POIEntity poiEntity, String poiName, Calendar periodStart, Boolean successful) {
		this.serviceEntity = serviceEntity;
		this.serviceName = serviceName;
		this.serviceType = serviceType;
		this.poiEntity = poiEntity;
		this.poiName = poiName;
		this.periodStart = periodStart;
		this.successful = successful;
		this.cnt = 1;
	}
	
	
	public void increaseCnt() {
		cnt++;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public ServiceEntity getServiceEntity() {
		return serviceEntity;
	}

	public void setServiceEntity(ServiceEntity serviceEntity) {
		this.serviceEntity = serviceEntity;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public ServiceTypeEnum getServiceType() {
		return serviceType;
	}

	public void setServiceType(ServiceTypeEnum serviceType) {
		this.serviceType = serviceType;
	}

	public POIEntity getPoiEntity() {
		return poiEntity;
	}

	public void setPoiEntity(POIEntity poiEntity) {
		this.poiEntity = poiEntity;
	}

	public String getPoiName() {
		return poiName;
	}

	public void setPoiName(String poiName) {
		this.poiName = poiName;
	}

	public Calendar getPeriodStart() {
		return periodStart;
	}

	public void setPeriodStart(Calendar periodStart) {
		this.periodStart = periodStart;
	}

	public Boolean getSuccessful() {
		return successful;
	}

	public void setSuccessful(Boolean successful) {
		this.successful = successful;
	}

	public Integer getCnt() {
		return cnt;
	}

	public void setCnt(Integer cnt) {
		this.cnt = cnt;
	}
}
