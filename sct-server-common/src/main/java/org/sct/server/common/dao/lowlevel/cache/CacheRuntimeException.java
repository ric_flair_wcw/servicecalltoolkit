package org.sct.server.common.dao.lowlevel.cache;

public class CacheRuntimeException extends RuntimeException {


	private static final long serialVersionUID = -5690284251601050474L;

	
	public CacheRuntimeException(String message, Throwable t) {
		super(message, t);
	}

	public CacheRuntimeException(String message) {
		super(message);
	}

}
