package org.sct.server.common.persistence.repository;

import org.sct.server.common.persistence.model.ExtractEntity;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ExtractEntityRepository extends JpaRepository<ExtractEntity, Long> {

}
