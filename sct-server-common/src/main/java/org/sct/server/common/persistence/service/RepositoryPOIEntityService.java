package org.sct.server.common.persistence.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.sct.server.common.beans.POIDTO;
import org.sct.server.common.persistence.model.ExtractEntity;
import org.sct.server.common.persistence.model.POIEntity;
import org.sct.server.common.persistence.model.ServiceEntity;
import org.sct.server.common.persistence.model.StatServicePoiDateCntEntity;
import org.sct.server.common.persistence.repository.ExtractEntityRepository;
import org.sct.server.common.persistence.repository.POIEntityRepository;
import org.sct.server.common.persistence.repository.ServiceEntityRepository;
import org.sct.server.common.persistence.repository.StatServicePoiDateCntEntityRepository;
import org.sct.server.common.persistence.utils.Converter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;


@Service(value="poiEntityService")
public class RepositoryPOIEntityService implements IPOIEntityService {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(RepositoryPOIEntityService.class);
	
	@Resource
	private POIEntityRepository poiEntityRepository;
	@Resource
	private ExtractEntityRepository extractEntityRepository;
	@Resource
	private ServiceEntityRepository serviceEntityRepository;
	@Resource
	private StatServicePoiDateCntEntityRepository statRepository;
	@Autowired
	private Converter converter;
	
	
	@Transactional
	public List<POIDTO> getPOIs() throws JsonParseException, JsonMappingException, IOException {
		LOGGER.debug("Getting the POI list from DB");
		List<POIDTO> poiDTOs = new ArrayList<POIDTO>();
		try {
			// getting the poi entities
			List<POIEntity> poiEntities = poiEntityRepository.findAll(new Sort(Sort.Direction.ASC, "name"));
			// converting them to ServiceDTOs
			for(POIEntity poiEntity : poiEntities) {
				poiDTOs.add(converter.convertToPOIDTO(poiEntity));
			}
			return poiDTOs;
		} finally {
			LOGGER.debug("Retrieved {} POI(s) from DB", poiDTOs.size());
		}
	}

	
	@Transactional
	public POIDTO getPOI(Long id) throws JsonParseException, JsonMappingException, IOException {
		LOGGER.debug("Getting the POI (id={}) from DB", id);
		// getting the POI entity
		POIEntity poiEntity = poiEntityRepository.findOne(id);
		
		if (poiEntity == null) {
			return null;
		} else {
			// converting it to POIDTO
			return converter.convertToPOIDTO(poiEntity);
		}
	}
	
	
	@Transactional
	public void deletePOI(Long id) {
		LOGGER.debug("Deleting a POI (id={}), its statistics and its extracts from DB", id);
		POIEntity poiEntity = poiEntityRepository.findOne(id);
		// delete the extracts
		extractEntityRepository.deleteInBatch(poiEntity.getExtracts());
		// delete the statistics
		List<StatServicePoiDateCntEntity> statServicePoiDateCntEntities = statRepository.findByPoiEntity(poiEntity);
		statRepository.deleteInBatch(statServicePoiDateCntEntities);
		// delete the poi
		poiEntityRepository.delete(id);
	}

	
	@Transactional
	public void savePOI(POIDTO poiDTO) throws JsonGenerationException, JsonMappingException, IOException {
		LOGGER.debug("Creating/updating a POI in DB. {}", poiDTO);
		if (poiDTO != null) {
			ServiceEntity serviceEntity = serviceEntityRepository.getOne(poiDTO.getServiceDTO().getId());
			// insert
			if (poiDTO.getId() == null) {
				LOGGER.debug("Inserting a new POI.");
				// saving the POI entity
				POIEntity poiEntity = poiEntityRepository.save(converter.convertToPOIEntity(poiDTO, new POIEntity(), serviceEntity));
				LOGGER.debug("The id of the new POI is {}.", poiEntity.getId());
				// saving the extracts
				saveExtracts(poiDTO.getExtracts(), poiEntity);
			}
			// update
			else {
				LOGGER.debug("Updating an existing POI.");
				// getting the entity from DB
				POIEntity poiEntity = poiEntityRepository.findOne(poiDTO.getId());
				// updating the values of the entity
				converter.convertToPOIEntity(poiDTO, poiEntity, serviceEntity);
				// removing the existing extracts
				extractEntityRepository.deleteInBatch(poiEntity.getExtracts());
				// saving the extracts
				saveExtracts(poiDTO.getExtracts(), poiEntity);
			}
		}
	}

	
	@Transactional
	public POIDTO getPOIByName(String poiName) {
		LOGGER.debug("Getting the POI by name (name={}) from DB", poiName);
		// getting the service entity
		POIEntity poiEntity = poiEntityRepository.findByName(poiName);

		if (poiEntity == null) {
			return null;
		} else {
			// converting it to ServiceDTO
			return converter.convertToPOIDTO(poiEntity);
		}
	}

	
	private void saveExtracts(List<String> extracts, POIEntity poiEntity) {
		List<ExtractEntity> extractEntities = new ArrayList<ExtractEntity>();
		for(String extract : extracts) {
			extractEntities.add(new ExtractEntity( extract, 
					                               poiEntity));
		}
		extractEntityRepository.save(extractEntities);
	}


}
