package org.sct.server.common.dao.highlevel.cache;

import java.util.List;

import org.sct.server.common.beans.POIDTO;
import org.sct.server.common.beans.ServiceDTO;


public interface ICacheBeanDAO {
	
	public POIDTO getPOICacheBean(String poiName);
	
	public void put(String poiName, POIDTO cacheBean);
	
	public void deletePOICacheBean(String poiName);
	
	public ServiceDTO getServiceCacheBean(String serviceName);

	public void put(String serviceName, ServiceDTO cacheBean);
	
	public void deleteServiceCacheBean(String serviceName);
	
	public List<String> getCacheKeys();
}
