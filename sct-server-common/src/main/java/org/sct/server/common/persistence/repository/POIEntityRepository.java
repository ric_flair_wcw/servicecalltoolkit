package org.sct.server.common.persistence.repository;


import org.sct.server.common.persistence.model.POIEntity;
import org.springframework.data.jpa.repository.JpaRepository;


public interface POIEntityRepository extends JpaRepository<POIEntity, Long> {

	/**
     * Finds the POIEntity by using its last name as a search criteria.
     * 
     * @param name the name of the POI
     * @return  the POIEntity
     */
	public POIEntity findByName(String poiName);

	
}
