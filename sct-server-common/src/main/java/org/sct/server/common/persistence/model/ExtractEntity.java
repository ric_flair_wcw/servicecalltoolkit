package org.sct.server.common.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "EXTRACT")
public class ExtractEntity {


	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
		
	@Column(name = "PATH")
	private String path;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "POI_ID")
	private POIEntity poiEntity;


	public ExtractEntity() { }
	
	public ExtractEntity(String path, POIEntity poiEntity) {
		this.path = path;
		this.poiEntity = poiEntity;
	}

	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public POIEntity getPoiEntity() {
		return poiEntity;
	}

	public void setPoiEntity(POIEntity poiEntity) {
		this.poiEntity = poiEntity;
	}
	
	
}
