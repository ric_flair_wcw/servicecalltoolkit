package org.sct.server.common.utils;

import java.util.Map;

import org.sct.common.objects.map.HierarchialHashMap;

import com.ximpleware.EOFException;
import com.ximpleware.EncodingException;
import com.ximpleware.EntityException;
import com.ximpleware.NavException;
import com.ximpleware.ParseException;
import com.ximpleware.XPathEvalException;
import com.ximpleware.XPathParseException;

public interface IXmlToMapConverter {

	public void start(String xml) throws NavException, EncodingException, EOFException, EntityException, ParseException, XPathParseException, XPathEvalException;
	
	public void start(String xml, Map<String, String> namespacesXPath) throws NavException, EncodingException, EOFException, EntityException, ParseException, XPathParseException, XPathEvalException;

	public HierarchialHashMap<String, Object> getMap();
	
	public Map<String, String> getNamespaces();

}