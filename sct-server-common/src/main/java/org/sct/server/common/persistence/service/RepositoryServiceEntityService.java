package org.sct.server.common.persistence.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.sct.server.common.beans.ServiceDTO;
import org.sct.server.common.persistence.model.EndpointEntity;
import org.sct.server.common.persistence.model.ServiceEntity;
import org.sct.server.common.persistence.model.StatServicePoiDateCntEntity;
import org.sct.server.common.persistence.repository.EndpointEntityRepository;
import org.sct.server.common.persistence.repository.ServiceEntityRepository;
import org.sct.server.common.persistence.repository.StatServicePoiDateCntEntityRepository;
import org.sct.server.common.persistence.utils.Converter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

@Service(value="serviceEntityService")
public class RepositoryServiceEntityService implements IServiceEntityService {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(RepositoryServiceEntityService.class);
	
	@Resource
	private ServiceEntityRepository serviceEntityRepository;
	@Resource
	private EndpointEntityRepository endpointEntityRepository;
	@Resource
	private StatServicePoiDateCntEntityRepository statRepository;	
	@Autowired
	private Converter converter;
	
	
	@Transactional
	public List<ServiceDTO> getServices() throws JsonParseException, JsonMappingException, IOException {
		LOGGER.debug("Getting the service list from DB");
		List<ServiceDTO> serviceDTOs = new ArrayList<ServiceDTO>();
		try {
			// getting the service entities
			List<ServiceEntity> serviceEntities = serviceEntityRepository.findAll(new Sort(Sort.Direction.ASC, "name"));
			// converting them to ServiceDTOs
			for(ServiceEntity serviceEntity : serviceEntities) {
				serviceEntity.getEndpoints().size();
				serviceDTOs.add(converter.convertToServiceDTO(serviceEntity));
			}
			return serviceDTOs;
		} finally {
			LOGGER.debug("Retrieved {} service(s) from DB", serviceDTOs.size());
		}
	}


	@Transactional
	public void deleteService(Long id) {
		LOGGER.debug("Deleting a service (id={}), its statistics and its endpoints from DB", id);
		ServiceEntity serviceEntity = serviceEntityRepository.findOne(id);
		// delete the endpoints
		endpointEntityRepository.deleteInBatch(serviceEntity.getEndpoints());
		// delete the statistics
		List<StatServicePoiDateCntEntity> statServicePoiDateCntEntities = statRepository.findByServiceEntity(serviceEntity);
		statRepository.deleteInBatch(statServicePoiDateCntEntities);
		// delete the service
		serviceEntityRepository.delete(id);
	}


	@Transactional
	public Long saveService(ServiceDTO serviceDTO) throws JsonGenerationException, JsonMappingException, IOException {
		LOGGER.debug("Creating/updating a service in DB. {}", serviceDTO);
		ServiceEntity serviceEntity = null;
		if (serviceDTO != null) {
			// insert
			if (serviceDTO.getId() == null) {
				LOGGER.debug("Inserting a new service.");
				// saving the service entity
				serviceEntity = serviceEntityRepository.save(converter.convertToServiceEntity(serviceDTO, new ServiceEntity()));
				LOGGER.debug("the id of the new service is {}.", serviceEntity.getId());
				// saving the endpoints
				saveEndpoints(serviceDTO.getEndpoints(), serviceEntity);
			}
			// update
			else {
				LOGGER.debug("Updating an existing service.");
				// getting the entity from DB
				serviceEntity = serviceEntityRepository.findOne(serviceDTO.getId());
				// updating the values of the entity
				converter.convertToServiceEntity(serviceDTO, serviceEntity);
				// removing the existing endpoints
				endpointEntityRepository.deleteInBatch(serviceEntity.getEndpoints());
				// saving the endpoints
				saveEndpoints(serviceDTO.getEndpoints(), serviceEntity);
			}
		}
		return serviceEntity.getId();
	}


	private void saveEndpoints(List<String> endpointUrls, ServiceEntity serviceEntity) {
		List<EndpointEntity> endpointEntities = new ArrayList<EndpointEntity>();
		for(String endpointUrl : endpointUrls) {
			endpointEntities.add(new EndpointEntity(endpointUrl, serviceEntity));
		}
		endpointEntityRepository.save(endpointEntities);
	}


	@Transactional
	public ServiceDTO getService(Long id) throws JsonParseException, JsonMappingException, IOException {
		LOGGER.debug("Getting the service (id={}) from DB", id);
		// getting the service entity
		ServiceEntity serviceEntity = serviceEntityRepository.findOne(id);
		
		if (serviceEntity == null) {
			return null;
		} else {
			// converting it to ServiceDTO
			return converter.convertToServiceDTO(serviceEntity);
		}
	}
	
	
	@Transactional
	public ServiceDTO getServiceByName(String serviceName) throws JsonParseException, JsonMappingException, IOException {
		LOGGER.debug("Getting the service by name (name={}) from DB", serviceName);
		// getting the service entity
		ServiceEntity serviceEntity = serviceEntityRepository.findByName(serviceName);
		
		if (serviceEntity == null) {
			return null;
		} else {
			// converting it to ServiceDTO
			return converter.convertToServiceDTO(serviceEntity);
		}
	}

}
