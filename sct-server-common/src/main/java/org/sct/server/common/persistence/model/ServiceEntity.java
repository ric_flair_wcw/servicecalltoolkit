package org.sct.server.common.persistence.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.sct.common.constants.ServiceTypeEnum;
import org.sct.server.common.constants.HttpMethodEnum;



@Entity
@Table(name = "SERVICE")
public class ServiceEntity {


	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	@Column(name="NAME", unique = true)
	private String name;
	
	@Column(name="SERVICE_TYPE")
	@Enumerated(EnumType.STRING)
	private ServiceTypeEnum serviceType;
	
	@Column(name="SOAP_ACTION")
	private String soapAction;

	@Column(name="HTTP_METHOD")
	@Enumerated(EnumType.STRING)
	private HttpMethodEnum httpMethod;

	@Column(name="REQUEST_TEMPLATE_TEXT")
	private String requestTemplateText;
	
	// converting the namespace map to JSON
	@Column(name="NAMESPACE_JSON")
	private String namespaces;
	
	@Column(name="DATABASE_QUERY")
	private String databaseQuery;

	@Column(name="DESCRIPTION")
	private String description;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = EndpointEntity.class, mappedBy = "serviceEntity")
	private List<EndpointEntity> endpoints;
	
	@OneToMany(fetch = FetchType.LAZY, targetEntity = POIEntity.class, mappedBy = "serviceEntity")
	private List<POIEntity> pois;
	
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ServiceTypeEnum getServiceType() {
		return serviceType;
	}

	public void setServiceType(ServiceTypeEnum serviceType) {
		this.serviceType = serviceType;
	}

	public String getSoapAction() {
		return soapAction;
	}

	public void setSoapAction(String soapAction) {
		this.soapAction = soapAction;
	}

	public String getRequestTemplateText() {
		return requestTemplateText;
	}

	public void setRequestTemplateText(String requestTemplateText) {
		this.requestTemplateText = requestTemplateText;
	}

	public String getNamespaces() {
		return namespaces;
	}

	public void setNamespaces(String namespaces) {
		this.namespaces = namespaces;
	}

	public List<EndpointEntity> getEndpoints() {
		return endpoints;
	}

	public void setEndpoints(List<EndpointEntity> endpoints) {
		this.endpoints = endpoints;
	}

	public List<POIEntity> getPois() {
		return pois;
	}

	public void setPois(List<POIEntity> pois) {
		this.pois = pois;
	}

	public HttpMethodEnum getHttpMethod() {
		return httpMethod;
	}

	public void setHttpMethod(HttpMethodEnum httpMethod) {
		this.httpMethod = httpMethod;
	}

	public String getDatabaseQuery() {
		return databaseQuery;
	}

	public void setDatabaseQuery(String databaseQuery) {
		this.databaseQuery = databaseQuery;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
