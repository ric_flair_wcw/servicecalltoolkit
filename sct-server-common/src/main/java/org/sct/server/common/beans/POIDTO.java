package org.sct.server.common.beans;

import java.util.List;


public class POIDTO {

	
	private Long id;
	private String name;
	private String singleValue;
	private Boolean encryptionNeeded;
	private ServiceDTO serviceDTO; 
	private List<String> extracts;
	private String description;

	
	public POIDTO() { }
	
	public POIDTO(Long id, String name, String singleValue, Boolean encryptionNeeded, ServiceDTO serviceDTO, List<String> extracts, String description) {
		super();
		this.id = id;
		this.name = name;
		this.singleValue = singleValue;
		this.encryptionNeeded = encryptionNeeded;
		this.serviceDTO = serviceDTO;
		this.extracts = extracts;
		this.description = description;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getExtracts() {
		return extracts;
	}

	public void setExtracts(List<String> extracts) {
		this.extracts = extracts;
	}

	public ServiceDTO getServiceDTO() {
		return serviceDTO;
	}

	public void setServiceDTO(ServiceDTO serviceDTO) {
		this.serviceDTO = serviceDTO;
	}

	public String getSingleValue() {
		return singleValue;
	}

	public void setSingleValue(String singleValue) {
		this.singleValue = singleValue;
	}

	public Boolean getEncryptionNeeded() {
		return encryptionNeeded;
	}

	public void setEncryptionNeeded(Boolean encryptionNeeded) {
		this.encryptionNeeded = encryptionNeeded;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.getClass().getName()).append("[");
		sb.append("id=").append(id);
		sb.append(", name=").append(name);
		sb.append(", singleValue=").append(singleValue);
		sb.append(", encryptionNeeded=").append(encryptionNeeded);
		sb.append(", serviceDTO=").append(serviceDTO);
		sb.append(", extracts=").append(extracts);
		sb.append(", description=").append(description);
		sb.append("]");
		return sb.toString();
	}

	
}
