package org.sct.server.common.persistence.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "POI")
public class POIEntity {

	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	@Column(name="NAME", unique = true)
	private String name;
	
	@Column(name="SINGLE_VALUE")
	private String singeValue;

	@Column(name="ENCRYPTION_NEEDED")
	private Boolean encryptionNeeded;

	@Column(name="DESCRIPTION")
	private String description;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "SERVICE_ID")
	private ServiceEntity serviceEntity;
	
	@OneToMany(fetch = FetchType.LAZY, targetEntity = ExtractEntity.class, mappedBy = "poiEntity")
	private List<ExtractEntity> extracts;

	
	public ServiceEntity getServiceEntity() {
		return serviceEntity;
	}

	public void setServiceEntity(ServiceEntity serviceEntity) {
		this.serviceEntity = serviceEntity;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<ExtractEntity> getExtracts() {
		return extracts;
	}

	public void setExtracts(List<ExtractEntity> extracts) {
		this.extracts = extracts;
	}

	public String getSingeValue() {
		return singeValue;
	}

	public void setSingeValue(String singeValue) {
		this.singeValue = singeValue;
	}

	public Boolean getEncryptionNeeded() {
		return encryptionNeeded;
	}

	public void setEncryptionNeeded(Boolean encryptionNeeded) {
		this.encryptionNeeded = encryptionNeeded;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
}
