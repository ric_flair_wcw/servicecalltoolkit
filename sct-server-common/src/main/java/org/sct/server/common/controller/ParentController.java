package org.sct.server.common.controller;

import javax.servlet.http.HttpServletResponse;

import org.sct.server.common.exceptions.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

public class ParentController {

	protected static final Logger LOGGER = LoggerFactory.getLogger(ParentController.class);
	
	@Autowired
	private ExceptionUtils exceptionUtils;

	
	@ExceptionHandler(Throwable.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	public Object handleThrowable(Throwable t, HttpServletResponse response) {
		LOGGER.error("Exception occurred!", t);
		return exceptionUtils.createMapFromThrowable(t);
	}

}
