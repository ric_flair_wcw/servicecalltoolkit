package org.sct.server.common.persistence.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.sct.server.common.persistence.model.POIEntity;
import org.sct.server.common.persistence.model.StatServicePoiDateCntEntity;
import org.sct.server.common.persistence.repository.POIEntityRepository;
import org.sct.server.common.persistence.repository.StatServicePoiDateCntEntityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;


@Service(value="statServicePoiDateCntEntityService")
public class RepositoryStatServicePoiDateCntEntityService implements IStatServicePoiDateCntEntityService {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(RepositoryStatServicePoiDateCntEntityService.class);
	
	@Resource
	private StatServicePoiDateCntEntityRepository repository;
	@Resource
	private POIEntityRepository poiEntityRepository;

	
	@Transactional
	public void changeStatistics(String poiName, Boolean successful, Calendar reqArrivalTime) {
		// the calendar type has to be converted to date string -> 20150323 17:25
		Calendar periodStart = getPeriodStart(reqArrivalTime);
		LOGGER.debug("Increasing the count of the POI '{}' and start period '{}'.", poiName, periodStart);
		// getting the entities with the specific POI and start period; the entities will be READ locked in DB!
		List<StatServicePoiDateCntEntity> entities = null;
		entities = repository.findByPoiNameAndPeriodStart(poiName, periodStart);
		// if the list is empty then a new record has to be inserted
		// NOTE: if two threads call the changeStatistics method and no record is inserted yet then both threads will insert => there will be 2 records with cnt 1 instead of
		//       having one record with cnt 2 -> however it is not a problem as I will use a SELECT ..., SUM(cnt) FROM ... when populating the diagram
		//       as a side effect in the else branch (see below) only one of the records' cnt has to be incremented
		if (entities.size() == 0) {
			LOGGER.debug("Inserting a new row.");
			// getting the POI entity from DB
			POIEntity poiEntity = poiEntityRepository.findByName(poiName);
			// creating a new StatServicePoiDateCntEntity
			StatServicePoiDateCntEntity entity = new StatServicePoiDateCntEntity(poiEntity.getServiceEntity(), 
					                                                             poiEntity.getServiceEntity().getName(), 
					                                                             poiEntity.getServiceEntity().getServiceType(), 
					                                                             poiEntity, 
					                                                             poiName, 
					                                                             periodStart,
					                                                             successful);
			// saving it
			repository.saveAndFlush(entity);
		}
		// otherwise the cnt of one of the records has to be incremented
		else {
			LOGGER.debug("Updating an existing row.");
			entities.get(0).increaseCnt();
			repository.saveAndFlush(entities.get(0));
		}
	}


	@Transactional
	public List<Object[]> getStatistics(String type, String name, Calendar startDate, Calendar endDate) {
		// getting the statistics from DB
		List<Object[]> stat = null;
		if (type.equals("service")) {
			stat = repository.getCntGroupedByStartDateAndSuccessfulForService(name, startDate, endDate);
		} else {
			stat = repository.getCntGroupedByStartDateAndSuccessfulForPOI(name, startDate, endDate);
		}
		
		// there might be holes in the statistics -> fill them with zero
		Calendar mainCalendar = Calendar.getInstance();
		mainCalendar.setTimeInMillis(startDate.getTimeInMillis());
		List<Object[]> result = new ArrayList<Object[]>();
		
		for(int i = 0; i < ((int)((endDate.getTime().getTime() / 60000 / 5) - (startDate.getTime().getTime() / 60000 / 5))) + 1; i++) {
			result.add(getRecord(stat, mainCalendar, true));
			result.add(getRecord(stat, mainCalendar, false));
			
			mainCalendar.add(Calendar.MINUTE, 5);
		}
		
		return result;
	}


	@Transactional
	public List<Object[]> getStatisticsForComparison(String type, List<String> names, Calendar startDateCalendar, Calendar endDateCalendar) {
		// getting the statistics from DB
		List<Object[]> stat = null;
		if (type.equals("service")) {
			stat = repository.getCntGroupedByServiceAndStartDateForService(names, startDateCalendar, endDateCalendar);
		} else {
			stat = repository.getCntGroupedByServiceAndStartDateForPOI(names, startDateCalendar, endDateCalendar);
		}
	
		// there might be holes in the statistics -> fill them with zero
		Calendar mainCalendar = Calendar.getInstance();
		List<Object[]> result = new ArrayList<Object[]>();
		
		for(int j = 0; j < names.size(); j++) {
			mainCalendar.setTimeInMillis(startDateCalendar.getTimeInMillis());
			int fj = ((int)((endDateCalendar.getTime().getTime() / 60000 / 5) - (startDateCalendar.getTime().getTime() / 60000 / 5))) + 1;
			for(int i = 0; i < ((int)((endDateCalendar.getTime().getTime() / 60000 / 5) - (startDateCalendar.getTime().getTime() / 60000 / 5))) + 1; i++) {
				result.add(getRecordForComparison(stat, names.get(j), mainCalendar));
				
				mainCalendar.add(Calendar.MINUTE, 5);
			}
		}
		
		return result;
		
	}

	
	private Object[] getRecordForComparison(List<Object[]> stat, String name, Calendar calendar) {
		Object[] rec = searchRecByDateInStatForComparison(name, calendar, stat);
		if (rec == null) {
			Calendar c = Calendar.getInstance();
			c.setTimeInMillis(calendar.getTimeInMillis());
			rec = new Object[] {name, c, 0l};
		}
		return rec;
	}


	private Object[] getRecord(List<Object[]> stat, Calendar calendar, Boolean successful) {
		Object[] rec = searchRecByDateInStat(calendar, stat, successful);
		if (rec == null) {
			Calendar c = Calendar.getInstance();
			c.setTimeInMillis(calendar.getTimeInMillis());
			rec = new Object[] {c, successful, 0l};
		}
		return rec;
	}
	
	
	private Calendar getPeriodStart(Calendar reqArrivalTime) {
		reqArrivalTime.set(Calendar.MILLISECOND, 0);
		reqArrivalTime.set(Calendar.SECOND, 0);
		reqArrivalTime.set(Calendar.MINUTE, reqArrivalTime.get(Calendar.MINUTE) / 5 * 5);
		
		return reqArrivalTime;
	}


	private Object[] searchRecByDateInStatForComparison(String name, Calendar calendar, List<Object[]> stat) {
		for(Object[] rec : stat) {
			if (    ((Calendar)rec[1]).getTime().getTime() == calendar.getTime().getTime() && 
					rec[0].toString().equals(name)
				) {
				return rec;
			}
		}
		return null;
	}

	
	private Object[] searchRecByDateInStat(Calendar calendar, List<Object[]> stat, Boolean successful) {
		for(Object[] rec : stat) {
			if (    ((Calendar)rec[0]).getTime().getTime() == calendar.getTime().getTime() && 
					!(((Boolean)rec[1]).booleanValue() ^ successful.booleanValue())
				) {
				return rec;
			}
		}
		return null;
	}


}