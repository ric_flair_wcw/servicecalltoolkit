package org.sct.server.common.beans;

import java.util.List;
import java.util.Map;

import org.sct.common.constants.ServiceTypeEnum;
import org.sct.server.common.constants.HttpMethodEnum;


public class ServiceDTO {

	
	private Long id;
	private String name;
	private ServiceTypeEnum serviceType;
	private String soapAction;
	private String requestTemplateText;
	private Map<String, String> namespaces;
	private List<String> endpoints;
	private HttpMethodEnum httpMethod;
	private String databaseQuery;
	private String description;

	
	public ServiceDTO() { }
	
	public ServiceDTO(Long id, String name, ServiceTypeEnum serviceType, String soapAction, String requestTemplateText, Map<String, String> namespaces, List<String> endpoints, 
			HttpMethodEnum httpMethod, String databaseQuery, String description) {
		super();
		this.id = id;
		this.name = name;
		this.serviceType = serviceType;
		this.soapAction = soapAction;
		this.requestTemplateText = requestTemplateText;
		this.namespaces = namespaces;
		this.endpoints = endpoints;
		this.httpMethod = httpMethod;
		this.databaseQuery = databaseQuery;
		this.description = description;
	}

	
	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public ServiceTypeEnum getServiceType() {
		return serviceType;
	}

	public String getSoapAction() {
		return soapAction;
	}

	public String getRequestTemplateText() {
		return requestTemplateText;
	}

	public Map<String, String> getNamespaces() {
		return namespaces;
	}

	public List<String> getEndpoints() {
		return endpoints;
	}

	public HttpMethodEnum getHttpMethod() {
		return httpMethod;
	}

	public void setHttpMethod(HttpMethodEnum httpMethod) {
		this.httpMethod = httpMethod;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setServiceType(ServiceTypeEnum serviceType) {
		this.serviceType = serviceType;
	}

	public void setSoapAction(String soapAction) {
		this.soapAction = soapAction;
	}

	public void setRequestTemplateText(String requestTemplateText) {
		this.requestTemplateText = requestTemplateText;
	}

	public void setNamespaces(Map<String, String> namespaces) {
		this.namespaces = namespaces;
	}

	public void setEndpoints(List<String> endpoints) {
		this.endpoints = endpoints;
	}

	public String getDatabaseQuery() {
		return databaseQuery;
	}

	public void setDatabaseQuery(String databaseQuery) {
		this.databaseQuery = databaseQuery;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.getClass().getName()).append("[");
		sb.append("id=").append(id);
		sb.append(", name=").append(name);
		sb.append(", serviceType=").append(serviceType);
		sb.append(", soapAction=").append(soapAction);
		sb.append(", requestTemplateText=").append(requestTemplateText);
		sb.append(", namespaces=").append(namespaces);
		sb.append(", endpoints=").append(endpoints);
		sb.append(", httpMethod=").append(httpMethod);
		sb.append(", databaseQuery=").append(databaseQuery);
		sb.append(", description=").append(description);
		sb.append("]");
		return sb.toString();
	}

	
}
