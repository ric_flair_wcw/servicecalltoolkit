package org.sct.server.common.controller.auth;

import org.sct.server.common.controller.ParentController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
@RequestMapping("/")
public class WelcomePageController extends ParentController {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(WelcomePageController.class);
	
	
	@RequestMapping(method = RequestMethod.GET)
	public String welcome() {
		LOGGER.debug("Received GET request to show the welcome page.");
		
		return "/welcome.tiles";
	}
	
}
