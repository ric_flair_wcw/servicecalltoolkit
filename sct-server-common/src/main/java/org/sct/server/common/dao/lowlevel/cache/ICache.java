package org.sct.server.common.dao.lowlevel.cache;

import java.util.List;


/**
 * Cache interface for the basic operations
 * 
 * @author Viktor Horvath
 */
public interface ICache {

	void lock(String key);
	
	void unlock(String key);
	
	<T> T get(String key, Class<T> type);
	
	void put(String key, Object value);
	
	List<String> getKeys();
	
	void remove(String key);
	
}
