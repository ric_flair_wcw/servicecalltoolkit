package org.sct.server.common.persistence.repository;

import org.sct.server.common.persistence.model.EndpointEntity;
import org.springframework.data.jpa.repository.JpaRepository;


public interface EndpointEntityRepository extends JpaRepository<EndpointEntity, Long> {

}
