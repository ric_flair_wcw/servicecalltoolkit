package org.sct.server.common.exceptions;

public class IllegalConfigurationException extends RuntimeException {

	private static final long serialVersionUID = -2691154818512020130L;

	public IllegalConfigurationException(String message) {
		super(message);
	}
	
}
