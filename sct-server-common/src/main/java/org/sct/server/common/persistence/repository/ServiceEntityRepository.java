package org.sct.server.common.persistence.repository;

import org.sct.server.common.persistence.model.ServiceEntity;
import org.springframework.data.jpa.repository.JpaRepository;


/* for accessing the following CRUD operations:
	(http://docs.spring.io/spring-data/data-commons/docs/1.1.0.RELEASE/api/org/springframework/data/repository/CrudRepository.html)
		- long	count():				Returns the number of entities available.
		- void	delete(ID id):			Deletes the entity with the given id.
		- void	delete(Iterable<? extends T> entities):	Deletes the given entities.
		- void	delete(T entity):		Deletes a given entity.
		- void	deleteAll():			Deletes all entities managed by the repository.
		- boolean	exists(ID id):		Returns whether an entity with the given id exists.
		- Iterable<T>	findAll():		Returns all instances of the type.
		- T	findOne(ID id):				Retrives an entity by its primary key.
		- Iterable<T>	save(Iterable<? extends T> entities):	Saves all given entities.
		- T	save(T entity):				Saves a given entity.	
*/
public interface ServiceEntityRepository extends JpaRepository<ServiceEntity, Long> {


	/**
     * Finds the ServiceEntity by using its last name as a search criteria.
     * @param name the name of the service
     * @return  the ServiceEntity
     */
	public ServiceEntity findByName(String serviceName);
	
}
