package org.sct.server.common.persistence.service;

import java.io.IOException;
import java.util.List;

import org.sct.server.common.beans.ServiceDTO;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

public interface IServiceEntityService {

	
	List<ServiceDTO> getServices() throws JsonParseException, JsonMappingException, IOException;
	
	void deleteService(Long id);
	
	Long saveService(ServiceDTO serviceBean) throws JsonGenerationException, JsonMappingException, IOException;

	ServiceDTO getService(Long id) throws JsonParseException, JsonMappingException, IOException;
	
	ServiceDTO getServiceByName(String serviceName) throws JsonParseException, JsonMappingException, IOException;
	
}
