package org.sct.server.common.utils;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import org.sct.common.objects.map.HierarchialHashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ximpleware.AutoPilot;
import com.ximpleware.EOFException;
import com.ximpleware.EncodingException;
import com.ximpleware.EntityException;
import com.ximpleware.NavException;
import com.ximpleware.ParseException;
import com.ximpleware.VTDGen;
import com.ximpleware.VTDNav;
import com.ximpleware.XPathEvalException;
import com.ximpleware.XPathParseException;


public class XmlToMapConverter implements IXmlToMapConverter {

	private static final Logger LOGGER = LoggerFactory.getLogger(XmlToMapConverter.class);
	
	private HierarchialHashMap<String, Object> mapAll = null;
	private HierarchialHashMap<String, Object> pointer = null;
	private Map<String, String> namespaces = null;
	private Map<String, String> namespacesXPath = null;
	private VTDGen vtdGen = null;
	
	
	public void start(final String xml) throws NavException, XPathEvalException, XPathParseException, EncodingException, EOFException, EntityException, ParseException {
		start(xml, getNamespaces(initVtd(xml)));
	}
	
	public void start(final String xml, final Map<String, String> namespacesXPath) throws NavException, EncodingException, EOFException, EntityException, ParseException, XPathParseException, XPathEvalException {
		initializing();

		this.namespacesXPath = namespacesXPath;

		// initializing the VTD Gen parser
		VTDNav vtdNav = initVtd(xml);
		
		// getting the namespaces from the response XML
		namespaces.putAll(getNamespaces(vtdNav));
		
		// starting the digging (recursively)
		AutoPilot autoPilot = new AutoPilot(vtdNav);
		autoPilot.selectElement("*");
		if (autoPilot.iterate()) {
			 String rootNodeName = getNodeName(vtdNav);
			 pointer = mapAll;
			 createMap("/"+rootNodeName, autoPilot, vtdNav, true);
		}
	}
	
	
	private VTDNav initVtd(String xml) {
		if (vtdGen == null) {
			// initializing the VTD Gen parser
			vtdGen = new VTDGen();
		}
		vtdGen.setDoc(xml.getBytes());
		try {
			vtdGen.parse(true);
		} catch (ParseException e) {
			LOGGER.error("Invalid XML text: {}", xml);
			throw new RuntimeException("The XML text is invalid!", e);
		}

		return vtdGen.getNav();
	}
	
	
	private String getNodeName(VTDNav vtdNav) throws NavException {
		String nodeName = vtdNav.toString(vtdNav.getCurrentIndex());
		return refreshNamespacePrefix(nodeName);
	}


	private void createMap(String xpath, AutoPilot autoPilot, VTDNav vtdNav, boolean needIterate) throws NavException {
		if (needIterate) {
			HierarchialHashMap<String, Object> newMap = new HierarchialHashMap<String, Object>(pointer, xpath);
			putTheValueToMap(xpath, newMap);
			pointer = newMap;
		}
		
		Integer currentDepth = null;
		while (!needIterate || autoPilot.iterate()) {
			needIterate = true;
			String nodeName = getNodeName(vtdNav);

			if (currentDepth == null) {
				currentDepth = vtdNav.getCurrentDepth();
			}
			// if the iteration goes upward (e.g. from /a/b/c to /a/b)
			if (currentDepth > vtdNav.getCurrentDepth()) {
				for(int i=0; i < currentDepth-vtdNav.getCurrentDepth(); i++) {
					pointer = pointer.getParent();
				}
				createMap(pointer.getCurrentXpath(), autoPilot, vtdNav, false);
			} else {
				int t = vtdNav.getText();
				if (t != -1) {
					String value = vtdNav.toNormalizedString(t);
					putTheValueToMap(xpath+"/"+nodeName, value);
				} else {
					if (vtdNav.getContentFragment() != -1) {
						createMap(xpath+"/"+nodeName, autoPilot, vtdNav, true);
					} else {
						putTheValueToMap(xpath+"/"+nodeName, new HashMap<String, String>());
					}
				}
			}
		}
	}
	
	
	private void putTheValueToMap(String key, Object value) {
		if (pointer.containsKey(key)) {
			List list = null;
			if (pointer.get(key) instanceof List<?> ) {
				list = (List<?>)pointer.get(key);
			} else {
//				list = Collections.synchronizedList(new ArrayList<String>());
				list = new CopyOnWriteArrayList<String>();
				list.add(pointer.get(key));
				pointer.remove(key);
			}
			list.add(value);
			pointer.put(key, list);
		} else {
			pointer.put(key, value);
		}
	}
	

	private String refreshNamespacePrefix(String nodeName) {
		if (nodeName.indexOf(":") != -1) {
			String namespacePrefix = nodeName.substring(0, nodeName.indexOf(":"));
			String theRestOfTheNodeName = nodeName.substring(nodeName.indexOf(":") + 1);
			String namespaceXml = namespaces.get(namespacePrefix);
			String namespaceXPathPrefix = getNamespaceXPathPrefix(namespaceXml);
			return namespaceXPathPrefix + ":" + theRestOfTheNodeName;
		} else {
			return nodeName;
		}
		
	}


	private String getNamespaceXPathPrefix(String namespaceXml) {
//System.out.println("_____________________" + namespaceXml + "  " + namespacesXPath);
		if (namespacesXPath != null) {
			for(String namespaceXPathPrefix : namespacesXPath.keySet()) {
				if (namespacesXPath.get(namespaceXPathPrefix).equals(namespaceXml)) {
					return namespaceXPathPrefix;
				}
			}
		}
		throw new RuntimeException("It is pretty unexpected... The '"+namespaceXml+"' namespace is not in the list of namespaces coming from extraction.");
	}


	private Map<String, String> getNamespaces(final VTDNav vtdNav) throws NavException, XPathEvalException, XPathParseException {
		Map<String, String> ns = new HashMap<String, String>();
		AutoPilot ap = new AutoPilot(vtdNav);
		ap.selectXPath("/*/namespace::*");
		int i = -1;
		while((i = ap.evalXPath()) != -1){
			// i will be attr name, i+1 will be attribute value
			// to get rid if the 'xmlns:' text in the namespace prefix
			String key = vtdNav.toNormalizedString(i);
			if (key.indexOf(":") != -1) {
				key = key.substring(key.indexOf(":")+1);
			}
			
			ns.put(key, vtdNav.toNormalizedString(i+1));
		}
		ap.resetXPath();
		return ns;
	}


	private void initializing() {
		mapAll = new HierarchialHashMap<String, Object>(null, "");
		pointer = null;
		namespaces = new HashMap<String, String>();
		namespacesXPath = null;
		vtdGen = null;
	}


	public HierarchialHashMap<String, Object> getMap() {
		return mapAll;
	}
	
	
	public Map<String, String> getNamespaces() {
		return namespaces;
	}


}