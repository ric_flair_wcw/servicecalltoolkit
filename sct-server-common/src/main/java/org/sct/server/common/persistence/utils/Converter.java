package org.sct.server.common.persistence.utils;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.sct.common.constants.ServiceTypeEnum;
import org.sct.server.common.beans.POIDTO;
import org.sct.server.common.beans.ServerDTO;
import org.sct.server.common.beans.ServiceDTO;
import org.sct.server.common.constants.HttpMethodEnum;
import org.sct.server.common.persistence.model.EndpointEntity;
import org.sct.server.common.persistence.model.ExtractEntity;
import org.sct.server.common.persistence.model.POIEntity;
import org.sct.server.common.persistence.model.ServerEntity;
import org.sct.server.common.persistence.model.ServiceEntity;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Converter {


	private ObjectMapper objectMapper;

	
	// the method cannot be used outside the transaction otherwise the endpoint list won't be loaded (lazy load) !!
	public ServiceDTO convertToServiceDTO(ServiceEntity serviceEntity) throws JsonParseException, JsonMappingException, IOException {
		// building the namespace map -> the entity contains JSON
		Map<String, String> namespaces = null;
		if (serviceEntity.getNamespaces() != null) {
			namespaces = objectMapper.readValue(serviceEntity.getNamespaces(), Map.class);
		}

		// building the endpoint list
		List<String> endpoints = new ArrayList<String>();
		for(EndpointEntity endpointEntity : serviceEntity.getEndpoints()) {
			endpoints.add(endpointEntity.getUrl());
		}
		
		return new ServiceDTO(serviceEntity.getId(), 
				serviceEntity.getName(), 
				serviceEntity.getServiceType(), 
				serviceEntity.getSoapAction(), 
				serviceEntity.getRequestTemplateText(), 
				namespaces, 
				endpoints,
				serviceEntity.getHttpMethod(),
				serviceEntity.getDatabaseQuery(),
				serviceEntity.getDescription());
	}
	
	
	public ServiceEntity convertToServiceEntity(ServiceDTO serviceDTO, ServiceEntity serviceEntity) throws JsonGenerationException, JsonMappingException, IOException {
		StringWriter stringWriter = new StringWriter();
		objectMapper.writeValue(stringWriter, serviceDTO.getNamespaces());
		
		if (serviceDTO.getId() != null) {
			serviceEntity.setId(serviceDTO.getId());
		}
		serviceEntity.setName(serviceDTO.getName());
		serviceEntity.setNamespaces(stringWriter.toString());
		serviceEntity.setRequestTemplateText(serviceDTO.getRequestTemplateText());
		serviceEntity.setServiceType(serviceDTO.getServiceType());
		serviceEntity.setSoapAction(serviceDTO.getSoapAction());
		serviceEntity.setHttpMethod(serviceDTO.getHttpMethod());
		serviceEntity.setDatabaseQuery(serviceDTO.getDatabaseQuery());
		serviceEntity.setDescription(serviceDTO.getDescription());
		
		return serviceEntity;
	}


	public POIDTO convertToPOIDTO(POIEntity poiEntity) {
		// building the endpoint list
		List<String> extracts = new ArrayList<String>();
		for(ExtractEntity extractEntity : poiEntity.getExtracts()) {
			extracts.add(extractEntity.getPath());
		}
		
		ServiceDTO serviceDTO = new ServiceDTO(poiEntity.getServiceEntity().getId(), 
				                               poiEntity.getServiceEntity().getName(), 
				                               poiEntity.getServiceEntity().getServiceType(), 
				                               null, 
				                               null, 
				                               null, 
				                               null,
				                               null,
				                               null,
				                               null);
		
		return new POIDTO(poiEntity.getId(), 
				          poiEntity.getName(),
				          poiEntity.getSingeValue(),
				          poiEntity.getEncryptionNeeded(),
				          serviceDTO,
				          extracts,
				          poiEntity.getDescription());
	}


	public POIEntity convertToPOIEntity(POIDTO poiDTO, POIEntity poiEntity, ServiceEntity serviceEntity) {
		if (poiDTO.getId() != null) {
			poiEntity.setId(poiDTO.getId());
		}
		poiEntity.setName(poiDTO.getName());
		poiEntity.setSingeValue(poiDTO.getSingleValue());
		poiEntity.setEncryptionNeeded(poiDTO.getEncryptionNeeded());
		poiEntity.setServiceEntity(serviceEntity);
		poiEntity.setDescription(poiDTO.getDescription());
		
		return poiEntity;
	}


	public ServerDTO convertToServerDTO(ServerEntity serverEntity) {
		return new ServerDTO(serverEntity.getId(), serverEntity.getName(), serverEntity.getUrl());
	}


	public ServerEntity convertToServerEntity(ServerDTO serverDTO, ServerEntity serverEntity) {
		if (serverDTO.getId() != null) {
			serverEntity.setId(serverDTO.getId());
		}
		serverEntity.setName(serverDTO.getName());
		serverEntity.setUrl(serverDTO.getUrl());
		
		return serverEntity;
	}


	public ServiceDTO convertToServiceDTOFromMap(Map<String, Object> record) {
		HttpMethodEnum httpMethod = null;
		if (record.get("httpMethod") != null) {
			httpMethod = HttpMethodEnum.valueOf(record.get("httpMethod").toString());
		}
		ServiceDTO serviceDTO = new ServiceDTO(new Long(record.get("id").toString()), 
				                               (String) record.get("name"), 
				                               ServiceTypeEnum.valueOf(record.get("serviceType").toString()),
				                               (String) record.get("soapAction"),
				                               (String) record.get("requestTemplateText"),
				                               (Map<String, String>) record.get("namespaces"),
				                               (List<String>) record.get("endpoints"),
				                               httpMethod,
				                               (String) record.get("databaseQuery"),
				                               (String) record.get("description"));
		return serviceDTO;
	}


	public POIDTO convertToPOIDTOFromMap(Map<String, Object> record) {
		Long id = new Long(record.get("id").toString());
		String name = (String) record.get("name");
		String singleValue = (String) record.get("singleValue");
		Boolean encryptionNeeded = new Boolean(record.get("encryptionNeeded").toString());
		
		Map<String, Object> serviceDTO = (Map<String, Object>) record.get("serviceDTO");
		Long serviceId = new Long(serviceDTO.get("id").toString());
		String serviceName = serviceDTO.get("name").toString();
		
		POIDTO poiDTO = new POIDTO(id,
				                   name,
				                   singleValue,
				                   encryptionNeeded,
				                   new ServiceDTO(serviceId, 
				                		          serviceName, 
				                		          null, 
				                		          null, 
				                		          null, 
				                		          null, 
				                		          null,
				                		          null,
				                		          null,
				                		          null),
				                   (List<String>)record.get("extracts"),
				                   (String) record.get("description"));
		return poiDTO;
	}


	public ObjectMapper getObjectMapper() {
		return objectMapper;
	}

	public void setObjectMapper(ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
	}


}
