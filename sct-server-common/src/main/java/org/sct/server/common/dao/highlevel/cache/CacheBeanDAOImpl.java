package org.sct.server.common.dao.highlevel.cache;

import java.util.List;

import org.sct.server.common.beans.POIDTO;
import org.sct.server.common.beans.ServiceDTO;
import org.sct.server.common.constants.Constants;
import org.sct.server.common.dao.lowlevel.cache.ICache;


/**
 * Cache DAO implementation class to get/put/delete POI and service 
 * 
 * @author Viktor Horvath
 *
 */
public class CacheBeanDAOImpl implements ICacheBeanDAO {

	
	private ICache cache = null;
	
	
	/**
	 * Returns a POIDTO from the cache by the name
	 */
	public POIDTO getPOICacheBean(String poiName) {
		return cache.get(Constants.CACHE_KEY_POI + poiName, POIDTO.class);
	}
	/**
	 * Adding a POIDTO to the cache
	 */
	public void put(String poiName, POIDTO cacheBean) {
		cache.put(Constants.CACHE_KEY_POI + poiName, cacheBean);
	}
	/**
	 * Deleting a POIDTO from the cache by name
	 */
	public void deletePOICacheBean(String poiName) {
		cache.remove(Constants.CACHE_KEY_POI + poiName);
	}

	
	/**
	 * Returns a ServiceDTO from the cache by the name
	 */
	public ServiceDTO getServiceCacheBean(String serviceName) {
		return cache.get(Constants.CACHE_KEY_SERVICE + serviceName, ServiceDTO.class);
	}
	/**
	 * Adding a ServiceDTO to the cache
	 */
	public void put(String serviceName, ServiceDTO cacheBean) {
		cache.put(Constants.CACHE_KEY_SERVICE + serviceName, cacheBean);
	}
	/**
	 * Deleting a ServiceDTO from the cache by name
	 */
	public void deleteServiceCacheBean(String serviceName) {
		cache.remove(Constants.CACHE_KEY_SERVICE + serviceName);
	}

	
	/**
	 * Getting all the keys from the cache
	 */	
	public List<String> getCacheKeys() {
		return cache.getKeys();
	}

	/**
	 * Getting the injected ICache object
	 */	
	public ICache getCache() {
		return cache;
	}
	/**
	 * Setting the injected ICache object
	 */	
	public void setCache(ICache cache) {
		this.cache = cache;
	}


}
