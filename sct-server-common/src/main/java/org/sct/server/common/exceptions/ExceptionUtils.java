package org.sct.server.common.exceptions;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

public class ExceptionUtils {

	
	public Map<String, String> createMapFromThrowable(Throwable t) {
		Map<String, String> exceptionJsonMap = new HashMap<String, String>();
		
		StringWriter stringWriter = new StringWriter();
		PrintWriter printWriter = new PrintWriter(stringWriter);
		t.printStackTrace(printWriter);
		String stackTrace = stringWriter.toString();

		exceptionJsonMap.put("exceptionStackTrace", (stackTrace.length() <= 699 ? stackTrace : stackTrace.substring(0, 699)+" ..."));
		exceptionJsonMap.put("exception", "true");
		exceptionJsonMap.put("exceptionClass", t.getClass().getName());
		exceptionJsonMap.put("exceptionMessage", t.getMessage());
		
		return exceptionJsonMap;
	}
	
}
