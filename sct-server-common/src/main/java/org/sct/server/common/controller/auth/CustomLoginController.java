package org.sct.server.common.controller.auth;

import org.sct.server.common.controller.ParentController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/auth")
public class CustomLoginController extends ParentController {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomLoginController.class);
	
	
	@RequestMapping(value = "/customLogin", method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView login(@RequestParam(value = "loginError", required = false) String loginError,
                              @RequestParam(value = "logout", required = false) String logout) {
		LOGGER.debug("Received GET request to log in. loginError={}, logout={}", loginError, logout);
        ModelAndView model = new ModelAndView("/auth/login.tiles");
        if (loginError != null) {
            model.addObject("loginErrorText", "Invalid username and password!");
        }

        if (logout != null) {
            model.addObject("logoutText", "You've been logged out successfully.");
        }

        return model;
    }
	
}
