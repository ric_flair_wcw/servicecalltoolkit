package org.sct.server.common.persistence.service;

import java.io.IOException;
import java.util.List;

import org.sct.server.common.beans.POIDTO;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

public interface IPOIEntityService {

	
	List<POIDTO> getPOIs() throws JsonParseException, JsonMappingException, IOException;
	
	void deletePOI(Long id);
	
	void savePOI(POIDTO poiBean) throws JsonGenerationException, JsonMappingException, IOException;

	POIDTO getPOI(Long id) throws JsonParseException, JsonMappingException, IOException;

	POIDTO getPOIByName(String poiName);
}
