package org.sct.server.common.persistence.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.sct.server.common.beans.POIDTO;
import org.sct.server.common.beans.ServiceDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:server-common-context-test.xml")
public class RepositoryServiceEntityServiceTest {

	
	@Autowired
	private ApplicationContext applicationContext;
	
	private IPOIEntityService poiEntityService;

	
	@Before
	public void setUp() {
		poiEntityService = (IPOIEntityService) applicationContext.getBean("poiEntityService");
	}
	
	
	@Test
	public void testGetPOIs_successful() throws JsonParseException, JsonMappingException, IOException {
		List<POIDTO> pois = poiEntityService.getPOIs();
		
		assertEquals(3, pois.size());
		assertEquals(39l, pois.get(0).getId().longValue());
		assertEquals("postcode->fullAddress", pois.get(1).getName());
		assertEquals("true", pois.get(0).getSingleValue());
		assertEquals(2, pois.get(2).getExtracts().size());
		assertEquals("GetAddressForPostCode-GetAddressForPostCode_by_id", pois.get(1).getServiceDTO().getName());
	}
	

	@Test
	public void testGetPOI_successful() throws JsonParseException, JsonMappingException, IOException {
		POIDTO poi = poiEntityService.getPOI(38l);
		
		assertEquals(38l, poi.getId().longValue());
		assertEquals("postcode->fullAddress", poi.getName());
		assertEquals("false", poi.getSingleValue());
		assertEquals(15, poi.getExtracts().size());
		assertEquals("GetAddressForPostCode-GetAddressForPostCode_by_id", poi.getServiceDTO().getName());
	}
	@Test
	public void testGetPOI_invalidId() throws JsonParseException, JsonMappingException, IOException {
		POIDTO poi = poiEntityService.getPOI(138l);
		
		assertNull(poi);
	}

	
	@Test
	public void testGetPOIByName_successful() throws JsonParseException, JsonMappingException, IOException {
		POIDTO poi = poiEntityService.getPOIByName("postcode->some_strange_value");
		
		assertEquals(41l, poi.getId().longValue());
		assertEquals("postcode->some_strange_value", poi.getName());
		assertEquals("false", poi.getSingleValue());
		assertEquals(2, poi.getExtracts().size());
		assertEquals("GetAddressForPostCode-GetAddressForPostCode_by_postcode", poi.getServiceDTO().getName());
	}
	@Test
	public void testGetPOIByName_invalidName() throws JsonParseException, JsonMappingException, IOException {
		POIDTO poi = poiEntityService.getPOIByName("paaaaaaaostcode->some_strange_value");
		
		assertNull(poi);
	}


	@Test
	public void testSavePOI_inserting_successful() throws JsonParseException, JsonMappingException, IOException {
		insertPOI();
		// retrieving it to be checked
		POIDTO poi = poiEntityService.getPOIByName("test");
		
//		assertEquals(999l, poi.getId().longValue());
		assertEquals("test", poi.getName());
		assertEquals("false", poi.getSingleValue());
		assertEquals(1, poi.getExtracts().size());
		assertEquals("/almafa/ag/alma", poi.getExtracts().get(0));
		assertEquals("GetAddressForPostCode-GetAddressForPostCode_by_id", poi.getServiceDTO().getName());
		assertEquals("desc2", poi.getDescription());
		
		// housekeeping after the test
		poiEntityService.deletePOI(poi.getId());
	}


	@Test
	public void testSavePOI_updating_successful() throws JsonParseException, JsonMappingException, IOException {
		// retrieving a POIDTO to be modified
		insertPOI();
		POIDTO poiBean = poiEntityService.getPOIByName("test");
		// modify it
		poiBean.setName(poiBean.getName() + "1");
		List<String> extracts = new ArrayList<String>();
		extracts.add("Kedves"); extracts.add("Izibaba");
		poiBean.setExtracts(extracts);
		IServiceEntityService serviceEntityService = (IServiceEntityService) applicationContext.getBean("serviceEntityService");
		ServiceDTO serviceDTO = serviceEntityService.getService(34l);
		poiBean.setServiceDTO(serviceDTO);
		poiBean.setSingleValue("true");
		// saving it
		poiEntityService.savePOI(poiBean);
		// retrieving it to be checked
		POIDTO poi = poiEntityService.getPOI(poiBean.getId());
		
//		assertEquals(999l, poi.getId().longValue());
		assertEquals("test1", poi.getName());
		assertEquals("true", poi.getSingleValue());
		assertEquals(2, poi.getExtracts().size());
		assertEquals("Kedves", poi.getExtracts().get(0));
		assertEquals("Izibaba", poi.getExtracts().get(1));
		assertEquals("GetAddressForPostCode-GetAddressForPostCode_by_postcode", poi.getServiceDTO().getName());
		assertEquals("desc2", poi.getDescription());
		
		// housekeeping after the test
		poiEntityService.deletePOI(poi.getId());
	}


	@Test
	public void testDeletePOI_successful() throws JsonParseException, JsonMappingException, IOException {
		insertPOI();
		POIDTO poiBean = poiEntityService.getPOIByName("test");
		// deleting it
		poiEntityService.deletePOI(poiBean.getId());
		// retrieving it to be checked
		POIDTO poi = poiEntityService.getPOI(poiBean.getId());
		
		assertNull(poi);
	}


	private void insertPOI() throws JsonParseException, JsonMappingException, IOException, JsonGenerationException {
		IServiceEntityService serviceEntityService = (IServiceEntityService) applicationContext.getBean("serviceEntityService");
		ServiceDTO serviceDTO = serviceEntityService.getService(29l);
		List<String> extracts = new ArrayList<String>();
		extracts.add("/almafa/ag/alma");
		POIDTO poiBean = new POIDTO(null, "test", "false", false, serviceDTO, extracts, "desc2"); 
		// saving it
		poiEntityService.savePOI(poiBean);
	}
	
}
