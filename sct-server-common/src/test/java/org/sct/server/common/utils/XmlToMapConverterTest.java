package org.sct.server.common.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.sct.common.objects.map.HierarchialHashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ximpleware.EOFException;
import com.ximpleware.EncodingException;
import com.ximpleware.EntityException;
import com.ximpleware.NavException;
import com.ximpleware.ParseException;
import com.ximpleware.XPathEvalException;
import com.ximpleware.XPathParseException;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:server-common-context-test.xml")
public class XmlToMapConverterTest {

	
	@Autowired
	private ApplicationContext applicationContext;

	private IXmlToMapConverter xmlToMapConverter;
	
	private String xml = 
			        "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns=\"http://virginmedia/schema/GetAddressForPostCode/4/0\" xmlns:ns1=\"http://virginmedia/schema/RequestHeader/1/4\" xmlns:ns2=\"http://virginmedia/schema/BaseType/1/3\">"+
					"   <soapenv:Header/>"+
					"   <soapenv:Body>"+
					"      <ns:GetAddressForPostCodeRequest>"+
					"         <ns:requestHeader>"+
					"            <ns1:id>${sctParam.id}</ns1:id>"+
					"            <!--Optional:-->"+
					"            <ns1:source>?</ns1:source>"+
					"            <ns1:user>"+
					"               <ns2:userId>en</ns2:userId>"+
					"               <ns2:credentials>?</ns2:credentials>"+
					"            </ns1:user>"+
					"         </ns:requestHeader>"+
					"         <!--Optional:-->"+
					"         <ns:postalAddress>"+
					"            <!--Optional:-->"+
					"            <ns:displayAddress>1</ns:displayAddress>"+
					"         </ns:postalAddress>"+
					"         <ns:postalAddress>"+
					"            <!--Optional:-->"+
					"            <ns:displayAddress>2</ns:displayAddress>"+
					"            <!--Optional:-->"+
					"            <ns:buildingID>?</ns:buildingID>"+
					"            <!--Optional:-->"+
					"            <ns:building>?</ns:building>"+
					"            <!--Optional:-->"+
					"            <ns:apartment>?</ns:apartment>"+
					"            <!--Optional:-->"+
					"            <ns:buildingName>?</ns:buildingName>"+
					"            <!--Optional:-->"+
					"            <ns:county>?</ns:county>"+
					"            <!--Optional:-->"+
					"            <ns:locality>?</ns:locality>"+
					"            <!--Optional:-->"+
					"            <ns:nonUKPostcode>BH1 3EH</ns:nonUKPostcode>"+
					"            <!--Optional:-->"+
					"            <ns:postcodeSuffix>?</ns:postcodeSuffix>"+
					"            <!--Optional:-->"+
					"            <ns:streetName>?</ns:streetName>"+
					"            <!--Optional:-->"+
					"            <ns:streetType>?</ns:streetType>"+
					"            <!--Optional:-->"+
					"            <ns:subBuildingID>?</ns:subBuildingID>"+
					"            <!--Optional:-->"+
					"            <ns:town>?</ns:town>"+
					"            <!--Optional:-->"+
					"            <ns:country>HU</ns:country>"+
					"            <!--Optional:-->"+
					"            <ns:postcode>BH1 3EH</ns:postcode>"+
					"            <!--Zero or more repetitions:-->"+
					"            <ns:addressIdentification>"+
					"               <ns:identifierType>?</ns:identifierType>"+
					"               <ns:addressIdentifier>?</ns:addressIdentifier>"+
					"               <!--Optional:-->"+
					"               <ns:instanceIdentifier>?</ns:instanceIdentifier>"+
					"            </ns:addressIdentification>"+
					"            <!--Optional:-->"+
					"            <ns:handoverPointDetails>"+
					"               <ns:L2SID>?</ns:L2SID>"+
					"               <ns:HandoverPortID>?</ns:HandoverPortID>"+
					"               <ns:SVLANID>?</ns:SVLANID>"+
					"               <ns:exchangeRef>?</ns:exchangeRef>"+
					"            </ns:handoverPointDetails>"+
					"         </ns:postalAddress>"+
					"         <!--Optional:-->"+
					"         <ns:serviceDirective>?</ns:serviceDirective>"+
					"      </ns:GetAddressForPostCodeRequest>"+
					"   </soapenv:Body>"+
					"</soapenv:Envelope>";			
	
	
	@Before
	public void setUp() {
		xmlToMapConverter = (IXmlToMapConverter) applicationContext.getBean("xmlToMapConverter");
	}
	
	
	@Test
	public void testStartWithNamespaces_successful_simpleValue() throws EncodingException, EOFException, EntityException, NavException, ParseException, XPathParseException, XPathEvalException {
		Map<String, String> namespacesXPath = new HashMap<String, String>();
		namespacesXPath.put("ans2", "http://virginmedia/schema/BaseType/1/3");
		namespacesXPath.put("ans1", "http://virginmedia/schema/RequestHeader/1/4");
		namespacesXPath.put("asoapenv", "http://schemas.xmlsoap.org/soap/envelope/");
		namespacesXPath.put("ans", "http://virginmedia/schema/GetAddressForPostCode/4/0");
		
		xmlToMapConverter.start(xml, namespacesXPath );
		HierarchialHashMap<String, Object> xmlMap = xmlToMapConverter.getMap();
		HierarchialHashMap<String, Object> soapenv_Envelope = ((HierarchialHashMap<String, Object>)xmlMap.get("/asoapenv:Envelope"));
		HierarchialHashMap<String, Object> soapenv_Body = ((HierarchialHashMap<String, Object>)soapenv_Envelope.get("/asoapenv:Envelope/asoapenv:Body"));
		HierarchialHashMap<String, Object> ns_GetAddressForPostCodeRequest = ((HierarchialHashMap<String, Object>)soapenv_Body.get("/asoapenv:Envelope/asoapenv:Body/ans:GetAddressForPostCodeRequest"));
		HierarchialHashMap<String, Object> ns_requestHeader = ((HierarchialHashMap<String, Object>)ns_GetAddressForPostCodeRequest.get("/asoapenv:Envelope/asoapenv:Body/ans:GetAddressForPostCodeRequest/ans:requestHeader"));
		HierarchialHashMap<String, Object> ns1_user = ((HierarchialHashMap<String, Object>)ns_requestHeader.get("/asoapenv:Envelope/asoapenv:Body/ans:GetAddressForPostCodeRequest/ans:requestHeader/ans1:user"));
		String userId = (String)ns1_user.get("/asoapenv:Envelope/asoapenv:Body/ans:GetAddressForPostCodeRequest/ans:requestHeader/ans1:user/ans2:userId");
		assertEquals("en", userId);
	}


	@Test
	public void testStartWithNamespaces_successful_list() throws EncodingException, EOFException, EntityException, NavException, ParseException, XPathParseException, XPathEvalException {
		Map<String, String> namespacesXPath = new HashMap<String, String>();
		namespacesXPath.put("ns2_", "http://virginmedia/schema/BaseType/1/3");
		namespacesXPath.put("ns1_", "http://virginmedia/schema/RequestHeader/1/4");
		namespacesXPath.put("soapenv", "http://schemas.xmlsoap.org/soap/envelope/");
		namespacesXPath.put("ns_", "http://virginmedia/schema/GetAddressForPostCode/4/0");
		
		xmlToMapConverter.start(xml, namespacesXPath );
		HierarchialHashMap<String, Object> xmlMap = xmlToMapConverter.getMap();
		HierarchialHashMap<String, Object> soapenv_Envelope = ((HierarchialHashMap<String, Object>)xmlMap.get("/soapenv:Envelope"));
		HierarchialHashMap<String, Object> soapenv_Body = ((HierarchialHashMap<String, Object>)soapenv_Envelope.get("/soapenv:Envelope/soapenv:Body"));
		HierarchialHashMap<String, Object> ns_GetAddressForPostCodeRequest = ((HierarchialHashMap<String, Object>)soapenv_Body.get("/soapenv:Envelope/soapenv:Body/ns_:GetAddressForPostCodeRequest"));
		List<?> ns_postalAddress = (List)ns_GetAddressForPostCodeRequest.get("/soapenv:Envelope/soapenv:Body/ns_:GetAddressForPostCodeRequest/ns_:postalAddress");
		assertEquals(2, ns_postalAddress.size());
		String ns_displayAddress_1 = (String) ((HierarchialHashMap<String, Object>)ns_postalAddress.get(0)).get("/soapenv:Envelope/soapenv:Body/ns_:GetAddressForPostCodeRequest/ns_:postalAddress/ns_:displayAddress");
		assertEquals("1", ns_displayAddress_1);
		String ns_displayAddress_2 = (String) ((HierarchialHashMap<String, Object>)ns_postalAddress.get(1)).get("/soapenv:Envelope/soapenv:Body/ns_:GetAddressForPostCodeRequest/ns_:postalAddress/ns_:displayAddress");
		assertEquals("2", ns_displayAddress_2);
	}


	@Test
	public void testStartWithoutNamespaces_successful_simpleValue() throws EncodingException, EOFException, EntityException, NavException, ParseException, XPathParseException, XPathEvalException {
		xmlToMapConverter.start(xml);
		HierarchialHashMap<String, Object> xmlMap = xmlToMapConverter.getMap();
		HierarchialHashMap<String, Object> soapenv_Envelope = ((HierarchialHashMap<String, Object>)xmlMap.get("/soapenv:Envelope"));
		HierarchialHashMap<String, Object> soapenv_Body = ((HierarchialHashMap<String, Object>)soapenv_Envelope.get("/soapenv:Envelope/soapenv:Body"));
		HierarchialHashMap<String, Object> ns_GetAddressForPostCodeRequest = ((HierarchialHashMap<String, Object>)soapenv_Body.get("/soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeRequest"));
		HierarchialHashMap<String, Object> ns_requestHeader = ((HierarchialHashMap<String, Object>)ns_GetAddressForPostCodeRequest.get("/soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeRequest/ns:requestHeader"));
		HierarchialHashMap<String, Object> ns1_user = ((HierarchialHashMap<String, Object>)ns_requestHeader.get("/soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeRequest/ns:requestHeader/ns1:user"));
		String userId = (String)ns1_user.get("/soapenv:Envelope/soapenv:Body/ns:GetAddressForPostCodeRequest/ns:requestHeader/ns1:user/ns2:userId");
		assertEquals("en", userId);
	}


	@Test
	public void testStartWithNamespaces_unsuccessful_notXmlString() throws EncodingException, EOFException, EntityException, NavException, ParseException, XPathParseException, XPathEvalException {
		Map<String, String> namespacesXPath = new HashMap<String, String>();
		namespacesXPath.put("ans2", "http://virginmedia/schema/BaseType/1/3");
		namespacesXPath.put("ans1", "http://virginmedia/schema/RequestHeader/1/4");
		namespacesXPath.put("asoapenv", "http://schemas.xmlsoap.org/soap/envelope/");
		namespacesXPath.put("ans", "http://virginmedia/schema/GetAddressForPostCode/4/0");
		
		try {
			xmlToMapConverter.start("it is not an XML", namespacesXPath );
			fail("RuntimeException should have been thrown!");
		} catch(RuntimeException e) {
			assertEquals("The XML text is invalid!", e.getMessage());
		}
	}


	@Test
	public void testStartWithNamespaces_unsuccessful_invalidNamespaces() throws EncodingException, EOFException, EntityException, NavException, ParseException, XPathParseException, XPathEvalException {
		Map<String, String> namespacesXPath = new HashMap<String, String>();
		namespacesXPath.put("ans2", "http://virginmedia/schema/BaseType/1/31");
		namespacesXPath.put("ans1", "http://virginmedia/schema/RequestHeader/1/41");
		namespacesXPath.put("asoapenv", "http://schemas.xmlsoap.org/soap/envelope/1");
		namespacesXPath.put("ans", "http://virginmedia/schema/GetAddressForPostCode/4/01");
		
		try {
			xmlToMapConverter.start(xml, namespacesXPath );
			fail("RuntimeException should have been thrown!");
		} catch(RuntimeException e) {
			assertEquals("It is pretty unexpected... The 'http://schemas.xmlsoap.org/soap/envelope/' namespace is not in the list of namespaces coming from extraction.", e.getMessage());
		}
	}
}
