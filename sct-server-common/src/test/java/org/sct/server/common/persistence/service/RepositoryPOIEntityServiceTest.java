package org.sct.server.common.persistence.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.sct.common.constants.ServiceTypeEnum;
import org.sct.server.common.beans.ServiceDTO;
import org.sct.server.common.constants.HttpMethodEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:server-common-context-test.xml")
public class RepositoryPOIEntityServiceTest {

	
	@Autowired
	private ApplicationContext applicationContext;
	
	private IServiceEntityService serviceEntityService;

	
	@Before
	public void setUp() {
		serviceEntityService = (IServiceEntityService) applicationContext.getBean("serviceEntityService");
	}
	
	
	@Test
	public void testGetServices_successful() throws JsonParseException, JsonMappingException, IOException {
		List<ServiceDTO> services = serviceEntityService.getServices();
		
		assertEquals(2, services.size());
		assertEquals(29l, services.get(0).getId().longValue());
		assertEquals("GetAddressForPostCode-GetAddressForPostCode_by_postcode", services.get(1).getName());
		assertEquals("<soapenv:Envelo", services.get(0).getRequestTemplateText().substring(0, 15));
		assertEquals(ServiceTypeEnum.WEBSERVICE, services.get(1).getServiceType());
		assertEquals("GetAddressForPostCode", services.get(0).getSoapAction());
		assertEquals(1, services.get(0).getEndpoints().size());
		assertEquals("http://lap50215:8083/mockGetAddressForPostCode", services.get(0).getEndpoints().get(0));
		assertNull(services.get(1).getHttpMethod());
		assertEquals(4, services.get(0).getNamespaces().keySet().size());
		assertEquals("http://virginmedia/schema/GetAddressForPostCode/4/0", services.get(0).getNamespaces().get("ns"));
	}
	

	@Test
	public void testGetService_successful() throws JsonParseException, JsonMappingException, IOException {
		ServiceDTO service = serviceEntityService.getService(34l);
		
		assertEquals(34l, service.getId().longValue());
		assertEquals("GetAddressForPostCode-GetAddressForPostCode_by_postcode", service.getName());
		assertEquals("<soapenv:Envelo", service.getRequestTemplateText().substring(0, 15));
		assertEquals(ServiceTypeEnum.WEBSERVICE, service.getServiceType());
		assertEquals("GetAddressForPostCode", service.getSoapAction());
		assertEquals(1, service.getEndpoints().size());
		assertEquals("http://lap50215:8083/mockGetAddressForPostCode", service.getEndpoints().get(0));
		assertNull(service.getHttpMethod());
		assertEquals(4, service.getNamespaces().keySet().size());
		assertEquals("http://virginmedia/schema/faults/2/1", service.getNamespaces().get("ns2"));
	}
	@Test
	public void testGetService_invalidId() throws JsonParseException, JsonMappingException, IOException {
		ServiceDTO service = serviceEntityService.getService(1383l);
		
		assertNull(service);
	}

	
	@Test
	public void testGetServiceByName_successful() throws JsonParseException, JsonMappingException, IOException {
		ServiceDTO service = serviceEntityService.getServiceByName("GetAddressForPostCode-GetAddressForPostCode_by_postcode");
		
		assertEquals(34l, service.getId().longValue());
		assertEquals("GetAddressForPostCode-GetAddressForPostCode_by_postcode", service.getName());
		assertEquals("<soapenv:Envelo", service.getRequestTemplateText().substring(0, 15));
		assertEquals(ServiceTypeEnum.WEBSERVICE, service.getServiceType());
		assertEquals("GetAddressForPostCode", service.getSoapAction());
		assertEquals(1, service.getEndpoints().size());
		assertEquals("http://lap50215:8083/mockGetAddressForPostCode", service.getEndpoints().get(0));
		assertNull(service.getHttpMethod());
		assertEquals(4, service.getNamespaces().keySet().size());
		assertEquals("http://virginmedia/schema/faults/2/1", service.getNamespaces().get("ns2"));
	}
	@Test
	public void testGetServiceByName_invalidName() throws JsonParseException, JsonMappingException, IOException {
		ServiceDTO service = serviceEntityService.getServiceByName("kanya2");
		
		assertNull(service);
	}


	@Test
	public void testSaveService_inserting_successful() throws JsonParseException, JsonMappingException, IOException {
		insertService();
		// retrieving it to be checked
		ServiceDTO service = serviceEntityService.getServiceByName("test");
		
		assertEquals("test", service.getName());
		assertEquals(2, service.getEndpoints().size());
		assertEquals("http://2", service.getEndpoints().get(1));
		assertNull(service.getHttpMethod());
		assertEquals(2, service.getNamespaces().keySet().size());
		assertEquals("http://2", service.getNamespaces().get("ns2"));
		assertEquals("requestTemplateText", service.getRequestTemplateText());
		assertEquals("soapAction", service.getSoapAction());
		assertEquals(ServiceTypeEnum.WEBSERVICE, service.getServiceType());
		
		// housekeeping after the test
		serviceEntityService.deleteService(service.getId());
	}


	@Test
	public void testSavePOIsavePOI_updating_successful() throws JsonParseException, JsonMappingException, IOException {
		insertService();
		// retrieving a ServiceDTO to be modified
		ServiceDTO oldService = serviceEntityService.getServiceByName("test");
		List<String> endpoints = new ArrayList<String>();
		endpoints.add("kkk://aaa");
		ServiceDTO newService = new ServiceDTO(oldService.getId(), 
											   oldService.getName() + "1", 
											   ServiceTypeEnum.REST, 
											   null, 
											   null, 
											   null, 
											   endpoints , 
											   HttpMethodEnum.POST,
											   null,
											   "desc1");
		// saving it
		serviceEntityService.saveService(newService);
		// retrieving it to be checked
		ServiceDTO service = serviceEntityService.getService(newService.getId());
		
		assertEquals("test1", service.getName());
		assertEquals(1, service.getEndpoints().size());
		assertEquals("kkk://aaa", service.getEndpoints().get(0));
		assertEquals(HttpMethodEnum.POST, service.getHttpMethod());
		assertNull(service.getNamespaces());
		assertNull(service.getRequestTemplateText());
		assertNull(service.getSoapAction());
		assertEquals(ServiceTypeEnum.REST, service.getServiceType());
		assertEquals("desc1", service.getDescription());
		
		// housekeeping after the test
		serviceEntityService.deleteService(service.getId());
	}


	@Test
	public void testDeletePOI_successful() throws JsonParseException, JsonMappingException, IOException {
		insertService();
		ServiceDTO service = serviceEntityService.getServiceByName("test");
		// deleting it
		serviceEntityService.deleteService(service.getId());
		// retrieving it to be checked
		service = serviceEntityService.getService(service.getId());
		
		assertNull(service);
	}


	private void insertService() throws JsonGenerationException, JsonMappingException, IOException {
		Map<String, String> namespaces = new HashMap<String, String>();
		namespaces.put("ns", "http://1"); namespaces.put("ns2", "http://2");
		List<String> endpoints = new ArrayList<String>();
		endpoints.add("http://1"); endpoints.add("http://2");
		ServiceDTO serviceBean = new ServiceDTO(null, "test", ServiceTypeEnum.WEBSERVICE, "soapAction", "requestTemplateText", namespaces, endpoints, null, null, null);
		// saving it
		serviceEntityService.saveService(serviceBean);
	}
}
