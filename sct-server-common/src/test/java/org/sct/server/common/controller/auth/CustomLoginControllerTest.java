package org.sct.server.common.controller.auth;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:server-common-context-test.xml")
//@WebAppConfiguration
public class CustomLoginControllerTest {

	
	private MockMvc mockMvc;
	@Autowired
	private ApplicationContext applicationContext;
	private CustomLoginController customLoginController;
	
	
	@Before
    public void setup() {
		customLoginController = (CustomLoginController) applicationContext.getBean("customLoginController");
//		CustomLoginController customLoginController = new CustomLoginController();
		this.mockMvc = MockMvcBuilders.standaloneSetup(customLoginController).build();
    }
	
	
	@Test
	public void testLogin_successfulViewRetrieval() throws Exception {
		mockMvc.perform(post("/auth/customLogin"))
						.andExpect(status().isOk())
						.andExpect(view().name("/auth/login.tiles"))
						.andExpect(model().attributeDoesNotExist("loginErrorText"))
						.andExpect(model().attributeDoesNotExist("logoutText"));;
	}


	@Test
	public void testLogin_loginError() throws Exception {
		mockMvc.perform(post("/auth/customLogin").param("loginError", "dfsdf"))
						.andExpect(status().isOk())
						.andExpect(view().name("/auth/login.tiles"))
						.andExpect(model().attribute("loginErrorText", "Invalid username and password!"))
						.andExpect(model().attributeDoesNotExist("logoutText"));
	}


	@Test
	public void testLogin_logout() throws Exception {
		mockMvc.perform(post("/auth/customLogin").param("logout", "dfsdf"))
						.andExpect(status().isOk())
						.andExpect(view().name("/auth/login.tiles"))
						.andExpect(model().attribute("logoutText", "You've been logged out successfully."))
						.andExpect(model().attributeDoesNotExist("loginErrorText"));
	}
}
