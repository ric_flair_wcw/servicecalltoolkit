package org.sct.server.common.exceptions;

import static org.junit.Assert.assertEquals;

import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class ExceptionUtilsTest {

	private ExceptionUtils instance = null;
	
	
	@Before
	public void setUp() {
		instance = new ExceptionUtils();
	}
	
	
	@Test
	public void testCreateMapFromThrowable() {
		Throwable t = new RuntimeException("tesst");
		
		Map<String, String> map = instance.createMapFromThrowable(t);
		
		assertEquals("true", map.get("exception"));
		assertEquals("java.lang.RuntimeException", map.get("exceptionClass"));
		assertEquals("tesst", map.get("exceptionMessage"));
	}
}
