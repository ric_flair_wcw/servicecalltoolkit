package org.sct.server.common.persistence.service;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:server-common-context-test.xml")
public class RepositoryStatServicePoiDateCntEntityServiceTest {

	
	@Autowired
	private ApplicationContext applicationContext;
	
	private IStatServicePoiDateCntEntityService service;

	
	@Before
	public void setUp() {
		service = (IStatServicePoiDateCntEntityService) applicationContext.getBean("statServicePoiDateCntEntityService");
	}
	
	
	@Test
	public void testChangeStatistics_POI_updateExisting() {
		String poiName = "postcode->fullAddress";
		Boolean successful = true;
		// 2015-04-14 14:40:00
		Calendar reqArrivalTime = Calendar.getInstance();
		reqArrivalTime.set(Calendar.YEAR, 2015); reqArrivalTime.set(Calendar.MONTH, 3); reqArrivalTime.set(Calendar.DAY_OF_MONTH, 14);
		reqArrivalTime.set(Calendar.HOUR_OF_DAY, 14); reqArrivalTime.set(Calendar.MINUTE, 41); reqArrivalTime.set(Calendar.SECOND, 32);
		
		// calling the changeStatistics method
		service.changeStatistics(poiName , successful , reqArrivalTime);
		
		// checking the count
		reqArrivalTime.set(Calendar.HOUR_OF_DAY, 14); reqArrivalTime.set(Calendar.MINUTE, 40); reqArrivalTime.set(Calendar.SECOND, 00); reqArrivalTime.set(Calendar.MILLISECOND, 0); 
		List<Object[]> list = service.getStatistics("POI", poiName, reqArrivalTime, reqArrivalTime);
		
		assertEquals(2, list.size());
		// calendar, successful, count
		long cnt = ( ((Boolean)list.get(0)[1]) ? (Long)list.get(0)[2] : (Long)list.get(1)[2] );
		assertEquals(26l, cnt);
	}


	@Test
	public void testChangeStatistics_POI_insertNew() {
		String poiName = "postcode->fullAddress";
		Boolean successful = true;
		Calendar reqArrivalTime = Calendar.getInstance();
		
		// calling the changeStatistics method
		service.changeStatistics(poiName , successful , reqArrivalTime);
		
		// checking the count
		List<Object[]> list = service.getStatistics("POI", poiName, reqArrivalTime, reqArrivalTime);
		
		assertEquals(2, list.size());
		// calendar, successful, count
		long cnt = ( ((Boolean)list.get(0)[1]) ? (Long)list.get(0)[2] : (Long)list.get(1)[2] );
		assertEquals(1l, cnt);
	}


	@Test
	public void testChangeStatistics_service_updateExisting() {
		String poiName = "postcode->buildingName";
		Boolean successful = false;
		// 2015-04-14 15:20:00
		Calendar reqArrivalTime = Calendar.getInstance();
		reqArrivalTime.set(Calendar.YEAR, 2015); reqArrivalTime.set(Calendar.MONTH, 3); reqArrivalTime.set(Calendar.DAY_OF_MONTH, 14);
		reqArrivalTime.set(Calendar.HOUR_OF_DAY, 15); reqArrivalTime.set(Calendar.MINUTE, 23); reqArrivalTime.set(Calendar.SECOND, 32);
		
		// calling the changeStatistics method
		service.changeStatistics(poiName , successful , reqArrivalTime);
		
		// checking the count
		reqArrivalTime.set(Calendar.HOUR_OF_DAY, 15); reqArrivalTime.set(Calendar.MINUTE, 20); reqArrivalTime.set(Calendar.SECOND, 00); reqArrivalTime.set(Calendar.MILLISECOND, 0); 
		List<Object[]> list = service.getStatistics("service", "GetAddressForPostCode-GetAddressForPostCode_by_id", reqArrivalTime, reqArrivalTime);
		
		assertEquals(2, list.size());
		// calendar, successful, count
		long cnt = ( !((Boolean)list.get(0)[1]) ? (Long)list.get(0)[2] : (Long)list.get(1)[2] );
		assertEquals(8l, cnt);
	}


	@Test
	public void testChangeStatistics_service_insertNew() {
		String poiName = "postcode->buildingName";
		Boolean successful = true;
		Calendar reqArrivalTime = Calendar.getInstance();
		reqArrivalTime.set(Calendar.YEAR, 2035); reqArrivalTime.set(Calendar.MONTH, 3); reqArrivalTime.set(Calendar.DAY_OF_MONTH, 14);
		reqArrivalTime.set(Calendar.HOUR_OF_DAY, 14); reqArrivalTime.set(Calendar.MINUTE, 41); reqArrivalTime.set(Calendar.SECOND, 32);
		
		// calling the changeStatistics method
		service.changeStatistics(poiName , successful , reqArrivalTime);
		
		// checking the count
		reqArrivalTime.set(Calendar.HOUR_OF_DAY, 14); reqArrivalTime.set(Calendar.MINUTE, 40); reqArrivalTime.set(Calendar.SECOND, 00); reqArrivalTime.set(Calendar.MILLISECOND, 0);
		List<Object[]> list = service.getStatistics("service", "GetAddressForPostCode-GetAddressForPostCode_by_id", reqArrivalTime, reqArrivalTime);
		
		assertEquals(2, list.size());
		// calendar, successful, count
		long cnt = ( ((Boolean)list.get(0)[1]) ? (Long)list.get(0)[2] : (Long)list.get(1)[2] );
		assertEquals(1l, cnt);
	}


	@Test
	public void testChangeStatistics_service_insertNew_parallel() throws InterruptedException {
		final String poiName = "postcode->buildingName";
		final Boolean successful = true;
		final Calendar reqArrivalTime = Calendar.getInstance();
		reqArrivalTime.set(Calendar.YEAR, 2055); reqArrivalTime.set(Calendar.MONTH, 3); reqArrivalTime.set(Calendar.DAY_OF_MONTH, 14);
		reqArrivalTime.set(Calendar.HOUR_OF_DAY, 14); reqArrivalTime.set(Calendar.MINUTE, 41); reqArrivalTime.set(Calendar.SECOND, 32);
		
		
		// calling the changeStatistics method parallel
		for (int i = 0; i < 150; i++) {
			new Thread(new Runnable() {
				public void run() {
					try {
						service.changeStatistics(poiName , successful , reqArrivalTime);
					} catch (Throwable t) { System.out.println(t); }
				}
			}).start();
		}
		
		// waiting for the threads
		Thread.sleep(4000);
		
		// checking the count
		reqArrivalTime.set(Calendar.HOUR_OF_DAY, 14); reqArrivalTime.set(Calendar.MINUTE, 40); reqArrivalTime.set(Calendar.SECOND, 00); reqArrivalTime.set(Calendar.MILLISECOND, 0);
		List<Object[]> list = service.getStatistics("service", "GetAddressForPostCode-GetAddressForPostCode_by_id", reqArrivalTime, reqArrivalTime);
		
		assertEquals(2, list.size());
		// calendar, successful, count
		long cnt = ( ((Boolean)list.get(0)[1]) ? (Long)list.get(0)[2] : (Long)list.get(1)[2] );
		assertEquals(150l, cnt);
	}
	
	
	@Test
	public void testGetStatistics() {
		// I used the getStatistics() method in the tests of changeStatistics -> I'll write them if I have time
	}
	
	
	@Test
	public void testGetStatisticsForComparison_service_successful() {
		String type = "service";
		
		List<String> names = new ArrayList<String>();
		names.add("GetAddressForPostCode-GetAddressForPostCode_by_id"); names.add("GetAddressForPostCode-GetAddressForPostCode_by_postcode");
		
		Calendar startDateCalendar = Calendar.getInstance();
		startDateCalendar.set(Calendar.YEAR, 2015); startDateCalendar.set(Calendar.MONTH, 3); startDateCalendar.set(Calendar.DAY_OF_MONTH, 14);
		startDateCalendar.set(Calendar.HOUR_OF_DAY, 14); startDateCalendar.set(Calendar.MINUTE, 40); startDateCalendar.set(Calendar.SECOND, 0); startDateCalendar.set(Calendar.MILLISECOND, 0);

		Calendar endDateCalendar = Calendar.getInstance();
		endDateCalendar.set(Calendar.YEAR, 2015); endDateCalendar.set(Calendar.MONTH, 3); endDateCalendar.set(Calendar.DAY_OF_MONTH, 14);
		endDateCalendar.set(Calendar.HOUR_OF_DAY, 15); endDateCalendar.set(Calendar.MINUTE, 17); endDateCalendar.set(Calendar.SECOND, 0); endDateCalendar.set(Calendar.MILLISECOND, 0);

		// calling the getStatisticsForComparison method
		List<Object[]> list = service.getStatisticsForComparison(type, names, startDateCalendar, endDateCalendar);
		
		// checking the count
		assertEquals(8*2, list.size());
		// name (String), calendar (Calendar), count (Long)
		Object[] rec = list.get(0);
		Object cnt = rec[2];
		assertEquals("GetAddressForPostCode-GetAddressForPostCode_by_id", rec[0].toString());
		assertEquals(50l, ((Long)cnt).longValue() );
		rec = list.get(1);
		cnt = rec[2];
		assertEquals("GetAddressForPostCode-GetAddressForPostCode_by_id", rec[0].toString());
		assertEquals(0l, ((Long)cnt).longValue() );
		rec = list.get(8);
		cnt = rec[2];
		assertEquals("GetAddressForPostCode-GetAddressForPostCode_by_postcode", rec[0].toString());
		assertEquals(25l, ((Long)cnt).longValue() );
	}
	
}
